{- | This module provides a way to measure the number of operations an
     interpreter in the State Monad (for example memoizeS from
     "Table.Interpreters").
     performs.

     It does this by counting the number of gets and stores.

     For example:

     @
     snd $ 'runStats' $ 'memoizeS' $ 'runProgram' $ program
     @

     returns a 'Stats' value that can be inspected with '_lookups' and
     '_stores'.

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Statistics
       (
         -- * Datatypes

         -- ** Stats
         Stats(..),

         -- ** Statistics
         Statistics,
         StatisticsT(..),
         
         -- * Extractors
         runStatsT,
         runStats,
       ) where

import           Control.Applicative
import           Control.Monad.Identity
import           Control.Monad.State
import           Control.Monad.Writer
import qualified Data.IntMap as IM
import qualified Data.Map as M

{- | The 'Stats' datatype maintains two values: the number of times that
     an interpreter looks up a stored value, and the number of times that
     a new value is stored.

     Stats values can be combined with their 'Monoid' instance.
-}
data Stats = Stats {
  _lookups  :: Int, -- ^ The number of lookups.
  _stores   :: Int  -- ^ The number of stores.
  } deriving (Eq, Show)

instance Monoid Stats where
  mempty = Stats 0 0
  mappend (Stats l1 s1) (Stats l2 s2) = Stats (l1 + l2) (s1 + s2)
             
-- | A typesynonym for 'StatistiscsT' where the base monad is Identity
type Statistics s a = StatisticsT s Identity a

-- | 'StatisticsT' is a monad transformer that adds an arbitrary state, and
--   a writer monad for 'Stats'.  
newtype StatisticsT s m a = Stat {
  unStatistics :: StateT s (WriterT Stats m) a
  } deriving (Functor, Monad, Applicative)

instance MonadTrans (StatisticsT s) where
  lift = Stat . lift . lift

instance Monad m => MonadState s (StatisticsT s m) where
  get   = tell oneLookup >> Stat get
  put s = tell oneStore  >> Stat (put s)

instance Monad m => MonadWriter Stats (StatisticsT s m) where
  tell            = Stat . lift . tell
  listen (Stat (StateT m)) = Stat . StateT $ \s -> do
    ((a, s'), w) <- listen (m s)
    return ((a, w), s')

  pass (Stat (StateT m)) = Stat . StateT $ \s -> do
    ((a, f), s') <- m s
    pass $ return ((a, s'), f)


oneLookup :: Stats
oneLookup = Stats 1 0

oneStore :: Stats
oneStore = Stats (-1) 1
--                ^
-- For each store a get occured that was interpreted as a lookup, so we correct
-- this here.

-- | Run the 'StatisticsT' transformer returning the compuation in the base
--   monad.
runStatsT ::
  Monad m => StatisticsT (IM.IntMap (M.Map b [c])) m a -> m (a, Stats)
runStatsT = runWriterT . flip evalStateT IM.empty . unStatistics

-- | Run the 'Statistics' monad, returning a result and the collected 'Stats'.
runStats :: Statistics (IM.IntMap (M.Map b [c])) a -> (a, Stats)
runStats = runIdentity . runStatsT
