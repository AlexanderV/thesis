{- QuickCheck tests on a number of example programs:

   * "Fibonacci"

   * "Matches"

   * "SameSCC"

   * "MutualRecursion"

   * "Dijkstra"

   * "Knapsack"

   Author: Alexander Vandenbroucke
-}

{-# LANGUAGE Rank2Types #-}

import qualified Dijkstra.Test as D
import qualified Fibonacci as F
import           Test.HUnit (runTestTT)
import qualified Knapsack.Test as K
import qualified Matches as M
import qualified SameSCC as SCC
import           Table
import qualified Table.Formal as F
import qualified Table.MutualRecursion as MR
import qualified Table.Advanced as A

import           Control.Applicative hiding ((<|>))
import           Control.Monad (void)
import           Test.QuickCheck

-- | A very simple example of non-determinism two predicates
--   that are tabled at the same time.
prop_recurseCorrect :: Int -> Property
prop_recurseCorrect x =
  x >= 0 && x <= 9 ==>
  let program :: TabledProgram Int Int Int
      program = TP $
        declare (\p i -> if i == 0 then
                               return (-1)
                             else
                               (2*) <$> p (i-1))
          $ \p -> 
             declare (\q i -> if i == 0 then
                                return 1
                              else
                                (2*) <$> q (i-1)
                                <|>
                                (2*) <$> p (i-1))
            $ \q -> 
               expression $ q x
  in A.ts program == F.tableSemantics program

-- | Run all tests.
main :: IO ()
main = do
  F.allCheck
  M.allCheck
  quickCheck prop_recurseCorrect
  quickCheck MR.prop_result
  putStrLn $ if SCC.checkCorrect then
                "SCC check succeeded!"
              else
                "SCC check Failed."
  putStrLn $ if SCC.checkFormal then
                "SCC Formal check succeeded!"
             else
               "SCC Formal check failed."
  putStrLn $ if SCC.checkFormalIter then
               "SCC Formal Iter check succeeded!"
             else
               "SCC Formal Itercheck failed."
  void $ runTestTT D.tests
  void $ runTestTT K.tests
