-- | A module containing test cases for the "Knapsack" module.
--
--   Author: Alexander Vandenbroucke

{-# LANGUAGE StandaloneDeriving  #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Knapsack.Test (
  -- * Individual testcases
  testTL, testIterTL, testAdvanced,
  -- * All tests
  tests
  )
where

import           Table
import qualified Table.Advanced as A
import           Table.Knapsack
import           Table.Knapsack.Knapsack
import qualified Table.Lattice as L
import qualified Table.Lattice.Iter as IL
import           Table.Lattice.Test
  
import           Test.HUnit hiding (Testable)
import           Test.QuickCheck
import           Test.QuickCheck.Poly



--------------------------------------------------------------------------------
-- Unit tests.

t :: Int -> TabledProgram Int Knapsack Knapsack
t = tabledKnapsackProgram items

items :: [Item]
items = [Item (mkWeight 2) (mkValue 1),
         Item (mkWeight 2) (mkValue 2),
         Item (mkWeight 5) (mkValue 20)]

testTL :: Test
testTL = TestCase $ do
  unMax (L.tl (t 2)) @=? Knapsack [Item (mkWeight 2) (mkValue 2)]
  unMax (L.tl (t 5)) @=? Knapsack [Item (mkWeight 5) (mkValue 20)]
  unMax (L.tl (t 8)) @=? Knapsack [Item (mkWeight 7) (mkValue 22)]

testIterTL :: Test
testIterTL = TestCase $ do
  unMax (L.tl (t 2)) @=? unMax (IL.iterTl (t 2))
  unMax (L.tl (t 5)) @=? unMax (IL.iterTl (t 5))
  unMax (L.tl (t 8)) @=? unMax (IL.iterTl (t 8))
  
testAdvanced :: Test
testAdvanced = TestCase $ do
  L.tl (t 2) @=? A.ts (t 2)
  L.tl (t 5) @=? A.ts (t 5)
  L.tl (t 8) @=? A.ts (t 8)

hUnitTests :: Test
hUnitTests =
  TestList [TestLabel "lattice"      testTL,
            TestLabel "iter-lattice" testIterTL,
            TestLabel "advanced"     testAdvanced
           ]


--------------------------------------------------------------------------------
-- QuickCheck properties

instance Arbitrary a => Arbitrary (Max a) where
  arbitrary = fmap Max arbitrary
  shrink (Max m) = map Max (shrink m)

quickCheckTests :: Test
quickCheckTests =
  let args = stdArgs{chatty = True}
      -- quickcheck with arguments
      qcwa :: Testable prop => (Max OrdA -> OrdA -> prop) -> Test
      qcwa = TestCase . quickCheckWith args
      -- quickcheck with Max, arguments
      qcwma :: Testable prop => (Max OrdA -> prop) -> Test
      qcwma = TestCase . quickCheckWith args
      -- quickcheck with Max, more arguments
      qcwmaa :: Testable prop => (Max OrdA -> prop) -> Test
      qcwmaa = TestCase . quickCheckWith args{maxDiscardRatio = 150}
      -- quickcheck with Max once
      qcwmo :: Testable prop => (Max OrdA -> prop) -> Test
      qcwmo = TestCase . quickCheckWith args{maxSuccess = 1}
  in TestList [
    TestLabel "Max: prop_reflexive"       (qcwma  prop_reflexive),
    TestLabel "Max: prop_transitive"      (qcwma  prop_transitive),
    TestLabel "Max: prop_antisymmetry"    (qcwmaa prop_antisymmetry),
    TestLabel "Max: prop_lubL"            (qcwmaa prop_lubL),
    TestLabel "Max: prop_lubR"            (qcwmaa prop_lubR),
    TestLabel "Max: prop_bottom"          (qcwmo  prop_bottom),
    TestLabel "Max: prop_mempty"          (qcwmo  prop_mempty),
    TestLabel "Max: prop_mappend"         (qcwma  prop_mappend),    
    TestLabel "Max: prop_coerce_uncoerce" (qcwa   prop_coerce_uncoerce),
    TestLabel "Max: prop_uncoerce_coerce" (qcwa   prop_uncoerce_coerce)]



--------------------------------------------------------------------------------
-- All Tests
   
-- | Tests for maximum weights 2, 5 and 8 and property tests for 'Max'.
tests :: Test
tests = TestList [hUnitTests, quickCheckTests]
