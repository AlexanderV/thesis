{-| This Module contains a number of properties that should be shared by
    all 'Lattice' and 'Coerce' instances.

    Author: Alexander Vandenbroucke
-}
{-# LANGUAGE ScopedTypeVariables #-}

module Table.Lattice.Test (
  -- * Ordering relation
  -- $ordering
  prop_reflexive,
  prop_transitive,
  prop_antisymmetry,
  -- * Least upper bound
  -- $lub
  prop_lubL,
  prop_lubR,
  -- * Bottom
  -- $bottom
  prop_bottom,
  -- * Monoid instance
  -- $monoid
  prop_mempty,
  prop_mappend,
  -- * Coerce and uncoerce
  -- $coerce
  prop_coerce_uncoerce,
  prop_uncoerce_coerce
  )
where

import Table.Lattice

import Data.Monoid
import Test.QuickCheck

{- $ordering
   '=<=' must be a valid ordering relation, i.e. it must be reflexive,
   transitive, and anti-symmetric.
-}
-- | The relation '=<=' must be reflexive.
prop_reflexive :: Lattice l => l -> Bool
prop_reflexive a = a =<= a

-- | The relation '=<=' must be transitive.
prop_transitive :: Lattice l => l -> l -> l -> Property
prop_transitive a b c = (a =<= b && b =<= c) ==> a =<= c

-- | The relation '=<=' must be anti-symmetric.
prop_antisymmetry :: (Eq l, Lattice l) => l -> l -> Property
prop_antisymmetry a b = (a =<= b && b =<= a) ==> a == b

{- $lub
   'lub' must satisfy the definition of a least upper bound:

   for any 'Lattice' @l@, any list @ls :: [l]@, for any @z :: l@

   > lub ls =<= z iff all (=<= z) ls
-}

-- | The only-if part of the l.u.b. definition.
prop_lubL :: (Eq l, Lattice l) => l -> [l] -> Property
prop_lubL z x = lub x =<= z ==> all (=<= z) x

-- | The if part of the l.u.b. definition.
prop_lubR :: (Eq l, Lattice l) => l -> [l] -> Property
prop_lubR z x = all (=<= z) x ==> lub x =<= z

-- $bottom
-- For any 'Lattice' @l@, 'bottom' is defined as @'lub' []@.

-- | @'bottom' == 'lub' []@ for any 'Lattice'.
prop_bottom :: forall l . (Eq l, Lattice l) => l -> Bool
prop_bottom _ = (bottom :: l) == lub []

-- $monoid
-- Any 'Lattice' @l@ is also a 'Monoid'.

-- | @'bottom' == 'mempty' for any 'Lattice'.
prop_mempty :: forall l . (Eq l, Lattice l) => l -> Bool
prop_mempty _ = (bottom :: l) == mempty

-- | @'lub [a, b] == a <> b == b <> a@ for any 'Lattice'.
prop_mappend :: (Eq l, Lattice l) => l -> l -> Bool
prop_mappend a b = let l = lub [a, b] in a <> b  == l && b <> a == l

{- $coerce
   'coerce' and 'uncoerce' should satisfy the following laws:

   * 'uncoerce' after 'coerce' should return the original value:

   > uncoerce (coerce a) == [a]

   * 'coerce' after 'uncoerce' should return the original container:

   > lub [coerce c | c <- uncoerce m] == m
-}

-- | 'uncoerce' after 'coerce'.
prop_coerce_uncoerce :: forall a l . (Eq a, Coerce a l) => l -> a -> Bool
prop_coerce_uncoerce _ a = uncoerce (coerce a :: l) == [a]

-- | 'coerce' after 'uncoerce'
prop_uncoerce_coerce :: forall a l . (Eq l, Coerce a l, Lattice l)
                        => l -> a -> Bool
prop_uncoerce_coerce l _ = lub [coerce c | (c :: a) <- uncoerce l] == l
