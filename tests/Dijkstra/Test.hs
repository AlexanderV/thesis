-- | A module containing test cases for the "Dijkstra" module.
--
--   Author: Alexander Vandenbroucke
module Dijkstra.Test (tests) where

import Dijkstra
import Table.Lattice.Test

import Test.HUnit hiding (Node, Testable)
import Test.QuickCheck



---------------------------------------------------------------------------------
-- Unit Tests

distanceTestTL :: Node -> Distance -> Test
distanceTestTL n expD = expD ~=? tlDistanceToProg 'a' n

distanceTestIterTL :: Node -> Distance -> Test
distanceTestIterTL n expD = expD ~=? iterTlDistanceToProg 'a' n

distanceTestAdvTL :: Node -> Distance -> Test
distanceTestAdvTL n expD = expD ~=? advTlDistanceToProg 'a' n

distanceTests :: String -> (Node -> Distance -> Test) -> Test
distanceTests name runner =
  let namedLabel desc = desc ++ " (" ++ name ++ ")"
  in TestList [TestLabel (namedLabel "distance a a") (runner a 0),
               TestLabel (namedLabel "distance a b") (runner b 3),
               TestLabel (namedLabel "distance a c") (runner c 2),
               TestLabel (namedLabel "distance a d") (runner d 1),
               TestLabel (namedLabel "distance a e") (runner e InfDistance)]
    
  
-- | Tests for distances to 'a' from 'a','b','c','d','e'
hUnitTests :: Test
hUnitTests = TestList [
  distanceTests "lattice"      distanceTestTL,
  distanceTests "lattice-iter" distanceTestIterTL,
  distanceTests "lattice-adv"  distanceTestAdvTL
  ]


---------------------------------------------------------------------------------
-- QuickCheck properties

instance Arbitrary Distance where
  arbitrary = fmap Distance arbitrary

quickCheckTests :: Test
quickCheckTests =
  let args = stdArgs{chatty = True}
      qcwa :: Testable prop => (Distance -> Distance -> prop) -> Test
      qcwa = TestCase . quickCheckWith args
      qcwma :: Testable prop => (Distance -> prop) -> Test
      qcwma = TestCase . quickCheckWith args
      qcwmaa :: Testable prop => (Distance -> prop) -> Test
      qcwmaa = TestCase . quickCheckWith args{maxDiscardRatio = 150}
      qcwmo :: Testable prop => (Distance -> prop) -> Test
      qcwmo = TestCase . quickCheckWith args{maxSuccess = 1}
  in TestList [
    TestLabel "Distance: prop_reflexive"       (qcwma  prop_reflexive),
    TestLabel "Distance: prop_transitive"      (qcwma  prop_transitive),
    TestLabel "Distance: prop_antisymmetry"    (qcwmaa prop_antisymmetry),
    TestLabel "Distance: prop_lubL"            (qcwmaa prop_lubL),
    TestLabel "Distance: prop_lubR"            (qcwmaa prop_lubR),
    TestLabel "Distance: prop_bottom"          (qcwmo  prop_bottom),
    TestLabel "Distance: prop_mempty"          (qcwmo  prop_mempty),
    TestLabel "Distance: prop_mappend"         (qcwma  prop_mappend),
    TestLabel "Distance: prop_coerce_uncoerce" (qcwa   prop_coerce_uncoerce),
    TestLabel "Distance: prop_uncoerce_coerce" (qcwa   prop_uncoerce_coerce)]



---------------------------------------------------------------------------------
-- All Tests

-- | Tests for distances to 'a' from 'a','b','c','d','e' and property tests for
--   'Distance'.
tests :: Test
tests = TestList [hUnitTests, quickCheckTests]
