\pdfminorversion=4
\documentclass[a4paper]{IEEEconf}

\usepackage{url}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{booktabs}

\title{The Table Monad in Haskell}
\author{
  Alexander Vandenbroucke\\
  \email{alexander.vandenbroucke@@student.kuleuven.be} 
}
\date{April 23, 2015}

%if False

> {-# LANGUAGE ScopedTypeVariables #-}
> {-# LANGUAGE MultiParamTypeClasses #-}
> {-# LANGUAGE FlexibleInstances #-}
>
> import Data.Set      (Set, singleton, empty, union)
> import Data.Foldable (foldMap)
> import Data.Monoid
> import Prelude hiding (fail)

%endif

%include polycode.fmt
%format >>> = "\ggg"
%format <|> = "\vee"
%format pOpen = "p_{open}"
%format pNondet = "p_{nd}"
%format pClose = "p_{close}"
%format ts = "\mathbb{T}"
%format cs = "\mathbb{C}"
%format tl = "\mathbb{T_L}"
%format cl = "\mathbb{C_L}"
%format lub = "\sqcup"
%format =<= = "\sqsubseteq"
%format bottom = "\bot"
%format coerce = "\uparrow\!"
%format uncoerce = "\downarrow\!"

\newcommand{\denot}[2]{\mathbb{#1}\llbracket\,#2\,\rrbracket}
\newcommand{\powerA}{\mathcal{P}(A)}
\newcommand{\powerC}{\mathcal{P}(C)}
\newcommand{\lfp}{l\!f\!p}
\newtheorem{definition}{Definition} 

\begin{document}
\maketitle


\begin{abstract}
Non-deterministic functions are functions that can return multiple answers.
When such a function calls itself recursively it can produce an infinite number
of answers where only finitely many distinct results exist.
Tabling is a technique that allows the combination of recursion and 
non-determinism in Prolog.

In this paper we construct the Table Monad, introducing tabling for 
non-deterministic functions in Haskell. 
This monad is constructed using the ``Effect-Handlers''-approach.
A variation on tabling, tabling with aggregates is also discussed.
Benchmarks show that for some cases this kind of tabling is more efficient.
\end{abstract}

\section{Introduction}

How long is the list of solutions computed by the following Haskell program?

< p :: [(Int, Int)]
< p = (1,2):[(y,x) | (x, y) <- p]
< >>> print p
< [(1,2), (2,1), (1,2), (2,1), ...]

How many solutions does the following Tabled Prolog program compute?

\begin{verbatim}
:- table p/2.
p(1,2).
p(Y,X) :- p(X,Y).

?- p(A,B).
A=1, B=2;
A=2, B=1;
no.
\end{verbatim}

The Haskell program runs forever, producing an infinite list of answers,
although only two distinct answers exist ((1,2) and (2,1)).
In contrast, the Prolog program produces two distinct answers only once.
The \texttt{table} directive is responsible for this behaviour.
It instructs Prolog to \emph{table} the predicate \texttt{p/2}.

Tabling \cite{xsbbook} is a technique that originated in Logic Programming. 
It allows programs like the one above to terminate or be computed more
efficiently.

Can we informally explain why the Haskell program does not terminate?
Intuitively the behaviour of the program is caused by the co-occurrence of
two concepts: recursion and non-determinism.
Clearly, a program that does not contain recursion cannot run forever. 
Conversely, a program that can run forever must contain recursion.
In this instance, it is the function |p| that recursively calls itself.

By non-determinism \cite{wadler1985} we mean that a function that returns zero,
one or more results (as opposed to just one):
the function |p| does not return a single result, but returns a list of all
its results (which are infinitely many).
The first answer that |p| produces is |(1,2)|. 
Then |p| can recursively derive a new answer |(2,1)|.
From |(2,1)| it derives |(1,2)| anew, and so on.
If we allow unbridled recursion in conjunction with non-determinism, infinite
recursive loops can be our reward.

Prolog has inherent non-determinism: resolution of a Prolog goal (such as
\texttt{?-p(A,B)} in the example) can return multiple answers.
Had the Prolog example program not been augmented with the \texttt{table} 
directive, it would have computed the infinite sequence 
\texttt{A=1,B=2;A=2,B=1;A=1;B=2;...}
It would suffer from exactly the same problem as the Haskell program.

If tabling solves these issues for Prolog, then it should solve them for
non-determinism in Haskell as well.

\noindent The main contributions of this paper are:
\begin{itemize}
\item
  An interface for writing recursive non-deterministic programs.
  It consists of the |Nondet|-typeclass and open recursion.
  This interface allows enough freedom to implement tabling.
  In particular, it allows the implementation to prevent infinite recursion
  (Section~\ref{sec:interface}).

\item
  An implementation of the \emph{Table Monad} that instantiates the
  |Nondet|-typeclass and deals with open recursion.
  This implementation is based on the ``Effect-Handlers''-approach
  \cite{haskell2014}, which factors the discovery of a monad in two tasks:
  \begin{enumerate}
  \item
    The abstract syntax, describing all operations that must be supported
    (Section~\ref{sec:syntax}).
  \item
    The effect handler that maps an abstract syntax tree to an effect,
    where Tabling is such an effect (Section~\ref{sec:effect-handler}).
  \end{enumerate}

\item
  An effect handler that implements a slightly different form of tabling
  based on aggregates. 
  This allows a much more efficient computation in certain cases
  (Section~\ref{sec:aggregates})

\item
  Benchmarks that demonstrate the benefits of aggregates and an effect handler
  that uses dependency analysis (Section~\ref{sec:benchmarks}).
\end{itemize}

\section{Overview}
To prevent non-deterministic recursion from going haywire, we look to
Prolog for inspiration.
Emulating Prolog was what sparked interest in non-determinism in the first
place \cite{logicT}.
By analogy, emulating Tabled Prolog with Tabled Non-determinism should be
equally viable.

Non-determinism, as provided by lists, admits a Monad instance:

< instance Monad [] where
<   return a = [a]
<   [] >>= f = []
<   (x:xs) >>= f = (f x) ++ (xs >>= f)

This is the standard |Monad| instance for lists \cite{wadler1985}. 
We want to model tabling with a monad as well. 
This gives us composable effects. 
When we have multiple tabled computations, they can be easily composed with
monadic bind (|>>=|) and we can layer multiple effects using monad transformers.

Discovering a new monadic structure that also implements tabling as well as
providing non-determinism and recursion is a difficult task that requires a
lot of creativity and ingenuity.
Instead we will use the ``Effect Handlers''-approach to create the monad,
and ``open recursion'' to rein in recursion.

\paragraph{Effect Handlers}
The idea is to decompose the creation of the monad in two phases.

First, an abstract syntax is created. This syntax is in essence an embedded
domain specific language (DSL) that allows us to describe all the desired
operations.
In particular, it should support non-determinism and recursion explicitly.
The abstract syntax tree (AST) for such a language always admits an easily
defined monad instance.

Second, we define an effect handler (or several handlers) that maps the AST to
the desired sequence of effects.
In doing so, we have effectively separated the act of coming up with the monad
into interface (abstract syntax supporting the operations and monad instance)
and implementation (the effect handler).

This process can be done abstractly first: we can invent a grammar
that precisely specifies the abstract syntax of the DSL,
and we can define a semantic function that gives this syntax a precise meaning.
Only then must we deal with the gritty details of implementing this abstract
syntax and semantics in Haskell (reasoning about abstract statements where such
clutter is absent is often easier).

In the abstract, we are chiefly concerned with correctness: has the desired
effect been achieved?
This correctness property then carries over into Haskell, where we
concern ourselves  operational issues such as efficiency.
The process is cyclic though: performance concerns that are raised in Haskell
are met with formally verified measures, before they are implemented
(unless they are trivial).

Programming languages have a concrete syntax and an abstract syntax.
Usually the concrete syntax is used to write programs, and abstract syntax
is used to interpret or compile the program.
Both the concrete and abstract syntax can be written in Haskell.
The DSL then becomes an Embedded DSL: written and interpreted entirely within
Haskell, as opposed to writing the DSL program in a separate source language.

Different effects can be assigned to the same syntax tree. 
Consider a semantic function that associates a program with the set of all its
distinct solutions.
This semantics gives the same solution as Tabled Prolog (modulo unification,
negation as failure).

Instead of always putting all solutions in the set, we can be more selective.
For instance, we can retain only the optimal solution with respect to
some ordering, allowing more efficient implementations of aggregates such as
minimum or maximum.

This idea is formalized by generalizing the semantics for sets to any 
complete lattice.
Intuitively, a complete lattice is a partially ordered set where for any subset
we can find a smallest element of the set that is greater or equal than all the
members of the subset.
In particular, sets form a complete lattice under union, and any totally
ordered set (for example the integers) can be extended to form a lattice
under maximum or minimum.

\paragraph{Open Recursion}
To gain control over recursion, the program must be rewritten to open up
recursion to external influences.
The recursion in the example

< p = (1,2):[(y,x) | (x,y) <- p]

is closed: code outside |p| cannot influence the recursion.
Contrast this with |pOpen|:

> pOpen :: [(Int, Int)] -> [(Int, Int)]
> pOpen r = (1,2):[(y,x) | (x, y) <- r]

The function |pOpen| takes the recursive call as an explicit argument.
Writing |p = fix pOpen| where |fix| is the standard Haskell fixed point
operator, recovers the original |p|.
By substituting a different fixed point operator we can decide when the
recursion stops.

\section{|Open| + |Nondet| Programming Interface}
\label{sec:interface}

Programmers do not usually write in abstract syntax directly.
In the same vein, a more convenient interface to open recursion and
non-determinism is provided.
The intent of open recursion can be more clearly stated using a type synonym:

> type Open s = s -> s

The example function |p| then becomes:

< pOpen :: Open [(Int, Int)]
< pOpen r = (1,2):[(y,x) | (x,y) <- r]

Non-determinism can be captured with the following typeclass:

> class Monad m => Nondet m where
>   fail  :: m a
>   (<|>) :: m a -> m a -> m a

The first function |fail| indicates a failed computation, i.e. a computation
without any results. 
The |<||>|-operator represents non-deterministic choice between two possible
non-deterministic computations (which can make choices in turn).
Notice that |Nondet| is a subclass of |Monad|.
As an immediate consequence, programmers may use |return a| to indicate a
successful computation witnessed by some result |a| and |>>=| to sequence
computations.
With this notation, the example is:

> pNondet :: Nondet m => Open (m (Int, Int))
> pNondet r = return (1,2) <|> do  (x,y) <- r
>                                  return (y,x)

To verify, we can instantiate |Nondet| for lists:

> instance Nondet [] where
>   fail   = []
>   (<|>)  = (++)

then

< >>> fix pNondet :: [(Int, Int)]
< [(1,2), (2,1), (1,2), (2,1), ...]

exactly like before.


\section{Abstract syntax}
\label{sec:syntax}

The data type |Table| defines the abstract syntax tree of a tabled computation:

> data Table p b c a
>   =  Pure a
>   |  Fail
>   |  Or (Table p b c a) (Table p b c a)
>   |  Call p b (c -> Table p b c a)

The |Table| type has 4 arguments: |p| is the type of the tabled function,
|b| is the argument to a tabled function, |c| is the type of its result, and
|a| is the type of the result of the entire computation.

Each of the constructors corresponds to a specific operation:
\begin{itemize}
\item
  \texttt{Pure a} is a successful computation witnessed by |a|, and
\item
  \texttt{Fail} is a failed computation (a computation without any results), 
\item
  \texttt{Or x y} is a non-deterministic choice between computations |x| and
  |y|,
\item
  \texttt{Call f b cont} is a recursive call to |f| with argument
  |b|, and |cont| is a continuation. A continuation is a function that resumes
  the computation when it is given the result of the call.
\end{itemize}

Thinking of |Table| as syntax tree, the following |Monad| instance simply
substitutes parts of the tree for |>>=|, and wraps a value in a |Pure| leaf for
|return|.

> instance Monad (Table f b c) where
>   return = Pure
>   (Pure a)       >>= f = f a
>   Fail           >>= f = Fail
>   a `Or` b       >>= f = (a >>= f) `Or` (b >>= f)
>   Call p b cont  >>= f = Call p b (\c -> cont c >>= f)

The |Nondet| instance simply uses the corresponding |Table| constructors:

> instance Nondet (Table f b c) where
>   fail   = Fail
>   (<|>)  = Or

The expression |fix pNondet :: Table p b c (Int, Int)| creates an infinite
AST (see Figure~\ref{fig:inf-tree}). 

\begin{figure}
\includegraphics[width = 0.9\linewidth]{ast.pdf}
\caption{The infinite tree |fix pNondet :: Table p b c (Int, Int)|.}
\label{fig:inf-tree}
\end{figure}

Such an infinite AST is not suitable as input to an effect handler.
The infinity is caused by the |fix|-operator, the this operator substitutes
a new infinite tree for the right child of any |Or| node.

A smarter fixed point operator |close| can produce a finite AST for open
recursive functions:

> newtype P b c = P { unP :: (b -> Table (P b c) b c c) }
>
> close  ::  Open (b -> Table (P b c) b c c) 
>        ->  b -> Table (P b c) b c c
> close o = let t b = Call (P (o t)) b Pure in t

Instead of simply substituting the function itself, a |Call| node with
a predicate |P| is substituted.
This closes of the recursion, making the tree finite. 
However, |pNondet :: Nondet m => Open (m (Int, Int))| does not quite match the
required type signature |Open (b -> Table (P b c) b c c)|. 
A type |Open (() -> s)| is isomorphic to |Open s|, so |pNondet| is equivalent
|pClose|:

> pClose :: Nondet m => Open (() -> m (Int, Int))
> pClose c ()  = return (1,2) <|> do  (y,x) <- c ()
>                                     return (x,y)

The expression |close pClose ()| creates a finite tree, displayed in 
Figure~\ref{fig:fin-tree}. 
The dashed arrow is used to denote that Call contains reference to the |Or|-node
in its predicate |P b c|.

\begin{figure}
\includegraphics[width = 0.9\linewidth]{ast2.pdf}
\caption{The finite tree |close pClose ()|.}
\label{fig:fin-tree}
\end{figure}

\section{Effect Handler}
\label{sec:effect-handler}

Given an abstract syntax tree, a compiler generates code corresponding to this
syntax.
The code that the compiler generates can be thought of as the \emph{meaning or
semantics} of the abstract syntax.
An effect handler associates a formal semantics with an AST in a similar
fashion, but the meaning is represented more abstractly to simplify reasoning
about the semantics.

For convenience, denote |Table (P b c) b c a| by |Table A|, and use $p(b)$ to
denote |unP p b|.
The effect handler is a function $\denot{T}{.} : |Table A| \rightarrow \powerA$,
mapping syntax to the set of all its solutions (a subset of $A$).
Owing to the recursive nature of the AST, $\denot{T}{.}$ is defined inductively.

\paragraph{base cases} For |Pure a| and |Fail| their set of solutions is
the singleton set $\{a\}$ and the empty set $\emptyset$ respectively:

\begin{align*}
\denot{T}{|Pure a|} &= \{a\}\\
\denot{T}{|Fail|}   &= \emptyset
\end{align*}

\paragraph{inductive cases} The solution |Or a b| of is the union of the
solutions of its two choices:

\[ \denot{T}{|Or a b|} = \denot{T}{a} \cup \denot{T}{b} \]

How do we compute the set of solutions for |Call p b cont|?
Assume that we have a function $s$ that knows all the solutions for all
predicates and arguments arguments that are used in $p(b)$, then we could
also compute the results for $p(b)$ with the function $\denot{C}{.}$.
The function $\denot{C}{.}$ is like $\denot{T}{.}$ except that it computes the
result of |Call q k cont| by applying |cont| on every solution in $s(q,k)$ and
taking the union of the results.

\begin{align*}
&\denot{C}{.} : |Table C|
\rightarrow (|P B C| \times B \rightarrow \powerC)
\rightarrow \powerC\\
&\denot{C}{|Pure c|}(s) = \{c\}\\
&\denot{C}{|Fail|}(s)   = \emptyset\\
&\denot{C}{|Or a b|}(s) = \denot{C}{a}(s) \cup \denot{C}{b}(s)\\
&\denot{C}{|Call q k cont|}(s) 
  = \bigcup\limits_{c \in s(q,k)} \!\!\!\!\denot{C}{cont(c)}(s)
\end{align*}

If $s$ contains not just the results for all predicates used in $p(b)$, but
\emph{all} predicates, then the result that $\denot{C}{.}$ computes must be
known to $s$, i.e. we have $\denot{C}{p(b)}(s) = s(p,b)$ for all
predicates $p$ and arguments $b$.
Then s must be a fixed point of the function
$\lambda s. \lambda(q,k) . \denot{C}{q(k)}(s)$.
In fact, we can prove\footnote{Proof is given in my master thesis.} that this
function has a least fixed point. 

\begin{align*}
\text{Let }
&table: |P B C| \times B \rightarrow \powerC\\
&table(p,b) = \lfp(\lambda s. \lambda(q,k) . \denot{C}{q(k)}(s))(p,b)
\end{align*}

where $\lfp(.)$ denotes the least fixed point operator.
With the $table$ function we can now define the semantics of a |Call p b cont|:
\[
\denot{T}{|Call p b cont|} = 
  \bigcup\limits_{c \in table(p,b)}\!\!\!\!\!\!\!\!\denot{T}{cont(c)}
\]

To demonstrate, consider that the following function is the least fixed point,
\[
s(q,k) = \begin{cases}
\{(1,2),(2,1)\} & \text{ if } $q = p$ \text{ and } k = |()|\\
\emptyset & \text{ otherwise}
\end{cases}
\]

where $p$ is the predicate constructed.

Now we have for $\denot{T}{|close pClose ()|}$:

\begin{align*}
\denot{T}{|close pClose ()|} 
&= \denot{T}{|Call p () (\(y,x) -> Pure (y,x))|}\\
&= \bigcup\limits_{(y,x) \in table(p,())}\!\!\!\!\!\!\!\!\denot{T}{Pure (x,y)}\\
&= \bigcup\limits_{(y,x) \in s(p,())}\!\!\!\!\!\!\!\!\denot{T}{Pure (x,y)}\\
&= \denot{T}{Pure (2,1)} \cup \denot{T}{Pure (1,2)}\\
&= \{(2,1), (1,2)\}
\end{align*}

this is exactly the solution found by Tabled Prolog.

\subsection{Effect Handler in Haskell}

Because $\denot{T}{.}$ is essentially a function, and Haskell is a functional
programming language, we can easily translate it into Haskell (using
|Set| from \texttt{Data.Set} and |foldMap| from \texttt{Data.Foldable}).

> ts :: (Ord a, Ord c) => Table (P b c) b c a -> Set a
> ts (  Pure a)         = singleton a
> ts    Fail            = empty
> ts (  Or a b)         = ts a `union` ts b
> ts (  Call p b cont)  = foldMap (ts . cont) (table p b)
>
> table :: Ord c => P b c -> b -> Set c
> table = lfp  (\s -> \q k -> cs (unP q k) s) 
>              (\q k -> empty)
>
> cs  ::  Ord c
>     =>     Table (P b c) b c c
>     ->  (  P b c -> b -> Set c)
>     ->     Set c
> cs (  Pure a)         s  = singleton a
> cs    Fail            s  = empty
> cs (  Or a b)         s  = cs a s `union` cs b s
> cs (  Call p b cont)  s  = foldMap (flip cs s . cont) (s p b) 
>
> lfp :: (a -> a) -> a -> a

%if False

> lfp f a = f (f (f (f (f (f a)))))

%endif

\subsection{Dependency analysis}
\label{sec:dep-analysis}
In the previous section, the definition of the least fixed point operator |lfp|
was intentionally left undefined.
Computing the least fixed point is where different implementations of the same
semantics have the most leeway.

The most performant solution (as shown by benchmarks, see later) performs
dependency analysis between the predicates.
Briefly, the idea is that when the dependencies of a predicate don't change,
the predicate doesn't change either.
When we iteratively compute the least fixed point, we only run |cs| on those
predicates that have changed dependencies. 
When there are no predicates with changed dependencies, we have reached a fixed
point.
This idea has been implemented in Haskell, and the results can be observed in
the Section~\ref{sec:benchmarks}. Due to space constraints the implementation
itself is elided.

\subsection{Aggregates}
\label{sec:aggregates}
The semantics $\denot{T}{.}$ described in Section~\ref{sec:effect-handler}
associates an abstract syntax tree with the full set of all its solutions.
Frequently, we are not interested in \emph{all} solutions but only
a select few, e.g. the optimum solution(s).

For some problems, where the solution space is really large (e.g. the Knapsack
Problem, the Subset Sum problem), it is worthwile to compute the optimum
with a different approach that avoids generating all intermediate results.

Selecting particular elements from a set is exactly the operation that an
aggregate (such as \texttt{MIN} or \texttt{MAX}) accomplishes. 
The notion of an aggregate as ``the worst thing that is is better than all
elements in a subset of A'' formalized with \emph{Complete Lattices}.

For a more in-depth explantation, see Appendix~\ref{app:aggregates}

\section{Benchmarks}
\label{sec:benchmarks}

To substantiate the claims in Section~\ref{sec:dep-analysis} and 
Section~\ref{sec:aggregates}, we compare implementations with and without 
dependency analysis or aggregates for a number of problems.
The detailed test results are listed in Appendix~\ref{app:bench-data}

The first problem is the knapsack problem. 
This problem is a natural fit for aggregates
(see Appendix~\ref{app:aggregates}).
There are three implementations, displayed in Figure~\ref{fig:knapsack}:
\emph{dumb}: dependency analysis, no aggregates; 
\emph{iter}: aggregates, no dependency analysis; 
\emph{advanced}: aggregates and dependency analysis.
Aggregates and dependency analysis are clearly hugely beneficial.

The second benchmark computes all strongly connected components in a small
directed graph (only 20 vertices, with 40, 80 and 160 edges). There
are three implementations, displayed in Figure~\ref{fig:scc}:
\emph{iter}: no dependency analysis, no aggregates;
\emph{advanced}: dependency analyis, no aggregates;
\emph{lattice}: dependency analysis, uses the aggregate of all solutions (this
aggregate is semantically identical to |ts|.
Dependency analysis outperforms \emph{iter}. 
It seems that the compiler can generate slightly faster code for the specific
\emph{advanced}-implementation, compared to the more general
\emph{lattice} code.

In the final problem we compute the shortest path between any two nodes in a
weighted directed graph. 
In this case there are four implementation, displayed in
Figure~\ref{fig:shortest-path}: \emph{dijkstra}: a straight-forward
implementation of Dijkstra's shortest path algorithm, without any tabling;
\emph{iter} and \emph{advanced}: both use aggregates, only the latter uses
dependency analysis. Dijkstra's algorithm significantly outperforms the other
implementations, but the (beneficial) impact of dependency analysis is still
clear.


\section{Related Work}

Pareja-Flores, and Pe\~na and Vel{\'a}zquez-Iturbide \cite{fuji95} discuss a
Tabling approach in Haskell that requires manual program transformations, as
opposed to our DSL approach.

Wadler\cite{wadler1985} introduced ``lists of successes'' as a way of modelling
non-determinism in functional programming, which enspired the simple example
used throughout this paper.
A later publication \cite{wadler1995} by the same author suggests using monads
as a way of structuring functional programs. 
Indeed, the monad instance for lists is already mentioned in this paper.
Several more advanced monad-transformers for non-determinism are discussed by 
Kiselyov, Shan, Friedman and Sabry \cite{logicT}.

A particularily accessible explanation of Effect Handlers is given by 
Schrijvers and Hinze\cite{haskell2014}.

Functional logic programming languages, such as Curry\cite{antoy2010} exhibit
properties of both functional and logical programming languages.
Curry, for instance is heavily based on Haskell, supporting almost all Haskell
constructs. 
In addition, it also provides syntactical constructs to return
non-deterministic results, or have the system search for non-deterministic
solutions for so called ``free'' variables.
Non-deterministic functions in Curry run into the same problem with recursion
that is discussed in this paper.

Warren \cite{xsbbook} gives a good introduction to Tabled Prolog using the
XSB-Prolog system. 
Several Prolog systems such as XSB-Prolog\cite{swift2010tabling} and 
YapTab\cite{santos2013} also support aggregates.

\section{Conclusion}

We demonstrated that there exists a problem when we combine non-determinism
with open recursion.
We present a solution in the form of Tabling, inspired by Tabled Prolog.
Tabling is implemented as the Table monad.
This monad is created using the ``Effect-Handlers''-approach, where we split
this task in abstract syntax and effect handlers.

After defining the abstract syntax, we can achieve different effects with
different effect handlers.
In particular, we discuss two optimizations of the basic effect handler:
dependency analysis and aggregates.
The benefit of these optimizations have been demonstrated with a set of
benchmarks.

\section{Future Work}

The implementation of effect handlers still has much room for improvement.
The YapTab Prolog system, for instance, supports parallel exectution of Tabling
\cite{rocha2000}. 
More fundamental changes are also possible.
Probabilistic Tabled Programs are such a possibility. 
In this case the result of a tabled computation is no longer a set of solutions
but a probability distribution on this set. 

\bibliographystyle{abbrv}
\bibliography{paper}

\newpage
\appendix
\section{Aggregates}
\label{app:aggregates}

We use complete lattices to capture the notion of an aggregate.

\begin{definition}[Complete Lattice]
A partially ordered set $(S, \sqsubseteq)$ is a complete lattice
$(S, \sqsubseteq, \cup)$ if for every $X \subseteq S$ the least upper bound
(l.u.b.) $\sqcup X$ is defined, such that:
\[
\forall z \in S: \sqcup X \sqsubseteq z \iff \forall x \in X: x \sqsubseteq z
\]
The l.u.b. of $\emptyset$ is denoted by the symbol $\bot$ (``bottom''), i.e
\[ \sqcup \emptyset = \bot \]
In Haskell lattices are captured by the following typeclass:

> class Lattice l where
>   (=<=)   ::    l   -> l -> Bool
>   lub     :: [  l]  -> l
>   bottom  ::    l
>   bottom = lub []

\end{definition}

The program only concerns itself with values of type $A$, but the end result
should be an aggregate (a lattice) of type $L(A)$.
Hence we need a way to convert between these two types.

\begin{definition} Consider two functions $|coerce| : A \rightarrow L(A)$ and
$|uncoerce|: L(A) \rightarrow \powerA$:

> class Coerce i a where
>   coerce    :: i  ->    a
>   uncoerce  :: a  -> [  i]

These functions must satisfy the following laws:
\begin{align*}
&|uncoerce (coerce s)| = \{s\}\\
&|lub|\{|coerce s|\:||\:s \in |uncoerce l|\} = l
\end{align*}
\end{definition}

Now we can define a new semantics |tl|\footnote{The
\texttt{ScopedTypeVariables} extension is required.}:

> tl  :: forall l a b c . (Lattice l, Coerce a l, Coerce c l) 
>     => Table (P b c) b c a -> l
> tl (  Pure a)         = coerce a
> tl    Fail            = bottom
> tl (  Or a b)         = lub [tl a, tl b]
> tl (  Call p b cont)  = 
>   lub [tl (cont c) | c <- uncoerce (tableL p b :: l)]

> tableL  :: (Lattice l, Coerce c l) => P b c -> b -> l
> tableL  = lfp  (\s -> \q k -> lub [s q k, cl (unP q k) s]) 
>                (\q k -> bottom)

> cl :: (  Lattice l, Coerce c l)
>    =>    Table (P b c) b c c -> (P b c -> b -> l) -> l
> cl (  Pure c)         s  = coerce c
> cl    Fail            s  = bottom
> cl (  Or a b)         s  = lub [cl a s, cl b s]
> cl (  Call p b cont)  s  = lub [cl (cont c) s | c <- uncoerce (s p b)]

Let's apply this to the knapsack problem:

An item has a |weight| and a |value|.

> type Knapsack = [Item]
> data Item  = Item { weight :: Int, value :: Int}
>            deriving (Show, Eq, Ord)

In this particular problem we only consider three items: with weight 2 and 
value 1, with weight 2 and value 2 and with weight 5 and value 20.

> item :: Nondet m => m Item
> item =  return (Item 2 1) 
>         <|> return (Item 2 2)
>         <|> return (Item 5 20)

The |knapsack| function is the tabled program that solves the problem:
The empty knapsack (|[]|) is always a solution. 
Otherwise, for every item, we try to fill a knapsack with the capacity that
remains after putting the item in the knapsack.

> knapsack :: Nondet m => Open (Int -> m Knapsack)
> knapsack ks w  | w <= 0     = return []
>                | otherwise  = return [] <|> do
>                    i <- item
>                    if weight i > w then
>                      fail
>                    else do
>                      is <- ks (w - weight i)
>                      return (i:is)

A |Knapsack| is a |Lattice|, and items can be coerced into a knapsack and vice
versa.

> instance Lattice Knapsack where
>   k1 =<= k2 = v1 < v2 || (v1 == v2 && w1 < w2) where
>     v1  = sum (map value   k1)
>     w1  = sum (map weight  k1)
>     v2  = sum (map value   k2)
>     w2  = sum (map weight  k2)
>   lub = foldr lmax bottom where
>     lmax a b = if a =<= b then b else a
>   bottom = []
> instance Coerce [Item] [Item] where
>   coerce = id
>   uncoerce ks = [ks]

The old effect handler |ts| generates all solutions (and all intermediate
solutions), |tl| generates only the optimal ones.

< >>> tl (close knapsack 5) :: Knapsack
< [Item 2 2, Item 5 20]
< >>> ts (close knapsack 5)
< fromList [[],[Item 2 1], [Item 2 1, Item 2 1],
< [Item 2 1, Item 2 2],[Item 2 2],[Item 2 2, Item 2 1],
< [Item 2 2, Item 2 2],[Item 5 20]]
`
Every complete lattice forms a commutative idempotent monoid\footnote{With the
following defintion: |mempty = bottom|, |mappend a b = lub [a, b]|. Then 
|lub [a, a] = a| (idempotent) and |lub [a, b] = lub [b, a]| (commutative)}.
Some aggregates do not form such a monoid. For instance, the Sum monoid
|mappend (Sum 1) (Sum 1) = (Sum 2)| is clearly not idempotent.
The List monoid is neither commutative nor idempotent.
Hence, we cannot capture every useful aggregate with a complete lattice.
Consequently, such aggregates cannot be solved with this method.

\section{Benchmark Data}
\label{app:bench-data}

\begin{figure}[h]
\includegraphics[width=\textwidth/2]{ks_lines_english.pdf}
\caption{The results of the knapsack benchmark.}
\label{fig:knapsack}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth/2]{scc_bars_english.pdf}
\caption{The results of the Strongly Connected Component benchmark.}
\label{fig:scc}
\end{figure}

\begin{figure}
\includegraphics[width=\textwidth/2]{sp_bars_english.pdf}
\caption{The results of the Shortest Path benchmark.}
\label{fig:shortest-path}
\end{figure}

\begin{table}[t]
\begin{tabular}{@@{}rlll@@{}}
\toprule
Capacity & Benchmark & \multicolumn{2}{c}{Time}\\
\cmidrule{3-4}
         &           & avg.       & std. dev.\\
\midrule
 10      & iter      & 5.86 ms    & 656 $\mu s$        \\
         & advanced  & 1.46 ms    & 13.4 $\mu s$       \\
         & dumb      & 15.1 ms    & 257 $\mu s$        \\
\midrule
 11      & iter      & 9.24 ms    & 1.39 ms            \\
         & advanced  & 1.67 ms    & 18.2 $\mu s$       \\
         & dumb      & 26.6 ms    & 205 $\mu s$        \\
\midrule
 12      & iter      & 10.8 ms    & 127 $\mu s$        \\
         & advanced  & 1.79 ms    & 27.5 $\mu s$       \\
         & dumb      & 45.1 ms    & 138 $\mu s$        \\
\midrule
 20      & iter      & 39.5 ms    & 187 $\mu s$        \\
         & advanced  & 4.51 ms    & 33.8 $\mu s$       \\
         & dumb      & 1.61 s     & 3.80 ms            \\
\midrule
 25      & iter      & 65.1 ms    & 610 $\mu s$        \\
         & advanced  & 5.79 ms    & 39.8 $\mu s$       \\
         & dumb      & 8.15 s     & 5.62 ms            \\
\midrule
 30      & iter      & 100 ms     & 832 $\mu s$        \\
         & advanced  & 8.05 ms    & 43.6 $\mu s$       \\
\midrule
 40      & iter      & 187 ms     & 3.35 ms            \\
         & advanced  & 12.6 ms    & 61.2 $\mu s$       \\
\midrule
 50      & iter      & 316 ms     & 939 $\mu s$        \\
         & advanced  & 19.8 ms    & 242 $\mu s$        \\
\midrule
 100     & iter      & 1.76 s     & 2.54 ms            \\
         & advanced  & 89.7 ms    & 1.43 ms            \\
\midrule
 200     & iter      & 10.8 s     & 25.1 ms            \\
         & advanced  & 238 ms     & 2.99 ms            \\
\midrule
 400     & advanced  & 699 ms     & 15.1 ms            \\
\bottomrule
\end{tabular}
\caption{The results of the knapsack benchmark.}
\end{table}

\begin{table}[t]
\begin{tabular}{@@{}rlll@@{}}
\toprule
\# Edges & Benchmark & \multicolumn{2}{c}{Time}\\
\cmidrule{3-4}
        &           & avg. & std. dev.                \\
\midrule
 40     & advanced  & 88.1 ms    & 1.38 ms            \\
        & lattice   & 98.5 ms    & 3.92 ms            \\
        & iter      & 252 ms     & 2.54 ms            \\
\midrule
 80     & advanced  & 935 ms     & 16.6 ms            \\
        & lattice   & 1.08 s     & 59.3 ms            \\
        & iter      & 3.09 s     & 1.52 ms            \\
\midrule
 160    & advanced  & 2.71 s     & 15.6 ms            \\
        & lattice   & 2.94 s     & 6.64 ms            \\
        & iter      & 4.97 s     & 52.7 ms            \\
\bottomrule

\end{tabular}
\caption{The results of the Strongly Connected Component benchmark.}
\end{table}

\begin{table}[t]
\begin{tabular}{@@{}rlll@@{}}
\toprule
\# Edges & Benchmark & \multicolumn{2}{c}{Time}\\
\cmidrule{3-4}
         &           & avg.       & std. dev.          \\
\midrule
 400     & dijkstra  & 11.6 ms    & 265 $\mu s$        \\
         & iter      & 505 ms     & 3.80 ms            \\
         & advanced  & 335 ms     & 2.68 ms            \\
\midrule
 800     & dijkstra  & 21.1 ms    & 275 $\mu s$        \\
         & iter      & 1.53 s     & 13.6 ms            \\
         & advanced  & 565 ms     & 3.69 ms            \\
\midrule
 1600    & dijkstra  & 29.5 ms    & 908 $\mu s$        \\
         & iter      & 2.57 s     & 22.2 ms            \\
         & advanced  & 969 ms     & 5.07 ms            \\
\bottomrule
\end{tabular}
\caption{The results fo the Shortest-Path benchmark.}
\end{table}


\end{document}

%%% Local Variables: 
%%% mode: latex
%%% End: 
