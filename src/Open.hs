{-| This module defines open recursive functions.
    An Open recursive function from a to b is a function that takes a
    function from a to b as its argument and returns another function
    from a to b, or more concisely:

    > type OpenF a b = (a -> b) -> a -> b

    More generally, one can write:

    > type Open s = s -> s

    Composition '<+>' of @'Open' s@ has left and right unit 'zero' and is
    associative, hence it forms a 'Monoid'.

    It is isomorphic to 'Endo' from Data.Monoid:

    > newtype Endo a = Endo { appEndo :: a -> a }

    See MRI: /Modular Reasoning about Inference in Incremental Programming,/
    /BRUNO C. D. S. OLIVEIRA, TOM SCHRIJVERS, WILLIAM R. COOK (2012)./
    /MRI: Modular Reasoning about interference in Incremental Programming/
 
    Author: Alexander Vandenbroucke
-}

module Open (
  -- * Type synonyms
  Open,
  -- * Instantiating, Base case and Combining
  new, zero, (<+>)
  ) where


-- | 'Open' @s@ is an open recursive function.
--
--   Open recursive functions will call their argument recursively. This allows
--   the modification of the recursive behaviour of the function,
--   similar to how dynamic method dispatch works in class-based object
--   oriented programming languages.
type Open s = s -> s

-- | Close an @'Open' s@, i.e. turn it into an s.
--   Using the OOP metaphor: instantiate the class @Open s@.
new :: Open s -> s
new a = let this = a this in this


-- | The left and right unit of composition '<+>'.
zero :: Open s
zero = id

-- | Composition of two @'Open' s@s into one @'Open' s@.
--   In OOP lingo, @a1 \<+\> a2@ means that @a1@ extends @a2@.
(<+>) :: Open s -> Open s -> Open s
a1 <+> a2 = a1 . a2
