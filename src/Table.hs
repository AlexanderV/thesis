{- | This module exports the constructs to define tabled computations.

     You can use the 'Monad' instance to structure your programs like you
     would in any other monad.
     A 'Nondet' instance allows you to specify non-deterministic actions
     using 'fail', '<|>' and 'choose'.

     For example:

     > choose [1,2] <|> return 3 <|> fail

     represents a choice between 1, 2 and 3. The final branch always fails.

     To perform tabling on a recursive computation, you need to specify the
     recursion with open recursion (using the 'Open' type),
     in the next example @fib@ defines the fibonacci function using open
     recursion.

     > fib :: Monad m => Open (Int -> m Int)
     > fib rec n | n == 0 = return 0
     >           | n == 1 = return 1
     >           | otherwise = do a <- rec (n-1)
     >                            b <- rec (n-2)
     >                            return (a + b)

     The next example combines open recursion with non-determinism:

     > predicate :: Nondet m => Open (() -> m (Int, Int)
     > predicate rec () = return (1,2) <|> do (x,y) <- rec ()
     >                                        return (y,x))

     Finally, effect handlers (such as those defined in "Table.Formal" or
     "Table.Advanced") expect a 'TabledProgram', so we need to compile the open
     recursive functions into a program.
     To do so we use the 'declare', 'declareRec' and 'expression' functions.

     For the previous program this becomes something like:

     > program :: TabledProgram () (Int,Int) (Int,Int)
     > program = TP $ declare predicate $ \p ->
     >                expression (p ())

     The 'declareRec' function is used to declare a list of mutually recursive
     functions. Note that you are not limited to a single 'declare' or
     'declareRec' per 'TabledProgram'.
   
     Author: Alexander Vandenbroucke
-}
module Table (
  -- * Open recursion
  Open,
  -- * Nondet
  Nondet(..),
  -- * TabledProgram
  TabledProgram(TP),
  declareRec,
  declare,
  expression
  )
where

import Open
import Nondet
import Table.Program
