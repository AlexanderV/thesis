{- | This module contains the fastest version of the tabling interpreter for
     the 'Table' monad. The semantics are identical to
     'Table.Formal.tableSemantics' from "Table.Formal", but in general 'ts'
     will be much faster. This speed is primarily by performing a less naive
     fixpoint computation using dependency analysis.

     Author: Alexander Vandenbroucke
-}


module Table.Advanced (ts) where

import           Table
import qualified Table.Internal.Advanced as A
import           Table.Internal.Formal (runProgram)

import qualified Data.Set as S

-- | A tabling interpreter using dependency analysis, specialised for Set.
ts :: (Ord a, Ord b, Ord c) => TabledProgram b c a -> S.Set a
ts = A.ts . runProgram
