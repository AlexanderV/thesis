{- | This module contains the fastest version of the lattice-based tabling
     interpreter for the 'Table' monad.
     The semantics are identical to 'Table.Lattice.tl' from
     "Table.Lattice", but in general 'ts' will be much faster.
     This speed is primarily by performing a less naive fixpoint computation
     using dependency analysis.

     Author: Alexander Vandenbroucke
-}



module Table.Lattice.Advanced (tl) where

import           Table
import           Table.Internal.Formal (runProgram)
import qualified Table.Internal.Lattice.Advanced as A
import           Table.Lattice hiding (tl)

-- | A tabling interpreter using dependency analysis.
tl :: (Ord b, Eq l, Lattice l, Coerce c l, Coerce a l)
   => TabledProgram b c a -> l
tl = A.tl . runProgram
