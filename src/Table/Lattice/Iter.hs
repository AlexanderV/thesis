{- | This module implements a more practical version of the denotational
     semantics in "Table.Lattice".

     Like 'tl', 'iterTl' computes a solution of a tabled computation by
     computing a fixed point of a (wrapper around the) 'iterCl' function.
     Unlike 'tl', the fixed point computation is not bounded in the number
     of iterations.

    Author: Alexander Vandenbroucke
-}

module Table.Lattice.Iter (
  iterTl
  )
where

import           Table
import           Table.Internal.Formal (runProgram)
import qualified Table.Internal.Lattice.Iter as IL
import           Table.Lattice

-- | A semantic function mapping a @'Table' ('Predicate' b c) b c c@ to a
--  'Lattice'.
iterTl :: (Ord b, Eq l, Lattice l, Coerce c l, Coerce a l)
       => TabledProgram b c a -> l
iterTl = IL.iterTl . runProgram

