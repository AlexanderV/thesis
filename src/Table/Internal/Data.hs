{- | The purpose of this module is to provide datastructures that can be useful
     to multiple different tabling interpreters

     This module provides a datastructure 'T' to store a table, keyed by
     predicate-argument pairs. It also provides structures to create ('emptyT'),
     'update' as well as, find ('findInT', 'findInTS') elements in the table.

     This module also provides a datastructure 'D' to store inverse
     dependencies. Inverse dependency means that 'D' stores all
     predicate-argument pairs that are dependend on a certain
     predicate-argument pair. Operations provided are 'emptyD', 'updateD',
     'findInD'.

     Finally there is a function 'new' to remove all predicate-argument pairs
     that are already in the table from a set.

     Author: Alexander Vandenbroucke
-}
module Table.Internal.Data (
  T, D,
  emptyT, emptyD,
  findInT, findInTS, findInD,
  updateT, updateD,
  new
  )
where

import           Table.Internal

import           Control.Monad.State
import qualified Data.IntMap as IM
import qualified Data.Map as M
import           Data.Monoid
import qualified Data.Set as S


-- | A datastructure containing the table.
--
--   @T b c s@ is a table containing predicates with arguments of type @b@,
--   results of type @c@, and a container (for the results) @s@.
newtype T b c s = T (IM.IntMap (Predicate b c, M.Map b s))
                deriving (Eq, Ord, Show)

-- | A datastructure containing the reverse dependencies.
--
--   @D b c@ contains a mapping from an predicate-argument pair, to
--   the predicate-argument pairs that depend on it.
newtype D b c = D (IM.IntMap (M.Map b (S.Set (Predicate b c, b))))
                deriving (Eq, Ord, Show)

emptyT :: T b c s
emptyT = T IM.empty

emptyD :: D b c
emptyD = D IM.empty

-- | Find a 'Monoid' @s@ in a table (@'T' b c s@) stored in the state.
findInTS ::
  (Ord b, Monoid s)
  => Predicate b c            -- ^ The 'Predicate' to search for,
  -> b                        -- ^ the argument to search for,
  -> State (T b c s, D b c) s -- ^ the result that was found.
                              --   This can be 'mempty' if the table did not
                              --   contain a valid entry.
findInTS p b = gets (findInT p b . fst)

-- | Find a 'Monoid' @s@ in a table (@'T' b c s@).
findInT ::
  (Ord b, Monoid s)
  => Predicate b c -- ^ the 'Predicate' to search for,
  -> b             -- ^ the argument to search for,
  -> T b c s       -- ^ The table to search in,
  -> s             -- ^ the result that was found, can be 'mempty' if the table
                   --   did not contain a valid entry.
findInT (Predicate i _) b (T t0) =
  M.findWithDefault mempty b (IM.findWithDefault M.empty i (IM.map snd t0))

-- | Update a table @'T' b c s@.
updateT ::
  (Ord b, Monoid s)
  => Predicate b c -- ^ The 'Predicate' to search for,
  -> b             --   the argument to search for,
  -> s             --   the new 'Monoid' s,
  -> T b c s       --   the original table
  -> T b c s       --   the new table.
updateT p@(Predicate i _) b s (T t0) =
  T (IM.insertWith tupUnion i (p, M.singleton b s) t0) where
    tupUnion (_,mA) (_,mB) = (p, M.unionWith (<>) mA mB)

-- | Update inverted dependencies @'D' b c@.
updateD ::
  Ord b
  => Predicate b c       -- ^ The 'Predicate' of the inverted dependency,
  -> b                   --   the argument of the inverted dependency
  -> [(Predicate b c,b)] --   the 'Predicates's and and arguments that are
                         --   dependencies.
  -> D b c               --   the original table of inverted dependencies
  -> D b c               --   the new table
updateD p b qcs (D d0) = D (foldr updateD' d0 qcs) where
  updateD' (Predicate i _ ,c) d1 =
    IM.insertWith (M.unionWith (<>)) i (M.singleton c (S.singleton (p,b))) d1

-- | Retain only those Predicate-argument pairs that don't occur in the table.
new :: Ord b => (((Predicate b c, b) -> Bool) -> w) -> T b c s -> w
new filterW (T t0) = filterW (not . member) where
  member (Predicate i _,b) = case IM.lookup i t0 of
    Nothing -> False
    Just (_, m)  -> M.member b m

-- | Find the predicate-argument pairs that depend on a given pair.
findInD :: Ord b => Predicate b c -> b -> D b c -> S.Set (Predicate b c, b)
findInD (Predicate i _) b (D d0) =
  M.findWithDefault S.empty b (IM.findWithDefault M.empty i d0)
