{- | This module implements a more practical version of the denotational
     semantics in "Table.Lattice".

     Like 'tl', 'iterTl' computes a solution of a tabled computation by
     computing a fixed point of a (wrapper around the) 'iterCl' function.
     Unlike 'tl', the fixed point computation is not bounded in the number
     of iterations.

    Author: Alexander Vandenbroucke
-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE ScopedTypeVariables        #-}

module Table.Internal.Lattice.Iter (
  -- * Helper Data types
  Iter(..), PredMap,
  newCall, findPredMap,
  -- * Effect Handlers
  iterTl, iterCl,
  -- * Helper functions
  fixM, step
  )
where

import Table.Internal
import Table.Internal.Lattice

import Control.Applicative
import Control.Monad.Writer
import Data.Traversable
import qualified Data.IntMap as IM
import qualified Data.Map as M

-- | A wrapper newtype around a @'Writer' [('Predicate' b c, b)] a@.
--   This allows the computation inside the Monad to return the new calls
--   (a pair of @'Predicate' b c@ and @b@) it has seen.
newtype Iter b c a = Iter { runIter :: Writer [(Predicate b c, b)] a}
                   deriving (Functor, Applicative, Monad)

-- | A type synonym for the datastructure that contains the discovered results.
type PredMap b c l = IM.IntMap (Predicate b c, M.Map b l)

-- | Record a new call in the 'Iter' Monad.
newCall :: Predicate b c -> b -> Iter b c ()
newCall p b = Iter $ tell [(p, b)]

-- | A semantic function mapping a @'Table' ('Predicate' b c) b c c@ to a
--  'Lattice'.
iterTl :: forall a b c l . (Ord b, Eq l, Lattice l, Coerce c l, Coerce a l)
       => Table (Predicate b c) b c a -> l
iterTl t = case t of
  Pure c        -> coerce c
  Fail          -> bottom
  a `Or` b      -> lub [iterTl a, iterTl b]
  Call p b cont ->
    let solution :: l
        solution = fixM step p b
    in lub [iterTl (cont c) | c <- uncoerce solution]

-- | A semantic function mapping a @'Table' ('Predicate' b c) b c c@ to a
--  'Lattice' when provided with a  function to look up predicate calls with.
--  It records any internal calls it sees in the 'Iter' Monad.
iterCl :: (Lattice l, Coerce c l)
       => Table (Predicate b c) b c c
       -> (Predicate b c -> b -> l)
       -> Iter b c l
iterCl t s = case t of
  Pure c        -> return (coerce c)
  Fail          -> return bottom
  a `Or` b      -> lubA [iterCl a s, iterCl b s]
  Call p b cont -> do newCall p b
                      lubA [iterCl (cont c) s | c <- uncoerce (s p b)]

-- | 'lub' lifted to applicatives.
--
--     > lubA = fmap lub . sequenceA     
lubA :: (Lattice l, Applicative f) => [f l] -> f l
lubA = fmap lub . sequenceA

-- | Provide lookup over the fixed point of a function that iterates on
--   'PredMap's.
fixM :: (Ord b, Eq l, Lattice l, Coerce c l)
     => (PredMap b c l -> PredMap b c l)
     -> (Predicate b c -> b -> l)
fixM f p@(Predicate i _) b = go (IM.singleton i (p, M.singleton b bottom)) where
  go im = let im' = f im
          in if im == im' then
               findPredMap im p b
             else 
               go im'

-- | Iteratively update a 'PredMap'.
--   For every pair of @'Predicate' b c@ and @b@ that is currently in the map,
--   re-evaluate the call with 'iterCl' and add the new result to the map.
step :: forall b l c . (Ord b, Lattice l, Coerce c l)
     => PredMap b c l
     -> PredMap b c l
step im0 =
  let updatePred :: (Lattice l, Coerce c l)
                    => M.Map b l -> Predicate b c -> Iter b c (M.Map b l)
      updatePred m0 p = (M.traverseWithKey
                         (\b l -> lubA [return l,
                                        (iterCl
                                         (_predicateBody p b)
                                         (findPredMap im0))])
                         m0)
      updateMap :: (Lattice l, Coerce c l)
                   => PredMap b c l -> Iter b c (PredMap b c l)
      updateMap im1 = (traverse
                       (\(p1, m1) -> do
                           m2 <- updatePred m1 p1
                           return (p1, m2))
                       im1)
      (im2, ps) = runWriter $ runIter $ updateMap im0
      addP :: Predicate b c -> b -> PredMap b c l -> PredMap b c l
      addP p@(Predicate i _) b im3 =
         (IM.insertWith
          (\_ (_, oldm) -> (p, M.insertWith (\x y -> lub [x, y]) b bottom oldm))
          i
          (p, M.singleton b bottom)
          im3)
  in foldr (\(p, b) im4 -> addP p b im4) im2 ps


-- | Find a call @('Predicate' b c, b)@ in a 'PredMap b c l'.
findPredMap :: (Ord b, Lattice l) => PredMap b c l -> Predicate b c -> b -> l
findPredMap im p@(Predicate key _) b =
  let m = snd $ IM.findWithDefault (p, M.empty) key im
  in M.findWithDefault bottom b m
