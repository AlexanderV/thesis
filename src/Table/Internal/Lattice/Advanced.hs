{- | This module contains the fastest version of the lattice-based tabling
     interpreter for the 'Table' monad.
     The semantics are identical to 'Table.Lattice.tl' from
     "Table.Lattice", but in general 'ts' will be much faster.
     This speed is primarily by performing a less naive fixpoint computation
     using dependency analysis.

     Author: Alexander Vandenbroucke
-}


{-# LANGUAGE ScopedTypeVariables #-}

module Table.Internal.Lattice.Advanced (tl, cl, table, tables) where

import           Table.Internal
import           Table.Internal.Data
import           Table.Internal.Lattice hiding (tl, cl)

import           Control.Applicative
import           Control.Monad.State
import           Data.Monoid
import qualified Data.Set as S

-- | A tabling interpreter using dependency analysis.
tl :: forall a b c l . (Ord b, Eq l, Lattice l, Coerce c l, Coerce a l)
   => Table (Predicate b c) b c a -> l
tl t = evalState (go t) (emptyT, emptyD) where
  go (Pure a)        = return (coerce a)
  go Fail            = return bottom
  go (a `Or` b)      = (<>) <$> go a <*> go b
  go (Call p b cont) = do (sols :: l) <- table p b
                          lub <$> mapM (go . cont) (uncoerce sols)

-- | Standard semantics for recursive calls.
cl :: (Lattice l, Coerce c l)
   => (Predicate b c -> b -> l)
   -> Table (Predicate b c) b c c
   -> l
cl s t = case t of
  Pure c        -> coerce c
  Fail          -> bottom
  a `Or` b      -> cl s a <> cl s b
  Call p b cont -> lub $ map (cl s . cont) $ uncoerce $ s p b

-- | Find all dependencies given the results computed so far.
deps :: (Ord b, Coerce c l)
     => (Predicate b c -> b -> l)
     -> Table (Predicate b c) b c c
     -> S.Set (Predicate b c, b)
deps s t = case t of
  Pure _        -> S.empty
  Fail          -> S.empty
  a `Or` b      -> deps s a <> deps s b
  Call p b cont ->
    S.insert (p,b) $ S.unions $ map (deps s . cont) $ uncoerce  $ s p b

-- | Fixpoint computation using dependency analysis.
--
--   This function runs in the 'State'-monad. The state consists of a table
--   @'T' b c l@ containing partial solutions and another table
--   @'D' b c@ containing dependencies.
table :: (Ord b, Coerce c l, Lattice l, Eq l)
      => Predicate b c -> b -> State (T b c l, D b c) l
table p b = do
  (t0, d0) <- get
  let findInT0 q c = findInT q c t0
      v = cl   findInT0 (_predicateBody p b)
      w = deps findInT0 (_predicateBody p b)
      t1 = updateT p b v t0
      d1 = updateD p b (S.toList w) d0
  put (t1, d1)
  if findInT p b t0 == findInT p b t1 then
    tables (new (flip S.filter w) t0)
  else
    tables (new (flip S.filter w) t0 <> findInD p b d1)
  findInTS p b

-- | Evaluate 'table' on a set of predicate-argument pairs.
tables :: (Ord b, Eq l, Lattice l, Coerce c l)
       => S.Set (Predicate b c, b)
       -> State (T b c l, D b c) ()
tables = mapM_ (uncurry table) . S.toList

