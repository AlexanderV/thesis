{- | This module implements formal denotational semantics for 'Table'-syntax.

     The denotational semantics is characterized by two semantic functions:
     'tl' and 'cl'.

     The semantics 'tl' computes a solution for a tabled computation, by
     computing a fixed point of (a wrapper around) the 'cl' function.
     The fixed point computation is limited to 10 iterations however.

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE ScopedTypeVariables   #-}

module Table.Internal.Lattice (
  -- * Lattice and Coerce typeclasses
  Lattice(..),
  Coerce(..),
  -- $pairs
  
  -- * Interpreters
  tl, cl,
  
  -- * Helper function
  fixF
  )
where
  
import           Table.Internal
  
import qualified Data.Set as S
import           Data.Monoid


{- | Instances of 'Lattice' correspond to the mathematical concept of a
     complete lattice. A partially ordered set @(L, '=<=')@ is a complete
     lattice, if for every @X@ subset of @L@, there exists a least upper bound
     (@lub@) s.t.
     for every @z@ in @L@: lub X =<= z iff forall x in X: x =<= z.

     Additionally we identify a special element 'bottom' that is the 'lub' of
     the empty subset.

     Finally, because 'Monoid' is a subclass of 'Lattice', we must also have:

     prop> mempty == bottom

     and

     > a <> b == lub [a, b]
-}
class Monoid l => Lattice l where
  (=<=)  ::  l  -> l -> Bool
  lub    :: [l] -> l
  bottom :: l
  bottom = lub []

{- | 'Coerce' represents a relation between two types @i@ and @a@ where any
     @i@ can be 'coerce'ed to an @a@, and any @a@ can be 'uncoerce'ed to a set
     of @i@s.

     Instances should satisfy the following law:

     prop> uncoerce (coerce i) === [i] ~ i

     where a === b = (nub a) == (nub b), i.e. equality when duplicates are
           removed and (~) denotes isomorphism.

     Additionally, instances that also instantiate 'Lattice' should satisfy the
     law:

     prop> lub [ coerce c | c <- uncoerce l] == l
-}  
class Coerce i a where
  coerce   :: i -> a
  uncoerce :: a -> [i]

{- $pairs
   The canonical 'Lattice' instance for pairs satisfies the lattice laws:
   
   > lub xs =<= (za, zb)
   > <=> (lub as, lub bs) =<= (za, zb) where (as, bs) = unzip xs
   > <=> lub as =<= za && lub bs =<= zb
   > <=> forall a in as: a =<= za && forall b in bs: b =<= zb
   > <=> forall (a, b) in xs: a =<= za && b =<= zb
   > <=> forall (a, b) in xs: (a, b) =<= (za, zb)
   > <=> forall x in xs: x =<= (za, zb)


   The canonical 'Coerce' instance for pairs does *NOT* satisisfy all the
   coerce laws:

   * 'uncoerce' after 'coerce' is identity up to isomorphisms:
  
     > uncoerce (coerce c) == uncoerce (coerceA c, coerceB c)
     >                     == uncoerce (coerceA c) ++ uncoerce (coerceB c)
     >                     === [c] ++ [c]    [ Coerce law. ]
     >                     === [c]
     >                     ~ c               [ [c] is isomorphic (~) to c. ]

   * However, @ lub [ coerce c | c <- uncoerce l] == l@ does not hold in
     general.

   For some specific combinations of 'Coerce's @a@ and @b@ the law may hold.
   It is therefore useful to at least provide the instance.
-}

-- | A canonical 'Lattice' instance for pairs.  
instance (Lattice a, Lattice b) => Lattice (a, b) where
  (a1, b1) =<= (a2, b2) = a1 =<= a2 && b1 =<= b2
  lub ls = (lub a, lub b) where (a, b) = unzip ls
  bottom = (bottom, bottom)

-- | A canonical 'Coerce' instance for pairs.
--
--   WARNING! This instance does not in general satisfy the 'Coerce' laws.
instance (Coerce c a, Coerce c b) => Coerce c (a, b) where
  coerce c = (coerce c, coerce c)
  uncoerce (a, b) = uncoerce a ++ uncoerce b

instance Ord c => Coerce c (S.Set c) where
  coerce = S.singleton
  uncoerce = S.toList

instance Ord c => Lattice (S.Set c) where
  (=<=)  = S.isSubsetOf
  lub    = S.unions
  bottom = S.empty

-- | Semantic funtion mapping a @'Table' ('Predicate' b c) b c c@ to a
--   'Lattice'. 
tl :: forall a b c l . (Lattice l, Coerce c l, Coerce a l)
      => Table (Predicate b c) b c a -> l
tl t = case t of
  Pure c        -> coerce c
  Fail          -> bottom
  a `Or` b      -> lub [tl a, tl b]
  Call p b cont ->
    let
      bottomF :: Predicate b c -> b -> l
      bottomF = \_ _ -> bottom
      sols    =
        fixF 10 bottomF (\s p' b' -> lub [s p' b', cl (_predicateBody p' b') s])
    in lub [tl (cont c) | c <- uncoerce (sols p b)]

-- | Semantic function mapping a @'Table' ('Predicate' b c) b c c@ to a
--  'Lattice' when provided with a function to look up predicate calls with.
cl ::
  (Lattice l, Coerce c l)
  => Table (Predicate b c) b c c
  -> (Predicate b c -> b -> l)
  -> l
cl t s = case t of
  Pure c        -> coerce c
  Fail          -> bottom
  a `Or` b      -> lub [cl a s, cl b s]
  Call p b cont -> lub ([cl (cont c) s | c <- uncoerce (s p b)])

-- | Compute the fixed point of a function starting from a base case, for a
--   fixed number of iterations.
fixF :: Int -> l -> (l -> l) -> l
fixF n0 l0 f = go n0 l0 where
  go n v | n <= 0    = v
         | otherwise = go (n - 1) (f v)
