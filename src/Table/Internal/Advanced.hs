{- | This module contains the fastest version of the tabling interpreter for
     the 'Table' monad. The semantics are identical to
     'Table.Formal.tableSemantics' from "Table.Formal", but in general 'ts'
     will be much faster. This speed is primarily by performing a less naive
     fixpoint computation using dependency analysis.

     Author: Alexander Vandenbroucke
-}


module Table.Internal.Advanced (ts, table, tables, cs, deps) where

import           Table.Internal
import           Table.Internal.Data

import           Control.Applicative
import           Control.Monad.State
import           Data.Monoid ((<>))
import qualified Data.Set as S
import qualified Data.Traversable as T

-- | A tabling interpreter using dependency analysis, specialised for Set.
ts :: (Ord a, Ord b, Ord c) => Table (Predicate b c) b c a -> S.Set a
ts t = evalState (go t) (emptyT, emptyD) where
  go (Pure a) = return (S.singleton a)
  go Fail     = return (S.empty)
  go (Or a b) = (<>) <$> go a <*> go b
  go (Call p b cont) = do sols  <- S.toList <$> table p b
                          S.unions <$> mapM (go . cont) sols

-- | Fixpoint computation using dependency analysis.
--
--   This function runs in the 'State'-monad. The state consists of a table
--   @'T' b c ('S.Set' c)@ containing partial solutions and another table
--   @'D' b c@ containing dependencies.
table :: (Eq b, Ord b, Ord c)
      => Predicate b c
      -> b
      -> State (T b c (S.Set c), D b c) (S.Set c)
table p b = do
  (t0, d0) <- get
  let findInT0 q c = findInT q c t0
      v = cs   findInT0 (_predicateBody p b) -- new results
      w = deps findInT0 (_predicateBody p b) -- new dependencies 
      t1 = updateT p b v t0             -- update results
      d1 = updateD p b (S.toList w) d0  -- update dependencies
  put (t1, d1)
  if findInT p b t0 == findInT p b t1 then
     -- run only new dependends
    tables (new (flip S.filter w) t0)
  else
    -- run dependends (old & new)
    tables (new (flip S.filter w) t0 <> findInD p b d1)
  findInTS p b -- result after recursion

-- | Evaluate 'table' on a set of predicate-argument pairs.
tables :: (Ord b, Ord c)
       => S.Set (Predicate b c, b) -> State (T b c (S.Set c), D b c) ()
tables = void . T.traverse (uncurry table) . S.toList

-- | Standard semantics for recursive calls.
cs :: Ord c => (p -> b -> S.Set c) -> Table p b c c -> S.Set c
cs s t = case t of
  Pure a        -> S.singleton a
  Fail          -> S.empty
  Or a b        -> cs s a <> cs s b
  Call p b cont -> S.unions [cs s (cont c) | c <- S.elems (s p b)]


-- | Find all dependencies given the results computed so far.
deps :: (Ord p, Ord b) => (p -> b -> S.Set c) -> Table p b c c -> S.Set (p, b)
deps s t = case t of
  Pure _ -> S.empty
  Fail   -> S.empty
  Or a b -> deps s a <> deps s b
  Call p b cont ->
    S.insert (p,b) $ S.unions $ map (deps s . cont) $ S.toList $ s p b
