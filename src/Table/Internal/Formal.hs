{- | This module implements formal denotational semantics for 'Table'-syntax.

     The denotational semantics is characterized by two semantic functions:
     'tableSemantics' and 'callSemantics'.

     The 'tableSemantics' and 'callSemantics' are hard-coded to execute
     at most 10 fixpoint iteration steps.

     The 'iterTableSemantics' and 'iterCallSemantics' provide the same
     functionality for an unlimited number of fixpoint iteration steps.

     Author: Alexander Vandenbroucke
-}


module Table.Internal.Formal
       (
         -- * Type synonyms
         Solutions, SolutionMap,
         -- * Limited to 10 iterations
         tableSemantics, callSemantics,
         -- * Unlimited number of iterations
         -- $iter
         iterTableSemantics, iterCallSemantics,
         -- * Running a 'TabledProgram'
         runProgram,
         tableProgram,
         -- * Helper functions
         step, update, find
       )
where

import           Table.Program
import           Table.Internal

import qualified Data.IntMap as IM
import qualified Data.Map as M
import           Data.Monoid ((<>))
import qualified Data.Set as S

-- | Type synonym for the intermediate result.
type Solutions b c = Predicate b c -> b -> S.Set c
-- | Type synonym for the intermediate result.
type SolutionMap b c = IM.IntMap (Predicate b c, M.Map b (S.Set c))
  

{- | The semantics of a non-deterministic computation is the set of all
     its results, more formally (@lfp@ denotes the least fixed point):

     @
     tableSemantics ('Pure' a)        == {a}
     tableSemantics 'Fail'            == {}
     tableSemantics ('Or' a b)        == (tableSemantics a) U (tableSemantics b)
     tableSemantics ('Call' p b cont) ==
       U{tableSemantics (cont c) | c in table(p,b)} where
         table = lfp (\\s k -> callSemantics p s) b
     @

     Here 'lfp' is limited to 10 successive applications.
-}
tableSemantics :: (Ord a, Ord c) => Table (Predicate b c) b c a -> S.Set a
tableSemantics = go where
  go t = case t of 
    Pure a        -> S.singleton a
    Fail          -> S.empty
    a `Or` b      -> go a `S.union`  go b
    Call p b cont ->
      let stepF s pK bK = callSemantics (_predicateBody pK bK) s
          sols = fixF 10 (const (const S.empty)) stepF p b
      in S.unions [go (cont c) | c <- S.elems sols]

{- |
  The semantics of a call in a non-deterministic tabled computation is
  given by:

  @
  callSemantics ('Pure' a) s      == {a}
  callSemantics 'Fail'     s      == {}
  callSemantics ('Or' a b) s      == (callSemantics s a) U (callSemantics b s)
  callSemantics ('Call' p b cont) == U{callSemantics (cont c) s | c in s p b}
  @
-}
callSemantics :: Ord c
              => Table (Predicate b c) b c c -> Solutions b c -> S.Set c
callSemantics t s = case t of
  Pure a        -> S.singleton a 
  Fail          -> S.empty
  t1 `Or` t2    -> callSemantics t1 s `S.union` callSemantics t2 s
  Call p b cont -> S.unions [callSemantics (cont c) s | c <- S.elems (s p b)]

-- | @fixF n@ computes a fixed point, provided that the fixed point is reached
--   within @n@ applications.
fixF :: Int -> a -> (a -> a) -> a
fixF n0 a0 f = go n0 where
  go 0 = a0
  go n = f (go (n-1))

--------------------------------------------------------------------------------
{- $iter
   The following functions obtain results based on fixpoint interation with an
   unlimited number of iterations.

   Consider the usual step function @stepF@ for fixpoint iteration with a
   fixed number of iterations:

   > stepF :: Solutions b c -> Solutions b c
   >       :: (Predicate b c -> b -> S.Set c) -> (Predicate b c -> b -> S.Set c)

   So @stepF@ maps a function ('Solutions') to another function, hence a
   fixpoint must be a @'Solutions' b c@, a function.

   Compare this to @step :: SolutionMap b c -> SolutionMap b c@, the function is
   replaced by a map, because we can detect convergence when iterating on
   the map, but convergence (equality) is hard to detect for functions.
-}

-- | The semantics of a non-deterministic computation is the monoidal
--   combination of its results. Similar to 'tableSemantics', but with a
--   unlimited number of iterations.
iterTableSemantics ::
  (Ord a, Ord b, Ord c)
  => Table (Predicate b c) b c a -- ^ The computation to evaluate.
  -> S.Set a                     -- ^ The results wrapped in the monoid @v@.
iterTableSemantics = go where
  go t = case t of
    Pure a        -> S.singleton a
    Fail          -> S.empty
    a `Or` b      -> iterTableSemantics a `S.union` iterTableSemantics b
    Call p b cont ->
      let start    = update p b S.empty IM.empty
          fixpoint = fixM start (step iterCallSemantics)
          sols     = S.elems (find fixpoint p b)
      in S.unions [go (cont c) | c <- sols]


-- | The semantics of a call in a non-deterministic tabled computation is
--   the set of all its results, based on the results from the previous
--   iteration.
iterCallSemantics ::
  Ord c
  => Table (Predicate b c) b c c      -- ^ Computation to evaluate.
  -> Solutions b c                    -- ^ Previous results.
  -> (S.Set c, [(Predicate b c, b)])  -- ^ Partial results and new 'Predicate's.
iterCallSemantics t0 s = go t0 where
  go t = case t of
    Pure c        -> (S.singleton c, [])
    Fail          -> (S.empty, [])
    a `Or` b      -> iterCallSemantics a s <> iterCallSemantics b s
    Call p b cont -> let (r, calls) = unzip [go (cont c) | c <- S.elems (s p b)]
                     in (S.unions r, (p, b):concat calls)

-- | The @step@ function updates a map containing the solutions found so far,
--   and adds empty entries for newly detected predicates.
step ::
  (Ord b, Ord c)
  => (Table (Predicate b c) b c c
      -> Solutions b c
      -> (S.Set c, [(Predicate b c, b)])
     )
  -> SolutionMap b c
  -> SolutionMap b c
step action im0 =
  let updatePredicate (p0, m0) =
        let updateArgument b cs =
              let (cs', l) = action (body p0 b) (find im0)
              in (l, cs <> cs')
            m1        = M.mapWithKey updateArgument m0
            (ps0, m2) = M.mapAccum (\ls (l, b) -> (l ++ ls, b)) [] m1
        in (ps0, (p0, m2))
      im1 = fmap updatePredicate im0
      (ps1, im2) = IM.mapAccum (\ls (l, b) -> (l ++ ls, b)) [] im1
  in foldr (\(p, b) im3 -> update p b S.empty im3) im2 ps1
  

-- | @fix@ iterates a monadic function on a map until it converges.
fixM ::
  (Eq b, Eq c)
  => SolutionMap b c                      -- ^ The starting value of the
                                          -- map.
  -> (SolutionMap b c -> SolutionMap b c) -- ^ The function to iterate.
  -> SolutionMap b c                      -- ^ The map that was
                                          --   converged on.
fixM start f = go start where
  go m = let m' = f m
         in if m == m' then m' else go m'

-- | Add a set of solutions to the entry for a predicate.     
update :: (Ord b, Ord c)
       => Predicate b c -> b -> S.Set c -> SolutionMap b c -> SolutionMap b c
update p@(Predicate i _) b newcs s =
  let (_, oldm) = IM.findWithDefault (p, M.empty) i s
      oldset    = M.findWithDefault S.empty b oldm
      newset    = oldset <> newcs
      newm      = M.insert b newset oldm
  in IM.insert i (p, newm) s

-- | Turn a SolutionMap into a Solutions function.
find :: (Ord b, Ord c) => SolutionMap b c -> Solutions b c
find im p@(Predicate i _) b =
  let (_, m) =  IM.findWithDefault (p, M.empty) i im
  in M.findWithDefault S.empty b m

-------------------------------------------------------------------------------
-- Evaluate 'TabledProgram' to obtain a 'Table'

{- | Interpret a 'TabledProgram b c a' in the 'Table (Predicate b c) b c a'
     monad.

     * Every @'Expr' m@ is simply interpreted as @m@.

     * Every @'Decl' gs cont@ declares a new list 'Predicate's for which
       fresh 'Int' identifiers are allocated.
       The bodies of the new 'Predicate' are @gs@ modified such that each
       recursive call will 'Call' the new 'Predicate'.
       Interpretation then continues with 'cont', which is passed
       a function to call the new predicates as.
-}
runProgram :: TabledProgram b c a -> Table (Predicate b c) b c a
runProgram p = runProgram' 0 (unTP p) where
  runProgram' _ (Expr m)      = m
  runProgram' i (Decl gs cont) =
    runProgram' (i+n) (cont predicates) where
      n = length predicates
      predicates = map call (zipWith Predicate [i..] (gs predicates))

-- | Evaluate a 'TabledProgram'
--
--   > tabledProgram == tableSemantics . runProgram
tableProgram :: (Ord a, Ord c) => TabledProgram b c a -> S.Set a
tableProgram = tableSemantics . runProgram
