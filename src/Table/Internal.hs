{- | This module declares the 'Table' AST.

     Unless you are implementing an effect handler, you probably don't need to
     import this module directly. Use the 'Table' module instead.

     'Table' is a 'Monad' representing a non-deterministic tabled computation.

       * You can use the 'Monad' instance to structure your programs like you
         would in any other monad.
       * A 'Nondet' instance allows you to specify non-deterministic actions
         using 'fail' and '<|>'.
       * Finally, the 'call' smart constructor allows you to call a tabled
         predicate.

     For example:

     > return 1 <|> fail <|> do i <- call p 1
     >                          if i == 0 then
     >                            fail
     >                          else
     >                            return i

   
     Author: Alexander Vandenbroucke
-}


{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE Rank2Types #-}
{-# LANGUAGE LiberalTypeSynonyms #-}
module Table.Internal (
  -- * Datatypes
  -- ** Table
  Table(..),
  -- ** Predicate
  Predicate (..),
  -- * Smart Constructors
  call,
  -- * Utility
  body
  )
where

import Nondet  
  
import Control.Applicative
import Control.Monad hiding (fail)
import Prelude hiding (fail)

--------------------------------------------------------------------------------
-- Table datatype and related instances.

{- | The 'Table' AST: @'Table' p b c a@  is a non-deterministic Tabled
     computation where:

     * @p@ is the type of a tabled predicate,
     * @b@ is the argument type of a predicate,
     * @c@ is the result type of a predicate,
     * and @a@ is the value that is returned by the computation.
-}
data Table p b c a
  -- |  Represents a call to a tabled predicate of type 'p', that takes an
  --    argument of type 'b' and returns values of type 'c', followed by a
  --    continuation.
  = Call p b (c -> Table p b c a)
  -- | Represents a non-deterministic choice between to computations.
  | Or (Table p b c a) (Table p b c a)
  -- | Represents a failed computation.
  | Fail
  -- | Represents a successful computation witnessed by a value of type @a@.
  | Pure a

instance Functor (Table p b c) where
  fmap = liftM

instance Applicative (Table p b c) where
  pure  = return
  (<*>) = ap

instance Nondet (Table p b c) where
  fail  = Fail
  (<|>) = Or

instance Monad (Table p b c) where
  return = Pure
  (Pure c)        >>= f = f c
  (Or t1 t2)      >>= f = Or (t1 >>= f) (t2 >>= f)
  Fail            >>= _ = Fail
  (Call p b cont) >>= f = Call p b ((>>= f) . cont)

{- | Smart constructor for a call to a tabled predicate.
     For example:

     > call p b

     This will call the predicate @p@ with argument @b@ and have a
     continuation that returns the result of the call.
-}
call :: p -> b -> Table p b c c
call p b = Call p b Pure


--------------------------------------------------------------------------------
-- Predicate


-- | The concrete predicate representation with argument 'b' and return
--   type 'c'.
--   It contains an @Int@ identifier and a non-deterministic computation in the
--   corresponding 'Table' monad that is the body of the predicate.
data Predicate b c = Predicate {
  _predicateId   :: Int,
  _predicateBody :: b -> Table (Predicate b c) b c c
  }

instance Show (Predicate b c) where
  show (Predicate i _) = "(Predicate " ++ show i ++ ")"

instance Eq (Predicate b c) where
  (Predicate i1 _) == (Predicate i2 _) = i1 == i2

instance Ord b => Ord (Predicate b c) where
  (Predicate i1 _) <= (Predicate i2 _) = i1 <= i2

-- | A shorthand for '_predicateBody'.
body :: Predicate b c -> b -> Table (Predicate b c) b c c
body = _predicateBody
