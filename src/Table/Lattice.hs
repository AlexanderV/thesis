{- | This module implements formal denotational semantics for 'Table'-syntax.

     The denotational semantics is characterized by two semantic functions:
     'tl' and 'cl'.

     The semantics 'tl' computes a solution for a tabled computation, by
     computing a fixed point of (a wrapper around) the 'cl' function.
     The fixed point computation is limited to 10 iterations however.

     Author: Alexander Vandenbroucke
-}

module Table.Lattice (
  -- * Lattice and Coerce typeclasses
  L.Lattice(..),
  L.Coerce(..),
  
  -- * Interpreters
  tl
  )
where
  
import qualified Table.Internal.Lattice as L
import           Table
import           Table.Internal.Formal (runProgram)
  
-- | Semantic funtion mapping a @'Table' ('Predicate' b c) b c c@ to a
--   'Lattice'. 
tl :: (L.Lattice l, L.Coerce c l, L.Coerce a l) => TabledProgram b c a -> l
tl = L.tl . runProgram
