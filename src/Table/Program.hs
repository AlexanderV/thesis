{-| This module declares the 'Program' AST.

    Unless you are implementing an effect handler, you probably don't need to
    import this module directly. Use the 'Table' module instead.

    A program consists of predicate declarations followed by an expression using
    the declared predicates.

    Predicates take the form of open recursive functions ('Open').

    Author: Alexander Vandenbroucke
-}

{-# LANGUAGE RankNTypes #-}


module Table.Program (
  -- * Program AST datatype
  Program(..),
  -- * Smart Constructors
  -- ** Declarations
  declare,
  declareRec,
  -- ** Expression
  expression,
  -- * TabledProgram
  TabledProgram(..)
  ) where

import Open
import Nondet

import Control.Applicative
import Control.Monad
import Prelude hiding (lookup)

{-|
    Program AST: @'Program' b c m a@ is a program where

        1. @b@ is the argument type of a predicate,
        2. @c@ is the result type of the predicate,
        3. @m@ is the 'Monad' wrapped in the 'Program'
        4. and @a@ is the value that is returned by the program.

    Programs can be created using the 'declare', 'declareRec' and 'expression'
    smart constructors

    For example:

    > predicateP  :: Open (b -> m c)
    > predicateQR :: Open [b -> m c]
    >
    > declare    predicateP  $ \p ->
    > declareRec predicateQR $ \[q, r] ->
    > expression $ do
    >    do stuff with p, q and r here
    >    return aResult
-}

data Program b c m a
   -- | @Decl body cont@: declares predicates of type @p@ with bodies @body@
   --    and continuation of the program @cont@.
   = Decl (Open [b -> m c]) ([b -> m c] -> Program b c m a)
   -- | @Expr exp@: A wrapper around an expression @exp@ in the contained monad
   --   @m@.
   | Expr (m a)


instance Functor m => Functor (Program b c m) where
  fmap f (Decl g cont) = Decl g (fmap f . cont)
  fmap f (Expr m)      = Expr (fmap f m)

{- | Declare a new predicate.
  
     > declare b (\p -> prog)

     declares the single predicate with body @b@ that will be bound to @p@
     in 'prog'.
-}
declare :: Open (b -> m c) -> ((b -> m c) -> Program b c m a) -> Program b c m a
declare p f = Decl (singleton .  p . head) (f . head) where singleton x = [x]
-- NOTE: do not use @map p@ instead of @singleton . p . head@.
-- It ensures that the list has a l@ength == 1@.
-- This is important because interpreters may rely on finding the length of the
-- list.


{- | Declare a set of mutually recursive predicates.

     > declareRec bs (\ps -> prog)

     declares a list of predicates with bodies @bs@.
     In @prog@ these will be bound to their respective elements in @ps@.
     @bs@ must be non-strict in its argument, because otherwise intepreters may
     not terminate. This can be resolved by specifying explict lazyness.

     For example by writing:

     > declareRec (\(~[p,q] -> ...) $ ...

     instead of

     > declareRec (\([p,q] -> ...) $ ...
-}
declareRec ::
  Open [b -> m c] -> ([b -> m c] -> Program b c m a) -> Program b c m a
declareRec = Decl


-- | Wrap an expression in a Program.
--   After and expression, no other expressions or declarations can follow.
expression :: m a -> Program b c m a
expression = Expr

--------------------------------------------------------------------------------
-- TabledProgram

-- | 'TabledProgram' is a wrapper-newtype that forces the wrapped 'Program' to
--   be polymorphic in the contained 'Nondet'.
newtype TabledProgram b c a = TP {
  unTP :: forall m . (Applicative m, Nondet m) => Program b c m a
  }

