{- | This module implements formal denotational semantics for 'Table'-syntax.

     The denotational semantics is characterized by two semantic functions:
     'tableSemantics' and 'callSemantics'.

     The 'tableSemantics' is hardcoded to compute at most 10 fixpoint iteration
     steps, which limits the size of the sets it can compute

     The function 'iterTableSemantics' provides the same functionality for an
     unlimited number of fixpoint iteration steps, and thus for sets of unlimited
     sizes.

     Author: Alexander Vandenbroucke
-}


module Table.Formal
       (
         -- * Limited to 10 iterations
         tableSemantics,
         -- * Unlimited number of iterations
         iterTableSemantics,
       )
where

import qualified Table.Internal.Formal as F
import           Table.Program

import qualified Data.Set as S

{- | The semantics of a non-deterministic computation is the set of all
     its results.
     This function returns correct results, only for small sets.
-}
tableSemantics :: (Ord a, Ord c) => TabledProgram b c a -> S.Set a
tableSemantics = F.tableSemantics . F.runProgram

--------------------------------------------------------------------------------
-- Iterative semantics

-- | The semantics of a non-deterministic computation is the set of all
--   its results.
--   Similar to 'tableSemantics', but with an unlimited set size.
iterTableSemantics :: (Ord a, Ord b, Ord c) => TabledProgram b c a -> S.Set a
iterTableSemantics = F.iterTableSemantics . F.runProgram
