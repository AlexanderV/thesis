{- | This module declares the 'Nondet' typeclass.

     Nondet is a typeclass that abstracts a non-deterministic monadic
     computation. There are three important constructions:

     * a deterministic computation with result @a@ is abstracted by 'return',
     * a failed computation is abstracted by 'fail',
     * a non-deterministic binary choice is abstracted by '<|>'.

     An example instance is provided for lists.

     Author: Alexander Vandenbroucke
-}

module Nondet (Nondet(..)) where

import Prelude hiding (fail)

--------------------------------------------------------------------------------
-- Nondet typeclass

-- | Instances of 'Nondet' are a non-deterministic monadic computation
class Monad m => Nondet m where
  -- | a failed computation
  fail  :: m a
  -- | a binary non-deterministic choice
  (<|>) :: m a -> m a -> m a
  -- | a n-ary non-deterministic choice
  choose :: [a] -> m a
  choose = foldr (<|>) fail . map return

infixl 3 <|>  

instance Nondet [] where
  fail  = []
  (<|>) = (++)
