{- | Solve the following problem using dynamic programming:

     A game consists of two players and a row of N matches on a table.
     Each player can take away one, two or three matches on their turn.
     A player loses when they take the final match.

     For a given N, does their exist a strategy for the current player, such
     that victory is assured?

     To verify correctness, a number of properties are provided:
     'prop_matchescorrect', 'prop_matchesStores', 'prop_matchesLookups',
     'prop_matchesSearch'.

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE Rank2Types #-}

module Matches (
  -- * Matches
  Action(..), Solution, Result(..),
  matches,
  matchesSearch,
  matchesProgram,
  matchesSearchProgram,
  -- * Properties
  allCheck,
  prop_matchescorrect,
  prop_matchesSearch
  ) where

import           Open
import           Table
import           Table.Advanced (ts)
import           Table.Formal

import           Control.Monad.Identity hiding (fail)
import           Data.List (sort, nub)
import qualified Data.Set as S
import           Prelude hiding (fail)
import           Test.QuickCheck hiding (Result)

-- | An 'Action' consists of taking either 'One', 'Two' or 'Three' matches.
data Action = One | Two | Three deriving (Show, Eq, Ord)

-- | A solution is a list of actions to be performed by the winning player.
type Solution = [Action]

-- | Result is either 'Victory' or 'Loss'.
--   In both cases, the Solution are the 'Action's that the /winning/ player
--   has to perform.
data Result = Victory Solution | Loss Solution deriving (Show, Eq, Ord)

-- | Solve the decision problem as stated in the module header.
-- Note: to fully implement this as a nondeterministic computation, some extra
-- machinery is required in the Tabling ASTs.
matches :: Monad m => Open (Int -> m Bool)
matches _ 1 = return False
matches _ 2 = return True
matches _ 3 = return True
matches _ 4 = return True
matches super n
  | n <= 0 =
      error $ "matches: Argument must be larger or equal to one, was: '"
      ++ show n ++ "'."
  | otherwise = do
      m1 <- super (n - 1)
      m2 <- super (n - 2)
      m3 <- super (n - 3)
      return (not (m1 && m2 && m3))

-- | Return a result, which is either an absolute 'Victory' or a 'Loss'.
matchesSearch :: Nondet m => Open (Int -> m Result)
matchesSearch _ 1 = return (Loss [])
matchesSearch _ 2 = return (Victory [One])
matchesSearch _ 3 = return (Victory [Two])
matchesSearch _ 4 = return (Victory [Three])
matchesSearch super n = do
  a1 <- super (n - 1)
  a2 <- super (n - 2)
  a3 <- super (n - 3)
  case (a1, a2, a3) of
   (Victory v1, Victory v2, Victory v3) ->
     return (Loss (One:v1))
     <|>
     return (Loss (Two:v2))
     <|>
     return (Loss (Three:v3))
   _ -> case a1 of
         Loss l1 -> return (Victory (One:l1))
         _       -> fail
        <|>
        case a2 of
         Loss l2 -> return (Victory (Two:l2))
         _       -> fail
        <|>
        case a3 of
         Loss l3 -> return (Victory (Three:l3))
         _       -> fail
             
-- | A tabled program for the decision problem.
matchesProgram :: Int -> TabledProgram Int Bool Bool
matchesProgram i = TP $ declare matches $
                        \p -> expression $ p i

-- | A tabled program for the search problem.
matchesSearchProgram :: Int -> TabledProgram Int Result Result
matchesSearchProgram i = TP $ declare matchesSearch $
                              \p -> expression $ p i


--------------------------------------------------------------------------------
-- Tests

-- | Check that the result of the memoized version of matches is correct.
prop_matchescorrect :: Int -> Property
prop_matchescorrect x =
  x > 0 &&  x < 30 ==>
  [runIdentity (new matches x)] == S.toList (ts (matchesProgram x))

-- | Check that the memoized version of matches returns the expected result.
prop_matchesSearch :: Int -> Property
prop_matchesSearch x =
  x > 0 && x < 10 ==>
  (nub $ sort $ S.elems $ tableSemantics $ matchesSearchProgram x)
  ==
  (nub $ sort $ new matchesSearch x)

-- | Check all properties.
allCheck :: IO ()
allCheck = do
  quickCheck prop_matchescorrect
  quickCheck prop_matchesSearch

