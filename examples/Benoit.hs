{- | Based on a Prolog example due to Benoit Desouter, where XSB goes awry.

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}

module Benoit (
  )
where

import Table
import Table.Lattice
import Table.Lattice.Advanced
import Prelude hiding (fail)

example :: TabledProgram () Int Int
example = TP $ declareRec (\(~[d, e]) -> [
                               \() -> do
                                 y <- e ()
                                 if y < 5
                                   then return (y + 1)
                                   else fail
                               <|> return 0,
                               \() -> do
                                 y <- d ()
                                 if y < 5
                                   then return (y + 1)
                                   else fail
                                 <|> return 0])

          (\[d,e] -> expression (d () >> e () >> d ()))

data Max a = Bottom | Max a deriving (Eq,Ord,Show)

instance Coerce a (Max a) where
  coerce = Max
  uncoerce Bottom  = []
  uncoerce (Max x) = [x]

instance Num a => Num (Max a) where
  Bottom + _ = Bottom
  _ + Bottom = Bottom
  Max a + Max b = Max (a + b)
  Bottom * _ = Bottom
  Max a * Max b = Max (a * b)
  _ * Bottom = Bottom
  abs Bottom = Bottom
  abs (Max a) = Max (abs a)
  signum Bottom = 0
  signum (Max a) = Max (signum a)
  negate Bottom = Bottom
  negate (Max a) = Max (negate a)
  fromInteger = Max . fromInteger
  
instance Ord a => Monoid (Max a) where
  mempty = Bottom
  mappend a b = max a b

instance Ord a => Lattice (Max a) where
  bottom = Bottom
  a =<= b = a <= b
  lub = foldr mappend mempty
