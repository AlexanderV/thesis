{- | This module contains an implementation of the domain elements for the
     unbounded knapsack problem. This includes 'Value's, 'Weight's, 'Item's and
     'Knapsack's.

     Author: Alexander Vandenbroucke
-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE FlexibleInstances          #-}

module Table.Knapsack.Knapsack (
  -- * Domain elements
  -- ** Items
  Item(..),
  Weight(unWeight),
  Value(unValue),
  mkWeight,
  mkValue,
  -- ** Knapsack  
  Knapsack(..),
  ksWeight,
  ksValue,
  -- * Generating random items
  generateItems
  )
where


import Control.DeepSeq
import Prelude hiding (fail)
import System.Random

--------------------------------------------------------------------------------
-- Weight & Value Datatypes

-- | 'Weight' wraps an item's weight.
--   'Weight' values are always positive integers.
newtype Weight = Weight { unWeight :: Int } deriving (Eq, Ord, Show, NFData)

-- | 'Value' wraps an item's value.
--   'Value' values are always positive integers
newtype Value  = Value  { unValue  :: Int } deriving (Eq, Ord, Show, NFData)

mkWeight :: Int -> Weight
mkWeight w | w <= 0    = error "mkWeight: Weights must be strictly positive!"
           | otherwise = Weight w

mkValue :: Int -> Value
mkValue v | v <= 0    = error "mkValue: Values must be strictly positive!"
          | otherwise = Value v

instance Random Weight where
  randomR (Weight wMin, Weight wMax) g
    | wMin <= 0 = error "randomR: Weights must be strictly positive!"
    | otherwise = (mkWeight w, g') where (w, g') = randomR (wMin, wMax) g
  random g = if w == 0 then random g' else (mkWeight w, g')
    where (w, g') = random g

instance Random Value where
  randomR (Value vMin, Value vMax) g
    | vMin <= 0 = error "randomR: Values must be strictly positive!"
    | otherwise = (mkValue v, g') where (v, g') = randomR (vMin, vMax) g
  random g = (mkValue w, g') where (w, g') = randomR (1, maxBound) g


--------------------------------------------------------------------------------
-- Item & Knapsack

-- | An 'Item' consists of a 'Weight' and a 'Value'.
data Item = Item { weight :: Weight, value :: Value } deriving (Eq, Ord, Show)

instance NFData Item where
  rnf (Item w v) = force w `seq` force v `seq` ()

-- | A 'Knapsack' is a collection of 'Item's.
newtype Knapsack = Knapsack { unKnapsack :: [Item] } deriving (NFData, Show)

ksWeight :: Knapsack -> Int
ksWeight = sum . map (unWeight . weight) . unKnapsack

ksValue :: Knapsack -> Int
ksValue = sum . map (unValue . value) . unKnapsack
                                   
instance Eq Knapsack where
  k1 == k2 = ksWeight k1 == ksWeight k2 && ksValue k1 == ksValue k2

instance Ord Knapsack where
  k1 <= k2 = let w1 = ksWeight k1
                 w2 = ksWeight k2
                 v1 = ksValue k1
                 v2 = ksValue k2
             in v1 < v2 || (v1 == v2 && w2 <= w1)

generateItems ::
  Int -> (Weight,Weight) -> (Value,Value) -> StdGen -> ([Item], StdGen)
generateItems 0 _ _ g = ([], g)
generateItems n wR@(minW, maxW) vR@(minV, maxV) g0
  | minW > maxW = error "weight: minimum must be smaller than maximum."
  | minV > maxV = error "value: minimum must be smaller than maximum."
  | otherwise = let (w, g1) = randomR wR g0
                    (v, g2) = randomR vR g1
                    (s, g3) = generateItems (n-1) wR vR g2
                in (Item w v : s, g3)
