{- | This modules contains 4 examples that demonstrate and test the functionality
     of the 'tableSemantics' intepreter in "Tabling".

     They are variations on the idea of finding all permutations using a tabled
     non-deterministic computation.

     The simplest variation 'example' computes the permutations of the pair
     @(1,2)@ (i.e. @(1,2)@ and @(2,1)@).

     Author: Alexander Vandenbroucke
-}
module Table.Examples (
  -- * Examples
  example, example2, example3, example4,
  -- * Properties
  checkExample, checkExample2, checkExample3, checkExample4
  )
where

import           Table
import qualified Table.Advanced as A
import qualified Table.Formal as F

prd :: Nondet m => Open (() -> m (Int, Int))
prd super () = return (1, 2) <|> do (x,y) <- super ()
                                    return (y,x)
prd2 :: Nondet m => Open (() -> m (Int, Int, Int))
prd2 super () = return (1,2,3) <|> do (x,y,z) <- super ()
                                      return (y, z, x)
prd3 :: Nondet m => Open (() -> m (Int, Int, Int))
prd3 super () = (super () >>= \(x,y,z) -> return (y, z, x)) <|> return (1,2,3)

-- | Compute all permuations of the pair @(1,2)@
example :: TabledProgram () (Int, Int) (Int, Int)
example = TP $ declare prd $ \ident -> expression $ ident ()

-- | Compute all permuations of the pair @(1,2,3)@
example2 :: TabledProgram () (Int, Int, Int) (Int, Int, Int)
example2 = TP $ declare prd2 $ \ident -> expression $ ident ()

-- | Compute all permuations of the pair @(1,2,3)@ in a different order.
example3 :: TabledProgram () (Int, Int, Int) (Int, Int, Int)
example3 = TP $ declare prd3 $ \ident -> expression $ ident ()

-- | Compute a combination of 'example2' and 'example3'.
example4 :: TabledProgram () (Int, Int, Int) (Int,Int, Int)
example4 = TP $ declare prd2 $ \p ->
                declare prd3 $ \q ->
                expression $ do (x, y, z) <- p ()
                                (u, v, w) <- q ()
                                return (x+u,y+v,z+w)

checkProgram :: (Ord c, Show c) => String -> TabledProgram () c c -> IO Bool
checkProgram name p = do
  let eq = A.ts p == F.tableSemantics p
  putStrLn $ name ++ if eq then " OK" else " deviated from formal semantics!"
  return eq

checkExample, checkExample2, checkExample3, checkExample4 :: IO Bool
checkExample = checkProgram "example" example
checkExample2 = checkProgram "example2" example2
checkExample3 = checkProgram "example3"  example3
checkExample4 = checkProgram "example4" example4
