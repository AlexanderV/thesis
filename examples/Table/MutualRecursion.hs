{- | This module demonstrates two mutually recursive predicates @p@ and @q@
     declared with the 'declareRec' construct.

     They are defined by:

     > predicatePQ ~[p,q] = [\i -> q (i - 1),
     >                       \i -> if i <= 0 then
     >                               return 42
     >                             else
     >                               p i
     >                      ]

     Predicate p will always call predicte q, after decrementing its argument.
     Predicate q will always call p with its argument unless the argument is
     less than or equal to 0.

     Author: Alexander Vandenbroucke
-}
module Table.MutualRecursion (
  -- * Program
  theProgram,
  -- * Properties
  prop_result
  )
where
  

import Table
import qualified Table.Advanced as A

import Data.Set as S

-- nasty: we need a lazyness annotation when matching on [p,q], to ensure we are
-- not forcing the list's spine when we don't need to.
predicatePQ :: Nondet m => Open [Int -> m Int]
predicatePQ ~[p,q] = [\i -> q (i - 1),
                      \i -> if i <= 0 then
                              return 42
                            else
                              p i
                     ]

-- | The program that executes a call to predicate @q@.
--   This should always eventually return @42@.
theProgram :: Int -> TabledProgram Int Int Int
theProgram i = TP $ declareRec predicatePQ $ \[_,q] ->
                    expression $ q i

-- | Verify the correctness of the result.
prop_result :: Int -> Bool
prop_result i = A.ts (theProgram i) == S.singleton 42
