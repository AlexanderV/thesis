{- | This module contains an implementation for the unbounded knapsack problem.

     Given a set of items @z1,..., zn@, with weights @w1,...,wn@ and values
     @v1,...,vn@, and given a maximum weight @W@, we try to maximize

     > sum [xi * vi | i <- [1..n]] s.t. sum [xi * wi | xi <- [1..n]] <= W
     >                                  xi non-negative for i=1,...,n
     >                                  wi positive for i=1,...,n
     >                                  vi positive for i=1,...,n

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Table.Knapsack (
  Max(..),
  unMax,
  knapsack,
  tabledKnapsackProgram,
  tabledKS,
  tabledKSIter,
  tabledKSAdv
  )
       
where

import Table.Knapsack.Knapsack

import Table as T
import Table.Lattice as L
import Table.Lattice.Advanced as AL
import Table.Lattice.Iter as IL
import Data.Monoid

knapsack :: Nondet m => m Item -> Open (Int -> m Knapsack)
knapsack items kn w
  | w <= 0    = return (Knapsack [])
  | otherwise =
      return (Knapsack [])
      <|>
      do (Item iW iV) <- items
         if unWeight iW > w then T.fail else do
           Knapsack kn' <- kn (w - unWeight iW)
           return (Knapsack (Item iW iV : kn'))

tabledKnapsackProgram ::
  [Item] -> Int -> TabledProgram Int Knapsack Knapsack
tabledKnapsackProgram is w =
  TP $
  declare (knapsack (choose is)) $ \kn ->
  expression (kn w)

newtype Max a = Max (Maybe a) deriving (Eq, Show)

instance Ord a => Monoid (Max a) where
  mempty = Max Nothing
  mappend (Max max1) (Max max2) = case (max1, max2) of
    (Nothing, m) -> Max m
    (m, Nothing) -> Max m
    (Just m1, Just m2) -> Max (Just (max m1 m2))

instance Ord a => Lattice (Max a) where
  Max Nothing =<= _ = True
  _ =<= Max Nothing = False
  Max (Just m1) =<= Max (Just m2) = m1 <= m2
  lub = foldr (<>) mempty
  bottom = mempty

instance Coerce a (Max a) where
  coerce = Max . Just
  uncoerce (Max m) = case m of
    Nothing -> []
    Just a  -> [a]

unMax :: Max Knapsack -> Knapsack
unMax (Max m) = case m of
  Nothing -> Knapsack []
  Just ks -> ks

{- | Run the 'tabledKnapsackProgram' problem with 'L.tl'.

     >>> unMax $ tabledKS 2
     Just (Knapsack{_items = [Item{_weight 2, _value = Value 2}]})

     >>> unMax $ tabledKS 5
     Just (Knapsack{_items = [Item {_weight = Weight 5, _value = Value 20}]})

     >>> unMax $ tabledKS 7
     Just (Knapsack{_items = [Item {_weight = Weight 5, _value = Value 20},
     Item {_weight = Weight 2, _value = Value 2}] })
-}
tabledKS :: [Item] -> Int -> Knapsack
tabledKS is w = unMax $ L.tl $ tabledKnapsackProgram is w

{- | Run the 'tabledKnapsackProgram' problem with 'IL.iterTl'.

     >>> unMax $ tabledKSIter 2
     Just (Knapsack{_items = [Item{_weight 2, _value = Value 2}]})

     >>> unMax $ tabledKSIter 5
     Just (Knapsack{_items = [Item {_weight = Weight 5, _value = Value 20}]})

     >>> unMax $ tabledKSIter 7
     Just (Knapsack{_items = [Item {_weight = Weight 5, _value = Value 20},
     Item {_weight = Weight 2, _value = Value 2}] })
-}
tabledKSIter :: [Item] -> Int -> Knapsack
tabledKSIter is w = unMax $ IL.iterTl $ tabledKnapsackProgram is w

{- | Run the 'tabledKnapsackProgram' problem with 'AL.tl'.

     >>> unMax $ tabledKSAdv 2
     Just (Knapsack{_items = [Item{_weight 2, _value = Value 2}]})

     >>> unMax $ tabledKSAdv 5
     Just (Knapsack{_items = [Item {_weight = Weight 5, _value = Value 20}]})

     >>> unMax $ tabledKSAdv 7
     Just (Knapsack{_items = [Item {_weight = Weight 5, _value = Value 20},
     Item {_weight = Weight 2, _value = Value 2}] })
-}
tabledKSAdv :: [Item] -> Int -> Knapsack
tabledKSAdv is w = unMax $ AL.tl $ tabledKnapsackProgram is w
