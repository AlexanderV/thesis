{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module SubsetSum (sssProg, runSSS) where

import           Open
import           Program
import           Table
import           Table.Advanced
import           Table.Interpreters
import           Table.Lattice
import qualified Table.Lattice.Advanced as AL

import           Data.Monoid
import qualified Data.Set as S
import           Data.Vector.Unboxed ((!?))
import qualified Data.Vector.Unboxed as U
import           Prelude hiding (fail)

newtype Pos = Pos { unPos :: Int } deriving (Eq, Ord, Num)
data Shortest = InfList | Shortest [Int]

instance Show Pos where
  show  = show . unPos

instance Eq Shortest where
  InfList     == InfList     = True
  Shortest a  == Shortest b  = length a == length b
  _           == _           = False


instance Monoid Shortest where
  mempty      = InfList
  mappend a b = if a =<= b then b else a

instance Lattice Shortest where
  InfList    =<= _          = True
  _          =<= InfList    = False
  Shortest a =<= Shortest b = length a >= length b
  lub = foldr mappend mempty

instance Coerce [Int] Shortest where
  coerce = Shortest
  uncoerce InfList      = []
  uncoerce (Shortest s) = [s]

unShortest :: Shortest -> [Int]
unShortest InfList       = []
unShortest (Shortest xs) = xs

subsetsum ::
  (Functor m, Nondet m) => (Pos -> m Int) -> Open ((Pos, Int) -> m [Int])
subsetsum xs sss (i, s) | i < 0  = fail
                        | otherwise = sss (i-1, s)
                                      <|>
                                      (do x <- xs i
                                          if x == s then return [x] else fail)
                                      <|>
                                      (do x <- xs i
                                          xs' <- sss (i-1, s-x)
                                          return (x:xs'))


access :: Nondet m => U.Vector Int -> Pos -> m Int
access v = maybe fail return . (v !?) . unPos

sssProg :: U.Vector Int -> Int -> TabledProgram (Pos, Int) [Int] [Int]
sssProg v s = TP $ declare (subsetsum (access v)) $ \sss ->
                   expression (sss (Pos (U.length v - 1), s))

runSSS :: Int -> [Int] -> S.Set [Int]
runSSS i = ts . runProgram . flip sssProg i . U.fromList

runSSSShortest :: Int -> [Int] -> [Int]
runSSSShortest i = unShortest . AL.tl . runProgram . flip sssProg i . U.fromList
