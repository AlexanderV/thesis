{- | A simple tabling example, taken from
     <http://www3.cs.stonybrook.edu/~warren/xsbbook/node14.html>.

     Given that a person avoids the people to whom he owes money, who does
     that person also avoid (by transitive closure)?

     Author: Alexander Vandenbroucke
-}
module Avoids (Person(..), avoids) where

import Open

import Control.Applicative
import Data.Foldable (asum)
import Prelude hiding (or)

-- | There are only three people in this world: Jeb, Bill and Bob.
--   They mostly try to avoid eachother.
data Person = Jeb | Bill | Bob deriving (Show, Eq)

allPeople :: [Person]
allPeople = [Jeb, Bill, Bob]

-- | The owes relationship.
owes :: Person -> Person -> Bool
owes Jeb Bill = True
owes Bill Bob = True
owes Bob Bill = True
owes _    _   = False

-- 'owesTab p' returns a non-deterministic computation of all the people to whom
-- p owes money.
owesTab :: (Alternative m) => Person -> m Person
owesTab p = asum . map pure . filter (owes p) $ allPeople

-- | A @'Person' p@ avoids the people to whom he owes money directly, and
--   also the people that are avoided by the 'Person's he owes.
avoids :: (Monad m, Alternative m) => Open (Person -> m Person)
avoids super p = owesTab p <|> do intermediary <- owesTab p
                                  super intermediary
