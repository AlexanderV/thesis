{- | This module contains an program to select assembly language instructions
     for a very simple expression language consisting of constants, temporaries,
     memory references (denoted by square brackets) and addition.

     The program uses a dynamic programming approach. Tabling can be used to
     speed up dynamic programming.

     Try running the program on simple expressions with 'runCost' and
     'runInstr'.

     For example:

     >>> runCost matcher "a + b"
     IntCost 1

     Another example:

     >>> runInstr matcher "a + 1 + 2"
     ["","","addi $d, $t, 1","add $d, $t1, $t2"]

     The instruction set format is loosely based on the MIPS instruction set. 

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE DeriveFunctor #-}

module InstructionSelection (
  -- * Datatypes
  Tree,
  Cost,
  Tile,
  Tiling(_tilingCost, _tilingInstr),
  Matcher,
  -- * Instruction selection
  matcher, matcherND, tile,
  -- * Running
  tlInstr, tlCost,
  -- * Parser
  readTree
  )
where

import           Open
import           Table

import           Control.Applicative ((<$>))
import           Data.Char (isSpace, isDigit)
import           Data.Foldable (foldMap)
import           Data.Monoid
import qualified Table.Lattice as L
import           Prelude hiding (fail)

-- | The expression tree type, supporting temporaries, constants,
--   memory references and addition.
data Tree
  = Plus Tree Tree
  | Mem Tree
  | Temp Char
  | Const Int
  deriving (Show, Eq, Ord)

-- | The abstract cost of a Tiling or instruction.
data Cost = InfCost | IntCost !Int deriving (Show, Read, Eq)

-- | Turn an Int into a 'Cost'.
cost :: Int -> Cost
cost i | i < 0     = IntCost 0
       | otherwise = IntCost i

instance Ord Cost where
  _ <= InfCost = True
  InfCost <= _ = False
  IntCost c1 <= IntCost c2 = c1 <= c2

instance Monoid Cost where
  mempty = IntCost 0
  InfCost `mappend` _ = InfCost
  _ `mappend` InfCost = InfCost
  IntCost c1 `mappend` IntCost c2 = IntCost (c1 + c2)

-- | A single instruction tile.
data Tile = Tile {
  _tileCost  :: Cost,
  _tileLeafs :: [Tree],
  _tileInstr :: String
  } deriving (Show, Eq, Ord)

-- | A tiling of the expression tree.
--   It consists of a total cost and a list of instructions.
data Tiling = Tiling {
  _tilingCost  :: Cost,
  _tilingInstr :: [String]
  } deriving (Show, Read)

instance Eq Tiling where
  t1 == t2 = _tilingCost t1 == _tilingCost t2

instance Ord Tiling where
  t1 <= t2 = _tilingCost t1 <= _tilingCost t2

instance Monoid Tiling where
  mempty = Tiling InfCost []
  Tiling InfCost _ `mappend` t = t
  t `mappend` Tiling InfCost _ = t
  t1 `mappend` t2 = min t1 t2

instance L.Lattice Tiling where
  (=<=)  = flip (<=)
  lub    = foldr (<>) L.bottom
  bottom = Tiling InfCost []

instance L.Coerce Tiling Tiling where
  coerce     = id
  uncoerce t = [t]

instance L.Coerce Cost Tiling where
  coerce     = flip Tiling []
  uncoerce t = [_tilingCost t]


--------------------------------------------------------------------------------
-- Minimum and Maximum
  
-- | A Maximum monoid wrapper.
newtype Max a = Max (Maybe a) deriving (Show, Functor, Eq)
-- | A Minimum monoid wrapper
newtype Min a = Min (Maybe a) deriving (Show, Functor, Eq)

-- | Unwrap 'Max'.
unMax :: Max a -> Maybe a
unMax (Max m) = m

-- | Unwrap 'Min'
unMin :: Min a -> Maybe a
unMin (Min m) = m

instance Ord a => Ord (Max a) where
  Max Nothing <= _ = True
  _ <= Max Nothing = False
  Max (Just m1) <= Max (Just m2) = m1 <= m2

instance Ord a => Ord (Min a) where
  Min Nothing <= _ = True
  _ <= Min Nothing = False
  Min (Just m1) <= Min (Just m2) = m2 <= m1

-- | Appending two 'Max'es results in the 'Max' containing the largest value.
instance Ord a => Monoid (Max a) where
  mempty = Max Nothing
  mappend (Max a1) (Max a2) = Max (max a1 a2)

-- | Appending two 'Min's results in the 'Min' containing the smallest value.
instance Ord a => Monoid (Min a) where
  mempty = Min Nothing
  mappend = max

instance Ord a => L.Lattice (Max a) where
  Max Nothing =<= _ = True
  _ =<= Max Nothing = False
  Max (Just m1) =<= Max (Just m2) = m1 <= m2
  lub = foldr (<>) L.bottom
  bottom = Max Nothing

instance Ord a => L.Lattice (Min a) where
  (=<=) = (<=)
  lub = foldr (<>) L.bottom
  bottom = Min Nothing

instance L.Coerce a (Max a) where
  coerce                  = Max . Just
  uncoerce (Max Nothing)  = []
  uncoerce (Max (Just a)) = [a]

instance L.Coerce a (Min a) where
  coerce                  = Min . Just
  uncoerce (Min Nothing)  = []
  uncoerce (Min (Just a)) = [a]

-- | Type synonym for a function that matches tiles to expression trees.
type Matcher = forall m . Nondet m => Tree -> m Tile


-- | The default matching function.
matcher :: Matcher
matcher tree = return $ case tree of
  Temp _           -> Tile (cost 0) []  []                          -- trivial
  Const c          -> Tile (cost 1) []  ("addi $d, $0, " ++ show c) -- addi
  Plus (Const c) t -> Tile (cost 1) [t] ("addi $d, $t, " ++ show c) -- addi
  Plus t (Const c) -> Tile (cost 1) [t] ("addi $d, $t, " ++ show c) -- addi
  Plus t1 t2       -> Tile (cost 1) [t1, t2] "add $d, $t1, $t2"     -- add
  Mem (Plus (Const c) t) -> Tile (cost 1) [t] ("lw $d, $t + " ++ show c) -- lw
  Mem (Plus t (Const c)) -> Tile (cost 1) [t] ("lw $d, $t + " ++ show c) -- lw
  Mem t                  -> Tile (cost 1) [t]  "lw $d, $t + 0"           -- lw

-- | The default matching function, non-deterministically.
matcherND :: Matcher
matcherND tree =
  case tree of
   Temp _ -> return $ Tile (cost 0) []  [] -- trivial
   _      -> fail
  <|>
  case tree of
   Const c -> return $ Tile (cost 1) []  ("addi $d, $0, " ++ show c)
   _       -> fail
  <|>
  case tree of
   Plus (Const c) t -> return $ Tile (cost 1) [t] ("addi $d, $t, " ++ show c)
   _                -> fail
  <|> 
   case tree of
    Plus t (Const c) -> return $ Tile (cost 1) [t] ("addi $d, $t, " ++ show c)
    _                -> fail
  <|> 
  case tree of
   Plus t1 t2 -> return $ Tile (cost 1) [t1, t2] "add $d, $t1, $t2"
   _          -> fail
  <|>
  case tree of 
   Mem (Plus (Const c) t) ->
     return $ Tile (cost 1) [t] ("lw $d, $t + " ++ show c)
   _ -> fail
  <|>
  case tree of 
   Mem (Plus t (Const c)) ->
     return $ Tile (cost 1) [t] ("lw $d, $t + " ++ show c)
   _ -> fail
  <|>
  case tree of 
   Mem t -> return $ Tile (cost 1) [t] "lw $d, $t + 0"
   _     -> fail
  

-- | Construct a tiling from a selected tile, and the tilings for the leafs.
tiling :: Tile -> [Tiling] -> Tiling
tiling t tilings =
  let newCost  = _tileCost t <> foldMap _tilingCost tilings
      newInstr = foldMap _tilingInstr tilings <> [_tileInstr t]
  in Tiling newCost newInstr

-- | Find a tiling for an expression tree, given a matcher.
tile :: Nondet m => Matcher -> Open (Tree -> m Tiling)
tile match super node = do
  nodeTile   <- match node
  subtilings <- mapM super (_tileLeafs nodeTile)
  return $ tiling nodeTile subtilings

--------------------------------------------------------------------------------
-- Program

-- | Compute the cost of the tiling computed by a given 'Matcher' for an
--   expression tree corresponding to the input string.
isCost :: Matcher -> String -> TabledProgram Tree Tiling Cost
isCost m str =
  TP $
  declare (tile m) $ \tiler ->
  expression $ case fst (readTree str) of
                Nothing   -> return InfCost
                Just tree -> _tilingCost <$> tiler tree

-- | Find the matching for an expression tree corresponding to the input string
--   using a given 'Matcher'
isInstr :: Matcher -> String -> TabledProgram Tree Tiling Tiling
isInstr m str = TP $ declare (tile m) $ \tiler ->
  expression $ case fst (readTree str) of
                Nothing   -> return (Tiling InfCost [])
                Just tree -> tiler tree

tlCost :: Matcher -> String -> Cost
tlCost m input = _tilingCost $ L.tl $ isInstr m input
  
tlInstr :: Matcher -> String -> [String]
tlInstr m input = _tilingInstr $ L.tl $ isInstr m input

--------------------------------------------------------------------------------
-- Parser

{- | A recursive-descent parser for expressions characterized by the following
     context free grammar:
     
     > E  -> (E) E'       -- parentheses
     > E  -> [E] E'       -- memory reference
     > E  -> I E'         -- constant expression
     > E  -> T E'         -- temporary expression
     > E' -> epsilon      -- empty expression continuation
     > E' -> + E          -- addition expression continuation
     > T  -> noneOf ['+'] -- temporaries
     > I  -> anyOf [0..9] -- constants

-}
readTree :: String -> (Maybe Tree, String)
readTree input = prod e input where
  -- * Utilities
  trim :: String -> String
  trim = dropWhile isSpace
  
  prod :: (Char -> String -> (Maybe a, String)) -> String -> (Maybe a, String)
  prod p str = case trim str of
    []     -> (Nothing, [])
    (c:cs) -> p c cs
    
  eat :: Char -> Maybe a -> String -> (Maybe a, String)
  eat a v str = case trim str of
    []     -> (Nothing, [])
    (c:cs) -> if c == a then (v, cs) else (Nothing, str)
    
  -- * Productions
  -- ** T
  t c cs = (Just (Temp c), cs)

  -- ** I
  i c cs = (Just (Const (read [c])), cs)
  
  -- ** E
  e c cs = case c of
    '(' -> case uncurry (eat ')') $ prod e cs of
            (Nothing, cs')   -> (Nothing, cs')
            (Just tree, cs') ->  e' tree cs'
    '[' -> case uncurry (eat ']') $ prod e cs of
            (Nothing, cs') -> (Nothing, cs')
            (Just tree, cs') -> e' (Mem tree) cs'
    _   -> if isDigit c then
             case prod i (c:cs) of
              (Nothing, cs')  -> (Nothing, cs')
              (Just cst, cs') -> e' cst cs'
           else
             case prod t (c:cs) of
              (Nothing, cs')   -> (Nothing, cs')
              (Just temp, cs') -> e' temp cs'
            
  -- ** E'
  e' temp str = case trim str of
    ('+':cs) -> case prod e cs of
                 (Nothing, cs')   -> (Nothing, cs')
                 (Just tree, cs') -> (Just (Plus temp tree), cs')
    _        -> (Just temp, str)
