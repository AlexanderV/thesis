{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module MinTest (run) where

import Table.Lattice hiding (tl)
import Table.Lattice.Advanced (tl)
import Table.Program
import Nondet

newtype Min = MkMin { unMin :: Int } deriving (Num,Show,Eq,Ord)

instance Coerce Min Min where
  coerce = id
  uncoerce x = [x]

instance Monoid Min where
  mempty = bottom
  mappend x y = lub [x,y]

instance Lattice Min where
  (=<=)  = (>=)
  bottom = 10
  lub [] = 10
  lub xs = minimum xs

p :: Nondet m => (Min -> m Min) -> Min -> m Min
p p' i | i == 2 = return 1 <|> p' 3
       | i == 3 = p' 2

program :: Min -> TabledProgram Min Min Min
program x = TP $ declare p $ \p' -> expression (p' x)

run :: Int -> Int
run = unMin . tl . program . MkMin
