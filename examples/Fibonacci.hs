{- | The classical Fibonacci-sequence example.
     <http://en.wikipedia.org/wiki/Fibonacci_number>

     The Fibonacci sequence can be defined inductively:

     @
     F[O] = 0
     F[1] = 1
     F[i] = F[i-1] + F[i - 2] for i = 2,3, ...
     @

     This is a deterministic computation, so tabling on 'fibonacci' reduces
     to momoization.

     To spice things up a little we introduce a non-deterministic variant
     'fibonacciND' that can return multiple results.

     To verify correctness of a tabling implementation with respect to
     'fibonacci' and 'fibonacciND', a number of testing properties are provided:
     'prop_correct', 'prop_memoizeCorrect', 'prop_memoizePerf',
     'prop_correctND', 'prop_memoizeCorrectND' and 'prop_memoizePerfND'.

     Author: Alexander Vandenbroucke
-}
module Fibonacci (
  -- * Reference implementations
  fibs, fibsND,
  -- * Tabled implementations
  fib, fibND,
  fibonacci, fibonacciND,
  -- * Properties
  allCheck,
  prop_correct,
  prop_correctND,
  )
where

  
import Open
import Table
import Table.Formal
import qualified Table.Advanced as A

import Control.Applicative hiding ((<|>))
import qualified Data.Set as S
import Test.QuickCheck

-- | The fibonacci sequence, implemented as an open recursive function that
--   returns its value in an 'Applicative'.
fib :: Applicative m => Open (Int -> m Int)
fib super i | i == 0 = pure 0
            | i == 1 = pure 1
            | otherwise = (+) <$> super (i - 1) <*> super (i - 2)

{- | The non-deterministic fibonacci sequence, implemented as an open recursive
     function that returns its value in a 'Nondet'.

     The sequence is defined as

     @
     FND[0] = 0
     FND[1] = -1 or 1
     FND[i] = F[i-1] + F[i-2]
     @

     Notice how FND[1] and FND[2] will have two solutions, FND[3] will have
     three distinct solutions, and so on.
-}
fibND :: (Applicative m, Nondet m) => Open (Int -> m Int)
fibND super i | i == 0 = pure 0
              | i == 1 = pure (-1) <|> pure 1
              | otherwise = (+) <$> super (i - 1) <*> super (i - 2)

-- | The tabled version of 'fib'.
fibonacci :: Int -> TabledProgram Int Int Int
fibonacci a = TP $ declare fib (\p -> expression $ p a)

-- | The tabled version of 'fibND'
fibonacciND :: Int -> TabledProgram Int Int Int
fibonacciND a = TP $ declare fibND (\p -> expression $ p a)

--------------------------------------------------------------------------------
-- Tests

-- | Reference fibonacci sequence implementation.
fibs :: [Int]
fibs = 0:1:zipWith (+) fibs (tail fibs)

-- | Reference non-deterministic fibonacci sequence implementation.
fibsND :: Int -> [Int]
fibsND 0 = [0]
fibsND 1 = [-1, 1]
fibsND i = do x <- fibsND (i-1)
              y <- fibsND (i-2)
              return (x + y)

-- | Verify that the tabled fibonacci Program gives the right answers
--   for values in [0,49] using the advanced syntax
prop_correct :: Int -> Property
prop_correct x =
  (x >= 0 && x < 50) ==>
  [fibs !! x] == S.toList (A.ts (fibonacci x))

-- | Verify that the tabled fibonacciND program gives the right answers
--   for x in [0,7]
prop_correctND :: Int -> Property
prop_correctND x =
  (x >= 0 && x < 8) ==>
  S.fromList (fibsND x) ==  (A.ts (fibonacciND x))


     

-- | Check all properties.
allCheck :: IO ()
allCheck = do quickCheck prop_correct
              quickCheck prop_correctND
          
