{-# LANGUAGE RankNTypes #-}

{- | This module finds strongly connected components in a graph.
     Inspired by <http://www3.cs.stonybrook.edu/~warren/xsbbook/node18.html>.

     In a graph @G = (V,E)@ where @V@ is the set of vertices of the graph, and
     @E@ the set of edges, a strongly connected component (SCC) @S@ is a
     set of vertices such that for every two vertices @v,w@ in @S@ there exists
     a path from @v@ to @w@ in @G@.


     Author: Alexander Vandenbroucke
-}

module SameSCC (
  -- * Implementation
  Node,
  edges,
  sameSCC,
  sameSCCProgram,
  -- * Properties
  checkCorrect, checkFormal, checkFormalIter
  )
where

import           Open
import           Table
import qualified Table.Formal as F
import qualified Table.Advanced as A

import qualified Data.Set as S
import Prelude hiding (fail)

-- | A 'Node' is represented by its label.
type Node = String

-- | The edges in the example graph.
edges :: [(Node, Node)]
edges = [("A", "B"), ("B", "C"), ("C", "B")]

{- | Forward edges from a node. 'edgesFor x' gives a non-deterministic
     computation that returns all nodes which are reachable from 'x' by a single
     edge. Currently the representation of the edge list implies a linear search
     through all edges  - O(|E|).
-}
edgesFor :: Nondet m => Node -> m Node
edgesFor x =
  foldr (<|>) fail $ map (return . snd) $ filter ((==x) . fst) $ edges

{- | Backward edges from a node. 'edgesBack x' gives a non-deterministic
     computation that returns all nodes from which 'x' is reachable by a single
     edge. Currently the representation of the edge list implies a linear search
     through all edges - O(|E|).
-}
edgesBack :: Nondet m => Node -> m Node
edgesBack x =
  foldr (<|>) fail $ map (return . fst) $ filter ((==x) . snd) $ edges

-- | 'reachFor super x' returns a non-deterministic computation of all nodes
--    that are reachable from 'x' in the graph.
reachFor :: Nondet m => Open (Node -> m Node)
reachFor super x = return x <|> do y <- edgesFor x
                                   super y

-- | 'reachBack super (x,y)' returns a non-deterministic computation that
--    succeeds only if 'y' is reachable from 'x' in the complementary graph.
reachBack :: Nondet m => Open (Node -> m Node)
reachBack super y = return y <|> do x <- edgesBack y
                                    super x

{- | @'sameSCC' reachF reachB x@ gives a non-deterministic computation of all
     nodes in the same SCC as @x@, given that reachF and reachB compute
     forward and backward reachability respectively.
-}
sameSCC :: Nondet m => (Node -> m Node) -> (Node -> m Node) -> Node -> m Node
sameSCC reachForP reachBackP x = do
  y  <- reachForP  x
  y' <- reachBackP x
  if y == y' then return y else fail

-- | A 'TabledProgram' that computes all the nodes that belong to the same SCC
--   as a given 'Node'.
sameSCCProgram :: Node -> TabledProgram Node Node Node
sameSCCProgram x = TP $
  declare reachFor  $ \rf ->
  declare reachBack $ \rb ->
  expression $ sameSCC rf rb x

--------------------------------------------------------------------------------
-- Properties

-- | Check that the program returns the expected result, when using
--   Advanced Tabling.
checkCorrect :: Bool
checkCorrect = do
  let computedSCC =
        map (S.toList . A.ts . sameSCCProgram) ["A","B","C"]
  let sccs = [["A"], ["B","C"], ["B", "C"]]
  and $ zipWith (==) computedSCC sccs

-- | Check that the program exhibits the expected result under formal semantics.
checkFormal :: Bool
checkFormal = do
  let computedSCC =
        map (F.tableSemantics . sameSCCProgram) ["A","B","C"]
  let sccs = map S.fromList [["A"], ["B","C"], ["B", "C"]]
  and $ zipWith (==) computedSCC sccs

-- | Check that the program exhibits the expected result under formal semantics.
checkFormalIter :: Bool
checkFormalIter = do
  let computedSCC =
        map (F.iterTableSemantics . sameSCCProgram) ["A","B","C"]
  let sccs = map S.fromList [["A"], ["B","C"], ["B", "C"]]
  and $ zipWith (==) computedSCC sccs
