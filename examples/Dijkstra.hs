{- | This module provides an example of how Tabling (with monoids) might be
     used to implement a simple depth first search over a graph.

     A directed graph is a recursive structure of 'Node's.
     As such, the entire graph can be represented by a single node.
     An example graph is included, consisting of 5 nodes: @a@, @b@, @c@, @d@,
     @e@.

     A 'Distance' is a non-negative integer or positive infinity.
     A 'Distance' from 'Node' @v@ to 'Node' @w@ is infinite if @w@ cannot
     be reached from @v@, i.e. there does not exist a path from @v@ to @w@.

     The depth first search is implemented in 'distanceTo'.
     Convenience functions for wrapping 'distanceTo' in a 'TabledProgram'
     ('distanceToProg'), and for execution ('runDistanceToProg',
     'runIterDistanceToProg', 'tlDistanceToProg' and 'iterTlDistanceToProg') are
     provided.

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE MultiParamTypeClasses #-}

module Dijkstra (
  -- * Datatype definitions
  Node(..),
  Distance(..),
  distance,
  -- * Example graph
  -- $example
  a, b ,c, d, e,
  -- * Algorithm
  distanceTo,
  distanceToProg,
  tlDistanceToProg,
  iterTlDistanceToProg,
  advTlDistanceToProg
  )
where

import           Open
import           Table
import qualified Table.Lattice as L
import qualified Table.Lattice.Advanced as A
import qualified Table.Lattice.Iter as IL

import           Data.Monoid
import           Prelude hiding (fail)

{- | 'Node' is a datatype representing a node in a graph.
     A recursive graph can be constructed by recursively including nodes in
    the adjacency list.
-}
data Node = Node {
  -- | The label.
  _label :: Char,
  -- | The adjacency list.
  _adjList :: [(Node, Distance)]
  }

instance Eq Node where
  Node l1 _ == Node l2 _ = l1 == l2

instance Ord Node where
  Node l1 _ <= Node l2 _ = l1 <= l2

instance Show Node where
  show (Node l _) = "(Node " ++ show l ++ ")"

-- | 'Distance' must be either positive inifity ('InfDistance') or
--   a non-negative integer ('Distance').
data Distance = InfDistance | Distance Int deriving (Show, Eq)

-- | 'distance' is a smart constructor for a non-negative 'Distance'.
distance :: Int -> Distance
distance i | i >= 0 = Distance i
           | otherwise = error "Distance must be non-negative."

instance Ord Distance where
  _ <= InfDistance = True
  InfDistance <= _ = False
  Distance d1 <= Distance d2 = d1 <= d2
  
instance Num Distance where
  d1 + d2 = case (d1, d2) of
    (InfDistance,_) -> InfDistance
    (_,InfDistance) -> InfDistance
    (Distance d1', Distance d2') -> Distance (d1' + d2') -- safe : d1 + d2 >= 0
  d1 - d2 = case (d1,d2) of
    (InfDistance, _) -> InfDistance
    (_, InfDistance) -> InfDistance
    (Distance d1', Distance d2') -> distance (d1' - d2')
  d1 * d2 = case (d1, d2) of
    (InfDistance,_) -> InfDistance
    (_,InfDistance) -> InfDistance
    (Distance d1', Distance d2') -> Distance (d1' * d2') -- safe : d1 + d2 >= 0
  abs w = w
  signum InfDistance = 1
  signum (Distance d0) | d0 == 0   = 0
                       | otherwise = 1
  fromInteger = distance . fromInteger

instance Monoid Distance where
  mempty = InfDistance
  d1 `mappend` d2 = min d1 d2

instance L.Coerce Distance Distance where
  coerce   x           = x
  uncoerce InfDistance = []
  uncoerce x           = [x]

instance L.Lattice Distance where
  (=<=)  = flip (<=)
  lub    = foldr (<>) InfDistance
  bottom = InfDistance

{- $example
  The structure of the example graph is, in beautiful ASCII:

  > |---
  > v  |
  > e --
  > ^ 
  > | 
  > a --> b
  > ^-    |
  > | \-  v
  > d <-> c

-}

a, b, c, d, e :: Node
[a, b, c, d, e] = [Node 'a' [(b, 1), (e,1)], 
                   Node 'b' [(c, 1)       ],
                   Node 'c' [(a,22), (d,1)],
                   Node 'd' [(c,1),  (a,1)],
                   Node 'e' [(e,1)        ]]


-- | This predicate computes the distance to a node identified by its 'Char'
--   label, from a 'Node' that is given as the argument.
distanceTo :: Nondet m => Char -> Open (Node -> m Distance)
distanceTo l super n0
  | l == _label n0 = return 0
  | otherwise = do
      (n, d0) <- foldr (<|>) fail $ map return $ _adjList n0
      dn <- super n
      return (dn + d0)

-- | A 'TabledProgram' that uses 'distanceTo'. 
distanceToProg :: Char -> Node -> TabledProgram Node Distance Distance
distanceToProg l n = TP $
  declare (distanceTo l) $ \dTo ->
  expression $ dTo n

{- | Run the 'distanceToProg' program with 'L.tl'.

     Expected results:

     >>> tlDistanceToProg 'a' a
     Distance 0

     >>> tlDistanceToProg 'a' b
     Distance 3

     >>> tlDistanceToProg 'a' c
     Distance 2

     >>> tlDistanceToProg 'a' d
     Distance 1

     >>> tlDistanceToProg 'a' e
     InfDistance
-}
tlDistanceToProg :: Char -> Node -> Distance
tlDistanceToProg l n = L.tl (distanceToProg l n)

{- | Run the 'distanceToProg' program with 'IL.iterTl'.

     Expected results:

     >>> iterTlDistanceToProg 'a' a
     Distance 0

     >>> iterTlDistanceToProg 'a' b
     Distance 3

     >>> iterTlDistanceToProg 'a' c
     Distance 2

     >>> iterTlDistanceToProg 'a' d
     Distance 1

     >>> iterTlDistanceToProg 'a' e
     InfDistance
-}
iterTlDistanceToProg :: Char -> Node -> Distance
iterTlDistanceToProg l n = IL.iterTl (distanceToProg l n)

{- | Run the 'distanceToProg' program with 'A.tl'.

     Expected results:

     >>> iterTlDistanceToProg 'a' a
     Distance 0

     >>> iterTlDistanceToProg 'a' b
     Distance 3

     >>> iterTlDistanceToProg 'a' c
     Distance 2

     >>> iterTlDistanceToProg 'a' d
     Distance 1

     >>> iterTlDistanceToProg 'a' e
     InfDistance
-}
advTlDistanceToProg :: Char -> Node -> Distance
advTlDistanceToProg l n = A.tl (distanceToProg l n)
