{- | This module implements an example of a non-deterministic computation.

     A 'Person' is always of the same generation as him or herself.
     Two 'Person's are of the same generation if their parents are of the same
     generation.

     This is implemented in the 'sameGeneration' function.

     Author: Alexander Vandenbroucke
-}

module SameGeneration (
  -- * Type Synonyms
  Person,
  -- * Driver Function
  sameGeneration
)
where

import           Open
  
import           Control.Applicative
import           Prelude hiding (or, fail)


-- | A person is represented by his or her name.
type Person = String

{- | Finds all 'Person's that share the same generation as the argument.
     This is an 'Open' recursive function.
-}
sameGeneration :: (Monad m, Alternative m) => Open (Person -> m Person)
sameGeneration super p = return p <|> do z1 <- up p
                                         z2 <- super z1
                                         down z2

-- LOTR family tree courtesy of http://lotrproject.com/
up :: Alternative m => Person -> m Person
up "Meriadoc Brandybuck" = pure "Esmeralda Took" <|> pure "Saradoc Brandybuck"
up "Esmeralda Took"      = pure "Aldagrim Took"
up "Peregrin Took I"     = pure "Paladin Took II" <|> pure "Eglantine Banks"
up "Paladin Took II"     = pure "Aldagrim Took"
up _ = empty

down :: Alternative m => Person -> m Person
down "Aldagrim Took"      = pure "Paladin Took II" <|> pure "Esmeralda Took"
down "Paladin Took II"    = pure "Peregrin Took I"
down "Esmeralda Took"     = pure "Meriadoc Brandybuck"
down "Eglantine Banks"    = pure "Peregrin Took I"
down "Saradoc Brandybuck" = pure "Meriadoc Brandybuck"
down _ = empty
