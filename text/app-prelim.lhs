\chapter{Bewijzen van Monad morhpisms}
\label{app:proofmonadmorph}
\begin{proof}[|solutions| is een Monad-morphism]
\textbf{Dit bewijs is nog niet af.}
\begin{enumerate}
\item \texttt{solutions} moet |return| behouden:
  $solutions\,(return\,x) = return\,x$
\[
  |solutions (return x) = solutions (Pure x) = [x] = return x|
\]

\item \texttt{solutions} moet bind behouden:\\
  |solutions (m >>= f) = solutions m >>= (solutions . f)|
  \begin{align*}
    |solutions (Pure x >>= f)| &= |solutions (f x)| \\
    &= |(solutions . f) x|\\
    &= |[x] >>= (solutions . f)|\\
    &= |solutions (Pure x) >>= (solutions . f)|\\
  \end{align*}

  \begin{align*}
    |solutions (Fail >>= f)| &= |solutions Fail|\\
    &= |[]|\\
    &= |[] >>= (solutions . f)|\\
    &= |solutions Fail >>= (solutions .f)|\\
  \end{align*}

  \begin{align*}
    |solutions (Or a b >>= f)| &= |solutions (Or (a >>= f) (b >>= f))|\\
    &= |solutions (a >>= f) ++ solutions (b >>= f)|\\
    &= |solutions a >>= (f . solutions) ++ solutions b >>= (f . solutions)|\\
    &= |solutions a ++ solutions b >>= (f . solutions)|\\
    &= |solutions (Or a b) >>= (f . solutions)|
  \end{align*}
\end{enumerate}
\end{proof}


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
