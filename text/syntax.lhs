\chapter{Syntax voor Tabulatie in Haskell}
\label{chap:syntax}

%if False

> {-# LANGUAGE RankNTypes #-}
> import Prelude hiding (fail)
> type Open s = s -> s

%endif

De ``Effect Handlers'' aanpak splitst het modelleren van tabulatie op in twee
delen: syntax en effect handlers.
De syntax beschrijft de acties waaruit een getabled programma bestaat. De effect
handlers interpreteren deze acties en verlenen zo semantiek aan het programma.
Het is van groot belang dat de syntax voldoende algemeen of abstract is,
zodat een grote vari\"eteit aan handlers mogelijk is.

Dit hoofdstuk focust zich op de syntax van tabulatie.
De syntax wordt gespecificeerd in de vorm van Haskell algebraisch datatype
declaraties (\texttt{Table} en \texttt{Program}), gekoppeld met ``slimme''
constructoren die het opbouwen van programma's vereenvoudigen.

De syntax moet toelaten om het volgende Prolog programma te vertalen in Haskell:
\begin{lstlisting}
:- table p/2.

p(1,2).
p(Y,X) :- p(X,Y).
p(X,Y) :- fail.

?- p(A,B).
\end{lstlisting}

Hierin kunnen we twee verschillende doelen onderscheiden: enerzijds is er het
niet-determinisme en faling uit normale Prolog, anderzijds is er de 
tabulatie-directive en de getablede goal (\texttt{?- p(A,B).}) waar de
berekening uit start.

\section{\texttt{Backtrack}: Gewone backtracking}

Niet-determinisme en faling vormen samen een backtrackende berekening.
Het volgende datatype beschrijft een gewone backtrackende berekening:

> data Backtrack a  =  BOr (Backtrack a) (Backtrack a)
>                   |  BFail
>                   |  BPure a

|BPure| a is een succesvolle berekening met resultaat a, |BFail| is
een mislukte berekening, en |BOr| geeft een niet-deterministische keuze aan.

Gewone Prolog programma's kunnen met \texttt{Backtrack} nageboodst worden:

\begin{lstlisting}
p(1,2).
p(X,Y) :- fail.
p(Y,X) :- p(X,Y).
\end{lstlisting}

In haskell wordt dit:

> p :: () -> Backtrack (Int, Int)
> p () = (BPure (1,2)) `BOr` (BFail) `BOr` (do  (x,y) <- p ()
>                                               BPure (y,x))

Als voorbereiding op de volgende sectie kan \texttt{p} geschreven worden op
een open-recursieve manier:

> pOpen :: Open (() -> Backtrack (Int, Int))
> pOpen p () = (BPure (1,2)) `BOr` (BFail) `BOr` (do  (x,y) <- p ()
>                                                     BPure (y,x))

\section{\texttt{Nondet}: Abstractie over backtracking}
Een belangrijk voordeel van \texttt{Backtrack} is dat hiervoor een correcte
\texttt{Monad} instance kan worden gedefinieerd.
Dit laat toe om deze niet-deterministische berekeningen op een elegante manier
samen te stellen, zoals uit latere voorbeelden blijkt.

> instance Monad Backtrack where
>   return x = BPure x
>   BPure x        >>= f  = f x
>   BFail          >>= f  = BFail
>   (b1 `BOr` b2)  >>= f  = (b1 >>= f) `BOr` (b2 >>= f)

De \texttt{Nondet}-typeclass maakt abstractie van \texttt{Backtrack}.

> class Monad m => Nondet m where
>   fail  :: m a
>   (<|>) :: m a -> m a -> m a
> instance Nondet Backtrack where
>   fail     = BFail
>   a <|> b  = a `BOr` b

Zoals eerder gezegd abstraheert deze typeclass backtrackende monads. 

De \texttt{fail} functie en de operator |<||>| uit de
\texttt{Nondet} typeclass doen hier dienst als constructors voor
\texttt{Fail} en \texttt{Or}.
De \texttt{return} uit \texttt{Monad} vervult dezelfde rol voor \texttt{Pure}.

Het voorbeeldje, geschreven met \texttt{Nondet} is nu:

> pNondet :: Nondet m => Open (() -> m (Int, Int))
> pNondet p () = return (1,2) <|> fail <|> do  (x,y) <- p ()
>                                              return (y,x)

\section{\texttt{Program}: Een getabled programma}
Een getabled programma bestaat uit een reeks
predicaatdeclaraties (\texttt{p/2}), eventueel aangeduid met een
\texttt{table}-directive (\texttt{:- table p/2}),
gevolgd door een goal (\texttt{?- p(A,B)}) die van deze predicaten gebruik
maakt:

\begin{lstlisting}
:- table p/2.

p(1,2).
p(Y,X) :- p(X,Y).
p(X,Y) :- fail.

?- p(A,B).
\end{lstlisting}

De volgende syntax maakt dezelfde structuur mogelijk in Haskell:

> data  Program b c m a
>      =   Decl (Open [b -> m c]) ([b -> m c] -> Program b c m a)
>      |   Expr (m a)

\texttt{Program b c m a} heeft 4 typevariabelen:
\texttt{b} is het type van het argument van een predicaat, 
\texttt{c} het type van het resultaat,
\texttt{m} het type van de monad waaruit expressies bestaan, en
\texttt{a} is het type van de uiteindelijke waarde van het getabled programma.

Een \texttt{Program} kan enerzijds een declaratie \texttt{Decl} zijn, waarop
nog declaraties of een expressie kunnen volgen. Anderzijds kan het een
expressie \texttt{Expr} zijn die een berekening van het \texttt{m a} omvat.

Een declaratie komt overeen met de table-directive en een expressie komt
overeen met de goal.

In het geval van \texttt{Decl} is het eerste argument een lijst van
\emph{bodies} van de predicaten.
Dit is een open lijst van wederzijds recursieve functies \cite{jfp_mri}.
Het tweede argument is het vervolg van het programma, verborgen achter een
functie die een lijst predicaten (type |[b -> m c]|) als argument verwacht.

%TODO change this
De slimme constructoren \texttt{declareRec} en \texttt{expression} zijn hier
slechts synoniemen voor \texttt{Decl} en \texttt{Expr}.

> declareRec   = Decl
> expression   = Expr

De constructor \texttt{declare} declareert een enkel predicaat door de body
te verpakken in een lijst met \'e\'en element.

> declare ::  Open (b -> m c)
>             -> ((b-> m c) -> Program b c m a)
>             -> Program b c m a
> declare p c  = Decl (singleton . p . head) (c . head) where singleton x = [x]

Nu kan het volledige Tabled-Prolog voorbeeldje in Haskell worden uitgedrukt:

> program :: Nondet m => Program () (Int, Int) m (Int, Int)
> program = declare  pNondet                    -- :- table p/2.  in Prolog
>                    (\p -> expression (p ()))  -- ?- p(A,B).     in Prolog

Zonder de externe definitie van \texttt{openP} kan ook:

> program2 :: Nondet m => Program () (Int, Int) m (Int, Int)
> program2 = declare  (\p () -> return (1,2) <|> fail <|> do  (x,y) <- p ()
>                                                             return (y,x))
>                     (\p -> expression (p ()))

Het volgende (pseudo-Haskell) voorbeeldje demonstreert hoe
\texttt{declareRec} een aantal wederzijds recursieve predicaten \texttt{p} en
\texttt{q} definieerd.

< programRec = 
<  declareRec  (\ ~[p, q] ->  [  \x -> q (x - 1),                 -- body of p
<                                \x -> if x <= 0 then x else p x  -- body of q
<                             ])
<              (\[p, q] -> ...)

De lazyness annotatie(|~|) in |\ ~[p,q]| is noodzakelijk, omdat de bodies (van
de predicaten \texttt{p} en \texttt{q}) anders onnodig strikt zouden zijn, wat
tot problemen kan leiden wanneer de syntax uitgevoerd moet worden.

Concreet wil dit zeggen dat het argument |[p,q]| pas ge\"evalueerd wordt op het
moment dat \texttt{p} en \texttt{q} echt nodig zijn.
Dit wil zeggen dat in het basisgeval van de recursie het argument niet meer
ge\"evalueerd moeten worden, waardoor de recursie ook effectief stopt.

De \texttt{Program}-syntax zoals deze hier is voorgesteld heeft twee belangrijke
beperkingen. 
Ten eerste, alle predicaten moeten hetzelfde argumentstype (\texttt{b}) en
resultaatstype(\texttt{c}) hebben. 
Ten tweede moeten alle predicaten gedeclareerd zijn voor de uiteindelijke
\texttt{expression}.


\section{\texttt{TabledProgram}: Polymorfisme afdwingen}
Het eerdere programmavoorbeeldje \texttt{program} had het type
|Program () (Int, Int) m (Int, Int)|.
Deze code is dus volledig polymorf in de parameter \texttt{m} (het type
van de monad waaruit expressies bestaan).
Dit is mogelijk omdat \texttt{Nondet} toelaat om alle nodige operaties 
(succesvolle berekening, faling en keuze) onafhankelijk van \texttt{m} te
schrijven.

Met behulp van een \texttt{newtype}-constructie kan het typesysteem deze
beperking afdwingen\footnote{Met de GHC extensie \texttt{RankNTypes}.}:

> newtype TabledProgram b c a = TP {
>   unTP :: forall m . Nondet m => Program b c m a
> }

Wanneer de programmeur moet werken binnen \texttt{TabledProgram}, dan beschikt
hij of zij alleen over de constructies van \texttt{Program} (\texttt{declare}, 
\texttt{declareRec}, \texttt{expression}), \texttt{Nondet} (\texttt{fail} en
|<||>|) en \texttt{Monad} (\texttt{return} en |>>=|).

> programTP :: TabledProgram () (Int, Int) (Int, Int)
> programTP = TP $ 
>   declare  (\p () -> return (1,2) <|> fail <|> do  (x,y) <- p ()
>                                                    return (y,x))
>            (\p -> expression (p ()))

Dit verhindert de programmeur natuurlijk niet om buiten \texttt{TabledProgram}
te werken, bijvoorbeeld wanneer er naast de operaties van \texttt{Nondet} nog
andere operaties gewenst zijn.

\section{\texttt{Table}: Een getablede berekening}

In een volgende stap moet een programma (van type \texttt{TabledProgram})
omgezet worden in een syntax \texttt{Table}. Naast non-determinisme laat
\texttt{Table} toe om recursieve calls te onderscheppen:

> data Table p b c a   =  Call p b (c -> Table p b c a)
>                      |  Or (Table p b c a) (Table p b c a)
>                      |  Fail
>                      |  Pure a
>
> call :: p -> b -> Table p b c c
> call p b = Call p b Pure

De datatype declaratie heeft nu extra parameters \texttt{p}, \texttt{b} en
\texttt{c}: \texttt{p} is het type van het predicaat, \texttt{b} het argument en
\texttt{c} het resultaat.

Een \texttt{Call} heeft drie argumenten: het predicaat \texttt{p}, het
argument van deze aanroep \texttt{b}, en het \emph{vervolg} (de continuatie) van
de berekening waarvan de waarde afhankelijk is van het resultaat van de oproep.

Eveneens wordt de slimme constructor \texttt{call} ge\"introduceerd. 
\texttt{call} constueert, gegeven een predicaat en een argument, een berekening 
die alleen het resultaat van de oproep teruggeeft en verder niets doet.

Het datatype \texttt{Table} heeft ook een \texttt{Monad}- en een
\texttt{Nondet}-instance.

> instance Monad (Table p b c) where
>   return a = Pure a
>   Pure a         >>= f = f a
>   t1 `Or` t2     >>= f = (t1 >>= f) `Or` (t2 >>= f)
>   Fail           >>= f = Fail
>   Call p b cont  >>= f = Call p b (\x -> cont x >>= f)
>
> instance Nondet (Table p b c) where
>   fail     = Fail
>   a <|> b  = a `Or` b

\subsection{Vertaling van \texttt{TabledProgram} naar \texttt{Table}}
\label{sec:tpToTable}
Deze sectie bespreekt hoe een \texttt{TabledProgram b c  a} naar een
\texttt{Table p b c a} wordt omgezet.
Eerst moet een concreet type voor de parameter \texttt{p} gekozen worden.
Dit type moet toe laten om enerzijds het predicaat uniek te identificeren en
anderzijds de body van het predicaat uit te voeren.
Deze functies worden vervuld door respectievelijk \texttt{predicateId} en
\texttt{PredicateBody} in het datatype \texttt{Predicate}.

> data Predicate b c = Predicate {
>   predicateId    :: Int,
>   predicateBody  :: b -> Table (Predicate b c) b c c
> }

Het vertalen van \texttt{TabledProgram b c a} naar
\texttt{Table (Predicate b c) b c a} verloopt redelijk eenvoudig.
\begin{itemize}
\item 
De vertaling van een |Expr e| met type |forall m . Nondet m => Program b c m a|
is |e| zelf, aangezien \texttt{Table} een instance is van \texttt{Nondet}.

\item
De vertaling van een |Decl ps cont| is de vertaling van de continuatie |cont|
toegepast op |predicates|, waar |predicates| de lijst van \texttt{Predicate}s is
die verkregen wordt door in ieder predicaat van |ps| de recursieve calls te
vervangen door een \texttt{call} naar het \texttt{Predicate} dat met dit
predicaat overeenkomt. 

\end{itemize}

> tpToTable :: TabledProgram b c a -> Table (Predicate b c) b c a
> tpToTable p = tpToTable' 0 (unTP p) where
>   tpToTable' i (Expr e)        = e
>   tpToTable' i (Decl ps cont)  = tpToTable' (i + n) (cont predicates) where
>     predicates  = map call (zipWith Predicate [i..] (ps predicates))
>     n           = length predicates

Eens een waarde van het type \texttt{Table} bekomen is, kunnen hierop Effect
Handlers losgelaten worden.

Typisch is dus de methode om een programma uit te voeren:

< >>> effectHandler $ tpToTable $ program
< <Het gewenste effect.>


\section{Samenvatting}
De programmeur stelt steeds zijn programma op in het type
\texttt{TabledProgram b c a}. 
Hierbij wordt enkel gebruik gemaakt van de slimme constructoren voor
\texttt{Table} en \texttt{Program} en wat Haskell verder aan taalconstructies
te bieden heeft.

De syntax is eenvoudig, wat de programmeur en de effect handlers ten goede komt.
Voor de programmeur is het aantal nieuwe concepten erg beperkt, zodat hij of
zij snel zijn of haar weg vindt binnen het systeem.

De complexiteit van Effect Handlers staat in rechtstreekse verhouding tot
de complexiteit van de syntax waarop ze betrekking hebben.
Een eenvoudige syntax maakt dus eenvoudigere handlers mogelijk.
Desondanks zijn nog steeds sterk verschillende (interessante) interpretaties van
de syntax mogelijk.
Deze interpretaties zullen in het vervolg van deze tekst aan bod komen.




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
