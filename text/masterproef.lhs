\documentclass[master=cws]{kulemt}
\setup{title={De Tabulatie Monad in Haskell},
  author={Alexander Vandenbroucke},
  promotor={Prof.\,dr.\,ir.\ Tom Schrijvers},
  assessor={Prof.\,dr.\,ir.\ Maurice Bruynooghe, Prof.\,dr.\ Bart Demoen},
  assistant={Prof.\,dr.\,ir.\ Tom Schrijvers}}
% De volgende \setup mag verwijderd worden als geen fiche gewenst is.
\setup{filingcard,
  translatedtitle={The Tabulation Monad in Haskell},
  udc=621.3,
  shortabstract={Short abstract for filingcard \endgraf}}
% Verwijder de "%" op de volgende lijn als je de kaft wil afdrukken
%\setup{coverpageonly}
% Verwijder de "%" op de volgende lijn als je enkel de eerste pagina's wil
% afdrukken en de rest bv. via Word aanmaken.
%\setup{frontpagesonly}

% Kies de fonts voor de gewone tekst, bv. Latin Modern
\setup{font=lm}

% Hier kun je dan nog andere pakketten laden of eigen definities voorzien
\usepackage{placeins}
\usepackage{mathtools}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{url}
\usepackage{listings}
\lstdefinestyle{customProlog}{
  xleftmargin=2\parindent,
  captionpos=b,
  language=Prolog
}
\lstset{style=customProlog}

%%%%% lhs2TeX
%include polycode.fmt
%format <+> = "\oplus"
%subst char a   = "\texttt{''" a "''}"
%subst string a = "\texttt{\char34 " a "\char34} "
%format <|> = "\vee"
%format <$> = "\odot"
%format <*> = "\circledast"
%format bigI = "\bigI"
%format PowI = "\powerI"
%format AggI = "\aggregateI"
%format mappend = "\diamond"
%format mempty  = "\varepsilon"
%format <> = "\diamond"
%format forall = "\forall"
%format >>> = "\ggg"
%format bottom = "\bot"
%format lub = "\sqcup"
%format =<= = "\sqsubseteq"
%format coerce = "\uparrow\!"
%format uncoerce = "\downarrow\!"
%format BOr = "Or_B"
%format BPure = "Pure_B"
%format BFail = "Fail_B"
%format tmax = "t_{max}"
%format tcount = "t_{count}"
%format tall = "t_{all}"


 % For Scott brackets and catamorphisms
\usepackage{stmaryrd}
\newcommand{\denot}[2]{\mathcal{#1}\llbracket\,#2\,\rrbracket}
\newcommand{\msum}[2]{\bigoplus\limits_{#1\,\leftarrow\,#2}}
\newcommand{\bigI}{\mathbb{I}}
\newcommand{\powerZ}{\mathcal{P}(\mathbb{Z})}
\newcommand{\powerI}{\mathcal{P}(\bigI)}
\newcommand{\aggregateI}{\mathcal{A}(\bigI)}
\newcommand{\cata}[1]{\llparenthesis\,#1\,\rrparenthesis}
\newcommand{\lfp}{l\!f\!p}
\newcommand{\kleenechain}{\mathcal{KC}}
\newcommand{\tableA}{table_{\mathcal{A}}}
\newtheorem{definition}{Definitie}
\newtheorem{theorem}{Theorema}
\newtheorem*{theorem*}{Theorema}
\newtheorem{lemma}{Lemma}


% Tenslotte wordt hyperref gebruikt voor pdf bestanden.
% Dit mag verwijderd worden voor de af te drukken versie.
\usepackage[pdfusetitle,colorlinks,plainpages=false]{hyperref}

%\includeonly{hfdst-n}
\begin{document}

\begin{preface}
  [Preface here] %TODO
\end{preface}

\tableofcontents*

\begin{abstract}
  [abstract here] %TODO
\end{abstract}

% Een lijst van figuren en tabellen is optioneel
%\listoffigures
%\listoftables
% Bij een beperkt aantal figuren en tabellen gebruik je liever het volgende:
%\listoffiguresandtables
% De lijst van symbolen is eveneens optioneel.
% Deze lijst moet wel manueel aangemaakt worden, bv. als volgt:
%% \chapter{Lijst van afkortingen en symbolen}
%% \section*{Afkortingen}
%% \begin{flushleft}
%%   \renewcommand{\arraystretch}{1.1}
%%   \begin{tabularx}{\textwidth}{@{}p{12mm}X@{}}
%%     LoG   & Laplacian-of-Gaussian \\
%%     MSE   & Mean Square error \\
%%     PSNR  & Peak Signal-to-Noise ratio \\
%%   \end{tabularx}
%% \end{flushleft}
%% \section*{Symbolen}
%% \begin{flushleft}
%%   \renewcommand{\arraystretch}{1.1}
%%   \begin{tabularx}{\textwidth}{@{}p{12mm}X@{}}
%%     42    & ``The Answer to the Ultimate Question of Life, the Universe,
%%             and Everything'' volgens de \cite{h2g2} \\
%%     $c$   & Lichtsnelheid \\
%%     $E$   & Energie \\
%%     $m$   & Massa \\
%%     $\pi$ & Het getal pi \\
%%   \end{tabularx}
%% \end{flushleft}

% Nu begint de eigenlijke tekst
\mainmatter

%% We use lhs2TeX include-directives

%include intro.lhs
%include prelim.lhs
%include syntax.lhs
%include denot.lhs
%%include planning.lhs
%include aggregate.lhs
%include benchmarks.lhs


% Indien er bijlagen zijn:
\appendixpage*          % indien gewenst
\appendix
%include app-denot.lhs
%include app-aggregate.lhs
%%include app-prelim.lhs



\backmatter
% Na de bijlagen plaatst men nog de bibliografie.
% Je kan de  standaard "abbrv" bibliografiestijl vervangen door een andere.
\bibliographystyle{abbrv}
\bibliography{referenties}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: t
%%% End: 
