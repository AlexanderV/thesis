\chapter{Benchmarks}

In dit hoofdstuk worden de verschillende Haskell implementaties met elkaar
vergeleken op het gebied van uitvoeringstijd.
De verschillende implementaties moeten met elkaar wedijveren in drie benchmarks:
het knapzakprobleem, het berekenen van de kortste paden in een grafe en het
berekenen van strongly connected components (SCCs) in een grafe.

De resultaten bewijzen het nut de afhankelijkheidsanalyse uit 
Hoofdstuk~\ref{chap:denot}. 

\section{Implementaties}

\paragraph{Formele implementatie}

\paragraph{Nai\"eve implementatie}

\paragraph{Geavanceerde implementatie}

\paragraph{Formele implementatie (aggregaten)}
\paragraph{Nai\"eve implementatie (aggregaten)}
\paragraph{Geavanceerde implementatie (aggregaten)}
