\chapter{Denotationele Semantiek}
\label{chap:denot}

%if False

> {-# LANGUAGE DeriveFunctor #-}

> import Program
> import Table (Table(..), TabledProgram(..), Nondet(..), Predicate(..))
> import Table.Data
> import Table.Interpreters (runProgram)

> import           Control.Applicative ((<$>), (<*>))
> import           Control.Monad (void)
> import           Control.Monad.State (State, get, put, evalState)
> import qualified Data.Set as S
> import qualified Data.Foldable as F
> import           Data.Monoid ((<>))
> import           Data.Traversable (traverse) 
> import           Prelude hiding (fail)

> predicateId   = _predicateId
> predicateBody = _predicateBody
> main :: IO ()
> main = return ()
> tpToProgram

%endif

Dit hoofdstuk bespreekt de wiskundige semantiek die we associ\"eren met 
de \texttt{Table} syntax. 
Het doel van een wiskundige semantiek is om een correcte en waardevolle
correspondentie te geven tussen getablede programma's (voorgesteld door de
syntax) en objecten uit een wiskundig domein. 

De auteur baseert de aanpak hier op \cite{scott1971}, waarin naast de wiskundige
basis ook de filosofie achter dit soort semantiek wordt uiteengezet.

Het wiskundige domein is hier $\powerI$, de machtsverzameling (eng. powerset) 
van een willekeurige verzameling |bigI|.
De semantiek is een functie $\denot{T}{.}:|Table bigI| \rightarrow \powerI$ die
programma's van het type |Table bigI| afbeeldt op een deelverzameling van
$\bigI$.
Deze deelverzameling stelt alle mogelijke resultaten van het programma voor.

Eens een semantiek is opgesteld kunnen we deze vertalen naar Haskell. 
De semantiek krijgt zo een uitvoerbaar karakter. 
In dit kader is het nuttig om \emph{Fusion}-optimalisatie toe te passen op
de semantiek.

Daarnaast gaf het inleidende hoofdstuk al aan dat de combinatie van
syntax en effect handler ook een semantiek geeft voor Tabled-Prolog. 
Er wordt gecontroleerd in hoeverre dit beeld klopt.

Namelijk, beschouw opnieuw het volgende Prolog voorbeeld:

\begin{lstlisting}
:- table p/2.
p(1,2).
p(Y,X) :- p(X,Y).
?- p(A,B).
\end{lstlisting}

Dit geeft \texttt{A=1, B=2} en \texttt{A=2, B=1} als oplossingen.
In Hoofdstuk~\ref{chap:syntax} wordt een Haskell-tegenhanger besproken:

> program :: TabledProgram () (Int, Int) (Int, Int) 
> program = TP $ declare  (\p () -> return (1,2) <|> do  (x,y) <- p ()
>                                                        return (y,x))
>                         (\p -> expression (p ()))

In dit concrete geval willen we dus dat:
\begin{align*}
&\denot{T}{|tpToTable program|} = \{(1,2),(2,1)\}\\
&\iff \text{ [ vertaling door \texttt{tpToTable}.] }\\
&\denot{T}{|Call p () (\a -> Pure a)|} = \{(1,2),(2,1)\}
\end{align*}
waarbij
 |p = Predicate 0 (\() -> Pure (1,2) `Or` Call p () (\(x,y) -> Pure (y,x))|

\section{Denotationele Semantiek voor \texttt{Backtrack}}
Het opstellen van de semantiek vertrekt vanuit de de \texttt{Backtrack} syntax.

Het AST datatype voor een backtrackende berekening is als volgt:

> data Backtrack bigI  =  BPure bigI
>                      |  BOr (Backtrack bigI) (Backtrack bigI)
>                      |  BFail

De semantiek $\denot{B}{.}$ voor \texttt{Backtrack} heeft de vorm van een
functie die een succesvolle berekening (|BPure|) mapt op
het singleton met zijn  resultaat, een mislukte berekening (|BFail|) op
de lege verzameling, en een keuze (|BOr|) op de unie van resultaten van
zijn keuzes.

\begin{align*}
  &\denot{B}{.} : |Backtrack bigI| \rightarrow \powerI\\
  &\denot{B}{b} =
  \begin{cases}
    \{x\}                          & \text{als } b = |BPure x|\\
    \emptyset                      & \text{als } b = |BFail|\\
    \denot{B}{a} \cup \denot{B}{a} & \text{als } b = |BOr a b|
  \end{cases}
\end{align*}

\section{Uitbreiding 1: Een vast predicaat $Pr:\powerI\rightarrow\powerI$}
In deze stap breiden we \texttt{Backtrack} uit met een constructor
\texttt{Call}.
Zo bekomen we een eerste benadering van de volledige syntax voor getablede
berekeningen.

< data Table bigI  =  Pure bigI
<                  |  Fail
<                  |  Or (Table bigI) (Table bigI)
<                  |  Call

Deze \texttt{Call} heeft nog geen argument! Er wordt verondersteld dat er een
vast predicaat $Pr:\powerI\rightarrow\powerI$ bestaat, waarvan \texttt{Call}
het resultaat geeft.

De idee is hier om een call tot zijn minimale vorm te herleiden.
Het resultaat van een call is steeds een verzameling oplossingen ($\powerI$).
Calls kunnen echter recursief zijn, dus om het resultaat van een call te
berekenen moeten we de resultaten van de call kennen.
Dit is de reden dat $Pr$ een functie is met als argument het resultaat van
recursieve calls.

Als we $\powerI$ als toestand beschouwen, dan vormt 
$Pr$ een functie die van toestand naar toestand gaat. 
We kunnen dus hierover itereren tot we alle oplossingen gevonden hebben
(de toestand niet meer veranderd als $Pr$ erop toegepast wordt).

\begin{align*}
  &\denot{T}{.} : |Table bigI| \rightarrow \powerI\\
  &\denot{T}{t} = 
  \begin{cases}
    \{x\}                          & \text{als } t = |Pure x|\\
    \denot{T}{a} \cup \denot{T}{b} & \text{als } t = |Or a b|\\
    \emptyset                      & \text{als } t = Fail\\
    \lfp(Pr)                       & \text{als } t = Call
  \end{cases}
\end{align*}

De operator $\lfp(.):\powerI \rightarrow \powerI)$ is de
kleinste-vastepuntsoperator (least-fixpoint operator). 
Als het argument van deze operator een monotone functie op een volledige tralie
(monotonic function on a complete lattice) is dan weten we zeker dat een
dergelijk vast punt moet bestaan (Theorema~\ref{theorem:knaster-tarski})
\cite{scott1971}.

In dit geval wil dit zeggen dat (1) $Pr$ monotoon moet zijn, en (2) $\powerI$
moet een volledige tralie zijn.

\section{Uitbreiding 2: Een vaste predicaatsbody $Prb:$ \texttt{Table |bigI|}}
In deze stap maken we het predicaat meer expliciet.
$Prb$ is nu de body van het predicaat (en dus van het type \texttt{Table}).
De abstracte syntax is niet veranderd tegenover de vorige sectie.

Voor calls gebruiken we nu een aparte semantiek $\denot{C}{.}$ die een
\texttt{Table |bigI|} afbeeldt op een functie
$f:\powerI \rightarrow \powerI$, zodat $Pr(s) = \denot{C}{Prb}(s)$ ($Pr$ is het
vaste predicaat uit de vorige sectie).
Hieruit volgt:
$\lfp(Pr) = \lfp(\denot{C}{Prb}) = \lfp(\lambda s.\denot{C}{Prb}(s))$.
$\lambda s.\denot{C}{Prb}(s)$ wordt hier gebruikt om duidelijker aan te geven
dat het argument van $\lfp(.)$ een functie is.

De intu\"itie is hier dat $\denot{C}{.}:\powerI\rightarrow\powerI$ de
resultaten (de laatste $\powerI$) van een call geeft, geven de resultaten
(de eerste $\powerI$) van eventuele recursieve calls.

$\denot{C}{.}$ is net zoals $\denot{T}{.}$ een stuksgewijze functie.
De semantiek van een recursieve \texttt{Call} is het vorige berekende resultaat
van het predicaat $s$. De semantiek van een keuze is de unie van $\denot{C}{.}$
van zijn keuzes.
Voor de andere gevallen zijn $\denot{C}{.}$ en $\denot{T}{.}$ hetzelfde, 
want hierbij kunnen geen recursieve oproepen gebeuren.

\begin{align*}
  &\denot{T}{.} : |Table bigI| \rightarrow \powerI\\
  &\denot{T}{t} =
  \begin{cases}
    \{x\}                             & \text{als } t = |Pure x|\\
    \denot{T}{a} \cup \denot{T}{b}    & \text{als } t = |Or a b|\\
    \emptyset                         & \text{als } t = Fail\\
    \lfp(\lambda s.\denot{C}{Prb}(s)) & \text{als } t = Call
  \end{cases}
\end{align*}

\begin{align*}
  &\denot{C}{.} : |Table bigI| \rightarrow \powerI \rightarrow \powerI\\
  &\denot{C}{t}(s) =
  \begin{cases}
    \denot{C}{a}(s) \cup \denot{C}{b}(s) & \text{als } t = |Or a b|\\
    s                                    & \text{als } t = Call\\
    \denot{T}{t}                         & \text{anders}
  \end{cases}
\end{align*}

$\denot{C}{Prb}(s)$ is monotoon, dus 
$\lfp(\lambda s.\denot{C}{Prb}(s))$ bestaat.
Voor het volledige bewijs, zie \ref{app:proofuitbreiding2}.

\section{Uitbreiding 3: Call met argument}
Nu benaderen we de echte syntax voor getablede berekeningen dichter door
aan een \texttt{Call} argumenten van type \texttt{Predicate $\bigI$ $\bigI$} en
$\bigI$ toe te voegen.

In de echte syntax is deze constructor |Call p b (c -> Table p b c a)|,
dus dit is een meer beperkte vorm waar we $a \sim b \sim c$ veronderstellen, 
maar dit volstaat voor theoretische doeleinden.

Verder bedoelen we met de notatie $p(k)$, voor een \texttt{Predicate} $p$, dat
de body van $p$ moet worden toegepast op argument $k$.

< data Table bigI  =  Pure bigI
<                  |  Fail
<                  |  Or (Table bigI) (Table bigI)
<                  |  Call (Predicate bigI bigI) bigI

Hoe moeten we $\denot{C}{.}$ nu defini\"eren? 
Gelijkaardig aan de vorige sectie moet $\denot{C}{p(k)}(s)$ nu resultaten geven
voor een call van $p$ met argument $k$, geven de resultaten van de recursieve
calls $s$. 
Omdat recursieve calls nu ook een argument hebben volstaat $\powerI$ niet
meer als type voor $s$. 
$s$ heeft nu de vorm van een functie
 $s:(|Predicate bigI bigI|, \bigI) \rightarrow \powerI$.
Dus
$\denot{C}{.}:|Table bigI| \rightarrow ((|Predicate bigI bigI|, \bigI) \rightarrow \powerI) \rightarrow \powerI$ is het juiste type voor call semantiek.

Dan heeft 
\begin{align*} 
  \lambda s.\lambda k.\denot{C}{Pr(k)}(s) 
  &\text{ het type }
 ((|Predicate bigI bigI|, \bigI) \rightarrow \powerI) \rightarrow \bigI \rightarrow \powerI,\\
  \text{en } \lfp\Big(\lambda s.\lambda k.\denot{C}{Pr(k)}(s)\Big)
  &\text{ het type }
  (|Predicate bigI bigI|, \bigI) \rightarrow \powerI. \tag{1}
\end{align*}

Als het bovenstaande fixpoint(1) wordt toegepast op het argument $b$ van
\texttt{Call b}, dan volgt hieruit het resultaat van de call(type $\powerI$).

\begin{align*}
  &\denot{T}{.} : |Table bigI| \rightarrow \powerI\\
  &\denot{T}{t} =
  \begin{cases}
    \{x\}                                                        & \text{als } t = |Pure x|\\
    \denot{T}{a} \cup \denot{T}{b}                               & \text{als } t = |Or a b|\\
    \emptyset                                                    & \text{als } t = Fail\\
    \lfp\Big(\lambda s.\lambda (p,k).\denot{C}{\!p(k)\!}(s)\Big)(b) & \text{als } t = |Call p b|
  \end{cases}
\end{align*}

\begin{align*}
  &\denot{C}{.} : |Table bigI| \rightarrow ((|Predicate bigI bigI|, \bigI)\rightarrow\powerI) \rightarrow \powerI\\
  &\denot{C}{t}(s) =
  \begin{cases}
    \denot{C}{a}(s) \cup \denot{C}{b}(s) & \text{als } t = |Or a b|\\
    s(p,b)                               & \text{als } t = |Call p b|\\
    \denot{T}{t}                         & \text{anders}
  \end{cases}
\end{align*}

In het kort gaat de redenering als volgt: a. Parti\"ele functies
$(|Predicate bigI bigI|, \bigI) \rightarrow \powerI$ vormen eveneens een
volledige tralie.
b. $\lambda s.\lambda (p,k).\denot{C}{p(k)}(s)$ is monotoon.
Dus $\lfp\Big(\lambda s.\lambda (p,k).\denot{C}{\!p(k)\!}(s)\Big)$ bestaat. 
Voor het volledige bewijs, zie \ref{app:proofuitbreiding3}.

\section{Uitbreiding 4: Continuaties}
\label{sec:denot:cont}
De \texttt{Call} instructie in deze laatste stap bevat naast een parameter
nu ook een continuatie van type |bigI -> Table bigI|. 
Deze syntax is functioneel equivalent met de werkelijke syntax, afgezien van
de polymorfe argumenten en resultaten.

< data Table bigI  =  Pure bigI
<                  |  Fail
<                  |  Or (Table bigI) (Table bigI)
<                  |  Call (Predicate bigI bigI) bigI (bigI -> Table)

Wat is nu het resultaat van een |Call b cont|? 
Eerst moet het predicaat $b$ met argument $b$ uitgevoerd worden. 
Dit geeft een resultaat $table(p,b)$ van het type $\powerI$. 
De continuatie moet nu voor iedere $c \in table(b)$ uitgevoerd worden.
Het uiteindelijke resultaat is de unie van de resultaten voor elke $c$.

\begin{align*}
  &\denot{T}{.} : |Table bigI| \rightarrow \powerI\\
  &\denot{T}{t} =
  \begin{cases}
    \{x\}                                           & \text{als } t = |Pure x|\\
    \denot{T}{a} \cup \denot{T}{b}                  & \text{als } t = |Or a b|\\
    \emptyset                                       & \text{als } t = Fail\\
    % this has got to be the most complicated formula I ever had te misfortune to align
    %\bigcup\limits_{c \in \lfp\Big(\lambda s.\lambda k.\denot{C}{\!Pr(k)\!}(s)\Big)(b)}\mkern-72mu cont(c) & \text{if } t = Call\,b\,cont\\
    \bigcup\limits_{c\,\in\,table(p,b)} \denot{T}{cont(c)} & \text{als } t = |Call p b cont|\\
  \end{cases}
\end{align*}

$table$ is dezelfde fixpoint operatie als voorheen, maar $\denot{C}{.}$ is nu
anders:
\begin{align*}
  &table : |Predicate bigI bigI| \times \bigI \rightarrow \powerI\\
  &table(p,b) =
  \lfp\Big(\lambda s.\lambda (q,k).\denot{C}{\!q(k)\!}(s)\Big)(p,b)
\end{align*}

Om de resultaten $\denot{C}{Call\,b\,cont}(s)$ te berekenen, nemen we net als
bij $\denot{T}{.}$ de unie over de continuaties voor alle oplossingen
$c \in s(b)$.
\begin{align*}
  &\denot{C}{.} : |Table bigI| \rightarrow (|Predicate bigI bigI| \times \bigI \rightarrow \powerI) \rightarrow \powerI\\
  &\denot{C}{t}(s) =
  \begin{cases}
    \denot{C}{a}(s) \cup \denot{C}{b}(s)            & \text{als } t = |Or a b|\\
    \bigcup\limits_{c\,\in\,s(p,b)}\denot{C}{cont(c)}(s) & \text{als } t = |Call p b cont|\\
    \denot{T}{t}                                    & \text{anders}
  \end{cases}
\end{align*}

$\lambda s.\lambda k.\denot{C}{p(k)}(s)$ is monotoon.
Dus $\lfp\Big(\lambda s.\lambda (p,k).\denot{C}{\!p(k)\!}(s)\Big)$ bestaat.
Voor het volledige bewijs, zie \ref{app-proofuitbreiding4}.

\section{Vergelijking met Tabled-Prolog}
In deze sectie komen we terug op de vraag uit de inleiding: is het resultaat
dat de semantiek bekomt equivalent aan het antwoord dat Tabled-Prolog berekend?

\begin{proof} 
Eerst vertalen we |program| naar een |Table|.

\begin{align*}
&\denot{T}{|tpToTable program|} \equiv \{(1,2),(2,1)\}\\
&\iff \text{ [ Vertaling door \texttt{tpToTable}. ]}\\
&\denot{T}{|Call p () (\a -> Pure a)|} \equiv \{(1,2),(2,1)\}
\end{align*}

waarbij 

> p = Predicate 0 (\() -> Pure (1,2) `Or` Call p () (\(x,y) -> Pure (y,x)))

Toepassing van $\denot{T}{.}$, $\denot{C}{.}$ en $table$ geeft het antwoord:

\begin{align*}
&\denot{T}{|Call p () (\a -> Pure a)|}\\
&\equiv \text{ [ $\denot{T}{.}$ toepassen. ]}\\
&\bigcup\limits_{c\,\in\,table((p,()))} \denot{T}{|Pure c|}\\
&\equiv \text{ [ $\denot{T}{.}$ toepassen. ]}\\
&\bigcup\limits_{c\,\in\,table((p,()))} \{c\}\\
&\equiv\\
&table((p, ()))\\
&\equiv \text{ [ $table$ toepassen. ]} \\
&\lfp\Big(\lambda s.\lambda (p, k).\denot{C}{p(k)}(s)\Big)(p,())\\
\end{align*}

Het kleinste vaste punt van $\lambda s.\lambda (p, k).\denot{C}{p(k)}(s)$ is
de functie $f$:

\begin{align*}
&f : (|Predicate () (Int, Int)|, ()) \rightarrow \mathcal{P}(|(Int, Int)|)\\
&f(q,()) = \begin{cases}
\{(1,2),(2,1)\} &\text{ als } $q = p$\\
\bot            &\text{ anders}
\end{cases}
\end{align*}

want

\begin{align*}
&\denot{C}{p(\,()\,)}(f)\\
&\equiv\\
&\denot{C}{|Pure (1,2) `Or` Call p () (\(x,y) -> Pure (y,x))|}(f)\\
&\equiv\\
&\denot{C}{|Pure (1,2)|}(f) 
 \cup
 \denot{C}{|Call p () (\(x,y) -> Pure (y,x))|}(f)\\
&\equiv\\
&\{(1,2)\}
 \cup
 \Big( \bigcup\limits_{(x,y) \in f(p,())} \denot{C}{|Pure (y,x)|}(f) \Big)\\
&\equiv \text{ [ $f(p,()) = \{(1,2), (2,1)\}$ ]}\\
&\{(1,2)\} \cup \denot{C}{|Pure (2,1)|}(f) \cup \denot{C}{|Pure (1,2)|}(f)\\
&\equiv\\
&\{(1,2)\} \cup \{(2,1)\} \cup \{(1,2)\}
\equiv
\{(1,2), (2,1)\}
\end{align*}

Dan volgt:
\[
\lfp\Big(\lambda s.\lambda (p, k).\denot{C}{p(k)}(s)\Big)(p,())\\
\equiv
f(p,())
\equiv
\{(1,2), (2,1)\}
\]
\end{proof}

\section{Uitvoerbaarheid}
De formele semantiek geeft een werkende, zij het gruwelijk ineffici\"ente,
interpreter voor de \texttt{Table}-syntax. 

Het datatype \texttt{Table} ziet er in zijn meest realistische vorm als volgt
uit:

< data Table p b c a   =  Call p b (c -> Table p b c a)
<                      |  Or (Table p b c a) (Table p b c a)
<                      |  Fail
<                      |  Pure a

De interpreter begrijpt de volgende tabulatie monad:
|Table (Predicate b c) b c c|, met \texttt{Predicate} gedefini\"eerd in 
Sectie~\ref{sec:tpToTable}.

|ts| is een vrij rechttoe-rechtaan vertaling van $\denot{T}{.}$.

> ts :: (Ord a, Ord b, Ord c) => Table (Predicate b c) b c a -> S.Set a
> ts (Pure a)         = S.singleton a
> ts Fail             = S.empty
> ts (Or t1 t2)       =  ts t1 `S.union` ts t2
> ts (Call p b cont)  =  let sols = fixF 10 (const (const S.empty)) table p b
>                        in F.foldMap (ts . cont) sols

|cs| is de Haskell vertaling van $\denot{C}{.}$ (met de volgorde van de
argumenten omgewisseld).

> type Solutions b c = Predicate b c -> b -> S.Set c
>
> cs  :: (Ord b, Ord c)
>     =>  Solutions b c -> Table (Predicate b c) b c c -> S.Set c
> cs s (Call p b cont)  = F.foldMap (cs s . cont) (s p b)
> cs s (t1 `Or` t2)     = cs s t1 `S.union` cs s t2
> cs s t                = ts t

\texttt{table} en \texttt{fixF} vormen samen de machinerie die het fixpoint
berekend.
\texttt{table} is een wrapper rond \texttt{cs},
$\lambda s.\lambda k.\denot{C}{\!Pr(k)\!}(s)$ vervult deze rol in 
de formele semantiek. \texttt{fixF} itereert |n0| keer over de functie |f|,
vertrekkende van een basisgeval |a0|. Het is dus in deze simpele implementatie
niet gegarandeerd dat \texttt{fixF} altijd een fixpoint vindt.
Deze code wordt hier gepresenteerd omwille van zijn eenvoud.

> table :: (Ord b, Ord c) => Solutions b c -> Solutions b c
> table s (Predicate _ p) b = cs s (p b)
>
> fixF :: Int -> a -> (a -> a) -> a
> fixF n0 a0 f = go n0 where
>   go 0 = a0
>   go n = f (go (n-1))

Dat deze procedure het kleinste fixpoint berekend volgt uit
Theorema~\ref{theorem:kleene-fixpoint}.
Dit Theorema steunt op de continuiteit van de semantiek, wat in 
Appendix~\ref{app:proofuitbreiding4-cont} wordt bewezen.

\subsection{Afhankelijkheidsanalyse}
\label{sec:dependency-analysis}

Veel snelheidswinst is te halen uit afhankelijkheidsanalyse van de predicaten
onderling.

De volgende semantiek voert een afhankelijkheidsanalyse uit:
\begin{align*}
  &\denot{D}{.} : |Table bigI| 
  \rightarrow (|Predicate bigI bigI| \times |bigI| \rightarrow \aggregateI)
  \rightarrow \mathcal{P}(|Predicate bigI bigI| \times |bigI|)\\
  &\denot{D}{t}(s) = \begin{cases}
    \emptyset                               &\text{als } t = |Pure a|\\
    \emptyset                               &\text{als } t = |Fail|\\
    \denot{D}{a}(s) \cup \denot{D}{b}(s)    &\text{als } t = |a `Or` b|\\
    \underbrace{\{(p,b)\}}_{\text{huidige call}}
    \cup
    \big(
    \underbrace{
      \bigcup \{\denot{D}{cont(c)}(s)
      \,||\,
      c \in |uncoerce|s(p,b)\}
    }_{\text{continuaties}}\big)
                                             &\text{als } t = |Call b cont|
  \end{cases}
\end{align*}

Pure resultaten of mislukte berekeningen hebben geen afhankelijkheden.
Een keuze is afhankelijk van de afhankelijkheden van zijn opties. 
Een recursieve oproep is afhankelijk van het predicaat en argument dat het
onderwerp vormen van de oproep en van de afhankelijkheden van de continuaties.

Beschouw de opeenvolgende stappen van de hulpfunctie $table$:
\begin{align*}
 table^0(p,b)    &= \emptyset\\
 &\subseteq\\
 &\cdots\\
 &\subseteq\\
 table^{n+1}(p,b) &= \denot{C_A}{p(b)}(table^n)\\
 &\subseteq\\
 &\cdots
\end{align*}

Dit kunnen we herschrijven als:
\begin{align*}
 table^0(p,b)    &= \emptyset\\
 table^1(p,b)    &= \denot{C_A}{p(b)}(table^0)\\
 table^{n+2}(p,b) &= \denot{C_A}{p(b)}(table^{n+1}) 
   \:\:\:(\forall n \in \mathbb{N})\\
\end{align*}

Het cruciale idee is nu dat als in $table^{n+1}$ niets veranderd aan de
afhankelijkheden van een predicaat, dan veranderen de oplossingen van het
predicaat zelf ook niet kunnen veranderen! 
Meer formeel:

\begin{gather*}
\overbrace{table^{n+2}(p,b) = table^{n+1}(p,b)
}^{\text{$(p,b)$ verandert niet.}}\\
\iff\\
\underbrace{
  \forall d \in \denot{D}{p(b)}(table^{n+1}):\:table^n(d) = table^{n+1}(d)
}_{\text{Dependencies van $(p,b)$ veranderen niet.}}
\end{gather*}

Dan kan $table^{n+2}$ als volgt herscherven worden:

\begin{align*}
  &table^{n+2}(p,b) = \begin{cases}
    table^{n+1}(p,b) & \text{als } 
      \forall d \in\denot{D}{p(b)}(table^{n+1}):\\ 
      &\:table^{n+1}(d) = table^n(d)\\
    \denot{C_A}{p(b)}(table^{n+1}) & \text{anders}
   \end{cases}
\end{align*}

Waardoor een nutteloze evaluatie van $\denot{C_A}{.}$ vermeden wordt.
Op een gelijkaardige manier verandert de verzameling van afhankelijkheden
niet als de afhankelijkheden zelf niet veranderen.
Dit wil zeggen dat afhankelijkheden eveneens gedeeld kunnen worden tussen
opeenvolgende stappen.

De implementatie van deze ide\"een evalueert in iedere iteratie
\'e\'en predicaat en onthoudt de nieuwe resultaten en afhankelijkheden.
Daarna voert het de afhankelijke predicaten uit, die op hun beurt hun
afhankelijkheden uitvoeren.
Een predicaat wordt dus alleen ge\"evalueerd als in de voorgaande iteratie een
afhankelijkheid veranderde.

Om de (abstracte) types |T| en |D| onthouden de tussentijdse resultaten en
tussentijdse omgekeerde afhankelijkheden. 

< data T b c s
< data D b c

Op deze types ondersteunen een aantal operaties:
\begin{itemize}
\item Aanmaak van een lege datastructuur:

< emptyT :: T b c s
< emptyD :: D b c

\item Opzoekwerk (al dan niet in een |State| monad):

< findInD   :: Ord b => Predicate b c -> b -> D b c -> S.Set (Predicate b c, b)
< findInT   :: (Ord b, Monoid s) => Predicate b c -> b -> T b c s -> s
< findInTS  :: (Ord b, Monoid s) 
<           => Predicate b c -> b -> State (T b c s, D b c) s
< findInTS p b = gets (findInT t p b . fst)

\item Updates:

< updateT  :: (Ord b, Monoid s) => Predicate b c -> b -> s -> T b c s -> T b c s
< updateD  :: Ord b 
<          => Predicate b c -> b -> [(Predicate b c, b)] -> D b c -> D b c

\item Filteren op nieuwe afhankelijkheden:

< new  :: Ord b => (((Predicate b c, b) -> Bool) -> w) -> T b c s -> w

\end{itemize}

De Haskell functie \texttt{deps} berekend afhankelijkheden, zoals 
$\denot{D}{.}$:

> deps :: (Ord p, Ord b) => (p -> b -> S.Set c) -> Table p b c c -> S.Set (p, b)
> deps s t = case t of
>   Pure c         -> S.empty
>   Fail           -> S.empty
>   Or a b         -> deps s a <> deps s b
>   Call p b cont  ->
>     S.insert (p,b) $ S.unions $ map (deps s . cont) $ S.toList $ s p b

De functie |tableA| berekend het fixed point:

> tableA  :: (Eq b, Ord b, Ord c) 
>         => Predicate b c -> b -> State (T b c (S.Set c), D b c) (S.Set c)
> tableA p b = do
>   (t0, d0) <- get
>   let  findInT0 q c = findInT q c t0
>        v   = cs    findInT0 (_predicateBody p b)
>        w   = deps  findInT0 (_predicateBody p b)
>        t1  = updateT p b v t0                    
>        d1  = updateD p b (S.toList w) d0         
>   put (t1, d1)
>   if findInT p b t0 == findInT p b t1 then
>     tables (new (flip S.filter w) t0)
>   else
>     tables (new (flip S.filter w) t0 <> findInD p b d1)
>   findInTS p b -- result after recursion

Met de oude toestand |(t0, d0)| worden voor |p| en |b| nieuwe resultaten |v| en
afhankelijkheden |w| berekend. 
De nieuwe toestand |(t1, d1)| wordt opgeslagen. 
Als de toestand onveranderd is, dan worden enkel de nieuwe predicaten
uitgevoerd, anders worden alle afhankelijke predicaten (en de nieuwe
predicaten) uitgevoerd.

De hulpfunctie |tables| voert |tableA| uit op een verzameling predicaat-argument
paren.

> tables  :: (Ord b, Ord c) 
>         => S.Set (Predicate b c, b) -> State (T b c (S.Set c), D b c) ()
> tables = void . traverse (uncurry tableA) . S.toList

De functie \texttt{traverse} komt uit |Data.Traversable|, de functie 
|void :: Functor f => f a -> f ()| uit |Control.Monad|.

Tenslotte is er de aangepaste semantische functie |tsA|.
Deze gebruikt nu |tableA| in plaats van |table| en |fixF|.

> tsA :: (Ord a, Ord b, Ord c, Show b) => Table (Predicate b c) b c a -> S.Set a
> tsA t = evalState (go t) (emptyT, emptyD) where
>   go (Pure a) = return (S.singleton a)
>   go Fail     = return S.empty
>   go (Or a b) = (<>) `fmap` go a <*> go b
>   go (Call p b cont) = do
>     sols  <- fmap S.toList (tableA p b)
>     fmap S.unions (mapM (go . cont) sols)

Waarbij |<*>| afkomstig is uit de \texttt{Applicative} typeclass, en |<>| uit
\texttt{Monoid}.

\section{Fusion}
\textbf{This is really fine-grained fusion, what about fusing away
 $\denot{T_F}{.}$?}
Wanneer een iteratieve methode wordt gebruikt om het vaste punt te berekenen
kan het tijdswinsten opleveren om het programma en de interpreter samen te
fusen, zodat de abstracte syntaxboom niet moet worden opgebouwd.

Deze formulering voorkomt namelijk dat de predicaatbodies ($Pr(k)$) iedere
iteratie opgebouwd en afgebroken moeten worden.

De syntax uit de vorige secties kan ook geschreven worden als het
vaste-puntstype van een functor:

> data Fix f a = In (f (Fix f a))

|f| is de \emph{pattern-functor}. De pattern-functor die overeenstemt
met \texttt{Table} is \texttt{TableF}:

> data TableF bigI a  =  CallF bigI (bigI -> a)
>                     |  PureF a
>                     |  FailF
>                     |  OrF a a
>                     deriving Functor

< type Table bigI = Fix (TableF bigI)

De semantiek $\denot{T}{.}$ is nu te schrijven als een fold (genoteerd als
$\cata{.}$) van de algebra $\denot{T_F}{.}$.
\begin{align*}
&\denot{T_F}{.} : |TableF bigI|\:\powerI\rightarrow\powerI\\
&\denot{T_F}{t} =
\begin{cases}
  \{x\}                                 & \text{als } t = |PureF x|\\
  \emptyset                             & \text{als } t = |FailF|\\
  a \cup b                              & \text{als } t = |OrF a b|\\
  \bigcup\limits_{c\,\in\,table(b)} cont(c) & \text{als } t = |CallF b cont|
\end{cases}\\
&\mathcal{T} = \cata{\mathcal{T_F}}
\end{align*}

\begin{align*}
  &table : \bigI \rightarrow \powerI\\
  &table(b) = \lfp\Big(\lambda s.\lambda k.\denot{C}{\!Pr(k)\!}(s)\Big)(b)
\end{align*}

$\denot{C}{.}$ kunnen we eveneens schrijven als een fold.
\begin{align*}
&\denot{C_F}{.} : |TableF bigI|\:\powerI\rightarrow(\bigI\rightarrow\powerI)\rightarrow\powerI\\
&\denot{C_F}{.} =
\begin{cases}
    \bigcup\limits_{c\,\in\,s(b)}cont(c) & \text{als } t = CallF\:b\:cont\\
    a \cup b                          & \text{als } t = OrF\,a\,b\\
    \denot{T_F}{t}                    & \text{anders}
\end{cases}\\
&\mathcal{C_F}^{flipped}:(\bigI\rightarrow\powerI)\rightarrow |TableF bigI|\: \powerI\rightarrow\powerI\\
&\mathcal{C_F}^{flipped}(s)(t) = \denot{C_F}{t}(s)\\
&\denot{C}{t}(s) = \cata{\mathcal{C_F}^{flipped}(s)}(t)   \tag{2}
\end{align*}

Veronderstel dat een
$Pr':\forall a.(|TableF bigI|\:a) \rightarrow\bigI\rightarrow a$ bestaat zodat
\[
   Pr = Pr'(In). \tag{3}
\]

Dan volgt nu:

\begin{align*}
  &\lambda k.\denot{C}{Pr(k)}(s)\\
  &=\text{ [ (2) en (3) ]}\\
  &\lambda k.\cata{\mathcal{C_F}^{flipped}(s)}\left(Pr'(In)(k)\right)\\
  &=\text{ [ functie compositie ]}\\
  &\cata{\mathcal{C_F}^{flipped}(s)}\circ Pr'(In)\\
  &=\text{ [ acid rain rule, zie \cite{hinze2011} ]}\\
  &Pr'\left(\mathcal{C_F}^{flipped}(s)\right)(k)
\end{align*}

De $table$ functie kan nu herschreven worden:
\[
  table(b) =
  \lfp\left(
    \lambda s.\lambda k.Pr'\left(\mathcal{C_F}^{flipped}(s)
  \right)(k)\right)(b)
\]

\section{Conclusie}
Het voornaamste resultaat van dit hoofdstuk is een denotationele semantiek
$\denot{T}{.}$ voor de abstracte \texttt{Table} syntax uit
Hoofdstuk~\ref{chap:syntax}.
De semantiek is de ``handler''-helft van de Effect Handlers aanpak.
Samen vormen \texttt{table} en $\denot{T}{.}$ de \emph{Tabulatie Monad}.

Deze monad gedraagt zich zoals Backtracking met Tabulatie.
Beschouw Tabled-Prolog als een opeenstapeling van effecten: Tabulatie, 
Backtracking, Unificatie en Negatie.
De monad beschrijft de tabulatie en backtracking van een Tabled-Prolog
programma.
De overige componenten kunnen we vaak op een ad-hoc manier beschrijven (zoals
in het voorbeeld met het predicaat $p$) of met hun eigen effect handler.
De samenstelling van deze effect handlers bekomt een samenstelling van de
effecten \cite{haskell2014}.

Door de functionele vorm van de semantiek ligt een vertaling in Haskell voor de
hand.
De grootste moeilijkheid ligt in het juist en effici\"ent berekenen van het
kleinste vaste punt. 
Op dit vlak biedt de afhankelijkheidsanalyse een uitweg: de effici\"entie
verbetert enorm, maar de correctheid blijft gegarandeerd.

Tenslotte kunnen fusion-technieken dit resultaat nog sterk verbeteren.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
