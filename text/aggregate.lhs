\chapter{Aggregaten}
\label{chap:aggregate}

%if False

> {-# LANGUAGE ScopedTypeVariables        #-}
> {-# LANGUAGE MultiParamTypeClasses      #-}
> {-# LANGUAGE FlexibleInstances          #-}
> {-# LANGUAGE GeneralizedNewtypeDeriving #-}
>
> import Table
> import Program
> import Open
>
> import           Data.Monoid
> import           Control.Applicative hiding ((<|>))
> import           Data.Foldable (asum)
> import Prelude hiding (fail)
>

%endif

De denotationele semantiek $\denot{T}{.}$ uit Hoofdstuk~\ref{chap:denot}
associeert een predicaat eenduidig met de verzameling van al zijn oplossingen.
Vaak zijn we slechts ge\"interesseerd in \'e\'en specifiek element, in plaats
van de hele verzameling.
In veel gevallen is bijvoorbeeld enkel het optimale element relevant.
Hoe kan een oplossingenverzameling herleid worden tot slechts \'e\'en element?
Dit is precies wat een aggregaat doet. 
Het vat een verzameling samen in \'e\'en of meer elementen.

Het predicaat |range| genereert alle gehele getallen in een bepaald interval:

> program :: Int -> Int -> TabledProgram (Int, Int) Int Int
> program a b = TP  $        declare range
>                   $ \r ->  expression (r (a,b))
> range :: Nondet m => Open ((Int, Int) -> m Int)
> range r (a, b)  | b < a      = fail
>                 | otherwise  = return b <|> r (a, b - 1)
\textbf{This is a bad example! It's too simple, aggregates don't gain anything.
Use subsetsum instead?}
De eerdere semantiek $\denot{T}{.}$ (|ts| genoemd in Haskell) geeft:

< >>>  ts $ tpToTable $ program 0 5
< [0, 1, 2, 3, 4, 5]
< >>>  ts $ tpToTable $ program 5 0
< []

Als we enkel ge\"intereseerd zijn in het grootste element, kunnen we dit als
volgt schrijven met de functie |maximum :: Ord a => [a] -> Just a|:

< >>>  maximum $ tpToTable $ program 0 5
< Just 5 

Voor grote $b - a$ is dit natuurlijk weinig efficient.

Veronderstel daarentegen dat we beschikken over een aggregaat |tmax| dat
het maximum onmiddelijk berekent:

< tmax :: Table (Predicate b c) b c c -> Maybe c
< tmax t = ...

De berekening |t| kan soms volledig falen. 
In dit geval is er geen enkele oplossing en is het resultaat |Nothing|. 
Anders wordt de \emph{maximale} oplossing in |Just| verpakt.

De combinatie van |tmax| en |range| in het volgende programma geeft dan:

< >>>  tmax $ tpToTable $ program 0 5
< Just 5
< >>>  tmax $ tpToTable $ program 5 0
< Nothing

Een aggregaat is dus een nieuw effect, ge\"implementeerd door een nieuwe
semantische functie $\denot{T_A}{.} : |Table bigI| \rightarrow \aggregateI$. 

De nieuwe semantiek opereert op een nieuw wiskundig domein $\aggregateI$.
Eerder moest $\powerI$ al een tralie zijn omwille van technische
redenen: om het bestaan van het kleinste vaste punt $\lfp(.)$ te garanderen.
Nu is er ook een intrinsieke reden: de l.u.b. (|lub|) aggregeert uit een
deelverzameling \'e\'en uniek element.
Er bestaat dus een conceptueel verband tussen aggregaten en tralies.

XSB-Prolog kent het gelijkaardige concept \emph{Lattice Answer
Subsumption}. Hierbij worden oude en nieuwe tabelitems samengevoegd met behulp
van tralies.

\section{Tralies als aggregaten}
In plaats van $\powerI$, de machtsverzameling van $\mathbb{I}$, gebruikt de
semantiek nu $\aggregateI$, de waardeverzameling van een bepaald 
aggregaatstype.

Net zoals $\powerI$ in Hoofdstuk~\ref{chap:denot} moet
($\aggregateI$, |=<=|, |lub|) een volledige tralie (eng. complete
lattice) zijn.

\begin{definition}
Een partieel geordende verzameling $(\aggregateI,|=<=|)$\footnote{|=<=| is een
orderelatie (reflexief, transitief en antisymmetrisch).} is een volledige
tralie $(\aggregateI, |=<=|, |lub|)$ als voor alle $X \subseteq \aggregateI$ 
de l.u.b. $|lub| X$ gedefinieerd is, zodat:

 \[
  \forall z \in \aggregateI:
  |lub|X |=<=| z \Leftrightarrow \forall x \in X: x |=<=| z.
  \]

De l.u.b. van $\emptyset$ duiden we aan met een het symbool |bottom| 
(``bottom''), namelijk:

  \[|lub|\emptyset = |bottom|.\]

De typeclass \texttt{Lattice} stelt dit voor in Haskell:

> class Monoid l => Lattice l where
>   (=<=)     ::    l    -> l -> Bool
>   lub       :: [  l]   -> l
>   lub       =  foldr mappend mempty
>   bottom    ::    l
>   bottom    =  lub []

De |mappend| en |mempty| in deze definitie komen uit de \texttt{Monoid} 
typeclass.
\end{definition}

Iedere volledige tralie $(\aggregateI, |=<=|, |lub|)$ vormt dus ook een monoid
$(\aggregateI, |bottom|, |lub|)$. \footnote{De notatie |lub| wordt hier
misbruikt voor zowel de binaire operatie in de monoid, als de unaire variant
voor \texttt{Lattice} die op verzamelingen van tralies werkt.}
Bovendien geldt dat $a \sqcup b = b \sqcup a$ en $a \sqcup a = a$, dus
de monoid is \emph{commutatief} en \emph{idempotent}.

Merk op dat waar de wiskundige l.u.b. een verzameling verwacht, de 
Haskell functie |lub| gebruik maakt van gelinkte lijsten.
De wetten die op \texttt{Lattice} (en later \texttt{Coerce}) van
toepassing zijn zorgen ervoor dit voor de relevante functie ($\denot{T_A}{.}$
\eqref{eq:agg-t-sem}) geen verschil maakt.

Daarnaast voorziet $\aggregateI$ een manier om van en naar |bigI| te 
converteren.
Dit laat toe om programma's te schrijven die onafhankelijk zijn van
een specifieke $\aggregateI$. 
Op die manier kunnen meerdere tralies \'e\'en programma op verschillende
manieren interpreteren.

\begin{definition}
We beschouwen twee functies |(coerce) : bigI -> AggI| en
|(uncoerce) : AggI -> PowI| (uitgesproken als ``coerce'' en ``uncoerce'').
In typeclassvorm wordt dit:


> class Coerce i a where
>   coerce    :: i  -> a
>   uncoerce  :: a  -> [i]

Deze twee functies moeten onderling aan de volgende vereisten voldoen:
\begin{itemize}
\item |uncoerce| na |coerce| geeft het oorspronkelijke element:
  \[|uncoerce| (|coerce s|) = \{s\},\]
\item |coerce| na |uncoerce| geeft de oorspronkelijke tralie (als
$\aggregateI$ een instance is van \texttt{Lattice} en \texttt{Coerce}):
  \[|lub|\{|coerce| s\,||\,s \in |uncoerce| l \} = l.\]
\end{itemize}

\end{definition}

De functie |coerce| zet een element om in zijn bijbehorende tralie. 
Omgekeerd zet |uncoerce| een tralie om in de verzameling van alle elementen
waaruit de tralie bestaat.

\paragraph{Monoid homomorfismes}
Zowel $\aggregateI$ als $\powerI$ vormen een monoid. 
Het is in deze context dus relevant om te bekijken hoe deze twee domeinen aan
elkaar gerelateerd zijn door middel van een \emph{homomorfismes} over monoids.

\begin{definition}
Zij ($A$, $\varepsilon_A$, $\diamond_A$) en ($B$, $\varepsilon_B$, $\diamond_B$)
monoids, dan is de functie $h: A \rightarrow B$ een monoid homomorfisme van
$A$ naar $B$ als
\begin{align*}
  h(\varepsilon_A)                            &= \varepsilon_B\\
  \forall a_1, a_2 \in A: h(a_1 \diamond_A a_2) &= h(a_1) \diamond_B h(a_2)
\end{align*}
\end{definition}

\begin{theorem}
\label{theorem:homomorph-set-to-agg}
$h(A) = |lub|\{|coerce|c\:||\:c \in A\}$ is een monoid-homomorfisme van de
monoid $(\powerI, \emptyset, \cup)$ naar de monoid
$(\aggregateI, |bottom|, |lub|)$.
\end{theorem}

Voor het bewijs, zie Appendix~\ref{app:proof-theo-mmorph}.

Tenslotte, als $\aggregateI$ zowel \texttt{Lattice} als \texttt{Coerce}
instantieert, dan \emph{kan} |uncoerce| een monoid-homomorfisme van
($\aggregateI$, |bottom|, |lub|) naar ($\powerI$, $\emptyset$, $\cup$) zijn

\begin{align*}
  |uncoerce| |bottom|    &= \emptyset,\\
  |uncoerce| (a |lub| b) &=\;|uncoerce| a \;\cup |uncoerce| b,
\end{align*}

Samen met Theorema~\ref{theorem:homomorph-set-to-agg} wil dit zeggen
dat $\aggregateI$ isomorf is aan $\powerI$.

Dit is zeker niet voor alle $\aggregateI$ gegarandeerd.
Bijvoorbeeld, als er een $s$ bestaat zodat $|coerce| s = |bottom|$, dan geldt:
$|uncoerce| |bottom| =\,|uncoerce|(|coerce| s) = \{s\} \neq \emptyset$.
Dus dan is |lub| geen monoid-homomorfisme (dit is eenvoudig op te lossen door
een speciaal |bottom| element toe te voegen).
Een eenvoudige tralie waar dit het geval is $(\mathbb{N}, \leq, \max)$, met
$\bigI = \mathbb{N}$ en $\forall i \in \mathbb{N}:\:|coerce| i = i$,
$|uncoerce| i = \{i\}$. Dan is $|bottom| = 0 =\;|coerce| 0$ en dus is
$|uncoerce| |bottom| = |uncoerce| 0 = \{0\} \neq \emptyset$

Anderzijds impliceert 
$|uncoerce| (a |lub| b) =\;|uncoerce| a \;\cup |uncoerce| b$ dat
\[
a |=<=| b \Rightarrow a |lub| b = b
          \Rightarrow |uncoerce|a \subseteq\:|uncoerce|(a |lub| b)
          =\:|uncoerce|b.
\]

Dan volgt dat $\tableA$ \eqref{eq:agg-table} monotoon is:
\[ s_i |=<=| s_j \Rightarrow \tableA(s_i) \sqsubseteq \tableA(s_j)\]

Voor veel nuttige tralies is $\tableA$ niet monotoon, bijvoorbeeld voor de
tralie $(\mathbb{N}, \leq, \max)$. 

\paragraph{Aggregaten die geen volledige tralie vormen.}

Sommige aggregaten vormen wel een monoid maar geen volledige tralie, 
bijvoorbeeld de som, het aantal oplossingen of het gemiddelde.

Veronderstel dat het aggregaat |tcount| het aantal oplossingen berekent:
< tcount :: Table p b c c -> Count
< tcount = ...
<
< newtype Count = Count Int
<
< instance Monoid Count where
<   mempty = Count 0
<   Count s1 mappend Count s2 = Count (s1 + s2)

De operator |mappend| is niet idempotent. 
Bijgevolg kan (|Count|, |mempty|, |foldr mappend mempty|) geen volledige 
tralie zijn. 
Een ander voorbeeld is de lijst-monoid, waar |mappend| niet commutatief en niet
idempotent is en dus zeker geen volledige tralie vormt.

< tall :: Table p b c c -> [c]
< tall = ...
<
< instance Monoid [] where
<   mempty   = []
<   mappend  = (++)

De lijst-monoid is dus niet de juiste monoid om alle oplossingen te vinden
(het is de set-monoid die hier gewenst is).

\section{De semantische functie}
Om getablede programma's met hun aggregaat te laten corresponderen is een 
nieuwe denotationele semantiek nodig. 
Deze nieuwe semantiek gelijkt sterk op de de semantiek $\denot{T}{.}$ uit 
Hoofdstuk~\ref{chap:denot}.
Het is in die zin nuttig om in het achterhoofd te houden dat verzamelingen
ook een volledige tralie $(\powerI, \emptyset, \cup)$ vormen en dat

< instance Coerce bigI PowI where
<   coerce c   = singleton c
<   uncoerce s = toList s

een geldige instantie is voor \texttt{Coerce}.

De nieuwe semantische functie is
$\denot{T_A}{.} : \bigI \rightarrow \aggregateI$
\eqref{eq:agg-t-sem}.

\begin{align}
  &\denot{T_A}{.} : |Table bigI| \rightarrow \aggregateI \label{eq:agg-t-sem}\\
  &\denot{T_A}{t} = \begin{cases}
    |coerce| a                          &\text{als } t = |Pure a|\\
    |bottom|                            &\text{als } t = |Fail|\\
    \denot{T_A}{a} |lub| \denot{T_A}{b} &\text{als } t = |a `Or` b|\\
    |lub|\{\denot{T_A}{cont(c)}\,||\,c \in |uncoerce|\tableA(p,b)\} 
                                        &\text{als } t = |Call p b cont|
  \end{cases} \nonumber
\end{align}

$\denot{T_A}{.}$ mapt een succesvolle berekening \texttt{Pure a} op |coerce a|, 
een mislukte berekening \texttt{Fail} op |bottom|, 
een niet-deterministische keuze \texttt{Or a b} op de l.u.b. van de resultaten
van zijn keuzes en een \texttt{Call p b cont} op de l.u.b. van de continuaties
van de call (voor elk element van $|uncoerce| \tableA(p, b)$ is er een
continuatie).


De hulp-functie $\tableA(p,b)$ \eqref{eq:agg-table} berekend het resultaat van
een getablede call. 

\begin{align}
  &\tableA : |Predicate bigI bigI| \times \bigI \rightarrow \aggregateI
  \label{eq:agg-table} \\
  &\tableA(p,b) = 
  |lub|\kleenechain\Bigg(\lambda s.\lambda (q,k).
  \Big(
    \underbrace{s(q,k)}_{\text{oud}}
    |lub|
    \underbrace{\denot{C_A}{q(k)}(s)}_{\text{nieuw}}
  \Big)\Bigg)
  (p,b)
  \nonumber
\end{align}

De functie $\tableA$ \eqref{eq:agg-table} gebruikt de semantische functie
$\denot{C_A}{.}$ \eqref{eq:agg-c-sem} voor het iteratief oplossen van
recursieve calls (analoog aan $\denot{C}{.}$ in Hoofdstuk~\ref{chap:denot}).
Een anonieme $\lambda$-functie wrapt rondt $\denot{C_A}{.}$ en voegt het oude
resultaat $s(q,k)$ en nieuwe resultaat $\denot{C_A}{q(k)}(s)$ samen.

Er bestaat niet altijd het kleinste vaste punt voor deze $\lambda$-functie.
In tegenstelling tot $table$ uit Sectie~\ref{sec:denot:cont} kan $\lfp(.)$ dus
niet gebruikt worden.
Maar we zien wel dat een argument kleiner moet zijn dan zijn afbeelding onder
de $\lambda$-functie.
Opeenvolgende oproepen $\bot, f(\bot), f(f(\bot)), \ldots$ vormen dus een
ketting $\bot \sqsubseteq f(\bot) \sqsubseteq f(f(\bot)) \sqsubseteq \cdots$.
Dit is een opwaartse kleene ketting ($\kleenechain(.)$, zie
Definitie~\ref{def:kleenechain}).

De l.u.b. van deze $\lambda$-functie is dus wel goed gedefini\"eerd.
Onder bepaalde voorwaarden (monotoniciteit en continuiteit, zie 
Theorema~\ref{theorem:kleene-fixpoint}) is deze l.u.b. eveneens het kleinste
vaste punt.
Deze voorwaarden zijn wel of niet vervuld afhankelijk van de gebruikte tralie
en het programma dat door de semantiek wordt ge\"evalueerd.

Tenslotte is er nog $\denot{C_A}{.}$, de semantische functie voor recursieve
calls.

\begin{align}
  &\denot{C_A}{.} : |Table bigI|
                    \rightarrow (|Predicate bigI bigI| \times \bigI 
                                 \rightarrow \aggregateI) 
                    \rightarrow \bigI \label{eq:agg-c-sem}\\
  &\denot{C_A}{t}(s) = \begin{cases}
    |coerce| a                               &\text{als } t = |Pure a|\\
    |bottom|                                 &\text{als } t = |Fail|\\
    \denot{C_A}{a}(s) |lub| \denot{C_A}{b}(s) &\text{als } t = |a `Or` b|\\
    |lub| 
      \{\underbrace{\denot{C_A}{ cont(c)}(s)}_{\text{continuaties}}
      \,||\,
      \underbrace{c \in |uncoerce|s(p,b)}_{\text{oude resultaten}}\}
      &\text{als } t = |Call b cont|
  \end{cases} \nonumber
\end{align}

Het resultaat van een succesvolle berekening is opnieuw |coerce a|, het
resultaat van een mislukte berekening is |bottom|, het resultaat van een
niet-deterministische keuze is de l.u.b. van de resultaten van de keuze. 
Het resultaat van een call |Call b cont| is gelijk aan de l.u.b. van de
continuatie van alle gekende resultaten $|uncoerce| s(p,b)$.

De eerder gesuggereerde vergelijking tussen $\denot{T}{.}$ en
$\denot{T_A}{.}$ kan nu concreet gemaakt worden door $\powerI$ te substitueren
voor $\aggregateI$ in $\denot{T_A}{.}$.
Dan is $\denot{C_{\powerI}}{.} = \denot{C}{.}$ en
uit Theorema~\ref{theorem:kleene-fixpoint} volgt dat
$table_{\powerI}(b) = table(b)$.
Dan volgt ook dat $\denot{T_{\powerI}}{.} = \denot{T}{.}$.

\section{Vergelijking met Prolog}
Standaard gedraagt Tabled-Prolog zich zoals de semantische functie
$\denot{T}{.}$ uit Hoofdstuk~\ref{chap:denot}.
Een resultaat wordt aan de oplossingstabel toegevoegd als het nog niet
in de tabel aanwezig is (meer precies: als er nog geen \emph{variant} van
de oplossing aanwezig is).

\paragraph{XSB-Prolog en Lattice Answer Subsumption}
XSB-Prolog laat toe dat de programmeur een ander gedrag kiest wanneer een
element aan de tabel wordt toegevoegd.. 
Met het \texttt{table}-directive kan de programmeur een predicaat aanduiden.
Dit predicaat wordt dan gebruikt om bestaande en nieuwe oplossingen te 
combineren:
\begin{lstlisting}[language=Prolog]
  :- table sp(_,_,lattice(min/3)).
\end{lstlisting}

In dit Prolog fragment wordt een predicaat \texttt{sp} getabled.
Wanneer een oplossing $sp(A1,B1,C1)$ wordt gevonden en een oplossing
$sp(A2,B2, C2)$ met $A1 = A2$, $B1 = B2$ staat al in de tabel, dan zal de l.u.b.
van $C1$ en $C2$ onthouden worden.
Deze techniek heet \emph{Lattice Answer Subsumption} \cite{swift2010tabling}.

XSB ondersteund ook een andere vorm van answer subsumption, \emph{Partial Order
Subsumption}.
Dit wordt gespecificeerd met het \texttt{table}-directive.

\begin{lstlisting}[language=Prolog]
  :- table sp(_,_,po(</2)).
\end{lstlisting}

Een oplossing $sp(A,B,C)$ wordt alleen aan de table toegevoegd als er geen
oplossing $sp(A,B,C')$ in de tabel is zodat $C < C'$. 
Merk op dat \texttt{</2} een \emph{pariti\"ele orde} kan zijn.

Met de juiste tralie |Min bigI|, kan een effect dat identiek is aan
\emph{Lattice Answer Subsumption} bereikt worden met $\denot{T_{|Min bigI|}}{.}$.

> data Min a = Minimum | Min a deriving (Eq, Ord)
>
> instance Coerce a (Min a) where
>   coerce a = Min a
>   uncoerce Minimum  = []
>   uncoerce (Min a)  = [a]
>
> instance Ord a => Monoid (Min a) where
>   mempty  = Minimum
>   mappend = min
>
> instance Ord a => Lattice (Min a) where
>   Minimum  =<= a        = True
>   a        =<= Minimum  = False
>   Min a    =<= Min b    = b <= a
>   lub l = foldr min Minimum l

De semantische functie $\denot{T_A}{.}$ komt dus overeen met Tabled-Prolog met
Lattice Answer Subsumption (modulo unificatie, negatie of cut).

\paragraph{Mode-directed tabling}
Andere Prolog systemen zoals YapTab (Yap Prolog met tabulatie)\cite{santos2013},
B-Prolog\cite{zhou2010} en ALS-Prolog\cite{guo2008} breiden tabled Prolog
op een andere manier uit.
Deze systemen bieden \emph{tabling-modes} aan. 
Dit wil zeggen dat men voor bepaalde argumenten van een getablede predicaat kan
selecteren hoe nieuwe oplossingen aan de tabel worden toegevoegd.
De flexibiliteit van deze methode is dus sterk afhankelijk
van het aantal aangeboden modes.

In alle systemen worden de gebruikte modes aangeduid met het
\texttt{table}-directieve, bijvoorbeeld voor een predicaat \texttt{sp/3},
in YapTab:
\begin{lstlisting}
:- table sp(index, index, min).
\end{lstlisting}

Dit geeft aan dat de eerste twee argumenten de mode \emph{index}
gebruiken en het laatste argument de mode \emph{min}. 

YapTab heeft het grootste aanbod aan tabling modes: \emph{index}, \emph{first},
\emph{last}, \emph{min}, \emph{max}, \emph{sum} en \emph{all}.

B-Prolog ondersteunt 5 modes: $+$ (invoer), $-$(uitvoer),
\emph{min} (minimum), \emph{max} (maximum) en \emph{nt} (niet-getabled).
Input en output hebben hier betrekking op hoe B-Prolog controleerd of een
variante van een oplossing al in de tabel is. 
Hierbij worden dan enkel invoer in rekening gebracht (min en max worden
verondersteld uitvoer te zijn).
Daarnaast voorziet B-Prolog in een kardinaliteits constraint, die het aantal
oplossingen in de tabel beperkt.

ALS-Prolog voorziet in 3 tabling modes: $+$ (ge\"indexeerd), $-$
(niet-ge\"indexeerd, alleen de eerste oplossing wordt onthouden) en @@
(niet-ge\"indexeerd, alle oplossingen worden onthouden).

YapTab biedt dus strikt meer opties dan de andere twee systemen.
XSB-Prolog kan met \emph{Lattice Answer Subsumption} alle zeven table modes
van YapTab nabootsen, op functioneel vlak.
De functionaliteit van de Haskell semantiek is dus gelijkaardig aan XSB en
YapTab.

Wat effici\"entie betreft, blijkt uit benchmarks\cite{santos2013} dat YapTab
de snelste is voor de problemen die van de aangeboden modes gebruik maken
(vergeleken met B-Prolog en XSB).

Omdat \emph{sum} niet idempotent is, kan deze mode niet correct
ge\"implementeerd worden met tralies. 
Een implementatie met |lub = foldr (+) 0| en |bottom = 0| overtreedt de wetten
die aan \texttt{Lattice} zijn opgelegd.\footnote{Bijvoorbeeld $1 \leq 2 \leq 2$,
dus $|lub|\{1,2\} \leq 2$, dus $1+2 = 3 \leq 2 \lightning$.}
Het resultaat van $\denot{T_{SUM}}{.}$ is dan soms juist en soms niet.
\emph{Lattice Answer Subsumption} stoot op een gelijkaardig probleem:
het resultaat is afhankelijk van de gebruikte scheduling strategie.

\section{Uitvoerbaarheid}

De semantische functie $\denot{T_A}{.}$ laat zich makkelijk vertalen in
Haskell:\footnote{Om deze code te compileren is wel de
\texttt{ScopedTypeVariables}-uitbreiding van de Glasgow Haskell Compiler nodig
\cite{ghcscopedtypevars}.}

> tl :: forall b c l . (Lattice l, Coerce c l) => Table (Predicate b c) b c c -> l
> tl t = case t of
>   Pure c         -> coerce c
>   Fail           -> bottom
>   a `Or` b       -> lub [tl a, tl b]
>   Call p b cont  -> 
>     let  sols :: Predicate b c -> b -> l
>          sols = fixF 10 (const (const bottom)) tableA
>          tableA s p'@(Predicate _ pbody') b' = lub [s p' b', cl (pbody' b') s]
>     in lub [tl (cont c) | c <- uncoerce (sols p b)]

De semantische functie $\denot{C_A}{.}$ kent eveneens een vrij letterlijke
Haskell variant \texttt{cl}:


> cl  :: (Lattice l, Coerce c l)
>     => Table (Predicate b c) b c c
>     -> (Predicate b c -> b -> l)
>     -> l
> cl t s = case t of
>   Pure c         -> coerce c
>   Fail           -> bottom
>   a `Or` b       -> lub [cl a s, cl b s]
>   Call p b cont  -> lub ([cl (cont c) s | c <- uncoerce (s p b)])

De vaste-puntsberekening wordt hier in een beperktere vorm gedaan.
De functie \texttt{fixF} berekend het vaste punt van een functie \texttt{f},
vertrekkende van \texttt{l0} voor \texttt{n0} iteraties.

> fixF :: Int -> l -> (l -> l) -> l
> fixF n0 l0 f = go n0 l0 where
>   go n v  | n <= 0    = v
>           | otherwise = go (n - 1) (f v)

Deze eenvoudige code kan soms prematuur eindigen. 
In dit geval is de oplossing natuurlijk geen vast punt.
Deze code wordt hier gepresenteerd omwille van zijn eenvoud.
Met een kleine aanpassing kan er wel altijd een vast punt gevonden worden,
als dit bestaat.
Deze aanpassing is als volgt. 
Initieel is het fixpoint |bottom|.
Vervolgens wordt |tableA| over dit fixpoint ge\"itereerd totdat het niet
meer veranderd.
Als de aard van het getablede programm zo is dat |tableA| monotoon en continu
is, dan zal volgens Kleene's vast-puntstheorema
(Theorema~\ref{theorem:kleene-fixpoint}) altijd het kleinste vaste punt worden
gevonden.

Net zoals in Sectie~\ref{sec:dependency-analysis} kan een 
implementatie met \emph{afhankelijkheidsanalyse} het juiste resultaat nog
veel sneller berekenen.


\section{Voorbeelden}

De volgende voorbeelden demonstreren het nut van aggregaten voor het oplossen
van bepaalde problemen.
Aggregaten zijn elementair voor het effici\"ent oplossen van problemen
die gebruik maken van \emph{Dynamisch Programmeren}.
Voor deze strategie is het van groot belang dat alleen de relevante oplossingen
worden bijgehouden.

\subsection{Knapzak (Knapsack)}
\begin{quote}
Gegeven een rugzak met een maximale capaciteit (in de vorm van een maximaal
gewicht) en een lijst van items met een gewicht en een waarde, welke selectie
van items leidt tot een maximale waarde, zonder het maximale gewicht te
overschrijden?
\end{quote}

Het \emph{onbegrensde (eng. unbounded) knapzak probleem}, met maximum gewicht
$W$ en strikt positieve gewichten $w_i$ en strikt positieve waarden $v_i$ is
gedefini\"eerd als

\begin{align*}
\max_{x_1,\ldots,x_n} \sum_{i=1}^n x_i . v_i \text{ zodat }
       & \sum_{i=1}^n x_i . w_i \leq W\\
       & x_i \geq 0 \text{ voor } i = 1,\ldots,n\\
       & v_i > 0    \text{ voor } i = 1,\ldots,n\\
       & w_i > 0    \text{ voor } i = 1,\ldots,n
\end{align*}

In het onbegrensde knapzak probleem wordt een gebruikt item opnieuw op de lijst
van beschikbare items geplaatst, zodat het meerdere keren gebruikt kan worden.

Veronderstel dat reeds een item gekozen is. Hoe moeten we de rugzak verder
aanvullen zodat de overgebleven capaciteit optimaal benut wordt?
Met andere woorden: hoe vullen we een rugzak met het overgebleven gewicht
zo optimaal mogelijk? 
Dit is een voorbeeld van een probleem dat met \emph{Dynamisch Programmeren}
wordt opgelost: een probleem aanpakken door kleinere instanties van het
probleem op te lossen.

We demonstreren dit idee in XSB-Prolog met \emph{Lattice answer subsumption}:

\lstinputlisting{knapsack.pl}

Dit levert dan bijvoorbeeld het volgende resultaat:
\begin{lstlisting}
?- knapsack(K, 7), knwv(K, W, V).

K = [(5,20), (1,2), (1,2)]
W = 7
V = 24
\end{lstlisting}

Nu modelleren we deze oplossing in Haskell.
Een \texttt{Item} is een datatype me een gewicht en een waarde:

> data Item = Item { weight :: Int, value :: Int }

Een knapzak is een lijst van items.

> newtype Knapsack = Knapsack { ksItems :: [Item] } deriving Monoid

Van een knapzak kunnen we ook het totale gewicht en de totale waarde berekenen.

> ksWeight :: Knapsack -> Int
> ksWeight (Knapsack ks) = sum (map weight ks)
>
> ksValue :: Knapsack -> Int
> ksValue (Knapsack ks) = sum (map value ks)

Twee \texttt{Knapsack}-en kunnen we vergelijken voor gelijkheid |==| en orde
|<=|:

> instance Eq Knapsack where
>   k1 == k2 =  ksWeight k1 == ksWeight k2 && ksValue k1 == ksValue k2
>
> instance Ord Knapsack where
>   k1 <= k2 = v1 < v2 || (v1 == v2 && w1 <= w2) where
>     v1  = ksValue k1
>     v2  = ksValue k2
>     w1  = ksWeight k1
>     w2  = ksWeight k2

De volgende functie lost het knapsack probleem op:

> unboundedKnapsack  :: Nondet m
>                    => m Item
>                    -> Open (Int -> m Knapsack)
> unboundedKnapsack is super w
>   | w <= 0     =  return (Knapsack [])
>   | otherwise  =  return (Knapsack []) <|> do
>                     item <- is
>                     let wI = weight item
>                     if wI > w then fail else do
>                       Knapsack ks <- super (w - wI)
>                       return (Knapsack (item:ks))

De functie |unboundedKnapsack| verwacht een keuze aan items (\texttt{Table p b
c Item}) en een maximum gewicht.
De lege knapsack (\texttt{Knapsack []}) is een geldige knapsack voor
ieder gewicht.
Als het gewicht positief is, dan probeert het programma elk item en vult 
recursief het overgebleven gewicht met de beste mix van items.

De \texttt{Lattice} is in dit geval \texttt{Max}, gedefinieerd als

> data Max a = Bottom | Max a deriving (Eq, Ord)
> unMax :: Max a -> Maybe a
> unMax Bottom   = Nothing
> unMax (Max a)  = Just a
> 
> instance Ord a => Monoid (Max a) where
>   mempty  = Bottom
>   mappend = max
> 
> instance Ord a => Lattice (Max a) where
>   a =<= b  = a <= b
>   lub l    = foldr max bottom l
>   bottom   = Bottom

Enkele voorbeelden, met 3 items:

\begin{center}
\begin{tabular}{||c||||c||c||}
$i$ & $v_i$ & $w_i$\\
\hline
1   & 1     & 2\\
2   & 2     & 2\\
3   & 5     & 20\\
\end{tabular}
\end{center}

> knapsackProgram :: [Item] -> Int -> TabledProgram Int Knapsack Knapsack
> knapsackProgram is w = TP $
>   let isT = foldr (<|>) fail $ map return $ is
>   in declare (unboundedKnapsack isT) (\p -> expression (p w))

> items :: [Item]
> items = [Item 1 2, Item 2 2, Item 5 20]

Dit geeft de volgende uitvoer:

< >>> unMax $ tl $ runProgram $ knapsackProgram items 5
< Just (Knapsack [Item 5 20])

< >>> unMax $ tl $ runProgram $ knapsackProgram items 7
< Just (Knapsack [Item 5 20, Item 2 2])


\subsection{Kortste Pad in een cyclische grafe}
\begin{quote}
Gegeven een gerichte grafe $G = (V, E)$ met vertices $V$ en edges $E$,
waar vertices voorzien zijn van labels. 
Wat is de lengte van het kortste pad tussen een gegeven vertex en een andere
vertex gelabeld met een geven label?
\end{quote}

\begin{figure}[tbh]
\includegraphics{thesis-example-dijkstra.png}
\caption{Een grafe met 5 nodes $a,b,c,d,e$.}
\label{fig:example-dijkstra}
\end{figure}

\FloatBarrier
In Prolog stellen we de grafe in Figuur~\ref{fig:example-dijkstra} voor met het
predicaat \texttt{edge/3}. 
Het predicaat \texttt{sp/3} heeft dezelfde signatuur en berekend het kortste
pad tussen twee vertices.
\FloatBarrier

\lstinputlisting{shortestpath.pl}

Dan kunnen we de volgende queries stellen:

\begin{lstlisting}
?-sp(a, a, C).
C = 0;
no

?-sp(b, a, C).
C = 3;
no

?-sp(c, a, C).
C = 2;
no

?-sp(d, a, C).
C = 1;
no

?-sp(e, a, C).
no
\end{lstlisting}

In Haskell kunnen we vertices voorstellen met het datatype \texttt{Node}, dat
bestaat uit een label (van type \texttt{Char}), en de structuur van de grafe, 
voorgesteld door een adjacency list (type \texttt{[(Node, Distance)]}).

> data Node = Node { label :: Char, neighbours :: [(Node, Distance)] }
> neighbour :: Nondet m => Node -> m (Node, Distance)
> neighbour n = foldr (<|>) fail $ map return $ neighbours n 

Omdat Haskell argumenten van een functie niet-strikt evalueert, kunnen we
hiermee cyclische grafen defini\"eren, zoals de grafe in 
Figuur~\ref{fig:example-dijkstra}:

> a, b, c, d, e :: Node
> [a, b, c, d, e] = [  Node 'a' [(b, 1), (e,1)], 
>                      Node 'b' [(c, 1)       ],
>                      Node 'c' [(a,22), (d,1)],
>                      Node 'd' [(c,1),  (a,1)],
>                      Node 'e' [(e,1)        ]]

Het predicaat \texttt{sp/2} wordt vervangen door |distanceTo|:

> distanceTo :: Nondet m => Char -> Open (Node -> m Distance)
> distanceTo l super n  | label n == l  = return 0
>                       | otherwise     = do  (n0, d0)  <- neighbour n
>                                             dn        <- super n0
>                                             return (d0 + dn)

|distanceTo l| berekend de afstand tot een node met label |l| op een recursieve
manier: een pad van een node $a$ naar een node $b$ moet eerst via een edge naar
een naburige node gaan.
Vervolgens wordt een pad van de naburige node naar node $b$ gevolgd.

De code hieronder definieerd \texttt{Distance}, een \texttt{Lattice} die het
kortste pad berekend.

> data Distance = Distance Int | InfDistance deriving (Eq, Ord)
>
> instance Num Distance where
>   InfDistance + b = InfDistance
>   a + InfDistance = InfDistance
>   Distance d1 + Distance d2 = Distance (d1 + d2)
>   fromInteger = Distance . fromInteger
>
> instance Monoid Distance where
>   mempty   = InfDistance
>   mappend  = min
>
> instance Lattice Distance where
>   d1 =<= d2  = d2 <= d1
>   lub        = foldr min bottom
>   bottom     = InfDistance
>
> instance Coerce Distance Distance where
>   coerce   a  = a
>   uncoerce a  = [a]

Merk op hoe |=<=| gelijk is aan |<=| met de plaats van de argumenten verwisseld.
Dit wil zeggen dat hoe kleiner de afstand, hoe \emph{beter} de oplossing.

De volgende voorbeelden berekenen de afstand tot de vertex $a$
van de vertices $a$, $b$, $c$, $d$ en $e$ in de duidelijk cyclische grafe
in Figuur~\ref{fig:example-dijkstra}.


> dijkstraProg :: Char -> Node -> TabledProgram Node Distance Distance
> dijkstraProg l n = TP $ declare (distanceTo l) (\p -> expression (p n))

< >>> (tl $ runProgram $ dijkstraProg 'a' a) :: Distance
< Distance 0

< >>> (tl $ runProgram $ dijkstraProg 'a' b) :: Distance
< Distance 3

< >>> (tl $ runProgram $ dijkstraProg 'a' c) :: Distance
< Distance 2

< >>> (tl $ runProgram $ dijkstraProg 'a' d) :: Distance
< Distance 1

< >>> (tl $ runProgram $ dijkstraProg 'a' e) :: Distance
< InfDistance

\section{Conclusie}
Aggregaten laten toe om bepaalde effecten te bekomen op een effici\"entere
manier.
Met name wanneer we ge\"intreseerd zijn in een beperkt aantal elementen uit
de hele oplossingsverzameling.

Sommige aggregaten vormen een volledige tralie. 
In dit geval kunnen we de semantiek $\denot{T_A}{.}$ gebruiken om het gewenste
effect te bekomen, zonder dat de interface \texttt{Nondet} of de syntax
\texttt{Table} verandert.

Hetzelfde gebeurt in Prolog: de structuur van het programma blijft hetzelfde,
maar de \texttt{table}-directive wordt aangepast met \emph{Lattice Answer
Subsumption} of extra \emph{table-modes}.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
