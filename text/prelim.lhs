\chapter{Preliminaries}
\label{chap:prelim}

Het doel van dit hoofdstuk is om de lezer voldoende voorkennis te geven om
de onderwerpen in het vervolg van deze tekst te kunnen begrijpen.

\section{Haskell}
Er bestaan veel goede inleidingen en tutorials voor Haskell. 
A gentle Introduction to Haskell\cite{hudak2000} en Learn You a Haskell 
\cite{lyah} zijn twee favorieten van de auteur.

Het doel van deze sectie is niet om een algemene introductie te geven tot
Haskell. 
In plaatst daarvan is de bedoeling om te verzekeren dat de auteur en de lezer
op dezelfde lijn zitten met betrekking tot een aantal concepten.

\subsection{Monads}
\label{sec:intromonad}

Monads vormen een standaard manier om effectvolle berekeningen te encapsuleren
in functionele programmeertalen \cite{wadler1995}.
Effectvolle berekeningen zijn berekeningen die naast hun resultaat ook 
extra effecten kunnen hebben.
Toestand, exceptions, niet-determinisme en (in deze tekst) Tabulatie zijn
allemaal voorbeelden van effecten.
Deze berekeningen worden samengesteld met een functie |>>=| 
(uitgesproken als ``bind''). 
In Haskell beschrijft men Monads met een typeclass:

< class Monad m where
<  return  :: a -> m a
<  (>>=)   :: m a -> (a -> m b) -> m b

Het type \texttt{m a} beschrijft berekeningen van het type \texttt{m} die
resultaten van het type \texttt{a} heeft. \texttt{return} tilt een waarde
\texttt{a} op naar een effectvolle berekening van type \texttt{m} die
alleen \texttt{a} produceert en geen andere effecten heeft. 
bind (|>>=|) voegt een berekening van het type \texttt{m a} die een
waarde van type \texttt{a} produceert samen met een functie die een
\texttt{a} accepteert en een berekening van type \texttt{m b} teruggeeft.

Om geldig te zijn moet een monad instance voldoen aan de volgende wetten:

< return x >>= f   = f x                      -- Linker neutraal element
< m >>= return     = m                        -- Rechter neutraal element
< (m >>= f) >>= g  = m >>= \x -> (f x >>= g)  -- Associativiteit

Omdat monads in Haskell zeer vaak gebruikt worden bestaat een speciale
|do|-notatie, die het aaneenrijgen van binds eenvoudiger maakt:
|do {x <- f; g}| is syntactic sugar voor |f >>= \x -> g|.

\section{Monoids}

Monoids zijn een eenvoudige structuur die twee operaties aanbied:
|mempty| het lege element, en |mappend| wat twee monoids samenvoegd.

In Haskell beschrijft men Monoids eveneens met een typeclass:

< class Monoid m where
<   mempty   :: m
<   mappend  :: m -> m -> m

Om geldig te zijn moeten Monoids ook 3 wetten respecteren:

< mempty <> a    = a              -- Linker neutraal element
< a <> mempty    = a              -- Rechter neutraal element
< (a <> b) <> c  = a <> (b <> c)  -- Associativiteit

Als de operatie |mappend| commutatief of idempotent is, dan spreekt men van
een commutatieve of idempotente monoid.

\section{Tabulatie in Prolog}
\label{sec:tabprolog}
De Tabulatie die in dit werk wordt gepresenteerd is ge\"insprireerd door
de Tabulatie in Prolog. 
Het is daarom nuttig om een eerste introductie tot Tabulatie in Prolog te geven.
De lezer kan dan parallellen en contrasten tussen Prolog en Haskell beter
kaderen.
Voorkennis van Prolog-syntax is niet nodig, aangezien Prolog nauwelijks syntax
heeft.
Voor een diepgaandere introductie, zie \cite{xsbbook} en de XSB Prolog manual.
\footnote{\url{http://xsb.sourceforge.net/manual1/manual1.pdf}}

Het volgende programma definieert een predicaat \texttt{p} met twee argumenten.
De eerste regel defini\"eert het feit \texttt{p(1,2)}, namelijk dat predicaat
\texttt{p} waar is voor argumenten 1 en 2.
De volgende regel defini\"eert een clause, die we als een omgekeerde implicatie
kunnen interpreteren: als \texttt{p(Y,X)} waar is, dan is \texttt{p(X,Y)} ook
waar.

\begin{lstlisting}[language=Prolog]
  p(1,2).
  p(X,Y) :- p(Y,X).
\end{lstlisting}

Er bestaan dus 2 mogelijke toewijzingen aan \texttt{A} en \texttt{B} die
de goal \texttt{?-p(A,B)} waar maken: \texttt{A=1,B=2} en \texttt{A=2,B=1}.
Maar zelfs op dit uiterst simpele programma gaat een gewone Prolog interpreter
de mist in: de uitvoering stopt niet hoewel er geen nieuwe resultaten bijkomen!
\begin{lstlisting}[language=Prolog]
?-p(A,B).
    A=1  B=2;
    A=2  B=1;
    A=1  B=2;
    A=2  B=1;
    ...
\end{lstlisting}

Het volgende programma toont hoe dit euvel met Tabulatie te verhelpen is.
Aan het begin van het programma wordt een \texttt{table p/2.}-directive
toegevoegd. 
Een Prolog interpreter die dit ondersteund zal de nu de resultaten van
\texttt{p} tabuleren.

\begin{lstlisting}[language=Prolog]
  :- table p/2.
  p(1,2).
  p(X,Y) :- p(Y,X).
\end{lstlisting}

De uitvoer wordt nu:
\begin{lstlisting}[language=Prolog]
?-p(A,B).
    A=1  B=2;
    A=2  B=1;
    no.
\end{lstlisting}
Prolog merkt nu dat extra pogingen geen nieuwe oplossingen hebben en stopt.

Sterker nog, Tabulatie kan ook links-recursieve programma's oplossen
(links-recursief verwijst naar de Prolog-uitvoeringsboom, die clauses
van links naar rechts diepte-eerst uitprobeert, en dus in een linkse
recursie vast kan komen te zitten, zonder oplossingen te produceren).
\begin{lstlisting}[language=Prolog]
  :- table p/2.
  p(X,Y) :- p(Y,X).
  p(1,2).
\end{lstlisting}

Dit programma geeft hetzelfde resultaat als het vorige programma.

Twee elementen die Tabulatie moeilijk, en dus interessant maken zijn in
dit programma al aanwezig. 
Enerzijds moet een recursief predicaat getabled worden.
Anderzijds heeft de goal die het predicaat oproept duidelijk meerdere 
(maar een eindig aantal) oplossingen.

\section{Open Recursie}
Open recursie \cite{jfp_mri} is een manier om functies op een zodanige manier
te structureren dat de interne recursieve structuur open ligt.
Dit maakt het gedrag van recursieve oproepen uitbreidbaar.
In dit werk wordt hiervan gebruik gemaakt om Tabulatie op recursieve
oproepen te implementeren.

De volgende Haskell code defini\"eert open recursie:

> type Open s = s -> s
> zero :: Open a
> zero = id
> 
> new :: Open s -> s
> new a =  let this = a this in this
>
> (<+>) :: Open s -> Open s -> Open s
> a1 <+> a2 = \super -> a1 (a2 super)

Het basisgeval is \texttt{zero} en doet niets. 
Een open recursieve functie |a2| kan uitgebreid worden door een andere open
recursieve functie |a1| met de |<+>|-combinator: 
|a1 <+> a2| is een nieuwe open recursieve functie waarin het gedrag van
|a2| uitgebreid is met dat van |a1|. 
Een open recursieve functie |f| wordt afgesloten door \texttt{new}:
|new f| is een gewone functie en kan als dusdanig gebruikt worden.

Een eenvoudig voorbeeld, overgenomen uit \cite{jfp_mri} is het volgende:

> fib :: Open (Int -> Int)
> fib this n = case n of
>  0  -> 0
>  1  -> 1
>  _  -> this (n - 1) + this (n - 2)

\texttt{fib n} is een recursieve berekening van het n-de fibonaccigetal waarin
de recursieve oproepen vervangen zijn door \texttt{this}.

De volgende functie \texttt{optfib} implementeert twee hard gecodeerde gevallen,
om de implementatie te ``versnellen''. \texttt{optfib} kan duidelijk
niet op zichzelf gebruikt worden voor andere waarden dan 10 en 30.


> optfib :: Open (Int -> Int)
> optfib super n = case n of
>   10  -> 55
>   30  -> 8320140
>   _   -> super n


De functies kunnen na combinatie met |<+>| gesloten worden met \texttt{new}
en zijn dan klaar voor gebruik.

> slowfib  :: Int -> Int
> slowfib  = new fib
> fastfib  :: Int -> Int
> fastfib  = new (optfib <+> fib)  

\section{Effect Handlers}
\label{sec:effecthandlers}

De Effect Handlers aanpak splitst het modelleren van Effecten op in twee delen.
De eerste stap defini\"eert een syntax die alle relevante acties bevat.
De tweede stap defini\"eert een functie die deze syntax omzet naar
de gewenste effecten. 

De syntax is vaak een abstracte syntax tree datatype,
waarvoor gemakkelijk een monad instance kan worden gedefini\"eerd.

In navolging van de paper ``Effect Handlers in Scope'' \cite{haskell2014} 
beschouwen we backtrackende berekeningen als een eenvoudig voorbeeld.

De volgende typeclass is een abstractie van backtracking:

> class Monad m => Nondet m where
>   fail  :: m a
>   (<|>) :: m a -> m a -> m a

De functie |<||>| stelt een niet-deterministische keuze voor en
\texttt{fail} een mislukte berekening.
De constraint \texttt{Monad m} verzekert dat iedere instance van 
\texttt{Nondet} ook een instance is van \texttt{Monad}.

Het voorbeeld uit sectie~\ref{sec:tabprolog} laat zich nu in Haskell schrijven:

> p :: Nondet m => m (Int, Int)
> p = return (1,2) <|> do  (x,y) <- p
>                          return (y, x)

De keuze tussen de twee clauses in het Prolog voorbeeld is nu een keuze tussen
twee backtrackende berekeningen verbonden met |<||>|.

De volgende twee instances voor \texttt{Monad} en \texttt{Nondet} interpreteren
een backtrackende berekening als een lijst van alle mogelijke oplossingen:


<  instance Monad [] where
<   return a = [a]
<   []      >>= _  = []
<   (x:xs)  >>= f  = f x ++ (xs >>= f)
< instance Nondet [] where
<   fail     = []
<   a <|> b  = a ++ b

Deze \texttt{Monad} instantie is de standaard instance voor lijsten, men noemt
dit daarom de \emph{lijst-monad}.
Om Haskell de juiste instances te laten kiezen voegen we een typesignatuur
toe: |p :: [(Int, Int)] = [(1,2),(2,1),...]|, dus de berekening is eveneens
oneindig, net zoals in Prolog. 
In het algemeen een oplossing van de vorm |[(1,2),(2,1)]| kunnen bekomen
(zoals getabuleerde Prolog) vormt het onderwerp van deze thesis en daar
komt deze tekst later uitgebreid op terug.

Nu gaan we over op de Effect Handlers aanpak. 
De syntax voor Backtracking is de volgende:

> data Backtrack a  =  Pure a
>                   |  Fail
>                   |  Or (Backtrack a) (Backtrack a)

\texttt{Pure a} stelt een succesvolle berekening met resultaat \texttt{a} voor,
\texttt{Fail} stelt een mislukte berekening voor, en \texttt{Or a b} is een
keuze tussen \texttt{a} of \texttt{b}.

De voor de hand liggende \texttt{Monad}- en \texttt{Nondet}-instances zijn:

> instance Monad Backtrack where
>   return a           = Pure a
>   (Pure  a)   >>= f  = f a
>   Fail        >>= _  = Fail
>   (a `Or` b)  >>= f  = (a >>= f) `Or` (b >>= f)
>
> instance Nondet Backtrack where
>   fail     = Fail
>   a <|> b  = a `Or` b

Het resultaat van |p :: Backtrack (Int, Int)| is nu een abstracte syntaxboom
van de vorm: |Pure (1,2) `Or` (Pure (2,1) `Or` (Pure (1,2) `Or` ... ))|, of
grafisch in Figuur~\ref{fig:ast}
\begin{figure}[tbh]
\includegraphics[scale=0.5]{syntaxtree.png}
\caption{De abstracte syntaxboom die overeenkomt met |p :: Backtrack (Int, Int)|.}
\label{fig:ast}
\end{figure}
\FloatBarrier

We kunnen nu een interpreter bedenken die alle mogelijke oplossingen in een
lijst teruggeeft (net zoals de instances voor |[]|).

> solutions :: Backtrack a -> [a]
> solutions (Pure a)    = [a]
> solutions Fail        = []
> solutions (a `Or` b)  = solutions a ++ solutions b

Het effect van \texttt{p} is dan |solutions p = [(1,2),(2,1),...]|, een
oneindige lijst van oplossingen.
Een voorbeeld van een triviale (en dus niet algemene) uitbreiding die wel
|[(1,2),(2,1)]| geeft is de volgende interpreter:

> nsolutions :: Int -> Backtrack a -> [a]
> nsolutions n = take n . solutions

Want |nsolutions 2 p = [(1,2),(2,1)]|.

|p :: Backtrack (Int, Int)| is volledig agnostisch over de gebruikte
interpreter! Meer bepaald, |p :: Backtrack (Int, Int)| heeft alleen weet
van de gebruikte syntax.

\paragraph{Andere interpreters} 
In de vorige paragrafen werden al twee mogelijke effecten besproken:
alle oplossingen, of alleen de eerste twee oplossingen.
In dit specifieke geval is het vrij eenvoudig om het nieuwe effect voor 
\texttt{p} te defini\"eren op basis van de lijst-monad (namelijk, 
|take 2 p = [(1,2),(2,1)]|).

In het volgende voorbeeld is dit niet zo eenvoudig.
\texttt{leftBranch} is een interpreter die altijd kiest voor de linkse optie.

> leftBranch :: Backtrack a -> [a]
> leftBranch (Pure a)  = [a]
> leftBranch Fail      = []
> leftBranch (Or a _)  = leftBranch a

Het nieuwe resultaat is |leftBranch p = [(1,2)]|.
De uitdrukking |Fail `Or` p| illustreert het verschil tussen \texttt{leftBranch}
en \texttt{solutions}:
|leftBranch (Fail `Or` p) = []|, terwijl
|solutions (Fail `Or` p) = [(1,2),(2,1),...]|).
De linkse keuze (en alleen de linkse keuze) wordt altijd genomen, 
zelfs als dit een mislukking is.

In de lijst-monad gaat dit echter niet, want in de \texttt{Nondet} 
instance verliezen we informatie wanneer we |<||>| gebruiken. 
Met andere woorden, er is een nieuwe \texttt{Nondet} instance nodig.


< instance Nondet [] where
<   fail     = []
<   a <|> _  = a

Maar hier ontstaat in Haskell een probleem: er kan niet meer dan \'e\'en
instance van een typeclass gedefini\"eerd worden per type.

Er is dus geen andere keuze dan over te schakelen op een ander type
\texttt{Left}.

> data Left a = Empty | Value a
> instance Monad Left where
>   return = Value
>   Empty       >>= _ = Empty
>   (Value a)   >>= f = f a
>
> instance Nondet Left where
>   fail     = Empty
>   a <|> _  = a

Het effect is nu zoals \texttt{leftBranch}:
|p :: Left (Int, Int) = Value (1,2)| en 
|fail <||> p :: Left (Int, Int) = Empty|.

Deze uiteenzetting illustreert het voordeel van de Effect Handlers aanpak 
tegenover de typeclasses \& instances aanpak. 
Iedere keer als een ander effect gewenst is moet men op zoek gaan naar een
ander datatype dat dit effect kan ondersteunen, en daarvoor de nodige
\texttt{Monad}- en \texttt{Nondet}-instances defini\"eren.
In deze simpele voorbeelden valt dit nog goed mee, maar voor complexe 
effecten (waarvan Tabulatie zeker een voorbeeld is) wordt het als maar
moeilijker om correcte instances te vinden. 
Bovendien is in deze sectie het controleren van de monad wetten onder de mat
geveegd. 
Hoe complexer de effecten en structuren, hoe moeilijker het wordt om dit
na te gaan.
Toch zou een echte applicatie dit moeten doen.

Bij de Effect Handlers aanpak kan men gewoon overstappen op een andere 
interpreter. Het voldoen aan de monad wetten valt nu ten deel aan de 
interpreters in plaats van de syntax.

Tenslotte wil de auteur nog opmerken dan beide technieken valabel zijn,
en nuttige monads kunnen opleveren. 
Zie \cite{logicT} als voorbeeld van de aanpak gebaseerd op typeclasses.

\section{Denotationele Semantiek}
Het doel van een denotationele semantiek is om een programma te voorzien
van een eenduidige formele betekenis.
Deze betekenis bestaat uit operaties op objecten uit een wiskundig domein.

Concreet moet een denotationele semantiek dus een mapping voorzien tussen
een programmasyntax en wiskundig objecten.

In deze tekst neemt de semantiek de vorm aan van een functie die
een syntaxboom datatype afbeeld op het wiskundige domein.

Bijvoorbeeld, de volgende grammatica voor een simpele expressietaal bestaat
enkel uit gehele getallen en plus:
\[
  Expr ::= \texttt{plus }Expr\:Expr\:||\:\texttt{num }\mathbb{Z}
\]

Met als semantische functie $\denot{E}{.}$ van |Expr| naar de gehele getallen:
\begin{align*}
  &\denot{E}{.}: Expr \rightarrow \mathbb{Z}\\
  &\denot{E}{e} =
  \begin{cases}
    a                           &\text{als } e = \texttt{num }a\\
    \denot{E}{a} + \denot{E}{b} &\text{als } e = \texttt{plus }a\:b
  \end{cases}
\end{align*}

De semantiek ontstaat dus door de grammatica productie per productie te
analyseren.

In deze tekst wordt de grammatica meestal gespecificeerd als een Haskell
algebraisch datatype, met een constructor per productie:

> data Expr = Plus Expr Expr | Num Int

\subsection{Vaste punten en tralies}
\label{sec:Vaste-punten-en-tralies}
Deze sectie geeft theoretische achtergrond bij Hoofdstukken~\ref{chap:denot} en
\ref{chap:aggregate}.
Ook zonder deze sectie te hebben doorgenomen kan de lezer de denotationele
semantiek in voorgenoemde hoofdstukken te begrijpen.
Als tijdens het lezen van deze hoofdstukken meer achtergrond gewenst
is, dan kan deze sectie opnieuw bezocht worden.

\subsubsection{Inleiding}

Als een punt niet verandert nadat er een functie op wordt uitgevoerd,
dan noemen we dit een vast punt van die functie:

\begin{definition}[Vast Punt]
Een punt $x$ is een vast punt (eng. fixed point of fixpoint) van een functie
$f$ als $x = f(x)$.
\end{definition}

Het vervolg van deze sectie draait rond het berekenen van dergelijke vaste
punten.
Meer specifiek is het doel om het \emph{kleinste} van alle vaste punten te
vinden.

In de eerste plaats moet getoond worden dat vaste punten van een bepaalde 
functie bestaan, en dat \'e\'en van hen kleiner is dan alle andere.
In de tweede plaats kunnen we demonstreren hoe dit kleinste vaste punt 
geconstrueert kan worden.

\subsubsection{Bestaan van het kleinste vaste punt}
Een geordende verzameling is een volledige tralie als voor elke deelverzameling
een element kan worden aangeduid dat ``groter'' is dan alle elementen in de
verzameling (dus ook de lege deelverzameling). 

Bijvoorbeeld, de machtsverzameling van een verzameling $A$ vormt een volledige
tralie: ze is geordend door deelverzameling-inclusie $\subseteq$, en
van elke verzameling van deelverzamelingen van $A$ kunnen we altijd een
``groter'' element aanduiden door de unie van de deelverzamelingen te nemen.

\begin{definition}[Volledige Tralie]
Een volledige tralie (eng. complete lattice) $(L, \sqsubseteq, \sqcup)$ is een
partieel geordende verzameling $(L,\sqsubseteq)$ zodat voor iedere
deelverzameling $X$ van $L$ een least upper bound (l.u.b.) $\sqcup X$ bestaat
zodat
\[
\forall z \in L, \forall x \in X: \sqcup X \sqsubseteq z \iff x \sqsubseteq z.
\]
$\sqcup X$ is hierdoor uniek gespecificeerd.
\end{definition}

De extreme gevallen van de least upper bound zijn $\sqcup \phi$ en $\sqcup L$.
Deze noteert men $\bot$ en $\top$ en spreekt men uit als ``bottom'' en ``top''.
Bovendien geldt 
\[
\forall x \in L: \bot \sqsubseteq x \sqsubseteq \top
\]

De machtsverzameling van $A$ is dus de volledige tralie ($\mathcal{P}(A)$,
$\subseteq$, $\cup$), met
$\bot = \sqcup \emptyset = \cup \emptyset = \emptyset$, 
$\top = \sqcup \mathcal{}(A) = \cup \mathcal{P}(A) = A$. 
Dus de lege verzameling is altijd $\bot$, en de verzameling $A$ is $\top$.

Parti\"ele functies op volledige tralies vormen eveneens een volledige tralie.
Stel dat de onderliggende tralie $(L, \preceq, \uplus)$ is, dan is 
$[L \rightarrow L]$ de verzameling van alle parti\"ele functies over $L$.
Als $f(\sigma)$ niet gedefini\"eerd is voor $\sigma \in L$, dan schrijven we
$f(\sigma) = \bot$. 
De volgende parti\"ele ordening $\sqsubseteq$ en l.u.b. $\sqcup$ maken dan van
$\big([L \rightarrow L], \sqsubseteq, \sqcup\big)$ een volledige tralie:

Zij $f, g \in [L \rightarrow L]$, dan is $\sqsubseteq$, met
\[
f \sqsubseteq g \iff \forall \sigma \in L: f(\sigma) \preceq g(\sigma),
\]
Voor $F \subseteq [L \rightarrow L]$ is $\sqcup F$ gedefini\"eerd als:
\begin{align*}
  &\sqcup F : L \rightarrow L\\
  &\sqcup F(\sigma) = \uplus\{f(\sigma) || f \in F\}
\end{align*}

De semantische functies in Hoofdstukken~\ref{chap:denot} en
\ref{chap:aggregate} zijn dergelijke parti\"ele functies.
Het $\bot$ element voor deze tralie is de functie die alle $\sigma$ afbeeldt
op het $\bot$ element van $L$. Het $\top$ element is de functie die alle
$\sigma$ afbeeldt op $\top$ van $L$.

Als een functie de orde van zijn argumenten bewaart, dan zeggen we dat deze
functie monotoon is. 

\begin{definition}[Monotoniciteit]
Zij $(A,\leq)$ en $(B, \preceq)$ partieel geordende sets.
Een functie $f:A\rightarrow B$ is monotoon  als en slechts als
\[
\forall x, y \in A: x \leq y \Rightarrow f(x) \preceq f(y)
\]
\end{definition}

We noteren het kleinste vaste punt van een functie $f$ als $\lfp(f)$.
Het volgende theorema stelt dat het kleinste vaste punt altijd bestaat als
$f:D\rightarrow D$ monotoon is en $D$ een volledige tralie:

\begin{theorem}[Knaster-Tarski]
\label{theorem:knaster-tarski}
Zij $f: D \rightarrow D$ een monotone functie op volledige tralie $D$, dan
bestaat het kleinste vaste punt (eng. least fixed point) $\lfp(f) \in D$ 
van $f$.
\end{theorem}

In zijn meest algemene vorm is dit bekend als \emph{het Theorema van 
Knaster-Tarski}
Voor meer informatie en het bewijs (van deze beperkte vorm), zie
\cite{scott1971}.

\subsubsection{Constructie van het kleinste vaste punt}

Een simpel idee om het kleinste vaste punt van de functie $f$ te berekenen is
door herhaaldelijk $f$ toe te passen op een bepaalde startwaarde.
Als het resultaat niet meer verandert, dan is het kleinste vaste punt gevonden.

Als het domein van $f$ een volledige tralie is, dan ligt het $\bot$ element
voor de hand als startwaarde.

De notie van een Toenemende Kleene Ketting maakt dit formeler:

\begin{definition}[Toenemende Kleene Ketting]
  \label{def:kleenechain}
  Zij $(L, \sqsubseteq, \sqcup)$ een volledige tralie, en zij
  $f : L \rightarrow L$ een functie, zodat 
  $\forall x \in L: x \sqsubseteq f(x)$, dan is de \emph{Toenemende Kleene
  Ketting} (eng. Ascending Kleene Chain) van $f$:

  \[
  \bot \sqsubseteq f(\bot) \sqsubseteq f(f(\bot)) \sqsubseteq \cdots \sqsubseteq
  f^n(\bot) \sqsubseteq \cdots
  \]

  waarbij $f^0(x) = x$, $f^{n+1}(x) = f(f^{n}(x))$, $\forall n \in \mathbb{N}$.
\end{definition}

Noteer $\kleenechain(f) = \{f^n(\bot)\:||\:n\in\mathbb{N}\}$. Merk op dat
$\kleenechain(f) \subseteq L$, dus $\sqcup\kleenechain(f)$
is uniek gedefini\"eerd.



\begin{definition}[Continuiteit]
  Zij $(L, \sqsubseteq, \sqcup)$ een volledige tralie.
  Een functie $f : L \rightarrow L$ is continu als 
  \begin{gather*}
  \forall s_0 \sqsubseteq s_1 \sqsubseteq s_2 \sqsubseteq \cdots \in L:
  f(\bigsqcup\limits_{i=0}^\infty s_i) = \bigsqcup\limits_{i=0}^\infty f(s_i)
  \end{gather*}

  Met deze notatie wordt het volgende bedoeld:
  \begin{gather*}
  \lim\limits_{n\rightarrow + \infty} f(\bigsqcup\limits_{i=0}^n s_i)
  = \lim\limits_{n\rightarrow + \infty} \bigsqcup\limits_{i=0}^n f(s_i)
  \end{gather*}

\end{definition}

Als een functie $f$ continu is, dan is het in de limiet dus eender of we eerst
de l.u.b. berekenen en dan $f$ toepassen, of eerst $f$ toepassen en dan de
l.u.b. berekenen. 
Hieruit volgt dat
\begin{align*}
f(\sqcup\kleenechain(f)) 
  &= \sqcup f(\kleenechain(f))
  &\text{ [ continuiteit $f$ ] }\\
  &= \sqcup\{f^{n+1}(\bot)\:||\:n\in\mathbb{N}\}
  &\text{ [ $f$ toepassen ] }\\
  &= \bot \sqcup\{f^{n+1}(\bot)\:||\:n\in\mathbb{N}\}
  &\text{ [ $\bot \sqcup a = a, \forall a \in L$  ] }\\
  &= \sqcup(\{\bot\} \cup \{f^{n+1}(\bot)\:||\:n\in\mathbb{N}\})
  &\\
  &= \sqcup\{f^n(\bot)\:||\:n\in\mathbb{N}\}
  &\text{ [ $f^0(\bot) = \bot$ ]}\\
  &= \sqcup\kleenechain(f)
\end{align*}

Dus $\sqcup\kleenechain(f)$ is een vast punt.

\begin{theorem}[Kleene's vaste-puntstheorema]
  \label{theorem:kleene-fixpoint}
  Als $f:L \rightarrow L$ monotoon en continu is, dan geldt:
  \[\sqcup\kleenechain(f) = \sqcup\{f^n(\bot)\:||\:n\in\mathbb{N}\} = \lfp(f).\]
\end{theorem}

\begin{proof}

Uit de bovenstaande redenering weten we dat $\sqcup\kleenechain(f)$ een vast
punt is.
Dan moet nog aangetoond worden dat dit ook het \emph{kleinste} vaste punt is.

We tonen met inductie dat voor alle vaste punten $z \in L$ van $f$ geldt:
$\forall n \in \mathbb{N}: f^n(\bot) \sqsubseteq z$.

\paragraph{basis $n = 0$}
\[ 
f^n(\bot) = f^0(\bot) = \bot \sqsubseteq z
\]

\paragraph{inductie} Veronderstel dat $f^n(\bot) \sqsubseteq z$, dan geldt:
\begin{align*}
f^n(\bot) \sqsubseteq z &\iff f(f^n(\bot)) \sqsubseteq f(z)
                        &\text{ [ $f$ is monotoon. ] }\\
                        &\iff f^{n+1}(\bot) \sqsubseteq z
                        &\text{ [ $z$ is een vast punt, dus $f(z) = z$ ] }
\end{align*}

Tenslotte volgt dan uit de definitie van de l.u.b. :
\[
  \forall n \in \mathbb{N}: f^n(\bot) \sqsubseteq z 
  \iff \forall x \in \kleenechain(f): x \sqsubseteq z
  \iff \sqcup\kleenechain(f) \sqsubseteq z
\]

\end{proof}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
