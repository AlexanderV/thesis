\chapter{Bewijzen van monotoniciteit voor denotationele semantiek}
\quotation{\--- Beware! Here be proofs.}

\section{Uitbreiding 2}
\label{app:proofuitbreiding2}
\begin{proof}[$\lambda s.\denot{C}{Prb}(s)$ is monotoon]

Door middel van inductie op de hoogte van de expressieboom $t$:
\paragraph{basis}
Als de hoogte van $t$ \'e\'en is dan is $t$ ofwel een |Fail|, |Pure x| 
of |Call|.

\begin{enumerate}

% Fail
\item Als $t = Fail$, dan is $\denot{C}{t}(s) = \phi$ ongeacht welke
  $s$, dus 
  \begin{align*}
    &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
    &\iff\text{ [Definitie monotoniciteit]}\\
%
    &s_i \subseteq s_j \Rightarrow \denot{C}{t}(s_i) \subseteq \denot{C}{t}(s_j)\\
%
    &\iff\\
%
    &s_i \subseteq s_j \Rightarrow \phi \subseteq \phi\\
%
    &\iff\\
%
    &true.
  \end{align*}

% Pure
\item Als $t = Pure\,x$, dan is $\denot{C}{t}(s) = \{x\}$ ongeacht welke
  $s$, dus
  \begin{align*}
    &\lambda s.\denot{C}{t}(s)\text{ is monotoon}\\
%
    &\iff \text{[Definitie monotoniciteit]}\\
%
    &s_i \subseteq s_j \Rightarrow \denot{C}{t}(s_i) \subseteq \denot{C}{t}(s_j)\\
%
    &\iff\\
%
    &s_i \subseteq s_j \Rightarrow \{x\} \subseteq \{x\}\\
%
    &\iff\\
%
    &true.
  \end{align*}

% Call
\item Als $t = Call$, dan $\denot{C}{t}(s) = \denot{C}{Call}(s) = s$ voor
  alle $s$, dus
  \begin{align*}
    &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
    &\iff\text{[Definitie monotoniciteit]}\\
%
    &s_i \subseteq s_j \Rightarrow \denot{C}{t}(s_i) \subseteq \denot{C}{t}(s_j)\\
%
    &\iff\\
%
    &s_i \subseteq s_j \Rightarrow s_i \subseteq s_j\\
%
    &\iff\\
%
    &true.
  \end{align*}
\end{enumerate}

\paragraph{inductie}

Als de hoogte van $t > 1$ dan is $t = Or\,a\,b$ met $a$ en $b$ deelbomen met 
een hoogte strikt kleiner dan de hoogte van $t$. 
De inductiehypothese zegt dan dat $\lambda s.\denot{C}{a}(s)$ en
$\lambda s.\denot{C}{b}(s)$ monotoon zijn. Dan:
\begin{align*}
  &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
  &\iff\text{ [Definitie monotoniciteit]}\\
%
  &s_i \subseteq s_j \Rightarrow \denot{C}{Or\,a\,b}(s_i) \subseteq \denot{C}{Or\,a\,b}(s_i)\\
%
  &\iff\text{ [Definitie $\denot{C}{.}$ ]}\\
%
  &s_i \subseteq s_j 
  \Rightarrow \Big( \denot{C}{a}(s_i) \cup \denot{C}{b}(s_i)\Big) \subseteq \Big(\denot{C}{a}(s_j) \cup \denot{C}{b}(s_j)\Big)\\
%
  &\Leftarrow\text{ [Definitie $\cup$]}\\
%
 &s_i \sqsubseteq s_j \Rightarrow \Big( \denot{C}{a}(s_i) \subseteq \denot{C}{a}(s_j)\Big)\wedge\Big(\denot{C}{b}(s_i) \subseteq \denot{C}{b}(s_j)\Big)\\
%
  &\iff\text{ [Inductie Hypothese: $\lambda s.\denot{C}{a}(s)$ en $\lambda s.\denot{C}{b}(s)$ zijn monotoon]}\\
%
  &true
\end{align*}
\end{proof}

\section{Uitbreiding 3}
\label{app:proofuitbreiding3}
\begin{proof}[$\lambda s.\lambda (q,k).\denot{C}{q(k)}(s)$ is monotoon.]
Eerst bewijzen we dat $\lambda s.\denot{C}{t}(s)$ monotoon is voor alle $t$,
door middel van inductie op de hoogte van de expressieboom $t$:

\paragraph{basis} Als de hoogte van $t$ \'e\'en is dan is $t$ ofwel een |Fail|,
|Pure x| of |Call p b|.

\begin{enumerate}
% Fail
\item Als $t = Fail$, dan is $\denot{C}{t}(s) = \phi$ ongeacht welke
  $s$, dus 
  \begin{align*}
    &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
    &\iff\text{ [Definitie monotoniciteit]}\\
%
    &s_i \sqsubseteq s_j \Rightarrow \denot{C}{t}(s_i) \subseteq \denot{C}{t}(s_j)\\
%
    & \iff\\
%
    &s_i \sqsubseteq s_j \Rightarrow \phi \subseteq \phi\\
%
    &\iff\\
%
    &true.
  \end{align*}

% Pure
\item Als $t = Pure\,x$, dan is $\denot{C}{t}(s) = \{x\}$ ongeacht welke
  $s$, dus
  \begin{align*}
    &\lambda s.\denot{C}{t}(s)\text{ is monotoon}\\
%
    &\iff\text{ [Definitie monotoniciteit]}\\
%
    &s_i \sqsubseteq s_j \Rightarrow \denot{C}{t}(s_i) \subseteq \denot{C}{t}(s_j)\\
%
    &\iff\\
%
    &s_i \sqsubseteq s_j \Rightarrow \{x\} \subseteq \{x\}\\
%
    &\iff\\
%
    &true.
  \end{align*}

% Call
\item Als $t = Call\,p\,b$ voor willekeurige $p$ en $b$, dan
  $\denot{C}{t}(s) = \denot{C}{Call\,p\,b}(s) = s(p,b)$ voor alle $s$, dus
  \begin{align*}
    &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
    &\iff\text{ [Definitie monotoniciteit]}\\
%
    &s_i \sqsubseteq s_j \Rightarrow \denot{C}{t}(s_i) \subseteq \denot{C}{t}(s_j)\\
%
    &\iff\\
%
    &s_i \sqsubseteq s_j \Rightarrow s_i(p,b) \subseteq s_j(p,b)\\
%
    &\iff\text{ [Definitie $\sqsubseteq$: $s_i \sqsubseteq s_j \Rightarrow \forall p,b: s_i(p,b) \subseteq s_j(p,b)$]}\\
%
    &true.
  \end{align*}
\end{enumerate}

\paragraph{inductie}
Als de hoogte van $t > 1$ dan is $t = Or\,a\,b$ met $a$ en $b$ deelbomen met 
een hoogte strikt kleiner dan de hoogte van $t$. 
De inductiehypothese zegt dan dat $\lambda s.\denot{C}{a}(s)$ en
$\lambda s.\denot{C}{b}(s)$ monotoon zijn. Dan:
\begin{align*}
  &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
  &\iff\text{ [Definitie monotoniciteit]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \denot{C}{|Or a b|}(s_i) \subseteq \denot{C}{|Or a b|}(s_i)\\
%
  &\iff\text{ [Definitie $\denot{C}{.}$ ]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \Big( \denot{C}{a}(s_i) \cup \denot{C}{b}(s_i)\Big) \subseteq \Big(\denot{C}{a}(s_j) \cup \denot{C}{b}(s_j)\Big)\\
%
  &\Leftarrow\text{ [Definitie $\cup$]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \Big( \denot{C}{a}(s_i) \subseteq \denot{C}{a}(s_j)\Big)\wedge\Big(\denot{C}{b}(s_i) \subseteq \denot{C}{b}(s_j)\Big)\\
%
  &\iff\text{ [Inductie Hypothese: $\lambda s.\denot{C}{a}(s)$ en $\lambda s.\denot{C}{b}(s)$ zijn monotoon]}\\
%
  &true
\end{align*}

Nu volgt:
\begin{align*}
  &\lambda s.\lambda (q, k).\denot{C}{q(k)}(s)\text{ is monotoon.}\\
%
  &\iff\text{ [Definitie monotoniciteit]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \lambda (q, k).\denot{C}{p(k)}(s_i) \sqsubseteq \lambda (q,k).\denot{C}{q(k)}(s_j)\\
%
  &\iff\text{ [Equivalentie tussen universele quantificatie en $\lambda$-abstractie]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \forall q, k: \denot{C}{q(k)}(s_i) \subseteq \denot{C}{q(k)}(s_j)\\
%
  &\iff\text{ [$\forall t:\lambda s.\denot{C}{t}(s)$ is monotoon.]}\\
%
  &true
\end{align*}
\end{proof}

\section{Uitbreiding 4}
\label{app-proofuitbreiding4}

\begin{proof}[$\lambda s.\lambda (q, k).\denot{C}{q(k)}(s)$ is monotoon.]
Eerst bewijzen we dat $\lambda s.\denot{C}{t}(s)$ monotoon is voor alle $t$,
door middel van inductie op de hoogte van de expressieboom $t$:

\paragraph{basis} Als de hoogte van $t$ \'e\'en is dan is $t$ ofwel een |Fail|,
|Pure x| of |Call p b cont|.

\begin{enumerate}
% Fail
\item Als $t = Fail$, dan is $\denot{C}{t}(s) = \phi$ ongeacht welke
  $s$, dus 
  \begin{align*}
    &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
    &\iff\text{[Definitie monotoniciteit]}\\
%
    &s_i \sqsubseteq s_j \Rightarrow \denot{C}{t}(s_i) \subseteq \denot{C}{t}(s_j)\\
%
    &\iff\\
%
    &s_i \sqsubseteq s_j \Rightarrow \phi \subseteq \phi\\
%
    &\iff\\
%
    &true.
  \end{align*}

% Pure
\item Als $t = Pure\,x$, dan is $\denot{C}{t}(s) = \{x\}$ ongeacht welke
  $s$, dus
  \begin{align*}
    &\lambda s.\denot{C}{t}(s)\text{ is monotoon}\\
%
    &\iff\text{ [Definitie monotoniciteit]}\\
%
    &s_i \sqsubseteq s_j \Rightarrow \denot{C}{t}(s_i) \subseteq \denot{C}{t}(s_j)\\
%
    &\iff\\
%
    &s_i \sqsubseteq s_j \Rightarrow \{x\} \subseteq \{x\}\\
%
    &\iff\\
%
    &true.
  \end{align*}

% Call
\item Als $t = Call\,b$ voor willekeurige $b$,
  dan 
  $\denot{C}{t}(s) = \denot{C}{Call\,p\,b\,cont}(s) = \bigcup_{c\in s(p,b)} cont(c)$ voor alle $s$, dus
  \begin{align*}
    &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
    &\iff\text{[Definitie monotoniciteit]}\\
%
    &s_i \sqsubseteq s_j \Rightarrow \denot{C}{t}(s_i) \sqsubseteq \denot{C}{t}(s_j)\\
%
    &\iff\\
%
    &s_i \sqsubseteq s_j \Rightarrow \bigcup_{c\in s_i(p,b)}\denot{C}{cont(c)}(s_i) \subseteq \bigcup_{c\in s_j(p,b)}\denot{C}{cont(c)}(s_j)\\
%
    &\Leftarrow\\
%
    &s_i \sqsubseteq s_j \Rightarrow \bigcup_{c\in s_i(p,b)}c \subseteq \bigcup_{c\in s_j(p,b)}c\\
%
    &\iff\\
%
    &s_i \sqsubseteq s_j \Rightarrow s_i(p,b) \subseteq s_j(p,b)\\
%
    &\iff\text{ [Definitie $\sqsubseteq$: $s_i \sqsubseteq s_j \Rightarrow \forall p, b:s_i(p,b) \subseteq s_j(p,b)$]}\\
%
    &true.
  \end{align*}
\end{enumerate}

\paragraph{inductie}
Als de hoogte van $t > 1$ dan is $t = Or\,a\,b$ met $a$ en $b$ deelbomen met 
een hoogte strikt kleiner dan de hoogte van $t$. 
De inductiehypothese zegt dan dat $\lambda s.\denot{C}{a}(s)$ en
$\lambda s.\denot{C}{b}(s)$ monotoon zijn. Dan:
\begin{align*}
  &\lambda s.\denot{C}{t}(s) \text{ is monotoon}\\
%
  &\iff\text{ [Definitie monotoniciteit]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \denot{C}{|Or a b|}(s_i) \subseteq \denot{C}{|Or a b|}(s_i)\\
%
  &\iff\text{ [Definitie $\denot{C}{.}$ ]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \Big( \denot{C}{a}(s_i) \cup \denot{C}{b}(s_i)\Big) \subseteq \Big(\denot{C}{a}(s_j) \cup \denot{C}{b}(s_j)\Big)\\
%
  &\Leftarrow\text{ [Definitie $\cup$]}\\
%
 &s_i \sqsubseteq s_j \Rightarrow \Big( \denot{C}{a}(s_i) \subseteq \denot{C}{a}(s_j)\Big)\wedge\Big(\denot{C}{b}(s_i) \subseteq \denot{C}{b}(s_j)\Big)\\
%
  &\iff\text{ [Inductie Hypothese: $\lambda s.\denot{C}{a}(s)$ en $\lambda s.\denot{C}{b}(s)$ zijn monotoon]}\\
%
  &true
\end{align*}

Nu volgt:
\begin{align*}
  &\lambda s.\lambda (q, k).\denot{C}{q(k)}(s)\text{ is monotoon.}\\
%
  &\iff\text{ [Definitie monotoniciteit]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \lambda (q,k).\denot{C}{q(k)}(s_i) \sqsubseteq \lambda (q,k).\denot{C}{q(k)}(s_j)\\
%
  &\iff\text{ [Equivalentie tussen universele quantificatie en $\lambda$-abstractie]}\\
%
  &s_i \sqsubseteq s_j \Rightarrow \forall q,k: \denot{C}{q(k)}(s_i) \subseteq \denot{C}{q(k)}(s_j)\\
%
  &\iff\text{ [$\forall t:\lambda s.\denot{C}{t}(s)$ is monotoon.]}\\
%
  &true
\end{align*}

\end{proof}

\section{Uitbreiding 4: Continuiteit}
\label{app:proofuitbreiding4-cont}

\begin{proof}[$\lambda s.\lambda(q,k).\denot{C}{q(k)}(s)$ is continu.]

Eerst bewijzen we met inductie dat
\[
\forall a, b \in L: 
  a \sqsubseteq b \Rightarrow 
  \denot{C}{t}(a) \cup \denot{C}{t}(b) = \denot{C}{t}(a \sqcup b)
\]
voor alle $t$ waarbij
$L=[(\bigI \rightarrow \powerI) \times \bigI \rightarrow \powerI]$ de volledige
tralie van parti\"ele functies is.

\paragraph{basis} 

\begin{itemize}

\item $t = |Fail|$ 
\[
\denot{C}{|Fail|}(a) \cup \denot{C}{|Fail|}(b) =\emptyset \cup \emptyset
                                               =\emptyset
                                               =\denot{C}{|Fail|}(a \sqcup b)
\]

\item $t = |Pure x|$
\[
\denot{C}{|Pure x|}(a) \cup \denot{C}{|Pure x|}(b) = \{x\} \cup \{x\}
                                               =\{x\}
                                               =\denot{C}{|Pure x|}(a \sqcup b)
\]
\end{itemize}

\paragraph{inductie}

Veronderstel $a \sqsubseteq b$, dan volgt dat $a \sqcup b = b$.

\begin{itemize}

\item $t = |Or x y|$
\begin{align*}
&\denot{C}{|Or x y|}(a) \cup \denot{C}{|Or x y|}(s)&\\
&=\Big(\denot{C}{x}(a) \cup \denot{C}{y}(a) \Big)
  \cup
  \Big(\denot{C}{x}(b) \cup \denot{C}{y}(b) \Big)
  &\text{ [ definitie $\denot{C}{.}$ ]}\\
&=\Big(\denot{C}{x}(a) \cup \denot{C}{x}(b) \Big)
  \cup 
  \Big(\denot{C}{y}(a) \cup \denot{C}{y}(b) \Big)&\\
&=\denot{C}{x}(a \sqcup b) \cup \denot{C}{y}(a \sqcup b)
  &\text{ [ Inductie op $x$ en $y$. ] }\\
&=\denot{C}{|Or x y|}(a \sqcup b) & \text{ [definite $\denot{C}{.}$ ]}
\end{align*}

\item $t = |Call p k cont|$
\begin{align*}
&\denot{C}{|Call p k cont|}(a) \cup \denot{C}{|Call p k cont|}(b)\\
%
&= \text{ [ definitie $\denot{C}{.}$ ] }\\
&\Bigg(\bigcup\limits_{c \in a(p,k)} \!\!\!\!\denot{C}{cont(c)}(a)\Bigg)
   \bigcup
   \Bigg(\bigcup\limits_{c \in b(p,k)} \!\!\!\!\denot{C}{cont(c)}(b)\Bigg)\\
%
&= \text{ [ $b(p,k) = (a(p,k) \cap b(p,k)) \cup (b(p,k) - a(p,k))$ ] }\\
&\Bigg(\bigcup\limits_{c \in a(p,k)}\!\!\!\!\denot{C}{cont(c)}(a)\Bigg)
 \bigcup
 \Bigg(
    \bigcup\limits_{c \in a(p,k) \cap b(p,k)}\!\!\!\!\!\!\!\!\denot{C}{cont(c)}(b)
 \Bigg)
 \bigcup
 \Bigg(
    \bigcup\limits_{c \in b(p,k) - a(p,k)}\!\!\!\!\!\!\!\!\denot{C}{cont(c)}(b)
 \Bigg)\\
%
&= \text{ [ $a \sqsubseteq b \Rightarrow a(p,k) \subseteq b(p,k)
                             \Rightarrow a(p,k) \cap b(p,k) = a(p,k)$ ] }\\
&\Bigg(
    \bigcup\limits_{c \in a(p,k)}\!\!\!\!\denot{C}{cont(c)}(a)
 \Bigg)
 \bigcup
 \Bigg(
    \bigcup\limits_{c \in a(p,k)}\!\!\!\!\denot{C}{cont(c)}(b)\Bigg)
 \bigcup
 \Bigg(
    \bigcup\limits_{c \in b(p,k) - a(p,k)}\!\!\!\!\!\!\!\!\denot{C}{cont(c)}(b)
 \Bigg)\\
%
&= \text{ [ Inductie op $cont(c)$, $a \sqcup b = b$ ] }\\
&\Bigg(\bigcup\limits_{c \in a(p,k)}\!\!\!\!\denot{C}{cont(c)}(a \sqcup b)\Bigg)
 \bigcup
 \Bigg(
 \bigcup\limits_{c \in b(p,k) - a(p,k)}\!\!\!\!\!\!\!\!\denot{C}{cont(c)}(a \sqcup b)
 \Bigg)\\
%
&= \text{ [ $b(p,k) = (a(p,k) \cap b(p,k)) \cup (b(p,k) - a(p,k))$ ] }\\
&\bigcup\limits_{c \in b(p,k)}\!\!\!\!\denot{C}{cont(c)}(a \sqcup b)\\
%
&= \text { [ definitie $\denot{C}{.}$ ] }\\
&\denot{C}{|Call p k cont|}(a \sqcup b)
\end{align*}

\end{itemize}

\paragraph{Conclusie}
\begin{align*}
&\lambda s.\lambda(q,k).\denot{C}{q(k)}(s) \text{ is continu }\\
&\iff\\
& a \sqsubseteq b \Rightarrow
  \Big(\lambda(q,k).\denot{C}{q(k)}(a)\Big) 
  \sqcup
  \Big(\lambda(q,k).\denot{C}{q(k)}(b)\Big)
  =
  \lambda(q,k).\denot{C}{q(k)}(a \sqcup b)\\
&\iff\\
&a \sqsubseteq b \Rightarrow \forall q, k:
  \denot{C}{q(k)}(a) \sqcup \denot{C}{q(k)}(b) 
  = \denot{C}{q(k)}(a \sqcup b)\\
\end{align*}

\end{proof}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
