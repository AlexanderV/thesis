% item(W,V) where W is weight, V is value
item(1, 1).
item(1, 2).
item(5, 20).

:- table knapsack(lattice(kncompare/3),_).

% kncompare(A,B,C) where C is the largest of A and B
kncompare(K1, K2, K1) :- 
  knwv(K1, _, V1),
  knwv(K2, _, V2),
  V1 > V2, !.
kncompare(_, K2, K2).

% knwv(K, W, V) where W is the weight of K, V 
% is the value of K
knwv([], 0, 0).
knwv([(WI,VI)|IS], W, V) :- 
  knwv(IS, WIS, VIS), 
  W is WI + WIS,
  V is VI + VIS.

% knapsack(KS, W) where KS is a  knapsack s.t.
% the weight of KS doesn't exceed W.
knapsack([], _).
knapsack([(WI, VI)|Items], W) :-
  item(WI,VI),
  WItems is W - WI,
  WItems >= 0,
  knapsack(Items, WItems).

