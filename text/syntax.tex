\chapter{Syntax}
\label{hoofdstuk:1}

De ``Effect Handlers'' aanpak splitst het modelleren van tabulatie op in twee
delen: syntax en effect handlers.
De syntax beschrijft de acties waaruit een getabled programma bestaat. De effect
handlers interpreteren deze acties en verlenen zo semantiek aan het programma.
Het is dus van groot belang dat de syntax voldoende algemeen of abstract is,
zodat een grote vari\"eteit aan handlers mogelijk is.

In dit hoofdstuk komt deze syntax aan bod.
De syntax wordt gespecificeerd in de vorm van Haskell data-type declaraties,
gekoppeld met ``smart'' constructors die het opbouwen van programma's 
vereenvoudigen. Gewone data-type constructors beginnen met een hoofdletter
terwijl smart constructors gewone functies zijn, en dus met een
kleine letter beginnen.

\section{\texttt{Program}: Een getabled programma}
%TODO: what about recursive definitions?
Een getabled programma bestaat uit een reeks niet-wederzijds recursieve
predicaatdeclaraties gevolgd door een expressie die van deze predicaten
gebruik maakt:

\begin{lstlisting}[caption=\texttt{Program}-syntax]
data Program p b c m a
     = Decl (Open (b -> m c)) (p -> Program p b c m a)
     | Expr (m a)

declare    = Decl
expression = Expr
\end{lstlisting}

\texttt{Program p b c m a} heeft 5 typevariabelen: \texttt{p} is het type
van de predicaten die gedefinieerd worden,
\texttt{b} is het type van het argument van een predicaat, 
\texttt{c} het type van het resultaat,
\texttt{m} het type van de monad waaruit expressies bestaan, en
\texttt{a} is het type van de uiteindelijke waarde van het getabled programma.

Een \texttt{Program} kan enerzijds een declaratie \texttt{Decl} zijn, met twee
argumenten, of anderzijds een expressie \texttt{Expr} die een berekening van het
type \texttt{m a} omvat.

In het geval van een declaratie is het eerste argument de ``body'' van het
predicaat. Dit is een open recursieve functie.
Het tweede argument is het vervolg van het programma, verborgen achter een
functie die een predicaat (type \texttt{p}) als argument verwacht.

De smart constructors \texttt{declare} en \texttt{expression} zijn hier slechts
synoniemen voor \texttt{Decl} en \texttt{Expr}. Per conventie gebruiken de
getabled programma's enkel de smart constructors.

In het volgende voorbeeld wordt de fibonacci reeks als een open recursieve
functie gedefinieerd. Daarna volgt het eigenlijke programma, waar twee
predicaten gedefinie\"erd worden (beide met als body \texttt{fibonacci}), 
waarna een expressie volgt die de pure waarde \texttt{()} teruggeeft.

%TODO verify example in ghci
\begin{lstlisting}[caption=Een voorbeeld van een getabled programma.]
fibonacci :: Applicative m => Open (Int -> m Int)
fibonacci _ 0 = pure 0
fibonacci _ 1 = pure 1
fibonacci i super = (+) <$> super (i-1) <*> super (i-2)

theProgram :: Applicative m => Program p Int Int m ()
theProgram = declare fibonacci $ \p -> 
             declare fibonacci $ \q ->
             expression $ pure ()
\end{lstlisting}

De \texttt{Program}-syntax zoals deze hier is voorgesteld heeft twee belangrijke
limitaties. Ten eerste, alle predicaten moeten hetzelfde argumentstype 
(\texttt{b}) en resultaatstype(\texttt{c}) hebben. Ten tweede kunnen geen
wederzijds recursieve predicaten worden gedeclareerd. 
Een predicaat kan alleen predicaten oproepen die al gedeclareerd zijn. In
het voorgaande voorbeeld, zou \texttt{q} het predicaat \texttt{p} kunnen
gebruiken, maar niet omgekeerd. Dit is geen op uniek probleem.
In oudere programmeertalen zoals bijvoorbeeld C zijn \emph{forward-declarations}
van wederzijds recursieve procedures noodzakelijk. In modernere talen (zoals
Python of Haskell) is dit niet meer het geval. Een C compiler kan hiermee
een extra compiler-pass uitsparen, ten koste van de programmeur.

Een expressie \texttt{pure ()} levert geen nuttig resultaat. De volgende sectie
bespreekt een uitbreiding van de syntax waarmee interessantere programma's te
schrijven zijn.

\section{\texttt{Table}: Een getablede berekening}
Fundamenteel is een getablede berekening niets anders dan een 
niet-deterministische (backtrackende) berekening die getablede predicaten
kan oproepen.

Het volgende datatype beschrijft een gewone niet-deterministische berekening:
\begin{lstlisting}[caption=\texttt{Backtrack}-syntax]
data Backtrack a
     = Or (Backtrack a) (Backtrack a)
     | Fail
     | Pure a
\end{lstlisting}
\texttt{Pure a} is een succesvolle berekening met resultaat a, \texttt{Fail} is
een mislukte berekening, en \texttt{Or} geeft een niet-deterministische keuze
aan.

Een belangrijk voordeel van deze structuur is dat hiervoor een correcte
\texttt{Monad} instance kan worden gedefinieerd. Dit laat toe om deze
niet-deterministische berekeningen op een elegante manier samen te stellen,
zoals uit het voorbeeld in listing~\ref{lst:tabledfib} zal blijken.
%TODO verify listing
\begin{lstlisting}[caption=\texttt{Monad}-instance voor \texttt{Backtrack}.]
instance Monad Backtrack where
  return x = Pure x
  Pure x       >>= f = f x
  Fail         >>= _ = Fail
  (b1 `Or` b2) >>= f = Or (b1 >>= f) (b2 >>= f)

\end{lstlisting}
\texttt{return} fungeert hier als een smart constructor voor
 \texttt{Pure}. 

De \texttt{empty} en \texttt{<|>} functies uit de
\texttt{Alternative} typeclass vervullen een eenzelfde rol voor 
\texttt{Fail} en \texttt{Or}.

\begin{lstlisting}[caption=
\texttt{Alternative}-instance voor \texttt{Backtrack}.]
instance Alternative Backtrack where
  empty = Fail
  (<|>) = Or
\end{lstlisting}

In een volgende stap breiden we dit datatype uit met een 
\texttt{Call}-constructor voor oproepen van een predicaat:

\begin{lstlisting}[caption=\texttt{Table}-syntax]
data Table p b c a
     = Call p b (c -> Table p b c a)
     | Or (Table p b c a) (Table p b c a)
     | Fail
     | Pure a

call :: p -> b -> Table p b c c
call p b = Call p b Pure
\end{lstlisting}

In de datatype declaratie verschijnen nu ook de parameters \texttt{p},
\texttt{b} en \texttt{c} zoals bij \texttt{Program} het geval was. 
Hun betekenis is onveranderd: \texttt{p} is het type van het predicaat,
\texttt{b} het argument en \texttt{c} het resultaat.

Een \texttt{Call} heeft drie argumenten: het predicaat \texttt{p}, het
argument van deze aanroep \texttt{b}, en het vervolg van de \emph{berekening}
(de continuatie) dat afhankelijk is van het resultaat van de oproep.

Eveneens wordt de smart constructor \texttt{call} ge\"introduceerd. 
\texttt{call} zal gegeven een predicaat en een argument een berekening 
construeren die simpelweg het resultaat van de oproep teruggeeft en verder
niets doet.

Voor \texttt{Table} kan ook een \texttt{Monad} en een \texttt{Alternative}
instance worden gedefinieerd:
\begin{lstlisting}[caption=\texttt{Monad}- en \texttt{Alternative}-instance
voor \texttt{Table}]
instance Monad (Table p b c) where
  return a = Pure a
  (Pure a)        >>= f = f a
  (Or t1 t2)      >>= f = Or (t1 >>= f) (t2 >>= f)
  Fail            >>= _ = Fail
  (Call p b cont) >>= f = Call p b (\x -> cont x >>= f)

instance Alternative (Table p b c) where
  empty = Fail
  (<|>) = Or
\end{lstlisting}

Het volgende programma combineert \texttt{Program} en \texttt{Table}:
%TODO test program
\begin{lstlisting}[caption={Een voorbeeld van een getablede berekening van
het i-de fibonacci getal in een getabled programma.}, label=lst:tabledfib]
fibonacciND :: Open (Int -> Table p Int Int Int)
fibonacciND super 0 = return 0
fibonacciND super 1 = return 1 <|> return (-1)
fibonacciND super i = 
  (+) <$> super (i - 1) <*> super (i - 2)

theProgram i = declare fibonacciND $ \p ->
               expression $ do x <- call p i
                               return (x - 1)
\end{lstlisting}

Dit voorbeeld demonstreert goed hoe \texttt{do}-notatie toelaat om gemakkelijk
programma's te schrijven.

Met de tot nu toe besproken smart constructors (inclusief diegene uit
instance-definities) is het mogelijk alle getablede programma's die geen
wederzijdse recursie van getablede predicaten toelaten te beschrijven.
Merk op dat al deze operaties polymorf zijn in het type van het predicaat
\texttt{p}. Met andere woorden, de operaties zijn volledig
onafhankelijk van de structuur van \texttt{p}. Er kan van de programmeur
hetzelfde worden afgedwongen door te eisen dat het programma het volgende
type heeft:
%TODO think about this, does polymorphic Table make sense?
\begin{lstlisting}[caption=\texttt{TabledProgram}-syntax]
newtype TabledProgram b c a = TP {
  unTP :: forall p . Program p b c (Table p b c) a
  }
\end{lstlisting}

Het vorige voorbeeld wordt dan:
\begin{lstlisting}[caption=Een berekening van het i-de fibonacci getal in een
\texttt{TabledProgram}.]
fibonacciND :: Open (Int -> Table p Int Int Int)
fibonacciND super 0 = return 0
fibonacciND super 1 = return 1 <|> return (-1)
fibonacciND super i = 
  (+) <$> super (i - 1) <*> super (i - 2)

theProgram :: TabledProgram Int Int Int
theProgram i = TP $ declare fibonacciND $ \p ->
                    expression $ do x <- call p i
                                    return (x - 1)
\end{lstlisting}

\section{Conclusie}
De programmeur zal steeds zijn programma opstellen in het type
\texttt{TabledProgram b c a}. Hierbij wordt enkel gebruik gemaakt van de
smart constructors en wat Haskell verder aan taalconstructies te bieden heeft.
De syntax is eenvoudig, wat de programmeur en de effect handlers
ten goede komt.



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
