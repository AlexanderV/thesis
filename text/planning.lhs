\chapter{Planning en Toekomstig werk}
\label{chap:futurework}
De volgende stap in dit project is om een performante interpreter te voorzien
voor de \texttt{Table}-syntax.

Om de performantie inschatting relevant te maken moet gekeken worden naar het
cree\"eren van een benchmark gebaseerd op realistische toepassingen.

Er is ook al een basisstap gezet in de richting van een multithreaded
intepreter. 
Dit is interessant omdat Haskell eerste klas ondersteuning heeft voor
multithreading.

De huidige syntax vereist dat alle getablede predicaten worden
gedefini\"eerd \emph{voor} de effectieve getablede berekening.
In een flexibelere systeem zijn andere interessante varianten denkbaar.
In zekere zin is het de bedoeling dat Tabulatie in Haskell zijn ontstaan
uit Prolog met Tabulatie voorbijstreeft.

Tenslotte kan Tabulatie  worden uitgebreid met constructies voor het berekenen
van aggregaten of kansen in plaats van niet-determinisme.
Met aggregaten kan bijvoorbeeld het minimum of maximum van alle oplossingen
van een berekening gevonden worden.
Door aan iedere oplossing van een niet-deterministische berekening een kans toe
te voegen, wordt het uiteindelijke resultaat een kansverdeling in plaats van
een lijst oplossingen.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 

