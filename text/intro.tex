\chapter{Backtracking en Event handlers}
\label{hoofdstuk:inleiding}
In dit hoofdstuk worden Backtrackende berekeningen besproken. Dit dient eveneens
als een illustratie de ``Event Handlers'' aanpak. In eerste instantie wordt
backtracking opgebouwd met gewone lijsten, met de bijbehordende
\texttt{Monad}-instance.
Daarna wordt overgegaan naar een constructie gebaseerd op syntax en handlers.
Hiervoor kan eveneens een \texttt{Monad}-instance gedefini\"eerd worden.
Het hoofdstuk besluit met een aantal handlers voor deze structuur.

\section{Inleiding}
Een backtrackende berekening kan nul, \'{e}\'{e}n of meerdere resultaten 
teruggeven. Bijvoorbeeld, veronderstel een functie \texttt{assoc} die, gegeven
een lijst key-value paren en een key, de waarden die bij de key horen
teruggeven.
De uitdrukking
\begin{lstlisting}
assoc [('a',1),('b', 2), ('a', 3)] 'a'
\end{lstlisting}
moet dan de waarden 1 en 3 teruggeven, en
\begin{lstlisting}
assoc [('a',1),('b', 2), ('a', 3)] "c"
\end{lstlisting}
zal geen resultaten opleveren. De berekening is \emph{gefaald}.
Backtrackende berekeningen kunnen op twee manieren gecombineerd worden.

Enerzijds bestaat er OF-combinatie. 
Op deze manier combineren van twee berekeningen \texttt{b1} en \texttt{b2}
geeft een nieuwe berekening die eerst alle resultaten van \texttt{b1} geeft en
vervolgens alle resultaten van {b2}.
De combinator \texttt{choice} zal OF-combinatie weergeven in de code. 
Bijvoorbeeld,
\begin{lstlisting}
assoc [('a',1)] 'a' `choice` assoc [('b',2)] 'b'
\end{lstlisting}
geeft 1 en 2 terug. De berekening biedt een \emph{niet-deterministische keuze}.

Anderzijds is er de EN-combinatie. EN-combinatie bestaat meestal impliciet in
een operatie die over twee (of meer) backtrackende berekeningen wordt
uitgevoerd.
De gecombineerde berekening voert de operatie uit voor iedere combinatie van
de oplossingen van de oorspronkelijke berekeningen. 
Bijvoorbeeld (plus neemt de som):
\begin{lstlisting}
plus (assoc [('a',1), ('a',4)]) 'a') (assoc [('b',2)] 'b')
\end{lstlisting}
geeft 3 en 6.
Een EN-combinatie van twee berekeningen met respectievelijk $n_1$ en $n_2$
resultaten heeft dus $n_1$ x $n_2$ resultaten.
Als \'{e}\'{e}n van de oorspronkelijke berekeningen geen resultaten heeft, 
dan heeft de combinatie duidelijk ook geen oplossingen.

\section{Backtracking met lijsten}
Backtrackende berekeningen kunnen vervangen worden door berekeningen die lijsten
teruggeven \cite{wadler1985}.
Een gefaalde berekening geeft de lege lijst (\texttt{[]}). Een berekening met
\'{e}\'{e}n  of meer oplossingen geeft een lijst met \'{e}\'{e}n of meer
elementen.

De functie \texttt{assoc} kan eenvoudig met \emph{list comprehensions}
gedefini\"eerd worden:
\begin{lstlisting}
assoc :: Eq k => [(k, v)] -> k -> [v]
assoc l k = [y | (x, y) <- l, x == k]
\end{lstlisting}

OF-combinatie is concatenatie(\texttt{++}) van lijsten:
\begin{lstlisting}
assoc [('a',1)] 'a' ++ assoc [('b',2)] 'b'
\end{lstlisting}
geeft \texttt{[1,2]}.

De list comprehensions volstaan ook voor EN-combinatie:
\begin{lstlisting}
plus b1 b2 = [x1 + x2 | x1 <- b1, x2 <- b2]
\end{lstlisting}, zodat
\begin{lstlisting}
plus (assoc [('a',1), ('a',4)]) 'a') (assoc [('b',2)] 'b')
\end{lstlisting}
\texttt{[3,6]} geeft.

\paragraph{\texttt{Alternative} en \texttt{Monad}}
Backtrackende berekeningen kunnen gevat worden in de volgende twee typeclasses:
\begin{lstlisting}
class Applicative f => Alternative f where
  empty :: f a
  (<|>) :: fa -> f a -> f a

class Monad m where
  return :: m a
  (>>=)  :: (a -> m b) -> m a -> m b

instance Alternative [] where
  empty = []
  (<|>) = (++)

instance Monad [] where
  return a = [a]
  l >>=  f = [x | x <- f y, y <- l]
\end{lstlisting}

\texttt{Alternative} voorziet in twee functies: \texttt{empty} en \texttt{<|>}
die respectievelijk mislukking en keuze uitdrukken.
\texttt{Monad} definieerd \texttt{return} (voor \'{e}\'{e}n enkel resultaat) en
\texttt{>>=} (voor EN-combinatie). Een \texttt{Monad}-instance garandeerd dat
een \texttt{Applicative}-instance bestaat, zodat de \texttt{Applicative}
constraint van de \texttt{Alternative} klasse voldaan is.

\texttt{assoc} kan nu herschreven worden als:
\begin{lstlisting}
assoc l k = l >>= \(k', v) -> if k == k' then return v else empty
\end{lstlisting}
Of, door gebruik te maken van \texttt{do}-notatie:
\begin{lstlisting}
assoc l k = do (k', v) <- l
               if k == k' then return v else empty
\end{lstlisting}
\texttt{choice} en \texttt{plus} zijn in deze notatie:
\begin{lstlisting}
choice = (<|>)
plus e1 e2 = do v1 <- e1
                v2 <- e2
                return (v1 + v2)
\end{lstlisting}

Vanaf nu worden backtrackende berekeningen alleen nog maar beschreven in termen
van \texttt{empty}, \texttt{return}, \texttt{<|>} en \texttt{>>=} (of
\texttt{do}-notatie).
Dit laat toe om backtrackende berekeningen te beschrijven met functies die
een ander datatype teruggeven (verschillend van lijsten), zolang deze de
voornoemde operaties maar ondersteunen.  

\section{Effect Handlers}
De ``Effect Handlers''-aanpak \cite{haskell2014} splits het modelleren van
backtracking op in twee delen: syntax en effect handlers. 
De syntax beschrijft de acties waaruit een backtrackende berkening bestaat.
De effect handlers interpreteren deze acties en verlenen zo semantiek aan
de berekening.

\paragraph{syntax} De syntax bestaat uit het volgende datatype:
\begin{lstlisting}
data Backtrack a
     = Or (Backtrack a) (Backtrack a)
     | Fail
     | Pure a
\end{lstlisting}
\texttt{Pure a} is een successvolle berekening met resultaat a, \texttt{Fail}
is een mislukte berekening en \texttt{Or} geeft een niet-deterministische keuze
aan.

Op dit datatype kunnen we nu de \texttt{Alternative} en \texttt{Monad}-instances
defini\"eren:
\begin{lstlisting}
instance Alternative Backtrack where
  empty = Fail
  (<|>) = Or

instance Monad Backtrack where
  return a = Pure a
  (Pure a)   >>= f = f a
  Fail       >>= _ = Fail
  (Or b1 b2) >>= f = Or (b1 >>= f) (b2 >>= f)
\end{lstlisting}

Concrete backtrackende programma's worden steeds geschreven met de operaties
\texttt{empty}, \texttt{return}, \texttt{<|>} en \texttt{>>=}.

De effect handlers interpreteren waarden van het type \texttt{Backtrack a}. 
Men kan dus de operaties beschouwen als een \emph{concrete}
syntax, terwijl het datatype een \emph{abstracte} syntax is.

\paragraph{Effect handlers} De semantiek wordt volledig bepaald door de
Effect Handler. Verschillende Effect Handlers kunnen dus ook sterk verschillende
semantiek hebben. De volgende handler interpreteert een \texttt{Backtrack a}
zoals voorheen als een functie die een lijst teruggeeft.
\begin{lstlisting}
asList :: Backtrack a -> [a]
asList (Pure a)   = [a]
asList Fail       = []
asList (Or b1 b2) = asList b1 ++ asList b2
\end{lstlisting}

Een andere mogelijkheid is een prettyprinter.
\begin{lstlisting}
prettyPrint :: Show a => Backtrack a -> IO ()
prettyPrint (Pure a)   = print a
prettyPrint Fail       = putStrLn "Fail"
prettyPrint (Or b1 b2) = do putStrLn "("
                            prettyPrint b1
                            putStrLn ") Or ("
                            prettyPrint b2
                            putStrLn ")"
\end{lstlisting}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
