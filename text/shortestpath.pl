% a directed weighted graph: edge(Node1,Node2,Cost).
edge(a,b,1).
edge(a,e,1).
edge(b,c,1).
edge(c,a,22).
edge(c,d,1).
edge(d,a,1).
edge(d,c,1).
edge(e,e,1).

min(X,Y,Z) :- Z is min(X,Y).

% shortest path predicate
:- table sp(_,_,lattice(min/3)).
sp(N,N,0).
sp(N1,N2,C) :- edge(N1,N2,C).
sp(N1,N2,Cost) :- sp(N1,N3,C1), edge(N3,N2,C2), Cost is C1 + C2.
