\chapter{Backtracking en Effect Handlers}
\label{hoofdstuk:backtrackingandeventhandlers}
Dit hoofdstuk bespreekt Backtrackende berekeningen. Dit zijn berekeningen die
nul of meerdere resultaten hebben. De concepten uit dit hoofdstuk vormen een
basis voor getablede berekening die in latere hoofdstukken aan bod komt.

In eerste instantie wordt backtracking opgebouwd met gewone lijsten.
Daarna volgt een implementatie van backtracking die de
``Effect Handlers''-aanpak illustreert. 
``Effect Handlers'' deelt het modeleren van backtracking op in een abstracte
syntax en concrete handlers. Een backtrackende berekening bestaat dan uit
twee fasen: eerst wordt een abstracte syntaxboom opgebouwd. Vervolgens 
interpreteert een handler de abstracte syntax. De interpretatie heeft een
bepaald ``Effect''.

\section{Inleiding}
Een backtrackende berekening kan nul, \'{e}\'{e}n of meerdere resultaten 
teruggeven. Bijvoorbeeld, veronderstel een functie \texttt{assoc}. Gegeven
een lijst key-value paren en een key, zal \texttt{assoc} de waarden die bij
de key horen teruggeven.

De uitdrukking
\begin{code}
assoc [('a', 1),('b', 2), ('a', 3)] 'a'
\end{code}
moet dan de waarden 1 en 3 teruggeven, en
\begin{code}
assoc [('a',1),('b', 2), ('a', 3)] 'c'
\end{code}
zal geen resultaten opleveren. De berekening is \emph{gefaald}.
Backtrackende berekeningen kunnen op twee manieren gecombineerd worden.

Enerzijds bestaat er OF-combinatie. 
Op deze manier combineren van twee berekeningen \texttt{b1} en \texttt{b2}
geeft een nieuwe berekening die eerst alle resultaten van \texttt{b1} geeft en
vervolgens alle resultaten van {b2}.
De combinator \texttt{choice} zal OF-combinatie weergeven in de code. 
Bijvoorbeeld,
\begin{code}
assoc [('a',1)] 'a' `choice` assoc [('b',2)] 'b'
\end{code}
geeft 1 en 2 terug. De berekening biedt een \emph{niet-deterministische keuze}.

Anderzijds is er de EN-combinatie. EN-combinatie bestaat meestal impliciet in
een operatie die over twee (of meer) backtrackende berekeningen wordt
uitgevoerd.
De gecombineerde berekening voert de operatie uit voor iedere combinatie van
de oplossingen van de oorspronkelijke berekeningen. 
Bijvoorbeeld (plus neemt de som):
\begin{code}
assoc [('a',1), ('a',4)] 'a' `plus` assoc [('b',2)] 'b'
\end{code}
geeft 3 en 6.
Een EN-combinatie van twee berekeningen met respectievelijk $n_1$ en $n_2$
resultaten heeft dus $n_1$ x $n_2$ resultaten.
Als \'{e}\'{e}n van de oorspronkelijke berekeningen geen resultaten heeft, 
dan heeft de combinatie duidelijk ook geen oplossingen.

\section{Backtracking met lijsten}
Backtrackende berekeningen kunnen vervangen worden door berekeningen die lijsten
teruggeven \cite{wadler1985}.
Een gefaalde berekening geeft de lege lijst (\texttt{[]}). Een berekening met
\'{e}\'{e}n  of meer oplossingen geeft een lijst met \'{e}\'{e}n of meer
elementen.

De functie \texttt{assoc} kan eenvoudig met \emph{list comprehensions}
gedefini\"eerd worden. List comprehensions zijn een notatie voor het construeren
van lijsten, gebaseerd op de wiskunde \emph{set comprehensions}. List
comprehensions hebben de vorm |[f || y <- l, guard]|. Waar |l| een lijst is,
|y| een variable, |f| een uitdrukking waarin |y| een vrije variable is
en |guard| een booleaanse uitdrukking.
\begin{code}
assoc :: Eq k => [(k, v)] -> k -> [v]
assoc l k = [y | (x, y) <- l, x == k]
\end{code}

OF-combinatie is concatenatie(|++|) van lijsten:
\begin{code}
assoc [('a',1)] 'a' ++ assoc [('b',2)] 'b'
\end{code}
geeft \texttt{[1,2]}.

De list comprehensions volstaan ook voor EN-combinatie:
\begin{code}
plus b1 b2 = [x1 + x2 | x1 <- b1, x2 <- b2]
\end{code}, zodat
\begin{code}
assoc [('a',1), ('a',4)] 'a' `plus` assoc [('b',2)] 'b'
\end{code}
\texttt{[3,6]} geeft.

\paragraph{\texttt{Alternative} en \texttt{Monad}}
Backtrackende berekeningen kunnen gevat worden in de volgende twee typeclasses:
\begin{code}
class Applicative f => Alternative f where
  empty  :: f a
  (<|>)  :: fa -> f a -> f a

class Monad m where
  return :: m a
  (>>=)  :: (a -> m b) -> m a -> m b

instance Alternative [] where
  empty  = []
  (<|>)  = (++)

instance Monad [] where
  return a  = [a]
  l >>= f   = [x | x <- f y, y <- l]
\end{code}

\texttt{Alternative} voorziet in twee functies: \texttt{empty} en \texttt{<||>}
die respectievelijk mislukking en keuze uitdrukken.
\texttt{Monad} definieerd \texttt{return} (voor \'{e}\'{e}n enkel resultaat) en
|>>=| (voor EN-combinatie). Een \texttt{Monad}-instance garandeerd dat
een \texttt{Applicative}-instance bestaat, zodat de \texttt{Applicative}
constraint van de \texttt{Alternative} klasse voldaan is.

\texttt{assoc} kan nu herschreven worden als:
\begin{code}
assoc l k = l >>= \(k', v) -> if k == k' then return v else empty
\end{code}
Of, door gebruik te maken van \texttt{do}-notatie:
\begin{code}
assoc l k = do
 (k', v) <- l
 if k == k' then return v else empty
\end{code}
\texttt{choice} en \texttt{plus} zijn in deze notatie:
\begin{code}
choice = (<|>)
plus e1 e2 = do
  v1 <- e1
  v2 <- e2
  return (v1 + v2)
\end{code}

Vanaf nu worden backtrackende berekeningen alleen nog maar beschreven in termen
van \texttt{empty}, \texttt{return}, \texttt{<||>} en |>>=| (of
\texttt{do}-notatie).
Dit laat toe om backtrackende berekeningen te beschrijven met functies die
een ander datatype teruggeven (verschillend van lijsten), zolang deze de
voornoemde operaties maar ondersteunen.  

\section{Effect Handlers}
De ``Effect Handlers''-aanpak \cite{haskell2014} splits het modelleren van
backtracking op in twee delen: syntax en effect handlers. 
De syntax beschrijft de acties waaruit een backtrackende berkening bestaat.
De effect handlers interpreteren deze acties en verlenen zo semantiek aan
de berekening.

\paragraph{syntax} De syntax bestaat uit het volgende datatype:
\begin{code}
data Backtrack a  = Or (Backtrack a) (Backtrack a)
                  | Fail
                  | Pure a
\end{code}
\texttt{Pure a} is een successvolle berekening met resultaat a, \texttt{Fail}
is een mislukte berekening en \texttt{Or} geeft een niet-deterministische keuze
aan.

Op dit datatype kunnen we nu de \texttt{Alternative} en \texttt{Monad}-instances
defini\"eren:
\begin{code}
instance Alternative Backtrack where
  empty = Fail
  (<|>) = Or

instance Monad Backtrack where
  return a = Pure a
  (Pure a)   >>= f = f a
  Fail       >>= _ = Fail
  (Or b1 b2) >>= f = Or (b1 >>= f) (b2 >>= f)
\end{code}

Concrete backtrackende programma's worden steeds geschreven met de operaties
\texttt{empty}, \texttt{return}, \texttt{<||>} en |>>=|.

De effect handlers interpreteren waarden van het type \texttt{Backtrack a}. 
Men kan dus de operaties beschouwen als een \emph{concrete}
syntax, terwijl het datatype een \emph{abstracte} syntax is.

\paragraph{Effect handlers} De semantiek wordt volledig bepaald door de
Effect Handler. Verschillende Effect Handlers kunnen dus ook sterk verschillende
semantiek hebben. De volgende handler interpreteert een \texttt{Backtrack a}
zoals voorheen als een functie die een lijst teruggeeft.
\begin{code}
asList :: Backtrack a -> [a]
asList (Pure a)    = [a]
asList Fail        = []
asList (Or b1 b2)  = asList b1 ++ asList b2
\end{code}

Het splitsen van syntax en semantiek geeft een sterke
\emph{separation of concern}. De syntax heeft geen weet van de Effect Handlers.
De Effect Handlers hebben weliswaar weet van de syntax, maar niet van elkaar.
Indien de syntax goed gekozen is, zal deze ook erg stabiel zijn.

De syntax staat ook semantiek toe die met lijsten niet mogelijk waren, zonder
dat de syntax moet worden aangepast. Bijvoorbeeld een \emph{pretty printer}.
\begin{code}
prettyPrint :: Show a => Backtrack a -> IO ()
prettyPrint (Pure a)    = print a
prettyPrint Fail        = putStrLn "Fail"
prettyPrint (Or b1 b2)  = do
  putStrLn "("
  prettyPrint b1
  putStrLn ") Or ("
  prettyPrint b2
  putStrLn ")"
\end{code}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
