\chapter{Bewijs Theorema~\ref{theorem:homomorph-set-to-agg}} 
\label{app:proof-theo-mmorph}

\begin{lemma} 
\label{lemma:app-agg-lemma}
  $\forall A,B \in \mathcal{P(\aggregateI)}:
  |lub| (A \cup B) = (|lub|A) |lub| (|lub| B)$
\end{lemma}

\begin{proof}
  \begin{align*}
    \forall z \in \aggregateI:\:
    &|lub|(A \cup B) |=<=| z\\
    &\iff \text{[ def. |lub| ]}\\
    &\forall x \in A \cup B: x |=<=| z\\
    &\iff\text{[ def. $\cup$ ]}\\
    &\forall x \in A: x |=<=| z \wedge \forall x \in B: x |=<=| z\\
    &\iff\text{[ def. |lub| ]}\\
    &|lub|A |=<=| z \wedge |lub|B |=<=| z\\
    &\iff\text{[ def. |lub| ]}\\
    &(|lub|A) |lub| (|lub| B) |=<=| z
  \end{align*}
  
  Dan volgt dankzij de reflexiviteit van |=<=|:
  \begin{align}
    |lub|(A \cup B) |=<=| |lub|(A \cup B) 
    &\Rightarrow |lub|(A \cup B) |=<=| (|lub|A) |lub| (|lub| B)
    \label{eq:app-agg-lemma-1}\\
%
    (|lub|A) |lub| (|lub| B) |=<=| (|lub|A) |lub| (|lub| B)
    &\Rightarrow(|lub|A) |lub| (|lub| B) |=<=| |lub| (A \cup B).
    \label{eq:app-agg-lemma-2}
  \end{align}

  Tenslotte volgt door de anti-symmetrie van |=<=|:
  \[
    \eqref{eq:app-agg-lemma-1} \wedge \eqref{eq:app-agg-lemma-2}
    \Rightarrow
    |lub|(A \cup B) = (|lub|A) |lub| (|lub| B).
  \]
\end{proof}

\begin{theorem*}
$h(A) = |lub|\{|coerce|c\:||\:c \in A\}$ is een monoid-homomorfisme van de
monoid $(\powerI, \phi, \cup)$ naar de monoid $(\aggregateI, |bottom|, |lub|)$.
\end{theorem*}

\begin{proof}

Opdat $h : \powerI \rightarrow \aggregateI$ een monoid-homomorfisme zou zijn,
moet gelden dat $h(\phi) = |bottom|$ en $h(A \cup B) = h(A) |lub| h(B)$.

\begin{itemize}

\item
 $h(A)$ bewaart het lege element:
  \[h(\phi) = |lub|\{|coerce| c\:||\:c \in \phi\} = |lub|\phi = |bottom|\]

\item
$h(A)$ bewaart samenvoeging:

  \begin{align*}
    h(A \cup b)
    &= |lub|\{|coerce| c\:||\:c \in A \cup B\}\\
    &= |lub|(\{|coerce| c\:||\:c \in A\} \cup \{|coerce| c\:||\:c \in B\})\\
    &= (|lub|\{|coerce| c\:||\:c \in A\})
       |lub|
       (|lub|\{|coerce| c\:||\:c \in B\})
       \text{ [ Lemma~\ref{lemma:app-agg-lemma} ]}\\
    &= h(A) |lub| h(B)
  \end{align*}

\end{itemize}

\end{proof}

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
