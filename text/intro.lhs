\chapter{Inleiding}

\section{Over deze thesis}
Eindigt het volgende Haskell programma?

> p :: [(Int, Int)]
> p = (1,2):[(y,x) | (x,y) <- p]
< >>> p
< [(1,2),(2,1),(1,2),(2,1),...]
Eindigt het volgende equivalente Tabled-Prolog programma?

\begin{lstlisting}
:- table p/2.
p(1,2).
p(Y,X) :- p(X,Y).

?- p(A,B).
A = 2
B = 1;

A = 1
B = 2;

no.
\end{lstlisting}

Het eerste voorbeeld geeft een oneindige lijst van antwoorden, hoewel er maar
twee verschillende antwoorden mogelijk zijn (|(1,2)| en |(2,1)|).
Het tweede voorbeeld geeft maar twee antwoorden  (\texttt{A=1,B=2} en
\texttt{A=2,B=1}) en stopt dan.
Dit gedrag wordt veroorzaakt door de \texttt{:- table p/2} directieve, die
aanduidt dat \emph{tabulatie} moet gebruikt worden om het predicaat \texttt{p/2}
op te lossen.

\paragraph{Tabulatie}
Tabulatie is een techniek uit de wereld van het logisch programmeren, die een
aantal tekortkoming van de logische programmeertaal Prolog oplost.

Logische programma's bestaan uit \emph{clauses} die relaties tussen variabelen
en constanten vastleggen in predicaten.
Berekening vloeit voort uit het oplossen van \emph{goals}.
Dit zijn uitdrukkingen die bestaan uit een predicaat, constanten en variabelen.
Het oplossen van de goal houdt in dat Prolog op zoek gaat naar concrete waarden
voor de variabelen in de goal die niet strijdig zijn met de clauses.

In Prolog is het mogelijk om recursieve predicaten te defini\"eren.
Tijdens het berekenen van goals over recursieve predicaten kan Prolog
oneindig veel oplossingen genereren, waar er maar eindig veel
\emph{verschillende} oplossingen bestaan.
Bereikbaarheid (reachability) in een eindige \emph{cyclische} grafe is hiervan
een concreet voorbeeld.
Prolog \emph{met} Tabulatie onthoudt de reeds geziene oplossingen, en stopt de
berekening wanneer er geen nieuwe oplossingen meer bijkomen.

\paragraph{Haskell}
Haskell is een moderne, sterk getypeerde en pure functionele programmeertaal.
Haskell wordt vaak gebruikt voor onderzoek naar programmeertalen,
en kent een bescheiden, maar groeiende populariteit in de industrie.

In Haskell kunnen we eveneens gebruik maken van logisch programmeren, door
concepten uit Prolog over te brengen naar Haskell.

Om Prolog te kunnen bestuderen binnen Haskell moet Prolog in eerste instantie
tot zijn essentie herleid worden.
Elementen als unificatie, negatie (negation as failure) en cuts worden
achterwege gelaten.
Wat overblijft zijn backtrackende berekeningen. 
Backtrackende berekeningen zijn berekeningen die niet-deterministische keuze en
faling ondersteunen. 
Bijvoorbeeld, wanneer meerdere clauses toepasselijk zijn voor een goal, dan
zal Prolog de clause die eerst voorkomt eerst proberen.
Als deze keuze tot een mislukking leidt, dan keert Prolog op zijn schreden terug
(het ``backtrackt'') en probeert de volgende clause.

\begin{quote}
Als Prolog zonder Tabulatie wordt vertaald als Backtracking,
dan moet Prolog met Tabulatie ook vertaald kunnen worden als
Backtracking \emph{met} Tabulatie. 
Hiervan kan een Haskell implementatie worden afgeleid.
\end{quote}

Dit is het kernidee van deze thesis.

De analyse van Backtracking met Tabulatie wordt eerst op een abstracte
manier gemaakt. 
De analyse heeft de vorm van een \emph{denotationele semantiek}.
Pas nadien wordt dit idee in Haskell ge\"implementeerd.

\paragraph{Monads}
Aan de analyse stellen we een bijkomende eis: het is
wenselijk dat deze \emph{composable} is, dit wil zeggen, gemakkelijk
samen te stellen en uit te breiden.
Een gebruikelijke strategie is om de semantiek de vorm van een \emph{Monad}
te geven.
Dit garandeert dat de semantiek bepaalde eigenschappen heeft die de 
samenstelbaarheid verzekeren.
Aangezien de semantiek naar Haskell vertaald wordt, heeft het Haskell programma
eveneens een monadische structuur.
Concreet wil dit zeggen dat in Haskell een geldige instantie van de
\texttt{Monad}-typeclass kan worden gemaakt.

Traditioneel begint nu een zoektocht naar een geschikte monadische structuur
\cite{logicT}.
Het spreekt voor zich dat het bedenken van een dergelijke voorstelling een
complexe zaak is, die een zekere creativiteit en inventiviteit vereist.

Deze thesis gebruikt de alternatieve ``Effect Handlers''-methode:
In een eerste stap worden de effecten voorgesteld met een abstracte syntaxboom.
Abstracte syntaxbomen hebben inherent een monadische structuur.

In de twee fase interpreteren ``Effect Handlers'' de abstracte syntax.
Dit zijn functies die een syntaxboom naar een bepaalde opeenvolging van
effecten sturen.
De Effect Handlers bepalen de effectieve semantiek.
Verschillende Effect Handlers kunnen verschillende concrete effecten
hebben, terwijl ze eenzelfde abstracte syntax uitvoeren.

Deze twee manieren kunnen vergeleken worden als een opeenvolging van
transformatiestappen.

In de traditionele methode wordt vertrokken uit Backtracking, waaraan een
bepaalde (monadische) wiskundige semantiek wordt gegeven, waarna de semantiek in
Haskell wordt ge\"implementeerd.
In dit systeem is experimentatie binnen de grenzen van de semantiek mogelijk
in Haskell. 

In de Effect Handlers methode wordt opnieuw vertrokken uit Prolog, maar
de wiskundige semantiek is een abstracte syntaxboom (die monadisch is).
De abstracte semantiek krijgt concrete betekenis door Effect Handlers die
de syntaxboom interpreteren.
Daarna wordt de semantiek (syntaxboom en handlers) in Haskell ge\"implementeerd.
In dit systeem is experimentatie op de semantiek \emph{zelf} mogelijk in
Haskell.

\paragraph{Aggregaten}
De implementatie van Tabulatie kan de basis vormen voor verdere uitbreidingen.
Het laatste deel van deze thesis bestudeert hoe Prolog en Backtracking met
Tabulatie uitgebreid kunnen worden met aggregaten.
Dit zijn operaties die getabuleerde oplossingen samenvatten, bijvoorbeeld
het minimum of het maximum.

\paragraph{Overzicht}
het vervolg van dit hoofdstuk bespreekt de relevante literatuur en bestaand
werk. Hoofdstuk~\ref{chap:prelim} overloopt de voorkennis die voor deze
thesis relevant is, waaronder een uitgebreide uiteenzetting over de
``Effect Handlers'' aanpak.

De belangrijkste bijdragen van deze thesis zijn:
\begin{itemize}
\item
Hoofdstuk~\ref{chap:syntax} introduceert de abstracte syntax voor getablede
berekeningen.
Deze syntax wordt uitgedrukt als Haskell datatypes. 
Concreet gaat het over de datatypes \texttt{Program} en \texttt{Table}.
Tegelijkertijd wordt de interface (\texttt{Nondet}) gedefini\"eerd aan de hand
waarvan een programma in dergelijke syntax kan worden opgesteld.

\item
Hoofdstuk \ref{chap:denot} bespreekt de denotationele semantiek
die we aan de syntax toewijzen. 
De semantiek is de ``handler''-helft van de Effect Handlers aanpak.
Samen vormen de syntax en semantiek de \emph{Tabulatie Monad}.
Deze monad gedraagt zich zoals Tabled-Prolog.
Tenslotte kan deze semantiek vertaald worden in Haskell.
Hier wordt dit op twee manieren gedaan: op een nai\"eve manier die sterk gelijkt
op de abstracte semantiek, en op een effici\"entere manier die gebruik maakt
van afhankelijkheidsanalyse.

\item
Hoofdstuk~\ref{chap:aggregate} breidt de semantiek verder uit naar
aggregaten.
Aggregaten laten toe om bepaalde programma's effici\"enter uit te voeren,
zonder dat het programma zelf moet veranderen.
De syntax blijft dus onveranderd, en opnieuw is de semantiek de
``handler''-helft.
Tenslotte wordt deze semantiek eveneens vertaald in Haskell.

\end{itemize}

\section{Literatuur}
Tabulatie is reeds lang bekend als een manier om recursieve programma's te
versnellen, in het bijzonder voor dynamisch programmeren. 
Bird \cite{bird1980} bespreekt een aantal implementatietechnieken voor
verschillende soorten recursieve programma's, gebaseerd op de 
afhankelijkheidsgrafen van de recursieve oproepen.

Pareja-Flores, Pe\~na en Vel\'asquez-Iturbide beschrijven een
programmatransformatie die een programmeur kan uitvoeren om de effici\"entie
van zijn recursieve functie in Haskell te verbeteren door middel van
Tabulatie \cite{fuji95}.
Deze thesis probeert daarentegen om de last op de programmeur
zoveel mogelijk te beperken. 
De programmeur moet geen expliciete transformaties doen, behalve dat
het programma in een open recursieve stijl moet worden geschreven.

Omdat een goal in Prolog meerdere oplossingen kan hebben krijgt Tabulatie in
Prolog een extra dimensie: Tabulatie be\"eindigt recursie op het moment
dat alle mogelijke oplossingen gevonden zijn.
Het (ongepubliceerde) boek ``Programming in Tabled Prolog'' \cite{xsbbook}
vormt een goede introductie tot Tabulatie in Prolog.

Deze extra dimensie is eveneens wat Tabulatie onderscheidt van gewone
memo\"isatie.
Gewone memo\"isatie onthoudt alleen resultaten van de vorige recursieve
oproepen.
Dit volstaat om bijvoorbeeld een na\"ief algoritme voor het berekenen van het
i-de fibonacci getal in lineaire in plaatst van exponenti\"ele
tijdscomplexiteit te laten uitvoeren.
Maar voor berekeningen die meerdere oplossingen hebben volstaat dit niet meer,
omdat de onthouden oplossing niet de enige oplossing is.

Monads zijn een begrip uit \emph{Category Theory}, een abstracte tak van de
wiskunde.
Het gebruik van monads om functionele programma's te structureren
\cite{wadler1995} is ge\"inspireerd door het gebruik van monads in denotationele
semantiek. \cite{moggi1988}
Het gebruik ervan is inmiddels zeer sterk ingeburgerd in de Haskell-wereld,
en in mindere mate (mogelijks in vermomming) in programmeren in het algemeen.

Effect Handlers zijn een actueel onderzoeksonderwerp
\cite{haskell2014}, \cite{plotkin2009handlers}.
Ter vergelijking, Kiselyov, Shan, Friedman en Sabry \cite{logicT} gebruiken de
meer traditionele manier om monads (en monad transformers \cite{jones1995}) te
construeren aan de hand van typeclasses.

Open recursie \cite{jfp_mri} is een techniek die toelaat om in te grijpen op
de recursieve oproepen van een functie.

Scott en Strachey \cite{scott1971} gaven voor het eerst een formeel
wiskundig fundament aan de denotationele semantiek van recursieve programma's, 
gesteund op tralies (eng. lattices).

CSP\cite{hoare1978} beschikt over een denotationele semantiek
\cite{francez1978semantics}.
In CSP komen twee soorten niet-determinisme naar boven: enerzijds keuze uit
verschillende ``boolean-guards'' die tegelijk true kunnen zijn, en minder
interessant voor deze thesis, niet-determinisme gegenereerd door concurrente
processen.
Het wiskundige model in deze thesis is echter veel minder complex.

\section{Onderzoeksaanpak}
Om Tabulatie zoveel mogelijk stapsgewijs te kunnen aanpakken is het relevant
om eerst Tabulatie te herleiden tot zijn essentie.

Naar het model van Prolog zijn er drie ingredi\"enten: 
recursie, mislukking en niet-determinisme. Mislukking en niet-determinisme
betekent in deze context dat een berekening geen, \'e\'en of
meerdere oplossingen kan hebben.

Het is de combinatie van deze ingredi\"enten die Tabulatie interessant maken.
Alleen recursie reduceert Tabulatie tot gewone memo\"isatie. 
Alleen niet-determinisme en faling reduceert tot backtrackende berekeningen
die al eerder in de literatuur bestudeerd zijn \cite{logicT}.

In een tweede fase worden deze essenti\"ele elementen gevat in een elegante
interface en wordt hiervoor een eenvoudige implementatie voorzien.

De finale fase omvat het optimaliseren van de implementatie, het voorzien
van relevante toepassingen en uitbreiding naar aggregaten.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "masterproef"
%%% End: 
