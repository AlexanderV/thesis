{-# LANGUAGE NoMonomorphismRestriction #-}

import Diagrams.Prelude
import Diagrams.Backend.SVG
import Graphics.SVGFonts

type DP = Diagram SVG

h, w :: Double
h = 0.6
w = 0.5

main :: IO ()
main = do
  renderSVG "paper/ast.svg" (mkHeight 800) $
    vsep h [
      renderOr # named "O1",
      hsep w [
        renderPure12 # named "p1",
        vsep h [
          renderOr # named "O2",
          hsep w [
            renderPure21 # named "p2",
            vsep h [
              renderOr # named "O3",
              hsep w [
                renderPure12 # named "p3",
                renderInf # named "inf"]
              # centerX]
            # centerX]
          # centerX]
        # centerX]
      # centerX]
    # centerX
    # connectOutside "O1" "p1"
    # connectOutside "O1" "O2"
    # connectOutside "O2" "p2"
    # connectOutside "O2" "O3"
    # connectOutside "O3" "p3"
    # connectOutside "O3" "inf"
  
  renderSVG "paper/ast2.svg" (mkHeight 800) $
    vsep h [
      renderCall # named "c1" # alignL,
      hsep w [
        vsep h [
           renderOr # named "o1",
           renderPure12 # named "p1"],
        vsep h [
          renderLambda # named "l1",
          hsep 0.4 [
            node "(y,x)" # named "d1",
            node "Pure (y,x)" # named "p2"]
          # centerX]
        # centerX]
      # centerX]
    # connectOutside "o1" "p1"
    # connectOutside "l1" "d1"
    # connectOutside "l1" "p2"
    # connectOutside "c1" "l1"
    # connectOutside "o1" "c1"
    # connectPerim' (with & arrowShaft .~ arc xDir (1/6 @@ turn)
                          & shaftStyle %~ dashingN [0.03, 0.03] 0)
      "c1" "o1" (2/4 @@ turn) (1/4 @@ turn)
  

node :: String -> DP
--node  w = center . (<> rect w 1.0 # moveOriginBy (V2 0 (-0.2)) # lw none)
node s = let txt :: DP
             txt = textSVG s 1 # stroke # fc black # center
         in txt <> boundingRect txt # scale 1.2 # lw none


renderFail, renderPure12, renderPure21, renderInf, renderOr, renderCall :: DP
renderFail     = node "Fail"
renderPure12   = node "Pure (1,2)"
renderPure21   = node "Pure (2,1)"
renderInf      = node "..."
renderOr       = node "Or"
renderCall     = node "Call"
renderLambda   = node "λ"
