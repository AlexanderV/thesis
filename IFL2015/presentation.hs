-- The Table Monad in Haskell
-- aka "Fixing non-determinism"
-- aka "/Fix/-ing non-determinism"

{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE DeriveFunctor         #-}
{-# LANGUAGE RankNTypes            #-}

import qualified Data.Map as M
import qualified Data.Set as S
import Control.Applicative
import Control.Monad (ap, (>=>))
import Data.Map (Map, (!))
import Data.Set (Set)
import Data.Monoid
import qualified Data.Foldable as F
main :: IO ()
main = return ()

-- = Non-determinism
-- == Recursion
-- == Haskell example

swap (x,y) = (y,x)
p :: S.Set (Int, Int)
p = S.singleton (1,2) `S.union` S.map swap p

 
-- ghci> p
-- fromList [...<hangs>

-- = Monadic model: Effect Handlers
-- == Open Recursion

type Open s = s -> s
p' :: Open (() -> Set (Int, Int))
p' rec () = S.singleton (1,2) `S.union` S.map swap (rec ())


-- == Monad: Syntax + Effect Handler

-- === Syntax

data ND arg res a = Success a
              | Fail
              | Or (ND arg res a) (ND arg res a)
              | Rec arg (res -> ND arg res a)
              deriving Functor

rec :: arg -> ND arg res res
rec i = Rec i Success

instance Applicative (ND arg res) where
  pure = Success
  (<*>) = ap

instance Monad (ND arg res) where
  return = Success
  Success a   >>= f = f a
  Fail       >>= f = Fail
  Or a b     >>= f = Or (a >>= f) (b >>= f)
  Rec i cont >>= f = Rec i (cont >=> f)

mapND :: (a -> b) -> (b -> a) -> ND i a a -> ND i b b
mapND f _  (Success x)  = Success (f x)
mapND f _ Fail         = Fail
mapND f g (Or l r)     = Or (mapND f g l) (mapND f g r)
mapND f g (Rec i cont) = Rec i (mapND f g . cont . g)

-- === Effect Handler

-- Semantics:

runND :: (Ord arg, Ord res) => Open (arg -> ND arg res res) -> arg -> S.Set res
runND op i0 = lfp step s0 ! i0 where
  s0 = M.singleton i0 S.empty
  -- step :: Map i (Set o) -> Map i (Set o)
  step m = foldr (\k -> go k (op rec k)) m (M.keys m)
  -- go :: i -> ND input output o -> M.Map i (S.Set o) -> M.Map i (S.Set o)
  -- [|Success a|] m = {a}
  go a (Success x) m  = M.insertWith S.union a (S.singleton x) m
  -- [|Fail|] m = {}
  go a Fail       m  = m
  -- [|Or l r|] m = [|l|] m U [|r|] m
  go a (Or l r)   m  = go a r (go a l m)
  -- [|Rec j k|] m = U{[|cont o|] m | o <- m ! j}
  go a (Rec b k)  m  = case M.lookup b m of
    Nothing -> M.insert b S.empty m
    Just s  -> foldr (go a . k) m (S.elems s)

-- ==== lfp

lfp :: (a -> a) -> a -> a
lfp f a = f (f (f (f (f a))))

-- === Example:

p'' rec () = return (1,2) `Or` do (x,y) <- rec ()
                                  return (y,x)
 
-- < ghci> runND p ()
-- < fromList [(1,2), (2,1)]

-- === Example 2:

-- Reachable nodes in a graph

-- = Extension "Dependency Analysis"

-- = Extension "Dynamic Programming"

data Node = Node { label :: Char, adj :: [(Node, Int)] }
instance Show Node where
  show n = "Node " ++ [label n]
instance Eq Node where
  n1 == n2 = label n1 == label n2
instance Ord Node where
  n1 <= n2 = label n1 <= label n2

neighbor :: Node -> ND input output (Node, Int)
neighbor = foldr Or Fail . map return . adj
instance Monoid Dist where
  mempty = InfDist
  Dist a `mappend` Dist b = Dist (a + b)
  InfDist `mappend` _ = InfDist
  _ `mappend` InfDist = InfDist


reach :: Open (Node -> ND Node Node Node)
reach op n0 = do n <- fmap fst (neighbor n0)
                 return n0 `Or` return n `Or` op n
sp :: Char -> Open (Node -> ND Node Int Int)
sp l rec n | label n == l = return 0
           | otherwise = do (n0, d0) <- neighbor n
                            d1 <- rec n0
                            return (d0 + d1)


-- ghci> runND (sp 'c') a
-- fromList [...<hangs>

-- == Lattices

-- a bounded join-semilattice
class Ord a => Lattice a where
  bottom :: a
  join   :: a -> a -> a

-- instance Ord a => Lattice (Set a) where

class Connection a c where
  abst :: c -> a
  conc :: a -> c

-- conc . abst =< id
-- abst . conc = =id

class Lattice f => Fold f where
  fold   :: (f -> m -> m) -> m -> f -> m

instance Ord a => Lattice (Set a) where
  bottom = S.empty
  join = S.union

instance Ord a => Fold (Set a) where
  fold f = F.foldr (f . S.singleton)

runL :: (Ord arg, Fold res) => Open (arg -> ND arg res res) -> arg -> res
runL op i0 = lfp step s0 ! i0 where
  s0 = M.singleton i0 bottom
  step m = foldr (\k -> go k (op rec k)) m (M.keys m)
  -- [|Success a|] m = a
  go a (Success x) m  = M.insertWith join a x m
  -- [|Fail|] m = bottom
  go a Fail       m  = m
  -- [|Or l r|] m = [|l|] m `join` [|r|] m
  go a (Or l r)   m  = go a r (go a l m)
  -- [|Rec j k|] m = lub{[|cont o|] m | o <- m ! j}
  go a (Rec b k)  m  = case M.lookup b m of
    Nothing -> M.insert b bottom m
    Just s  -> fold (go a . k) m s

-- == a : Lattice, c: Set out

-- Example (Shortest Path Lattice):

data Dist =  InfDist | Dist Int deriving (Eq, Show)
instance Ord Dist where
  InfDist <= _ = True
  _ <= InfDist = False
  Dist a <= Dist b = b <= a

instance Lattice Dist where
  bottom = InfDist
  join InfDist  d        = d
  join d        InfDist  = d
  join (Dist a) (Dist b) = Dist (min a b)

instance Connection Dist (Set Int) where
  abst = foldr join bottom . map Dist . S.elems
  conc InfDist  = S.empty
  conc (Dist x) = S.singleton x
instance Fold Dist where
  fold f m InfDist  = m
  fold f m d = f d m


n1, n2, n3, n4, n5 :: Node
[n1, n2, n3, n4, n5] =
  [Node '1' [(n2,1),(n5,1)],
   Node '2' [(n3,1)],
   Node '3' [(n1,22),(n4,1)],
   Node '4' [(n3,1),(n1,1)],
   Node '5' [(n5,1)]]

-- ghci> runL (sp 'c') a :: Min
-- Min 2

-- = Extension "Mutually Recursive Functions"

-- = Summary
-- * Recursion & Non-determinism
-- * Fixed with monadic model using "Effect Handlers"
-- * Improve performance
-- * Dynamic Programming

-- = Related Work
-- * Tabulation in Prolog
-- * Dynamic Programming
-- * Lattice subsumption tabling, mode-directed tabling, ...

-- = Questions
