{- | Benchmarks for the IFL 2015 paper "Fixing Non-determinism".

     Author: Alexander Vandenbroucke
-}

import Criterion.Main
import System.Random
import Control.DeepSeq

import ND
import ND.Lattice
import ND.DependencyTracking
import ND.DependencyTracking.Lattice

import Fib
import Knapsack
import NQueens
import ShortestPath
import StronglyConnectedComponents

main :: IO ()
main = defaultMain [
  bgroup "nqueens"     (map bqueens    [6..10]),
  bgroup "nqueens-dt"  (map bqueensk   [6..10]),
--  bgroup "nqueens-l"   (map bqueenslk  [6..10]),
  bgroup "knapsack"    (map bknapsack  [10..20]),
  bgroup "knapsack-dt" (map bknapsackk [10..20]),
  bgroup "scc"         (map bscc       [30..35]),
  bgroup "scc-dt"      (map bscck      [30..35]),
--  bgroup "scc-l"       (map bscclk     [30..35]),
  bgroup "fib"         (map bfib       [800..810]),
  bgroup "fib-dt"      (map bfibk      [800..810]),
--  bgroup "fib-l"       (map bfiblk     [800..810]),
  bgroup "sp"          (map bsp        [80..90]),
  bgroup "sp-dt"       (map bspk       [80..90]),
  bgroup "lin"         (map blin       [30..40]),
  bgroup "lin-dt"      (map blink      [30..40])]

bqueens :: Int -> Benchmark
bqueens n = bench (show n) (nf (runNDRec (nqueens n)) n)

bqueensk :: Int -> Benchmark
bqueensk n = bench (show n) (nf (runNDReck (nqueens n)) n)

-- bqueenslk :: Int -> Benchmark
-- bqueenslk n = bench (show n) (nf (runNDRecLk (liftSet . nqueens n)) n)


-- Change to scale based on capacity instead of items.
bknapsack :: Int -> Benchmark
bknapsack n = bench (show n) (nf (runNDRecL (knapsack items)) cap) where
   cap = 200
   items =
     replicate n (Item 4 20)
     ++
     replicate n (Item 2 2)
     ++
     replicate n (Item 2 4)

bknapsackk :: Int -> Benchmark
bknapsackk n = bench (show n) (nf (runNDRecLk (knapsack items)) cap) where
   cap = 200
   items =
     replicate n (Item 7 20)
     ++
     replicate n (Item 2 2)
     ++
     replicate n (Item 2 4)

bscc :: Int -> Benchmark
bscc n = g `deepseq` bench (show n) (nf (runNDRec (scc g)) (Scc 1)) where
  (g,_) = generateGraph n ((n `div` 2) * n) (mkStdGen 420)

bscck :: Int -> Benchmark
bscck n = g `deepseq` bench (show n) (nf (runNDReck (scc g)) (Scc 1)) where
  (g,_) = generateGraph n ((n `div` 2) * n) (mkStdGen 420)

-- bscclk :: Int -> Benchmark
-- bscclk n = g `deepseq` bench (show n) (nf (runNDRecLk liftedScc) (Scc 1))
--   where
--     liftedScc = liftSet . scc g
--     (g,_) = generateGraph n ((n `div` 2) * n) (mkStdGen 420)


bfib :: Int -> Benchmark
bfib n = bench (show n) (nf (runNDRec fibonacci) n)

bfibk :: Int -> Benchmark
bfibk n = bench (show n) (nf (runNDReck fibonacci) n)

--bfiblk :: Int -> Benchmark
--bfiblk n = bench (show n) (nf (runNDRecLk (liftSet . fibonacci)) n)

bsp :: Int -> Benchmark
bsp n = g `deepseq` bench (show n) (nf (map (runNDRecL (sp g 1))) dsts) where
  (g,_) = generateGraph 100 (n * 100) (mkStdGen 420)
  dsts  = [1..100]

bspk :: Int -> Benchmark
bspk n = g `deepseq` bench (show n) (nf (map (runNDRecLk (sp g 1))) dsts) where
  (g,_) = generateGraph 100 (n * 100) (mkStdGen 420)
  dsts  = [1..100]

blin :: Int -> Benchmark
blin n = g `deepseq` bench (show n) (nf (map (runNDRecL (sp g 40))) dsts) where
  g = linearGraph n
  dsts  = [1..n]

blink :: Int -> Benchmark
blink n = g `deepseq` bench (show n) (nf (map (runNDRecLk (sp g 40))) dsts) where
  g = linearGraph n
  dsts  = [1..n]
