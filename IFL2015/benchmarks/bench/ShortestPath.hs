{- | This benchmark is based on finding the shortest path in a directed graph.

     author: Alexander Vandenbroucke
-}

module ShortestPath (sp) where

import Control.DeepSeq

import ND
import StronglyConnectedComponents

data Dist = InfDist | Dist Int deriving (Eq, Show)

instance Monoid Dist where
  mempty = InfDist
  mappend InfDist a = a
  mappend a InfDist = a
  mappend (Dist a) (Dist b) = Dist (min a b)

instance NFData Dist where
  rnf InfDist   = ()
  rnf (Dist d)  = rnf d

instance Num Dist where
  fromInteger = Dist . fromInteger
  InfDist + _ = InfDist
  _ + InfDist = InfDist
  Dist a + Dist b = Dist (a + b)
  (*) = error "Dist.Num.*: not implemented."
  (-) = error "Dist.Num.-: not implemented."
  abs = error "Dist.Num.abs: not implemented."
  signum = error "Dist.Num.signum"

sp :: Graph -> Int -> Int -> NDRec Int Dist Dist
sp graph dst src | dst == src = return 0
                 | otherwise  = do
                     n <- choose (neighbours (findNode src graph))
                     fmap (+ 1) (rec n)
