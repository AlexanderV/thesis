{- | This module implements a benchmark based on strongly connected components.

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}

module StronglyConnectedComponents (
  -- * Graph datatypes
  Graph, Node, label, neighbours, findNode, invert,
  -- * Generate graphs
  linearGraph, generateGraph,
  -- * Argument type
  GraphProblem(Scc),
  -- * Compute the SCC
  scc)
where

import Control.DeepSeq
import Control.Monad.State
import Data.Function (on)
import GHC.Generics (Generic)
import Data.List (sort, groupBy)
import System.Random

import ND

type Graph = [Node]

data Node
  = Node
    {
      label      :: Int,
      neighbours :: [Int]
    }
    deriving (Show, Generic, NFData)

data GraphProblem
  = Scc      Int
  | Forward  Int
  | Backward Int
  deriving (Eq, Ord)

invert :: Graph -> Graph
invert graph =
  [ Node i [label n | n <- graph, i `elem` (neighbours n)] |
    i <- map label graph ]

linearGraph :: Int -> Graph
linearGraph n = map mkNode [1..n] where
  mkNode i | i == n    = Node n []
           | otherwise = Node i [i+1]

generateGraph :: Int -> Int -> StdGen -> (Graph, StdGen)
generateGraph noNodes noEdges g =
  let (edges,g') = runState (generateEdges noNodes noEdges []) g where
      toGraph = map toNode . groupBy ((==) `on` fst) . sort
      toNode es = Node (fst (head es)) (map snd es)
  in (toGraph edges, g')

generateEdges :: Int -> Int -> [(Int, Int)] -> State StdGen [(Int, Int)]
generateEdges _       0 es = return es
generateEdges noNodes n es = do
  n1 <- state $ randomR (1,noNodes)
  n2 <- state $ randomR (1,noNodes)
  if n1 == n2 || (n1,n2) `elem` es then
    generateEdges noNodes n es
  else do
    generateEdges noNodes (n-1) ((n1,n2):es)

findNode :: Int -> Graph -> Node
findNode i g = case filter (\x -> label x == i) g of
  []    -> Node i []
  (n:_) -> n

scc :: Graph -> GraphProblem -> NDRec GraphProblem Int Int
scc graph p = case p of
  Scc i -> do
    a <- rec (Forward i)
    b <- rec (Backward i)
    ND.guard (a == b)
    return a
  Forward i ->
    return i `Or` do
      n <- choose (neighbours (findNode i graph))
      rec (Forward n)
  Backward i ->
    return i `Or` do
      n <- choose (neighbours (findNode i invGraph))
      rec (Backward n)
  where invGraph = invert graph
