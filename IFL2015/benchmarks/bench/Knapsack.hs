{- | This module implements a benchmark based on the Knapsack problem.

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Knapsack (
  -- * Domain model
  Weight,
  Value,
  Item(..),
  Knapsack,
  knValue,
  knWeight,
  -- * Solver
  knapsack)
where

import Control.DeepSeq
  
import ND

newtype Weight = Weight Double deriving (Show, Eq, Ord, Num, NFData)
newtype Value  = Value  Double deriving (Show, Eq, Ord, Num, NFData)

data Item
  = Item
    {
      weight :: Weight,
      value  :: Value
    }
  deriving (Show, Eq, Ord)

instance NFData Item where
  rnf (Item w v) = w `deepseq` v `deepseq` ()

newtype Knapsack = Knapsack [Item] deriving (Show, NFData)

instance Eq Knapsack where
  k1 == k2 = knValue k1 == knValue k2 && knWeight k1 == knWeight k2

instance Ord Knapsack where
  k1 <= k2 = (v1 <= v2) || (v1 == v2 && w1 <= w2) where
    v1 = knValue k1
    v2 = knValue k2
    w1 = knWeight k1
    w2 = knWeight k2

instance Monoid Knapsack where
  mempty  = Knapsack []
  mappend = max

knValue :: Knapsack -> Value
knValue (Knapsack k) = sum (map value k)

knWeight :: Knapsack -> Weight
knWeight (Knapsack k) = sum (map weight k)

knapsack :: [Item] -> Weight -> NDRec Weight Knapsack Knapsack
knapsack items w | w <= 0    = return (Knapsack [])
                 | otherwise = return (Knapsack []) `Or` do
                     i <- choose items
                     let w' = w - weight i
                     guard (w' >= 0)
                     Knapsack is <- rec w'
                     return (Knapsack (i:is))
