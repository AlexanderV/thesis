{- | Fibonacci benchmark for the IFL 2015 paper "Fixing Non-determinism".


     Author: Alexander Vandenbroucke
-}

module Fib (fibonacci) where


import ND


fibonacci :: Int -> NDRec Int Integer Integer
fibonacci n | n == 0    = return 0
            | n == 1    = return 1
            | otherwise = (+) <$> rec (n - 1) <*> rec (n - 2)
                
