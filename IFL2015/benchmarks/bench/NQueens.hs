{- | This module implements a benchmark based on the Knapsack problem.

     Author: Alexander Vandenbroucke
-}

module NQueens (X,Y,Queen(qX,qY),nqueens,printQueens) where

import Control.DeepSeq

import ND

type X = Int
type Y = Int

data Queen
  = Queen
    {
      qX :: X,
      qY :: Y
    }
  deriving (Eq, Ord, Show)

instance NFData Queen where
  rnf (Queen x y) = x `deepseq` y `deepseq` ()

nqueens :: X -> Y -> NDRec Int [Queen] [Queen]
nqueens width height = nqueens' height where
  nqueens' 0 = return []
  nqueens' n = do
    x <- choose [1..width]
    let queen = Queen x n
    queens <- rec (n-1)
    guard (safe queen queens)
    return (queen:queens)

safe :: Queen -> [Queen] -> Bool
safe _ [] = True
safe (Queen x y) qs = horzSafe && diag1Safe && diag2Safe where
  horzSafe  = not (x `elem` (map qX qs))
  diag1Safe = all safe' qs where
    safe' (Queen x' y') = x - x' /= y - y'
  diag2Safe = all safe' qs where
    safe' (Queen x' y') = x - x' /= y' - y

printQueens :: [Queen] -> IO ()
printQueens qs = do
  let n      = length qs
      coords = map (\(Queen x y) -> (x,y)) qs
      printQueens' x y
        | y > n               = return ()
        | x > n               = putStrLn ""  >> printQueens' 1 (y + 1)
        | (x,y) `elem` coords = putStr "QQ"  >> printQueens' (x + 1) y
        | otherwise           = putStr "__"  >> printQueens' (x + 1) y
  printQueens' 1 1
