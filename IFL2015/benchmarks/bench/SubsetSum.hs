{- | Code for the "Subset Sum" example for the IFL 2015 paper

     author: Alexander Vandenbroucke
-}


module SubsetSum (
  -- * A lattice for lists
  Shortest(..),
  -- * Solving the subset sum problem
  sss)
where

import ND

-- | A lattice on lists, ordered by decreasing length.
--   'InfList' is the bottom element.
data Shortest = InfList | List [Int] deriving Show

cons :: Int -> Shortest -> Shortest
cons _ InfList   = InfList
cons x (List xs) = List (x:xs)

instance Eq Shortest where
  InfList == InfList = True
  List a  == List b  = length a == length b
  _       == _       = False

-- | Implements bottom and join of the lattice.
instance Monoid Shortest where
  mempty = InfList
  mappend InfList a = a
  mappend a InfList = a
  mappend (List a) (List b) | length a <= length b = List a
                            | otherwise            = List b

sss :: (Int, [Int]) -> NDRec (Int,[Int]) Shortest Shortest
sss (_,[])    = Fail
sss (n, x:xs) = choice [ rec (n, xs),
                         guard (n == x) >> return (List [x]),
                         fmap (cons x) (rec (n - x, xs))]
  
