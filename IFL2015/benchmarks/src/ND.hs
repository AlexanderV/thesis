{- | This module contains the basic syntax and effect handlers.

     Author: Alexander Vandenbroucke
-}

{-# LANGUAGE DeriveFunctor #-}

module ND (
  -- * Syntax & smart constructors
  NDRec(..),
  rec, choice, choose, guard,
  -- * Effect Handler
  runNDRec, runNDRec',
  lfp)
where

import           Control.Monad hiding (guard)
import qualified Data.Map as M
import qualified Data.Set as S
import Data.Map ((!))


-- | Basic syntax for recursive non-deterministic computations
data NDRec i o a
  =  Success a
  |  Fail
  |  Or (NDRec i o a) (NDRec i o a)
  |  Rec i (o -> NDRec i o a)
  deriving Functor

instance Applicative (NDRec i o) where
  pure a = Success a
  (<*>)  = ap

-- | Monad instance for 'NDRec' where 'return' is 'Success' and '(>>=)' is
--   substitution of 'Success a' by 'f a'.
instance Monad (NDRec i o) where
  return a = Success a
  Success a >>= f = f a
  Fail      >>= _ = Fail
  Or l r    >>= f = Or (l >>= f) (r >>= f)
  Rec i k   >>= f = Rec i (\x -> k x >>= f)

-- | @'rec' i@  performs a recursive call to argument i, and returns the
--   result.
rec :: i -> NDRec i o o
rec i = Rec i Success

-- | @'choose' l@ non-deterministically chooses a computation from l.
choice :: [NDRec i o a] -> NDRec i o a
choice = foldr Or Fail

-- | @'choose' l@ non-deterministically chooses a value from l.
choose :: [a] -> NDRec i o a
choose = choice . map Success

-- | @'guard' b@ returns unit if b is 'True' and fails otherwise.
guard :: Bool -> NDRec i o ()
guard b = if b then return () else Fail

-- | Effect handler for running a non-deterministic computation.
--
--   This effect handler implements the denotational semantics @[|.|]@ based
--   on sets.
runNDRec :: (Ord i, Ord o)
         => (i -> NDRec i o o) -> i -> S.Set o
runNDRec expr i0 = runNDRec' expr i0 ! i0 where

-- | Like 'runNDRec', but returns the environment instead.
--
--   prop> runNDRec e i = runNDRec' e i ! i  
runNDRec' :: (Ord i, Ord o)
          => (i -> NDRec i o o)
             -- ^ non-deterministic computation
          -> i
             -- ^ argument
          -> M.Map i (S.Set o)
runNDRec' expr i0 = lfp step s0 where
  s0 = M.singleton i0 S.empty
  step m = foldr (\k -> go k (expr k)) m (M.keys m)
  go i (Success a)  m = M.insertWith S.union i (S.singleton a) m
  go _ Fail         m = m
  go i (Or l r)     m = go i r (go i l m)
  go i (Rec j k)    m = case M.lookup j m of
    Nothing -> M.insert j S.empty m
    Just s  -> foldr (go i . k) m (S.toList s)

-- | Iteratively compute the least fixed point.
--
--   Given that:
--
--    1. @f@ is continuous, and
--    2. the domain @a@ possesses a finite ascending chain property,
--    3. @a0@ is the bottom element of the domain @a@
--
--   @'lfp' f a0@ will compute the least fixed point of @f@ in a finite amount
--   of time.
lfp :: Eq a => (a -> a) -> a -> a
lfp f a0 = let a1 = f a0 in if a1 == a0 then a1 else lfp f a1
