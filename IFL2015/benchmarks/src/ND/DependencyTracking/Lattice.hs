{- | This module implements a dependency tracking effect handler for lattices.

     This effect handler tracks the continuations that depend on a recursive
     call.

     Author: Alexander Vandenbroucke
-}

module ND.DependencyTracking.Lattice (
  -- * Effect Handler
  runNDRecLk, runNDRecLk',
  -- * Environment
  ifNew
  )
where

import ND (NDRec(..))
import ND.DependencyTracking (
  Env,
  emptyEnv,
  store,
  results,
  (!!!),
  conts,
  addCont)

-- | This effect handler implements the denotational semantics [|.|]L more
--   efficiently.
runNDRecLk :: (Ord i, Eq o, Monoid o) => (i -> NDRec i o o) -> i -> o
runNDRecLk expr i0 = runNDRecLk' expr i0 !!! i0

runNDRecLk' :: (Ord i, Eq o, Monoid o) => (i -> NDRec i o o) -> i -> Env i o
runNDRecLk' expr i0 = go i0 (expr i0) s0 where
  s0 = store i0 mempty emptyEnv
  go i (Success x) m = ifNew i x m newEnv where
    newEnv = foldr ($ x) (store i x m) (conts i m)
  go _ Fail      m = m
  go i (Or l r)  m = go i r (go i l m)
  go i (Rec j k) m =
    let newEnv  = addCont j (go i . k) m
    in case results j m of
         Nothing -> go j (expr j) newEnv
         Just r  -> go i (k r) newEnv

ifNew :: (Ord i, Eq o, Monoid o) => i -> o -> Env i o -> Env i o -> Env i o
ifNew i o env newEnv =
  if maybe True (\o' ->  mappend o o' /= o') (results i env) then
    newEnv
  else
    env
