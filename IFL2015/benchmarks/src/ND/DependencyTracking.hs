{- | This module implements a dependency tracking effect handler.

     This effect handler tracks the continuations that depend on a recursive
     call.

     Author: Alexander Vandenbroucke
-}

module ND.DependencyTracking (
  -- * Effect Handler
  runNDReck, runNDReck',
  -- * Environments
  Env, emptyEnv, store, results, (!!!),conts, addCont)
where

import qualified Data.Set as S
import qualified Data.Map as M
import Data.Maybe (fromJust)

import ND (NDRec(..))


------------------------------------------------------------------------------
-- Effect Handler

-- | An effect handler that tracks dependencies.
--
--   This effect handler implements the denotational semantics [|.|] more
--   efficiently.
runNDReck :: (Ord i, Ord o) => (i -> NDRec i o o) -> i -> S.Set o
runNDReck expr i0 = runNDReck' expr i0 !!! i0

-- | Like 'runNDReck', but returns the environment instead.
--
--   prop> runNDReck e i = runNDReck' e i !!! i
runNDReck' :: (Ord i, Ord o) => (i -> NDRec i o o) -> i -> Env i (S.Set o)
runNDReck' expr i0 = go i0 (expr i0) s0 where
  s0 = store i0 S.empty emptyEnv
  go i (Success x)  m = ifNew i x m newEnv where
    -- apply continuations
    newEnv = foldr ($ S.singleton x) (store i (S.singleton x) m) (conts i m)
  go _ Fail         m = m
  go i (Or l r)     m = go i r (go i l m)
  go i (Rec j k)    m =
    let newEnv  = addCont j k' m
        k' o m' = foldr (go i . k) m' (S.toList o)
    in case results j m of
         Nothing -> go j (expr j) newEnv
         Just rs -> foldr (go i . k) newEnv (S.toList rs)


------------------------------------------------------------------------------
-- Environments

-- | The environment containing the intermediary results of the least fixed
--   point computation.
--   It contains both results (@o@) and dependencies (continuations).
newtype Env  i o = Env (M.Map i (o, [Cont i o]))

-- | 'Env' specialized to 'S.Set'.
type EnvS i o = Env i (S.Set o)

-- | 'Cont' is a newtype wrapper around a continuation
--   @o -> 'Env' i o -> 'Env' i o@
newtype Cont i o = Cont { runCont :: o -> Env i o -> Env i o }

-- | An empty environment.
emptyEnv :: Env i o
emptyEnv = Env M.empty

-- | Choose one of two environments based on the newness of @o@.
--
--   @'ifNew' i o env newEnv@ will choose @newEnv@ if @o@ is not
---  the environment for key @i@, otherwise it will choose @env@.
ifNew :: (Ord i, Ord o) => i -> o -> EnvS i o -> EnvS i o -> EnvS i o
ifNew i o env newEnv =
 if maybe True (not . S.member o) (results i env) then
   newEnv
 else
   env

-- | Store a new result in the environment.
store :: (Ord i, Monoid o) => i -> o -> Env i o -> Env i o
store i o (Env m) = Env (M.insertWith mappend i (o, []) m)

-- | Retrieve the results associated with a key.
results :: Ord i => i -> Env i o -> Maybe o
results i (Env m) = fmap fst (M.lookup i m)

-- | Unsafely retrieve the results assoicated with a key.
(!!!) :: Ord i => Env i o -> i -> o
m !!! i = fromJust (results i m)

-- | Retrieve the set of continuations (dependencies) associated with a key.
conts :: Ord i => i -> Env i o -> [o -> Env i o -> Env i o]
conts i (Env m) = map runCont (M.findWithDefault [] i (fmap snd m))

-- | Add a continuation for a key.
addCont :: (Ord i, Monoid o)
        => i -> (o -> Env i o -> Env i o) -> Env i o -> Env i o
addCont i k (Env m) = Env (M.insertWith mappend i (mempty, [Cont k]) m)
