{- | This module implements the 'Lattice' based effect handler.

     Author: Alexander Vandenbroucke
-}

module ND.Lattice (
  -- * Effect handler
  runNDRecL, runNDRecL',
  -- * Lifting sets
  liftSet)
where

import           Data.Map ((!))
import qualified Data.Map as M
import qualified Data.Set as S

import           ND (NDRec(..),lfp, choice)

-- | Effect handler for running a non-deterministic computation.
--
--   This effect handler implements the denotational semantics [|.|]L based on
--   lattices.
--   We use 'Monoid' to represent lattices.
--   The least fixed point is only guaranteed to exist when 'expr' is a
--   continuous function, and the domain @o@ forms a complete lattice.
runNDRecL :: (Ord i, Eq o, Monoid o)
          => (i -> NDRec i o o) -> i -> o
runNDRecL expr i0 = runNDRecL' expr i0 ! i0

-- | Like 'runNDRecL' but returns the environment instead.
--
--   prop> runNDRecL e i = runNDRecL' e i ! i
runNDRecL' :: (Ord i, Eq o, Monoid o)
           => (i -> NDRec i o o) -> i -> M.Map i o
runNDRecL' expr i0 = lfp step s0 where
  s0 = M.singleton i0 mempty
  step m = foldr (\k -> go k (expr k)) m (M.keys m)
  go i (Success a) m = M.insertWith mappend i a m
  go _ Fail        m = m
  go i (Or l r)    m = go i r (go i l m)
  go i (Rec j k)   m = case M.lookup j m of
    Nothing -> M.insert j mempty m
    Just s  -> go i (k s) m



-- | Lift a non-deterministic computation.
--
--   A non-deterministic computation @'Ord' o => 'NDRec' i o o@ is lifted to
--   a computation @'NDRec' i ('S.Set' o) ('S.Set' o).
liftSet :: (Ord i, Ord o) => NDRec i o o -> NDRec i (S.Set o) (S.Set o)
liftSet (Success x) = Success (S.singleton x)
liftSet Fail        = Fail
liftSet (Or l r)    = Or (liftSet l) (liftSet r)
liftSet (Rec j k)   = Rec j k' where
  k' s = choice (map (liftSet . k) (S.toList s))
