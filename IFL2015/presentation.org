* The Table Monad in Haskell
  aka "Fixing non-determinism"
  aka "/Fix/-ing non-determinism"
* Non-determinism
** Recursion
** Haskell example
> swap (x,y) = (y,x)
> p :: Set (Int, Int)
> p = singleton (1,2) `union` map swap p
                               ^- map from Data.Set
> ghci> p
fromList [...<hangs>

* Monadic model: Effect Handlers
** Open Recursion
> type Open s = s -> s
> p :: Open (() -> ND (Int, Int))
> p rec () = return (1,2) `Or` do (x,y) <- red
>                                 return (y,x)

** Monad: Syntax + Handler
*** syntax
data ND in out = Return out
               | Fail
               | Or (ND in out) (ND in out)
	       | Rec in (out -> ND in out)

rec :: in -> ND in in
rec i = Rec i Return

instance Monad (ND in) where
  ...
  Call in cont >>= f = Call in (\out -> cont out >>= f)

*** Fixing the handler
runND :: (Ord in, Ord out) => Open (in -> ND in out) -> in -> Set out
runND op i = fix step s0 ! i where
  m0 = M.insert x S.empty S.empty
  find = elems $ M.findWithDefault S.empty i s
  update = M.updateWith S.union 
  op' = op rec

  go :: i -> ND in out -> Map in (Set out) -> Map in (Set out)
  go i (Return a) = update i (singleton a)
  go i Fail       = id
  go i (Or l r)   = go i l . go i r
  go m (Rec i k)  = S.unions [go s (k o) | o <- find s i]

  step :: Map in out -> Map in out
  step m = foldr (\k m' -> update i (go m' (op' k)) m') m (keys m)

*** Example:
p rec () = return (1,2) `Or` do (x,y) <- rec ()
                                return (y,x)
ghci> runND p ()
fromList [(1,2), (2,1)]

* Extension "Dependency Analysis"
* Extension "Dynamic Programming*

class Connection a c where
  abst :: c -> a
  conc :: a -> c

conc . abst == id
abst . conc =< id

** a : Lattice, c: Set out
