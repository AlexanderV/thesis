{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Advanced (
  ND(..),
  rec,
  guard,
  runND,
  kND,
  p,
  choose,
  Node(..),
  reach,
  n1,n2,n3,n4,n5,
  blow
)
where

import qualified Data.Set as S
import qualified Data.Map as M
import Data.Map ((!))
import Data.Monoid
import Control.Applicative

data ND arg res a
  = Success a
  | Fail
  | Or (ND arg res a) (ND arg res a)
  | Rec arg (res -> ND arg res a)
  deriving Functor

instance Applicative (ND arg res) where
  pure = Success
  _          <*> Fail       = Fail
  Fail       <*> _          = Fail
  Or l r     <*> fa         = Or (l <*> fa) (r <*> fa)
  ff         <*> Or l r     = Or (ff <*> l) (ff <*> r)
  Rec arg k  <*> fa         = Rec arg (\res -> k res <*> fa)
  ff         <*> Rec arg k  = Rec arg (\res -> ff <*> k res)
  Success ff <*> Success fa = Success (ff fa)

choose :: [a] -> ND arg res a
choose = foldr Or Fail . map Success

instance Monad (ND arg res) where
  return = Success
  Success a >>= f = f a
  Fail      >>= _ = Fail
  Or l r    >>= f = Or (l >>= f) (r >>= f)
  Rec arg k >>= f = Rec arg (\res -> k res >>= f)


rec :: arg -> ND arg res res
rec i = Rec i Success

guard :: Bool -> ND arg res ()
guard False = Fail
guard True  = return ()

type Open a = a -> a
type OpenND arg res = Open (arg -> ND arg res res)
type State  arg res =
  (M.Map arg (S.Set res, [res -> Update arg res]), [Update arg res])

newtype Update arg res = Update { runUpdate :: State arg res -> State arg res }

runND :: (Ord arg, Ord res) => Open (arg -> ND arg res res) -> arg -> S.Set res
runND open arg0 = fst (lfp (s0 ,[arg0]) step ! arg0) where
  s0 = M.singleton arg0 (S.empty, [])
  step arg = go arg (open rec arg)
  go arg (Success x)     (m,q) = insertValue arg x (m, q)
  go _    Fail           (m,q) = (m,q)
  go arg (Or l r)        (m,q) = go arg r (go arg l (m,q))
  go arg (Rec arg' cont) (m,q) = 
    let m' = insertDependency arg' (const arg) m
    in case M.lookup arg' m of
         Nothing     -> (m', arg':q)
         Just (s, _) -> foldr (go arg . cont) (m',q) (S.elems s)

-- | but now with 'continuations'.
kND :: forall arg res . (Ord arg, Ord res)
    => Open (arg -> ND arg res res) -> arg -> S.Set res
kND open arg0 = fst (lfp (s0, [update arg0]) runUpdate ! arg0)
  where
    s0 = M.singleton arg0 (S.empty, [])
    update :: arg -> Update arg res
    update arg = Update (go arg (open rec arg))
    go :: (Ord arg, Ord res)
       => arg
       -> ND arg res res
       -> State arg res
       -> State arg res
    go arg (Success x)     (m,q) =
      let (v, m') =
            M.insertLookupWithKey (\_ -> mappend) arg (S.singleton x, []) m
          q' = case v of
            Nothing -> []
            Just (oldSet, deps) | S.member x oldSet -> []
                                | otherwise         -> map ($ x) deps
      in (m', q' ++ q)
    go _    Fail           (m,q) = (m,q)
    go arg (Or l r)        (m,q) = go arg r (go arg l (m,q))
    go arg (Rec arg' cont) (m,q) =
      let m' = insertDependency arg' (Update . go arg . cont) m
      in case M.lookup arg' m of
           Nothing     -> (m', update arg':q)
           Just (s, _) -> foldr (go arg . cont) (m',q) (S.elems s)

lfp :: (a, [b]) -> (b -> (a,[b]) -> (a, [b])) -> a
lfp (a, [])   _ = a
lfp (a, b:bs) f = lfp (f b (a, bs)) f


-- | Insert an @(arg,res)@-pair in the map. Flushes the dependencies when
--   @res@ is new.
insertValue :: (Ord arg, Ord res)
            => arg
            -> res
            -> (M.Map arg (S.Set res, [res -> b]), [b])
            -> (M.Map arg (S.Set res, [res -> b]), [b])
insertValue arg res (m, q) =
  let (v, m') = M.insertLookupWithKey flushDeps arg (S.singleton res, []) m
      flushDeps _ (newSet, _) (oldSet, deps) =
        let u = S.union newSet oldSet
        in (u, if S.size u == S.size oldSet then deps else [])
      q' = case v of
        Nothing                                   -> []
        Just (oldSet, deps) | S.member res oldSet -> [] -- arg another O(log n)
                            | otherwise           -> map ($ res) deps
  in (m', q' ++ q)

insertDependency :: (Ord a, Ord k)
                 => k -> t -> M.Map k (S.Set a, [t]) -> M.Map k (S.Set a, [t])
insertDependency k d m = M.insertWith mappend k (S.empty,[d]) m

p :: OpenND () (Int, Int)
p open () = return (1,2) `Or` do (x,y) <- open ()
                                 return (y,x)

data Node = Node { label :: Char, adj :: [Node] }

instance Eq Node where
  na == nb = label na == label nb
instance Ord Node where
  na <= nb = label na <= label nb
instance Show Node where
  show = show . label

n1,n2,n3,n4,n5 :: Node
[n1,n2,n3,n4,n5] = [Node '1' [n2,n5],
                    Node '2' [n3],
                    Node '3' [n4,n1],
                    Node '4' [n1,n3],
                    Node '5' [n5]]

reach :: OpenND Node Node
reach open n0 = do
  n <- choose (adj n0)
  return n0 `Or` return n `Or` open n

blow :: OpenND Int Int
blow _    1 = return 20
blow open k = open (k-1) `Or` do i <- open k
                                 guard (i > 0)
                                 return (i-1)
