\pdfminorversion=4
\documentclass{sig-alternate-05-2015}

\usepackage{url}
\usepackage{color}
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{booktabs}
\usepackage{tkz-graph}
\usepackage[english]{babel}
\usepackage{hyphenat}
\usepackage{times}

%if False

> {-# LANGUAGE ScopedTypeVariables #-}
> {-# LANGUAGE MultiParamTypeClasses #-}
> {-# LANGUAGE FlexibleInstances #-}
> {-# LANGUAGE TypeOperators #-}
> {-# LANGUAGE DeriveFunctor #-}
> {-# LANGUAGE GADTs #-}
> {-# LANGUAGE ExistentialQuantification #-}
> {-# LANGUAGE TypeFamilies #-}
> {-# LANGUAGE RankNTypes #-}
> {-# LANGUAGE FlexibleContexts #-}
> {-# LANGUAGE TupleSections #-}
> {-# LANGUAGE GeneralizedNewtypeDeriving #-}
>
> import Data.Set       (
>   Set, singleton, empty, union, intersection, unions, toList,
>   fromList, member, null)
> import qualified Data.Set (map)
> import Data.Foldable  (foldMap, foldrM)
> import Data.Traversable  (for)
> import Data.Monoid hiding (First,Any,getAny)
> import Prelude hiding (fail, init, lookup, null)
> import Data.List      (sort, nub, groupBy)
> import Data.Function (on)
> import Control.Monad  (liftM, ap)
> import qualified Data.Map as M
> import Data.Map ((!))
> import Control.Applicative (Applicative(pure, (<*>)), (<$>))
> import Data.Maybe (fromJust)
> import Data.Char (isUpper)
>
> instance Functor ND where
>   fmap = liftM
> instance Applicative ND where
>   pure  = return
>   (<*>) = ap
> instance Functor (NDRec i o) where
>   fmap = liftM
> instance Applicative (NDRec i o) where
>   pure  = return
>   (<*>) = ap
> instance Show Node where
>   showsPrec d (Node l _) s | 10 < d    = "(Node " ++ l ++ ")" ++ s
>                            | otherwise = "Node " ++ l ++ s
>
> instance (Show i, Show a) => Show (NDRec i o a) where
>   showsPrec _ Fail s = "Fail" ++ s
>   showsPrec d ndr s
>     | 10 < d = "(" ++ showsPrec 0 ndr (")" ++ s)
>     | otherwise = case ndr of
>         Success a -> "Success " ++ showsPrec 11 a s
>         Or l r    -> "Or " ++ showsPrec 11 l (" " ++ showsPrec 11 r s)
>         Rec j k   -> "Rec " ++ showsPrec 11 j s
>
> instance Functor f => Functor (Free f) where
>   fmap = liftM
> instance Functor f => Applicative (Free f) where
>   pure  = return
>   (<*>) = ap
>
> instance Eq Shortest where
>   InfList  ==  InfList  = True
>   List l1  ==  List l2  = length l1 == length l2
>   s1       ==  s2       = False
>
> newtype Any = Any { getAny :: Bool } deriving (Eq, Ord)
> instance Lattice Any where
>   bottom = Any False
>   join (Any a) (Any b) = Any (a || b)
>
> instance Functor (NDM i) where
>   fmap = liftM
> instance Applicative (NDM i) where
>   pure  = return
>   (<*>) = ap
> instance Monad (NDM i) where
>   return = SuccessM
>   SuccessM x  >>= f = f x
>   FailM       >>= f = FailM
>   OrM l r     >>= f = OrM (l >>= f) (r >>= f)
>   RecM i k    >>= f = RecM i (\x -> k x >>= f)
> instance Eq (Input Analyze) where
>   Input (Nullable s1)  == Input (Nullable s2)  = s1 == s2
>   Input (First s1)     == Input (First s2)     = s1 == s2
>   i1                   == i2                   = False
>
> instance Ord (Input Analyze) where
>   Input (Nullable s1)  <= Input (Nullable s2)  = s1 <= s2
>   Input (First s1)     <= Input (First s2)     = s1 <= s2
>   Input (Nullable s1)  <= Input (First s2)     = True
>   i1                   <= i2                   = False
>
> instance Eq (Output Analyze) where
>   Output (Nullable s1) o1 == Output (Nullable s2) o2 =
>     s1 == s2 && o1 == o2
>   Output (First s1) o1 == Output (First s2) o2 =
>     s1 == s2 && o1 == o2
>   i1 == i2 = False
>
> recM :: Lattice o => i o -> NDM i o
> recM i = RecM i SuccessM
>
> chooseM :: [a] -> NDM i a
> chooseM = choiceM . map return
>
> choiceM :: [NDM i a] -> NDM i a
> choiceM = foldr OrM FailM
>
> guardM :: Bool -> NDM i ()
> guardM b = if b then return () else FailM




%% > instance Functor (NDRecV i) where
%% >   fmap = liftM
%% > instance Applicative (NDRecV i) where
%% >   pure  = return
%% >   (<*>) = ap                


%endif

%include polycode.fmt
\setlength{\mathindent}{2mm}

%format >>> = "\ggg"
%format .+. = "\oplus"
%format SuccessND = "Success_{ND}"
%format FailND = "Fail_{ND}"
%format OrND   = "Or_{ND}"
%format >=> = ">\!\!=\!\!>"
%format <$> = "<\!\!\$\!\!>"
%format <*> = "<\!\!\!*\!\!\!>"
%format a0
%format a1
%format d1
%format d2
%format e1
%format e2
%format f1
%format f2
%format i0
%format n1
%format n2
%format n3
%format n4
%format n5
%format k0
%format l1
%format l2
%format o1
%format o2
%format s0
%format s1
%format s2
%format S1
%format S2
%format S3
%format pS1
%format pS2
%format pS3
%format grammar2
%format :--> = "\longmapsto"
%format forall = "\forall"
%format fromListException = "\texttt{fromList *** Exception: <loop>}"

\newcommand{\denot}[2]{\mathcal{#1}\llbracket\,#2\,\rrbracket}
\newcommand{\powerset}[1]{\mathcal{P}(#1)}
\newcommand{\lfp}{l\!f\!p}
\newcommand{\monotonicit}{\Uparrow}
\newcommand{\myparagraph}[1]{\noindent\textbf{#1\quad}}
\newtheorem{definition}{Definition} 
\newcommand{\tom}[1]{\textcolor{red}{#1}}
\newcommand{\todo}[1]{\textcolor{red}{#1}}

\def\sharedaffiliation{%
\end{tabular}
\begin{tabular}{c}}

\title{Fixing Non-determinism}

\special{papersize=8.5in,11in}
\setlength{\pdfpageheight}{\paperheight}
\setlength{\pdfpagewidth}{\paperwidth}

\setcopyright{acmcopyright}

\numberofauthors{3}
% Three authors sharing the same affiliation.
    \author{
      \alignauthor \mbox{Alexander Vandenbroucke}\\      
%
      \alignauthor Tom Schrijvers\\     
%
      \alignauthor Frank Piessens\\
      \sharedaffiliation
      \email{\{alexander.vandenbroucke, tom.schrijvers, frank.piessens\}@@kuleuven.be}\\
%
      \sharedaffiliation
      \affaddr{Department of Computer Science }  \\
      \affaddr{KU Leuven }   \\
          }
%
\date{August 4, 2015}

\begin{document}
           
\maketitle

\begin{abstract}
  Non\hyp{}deterministic computations are conventionally modelled by lists of their
  outcomes.
  This approach provides a concise declarative description of certain problems,
  as well as a way of generically solving such problems.

  However, the traditional approach falls short when the non\hyp{}deterministic
  problem is allowed to be recursive: the recursive problem may have infinitely
  many outcomes, giving rise to an infinite list.

  Yet there are usually only finitely many distinct \emph{relevant} results.
  This paper shows that this set of interesting results corresponds to a
  least fixed point.
  We provide an implementation based on algebraic effect handlers to compute
  such least fixed points in a finite amount of time, thereby allowing
  non\hyp{}determinism and recursion to meaningfully co-occur in a single program.
\end{abstract}


\begin{CCSXML}
<ccs2012>
<concept>
<concept_id>10011007.10011006.10011008.10011009.10011012</concept_id>
<concept_desc>Software and its engineering~Functional languages</concept_desc>
<concept_significance>500</concept_significance>
</concept>
<concept>
<concept_id>10011007.10011006.10011008.10011024.10011033</concept_id>
<concept_desc>Software and its engineering~Recursion</concept_desc>
<concept_significance>500</concept_significance>
</concept>
</ccs2012>
\end{CCSXML}

\ccsdesc[500]{Software and its engineering~Functional languages}
\ccsdesc[500]{Software and its engineering~Recursion}

\printccsdesc
\keywords
Haskell, Tabling, Effect Handlers, Logic Programming,
Non\hyp{}determinism, Least Fixed Point

\CopyrightYear{2015} 
\setcopyright{acmcopyright}
\conferenceinfo{IFL '15,}{September 14-16, 2015, Koblenz, Germany}
\isbn{978-1-4503-4273-5/15/09}\acmPrice{\$15.00}
\doi{http://dx.doi.org/10.1145/2897336.2897342}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}

Non\hyp{}determinism \cite{wadler1985} models a variety of problems in a
declarative fashion, especially those problems where the solution depends on
the exploration of different choices.
The conventional approach represents non\hyp{}determinism as lists of
possible outcomes.
For instance, consider the semantics of the following non\hyp{}deterministic
expression:
%
\[|1 ? 2|\]
%
This expression represents a non\hyp{}deterministic choice (with the operator~|?|)
between |1| and |2|.
Traditionally we model this with the list |[1,2]|.
%
Now consider the next example:
%
\begin{align*}
  &|swap (m,n)| = |(n,m)|\\
  &|pair|       = |(1,2) ? swap pair|
\end{align*}
%
The corresponding Haskell code is:

< swap :: [(a,b)] -> [(b,a)]
< swap e = [ (m,n) | (n,m) <- e ]
< pair :: [(Int,Int)]
< pair = [(1,2)] ++ swap pair

This is an executable model (we use |>>>| to denote the prompt of the GHCi
Haskell REPL):

< >>> pair
< [(1,2),(2,1),(1,2),(2,1)...

We get an infinite list, although only two distinct outcomes  (|(1,2)| and
|(2,1)|) exist.
The conventional list-based approach is clearly inadequate in this example.
In this paper we model non\hyp{}determinism with sets of values instead,
such that duplicates are implicitly removed.
The expected model of |pair| is then the set $\{(1,2),(2,1)\}$.
We can execute this model:
\footnote{Here |map| comes from \texttt{Data.Set}} 

< swap :: (Ord a, Ord b) => Set (a,b) -> Set (b, a)
< swap = map (\(m,n) -> (n,m))
< pair :: Set (Int,Int)
< pair = singleton (1,2) `union` swap pair
<
< >>> pair
< fromListException

Haskell lazily prints the first part of the |Set| constructor, and then has to
compute |union| infinitely many times.
As an executable model of non\hyp{}determinism it clearly remains inadequate: it
fails to compute the solution $\{(1,2),(2,1)\}$.

This paper solves the problem caused by the co-occurence of
non\hyp{}determinism and recursion, by recasting it as the least fixed point problem
of a \emph{different} function.
The least fixed point is computed explicitly by iteration, instead of
implicitly by Haskell's recursive functions.

The contributions of this paper are:
\begin{itemize}
  \vspace{-2mm}
  \item We define a monadic model that captures both non\hyp{}determinism \emph{and}
    recursion.
    This yields a finite representation of recursive non\hyp{}deterministic
    expressions.
    We use this representation as a light-weight (for the programmer) embedded
    Domain Specific Language to build non\hyp{}deterministic expressions in Haskell.
  \vspace{-2mm}    
  \item We give a denotational semantics of the model in terms of the
    least fixed point of a semantic function $\denot{R}{\cdot}$.
    The semantics is subsequently implemented as a Haskell function that
    interprets the model.
  \vspace{-2mm}
  \item We generalize the denotational semantics to arbitrary complete
    lattices.
    We illustrate the added power on a simple graph problem,
    which could not be solved with the more specific semantics.
  \vspace{-2mm}
  \item We provide a set of benchmarks to demonstrate the expressivity of our
    approach and evaluate the performance of our implementation.

\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Overview}
\subsection{Non\hyp{}determinism}
A computation is non\hyp{}deterministic if it has several possible outcomes.
In this paper, we interpret such non\hyp{}deterministic computations as
the set of their results.

Consider the following non\hyp{}deterministic expression that produces either
|1| or |2|:
%
\[1~?~2\]
%
For this example, the semantics is given by the set $\{1,2\}$.
The semantic function $\denot{}{\cdot}$ formally characterizes this
interpretation:
%
\begin{align*}
  \denot{}{n} &= \{n\}\\
  \denot{}{|e1 ? e2|} &= \denot{}{|e1|} \cup \denot{}{|e2|}
\end{align*}
%
The semantics of a literal $n$ is the singleton of $n$,
and the semantics of a choice |e1 ? e2| is the union of the semantics
of the left and right branches.
A simple calculation shows that $\denot{}{|1 ? 2|}$ is indeed $\{1,2\}$.
%
\[
\denot{}{|1 ? 2|} = \denot{}{1} \cup \denot{}{2}
= \{1\} \cup \{2\} = \{1,2\}
\]
%
Let us extend our semantics to allow for addition of non\hyp{}deterministic
values:
%
\[
\denot{}{e_1 + e_2} = \{ n + m~||~n \in \denot{}{e_1}, m \in \denot{}{e_2} \}
\]
%
Now, consider the expression:
%
\[(1~?~2) + (1~?~2)\]
%
Here we have an expression that contains an addition of two choices.
This expression has 4 possible outcomes of which two coincide: the result
is either 2 (1+1), 3 (1+2 or 2+1) or 4 (2+2).\footnote{The conventional
  list-based semantics would be |[2,3,3,4]|.}
Again calculation gives the expected result:
%
\begin{align*}
  &\denot{}{|(1 ? 2) + (1 ? 2)|}\\
  &= \{ n + m~||~n \in \denot{}{|1 ? 2|},
  m \in \denot{}{|1 ? 2|} \}\\
  &= \{ n + m~||~n \in \{1,2\}, m \in \{1,2\} \}\\
  &= \{2, 3, 4\}
\end{align*}
%
\myparagraph{Recursion}
The next example presents a recursive non\hyp{}deterministic expression |pair|
which chooses between |(1,2)| and the new primitive |swap| to
swap |pair|'s components around.
%
\[ |pair = (1,2) ? swap pair| \]
%
The semantics of |swap e| is the set obtained by flipping all pairs
in the set of results of |e|:
%
\[
  \denot{}{|swap e|} = \{(m,n)~||~(n,m) \in \denot{}{e} \}
\]
%
The semantics of |pair| are given by the following recursive equation:
%
\begin{align}
      \denot{}{|pair|} &= \denot{}{|(1,2) ? swap pair|}
      \nonumber\\
  \iff\denot{}{|pair|} &= \denot{}{|(1,2)|} \cup \denot{}{|swap pair|}
      \nonumber\\
  \iff\denot{}{|pair|} &= \{(1,2)\} \cup \{(m,n)~||~(n,m) \in \denot{}{|pair|}\}
      \label{eq:recursive-pair}
\end{align}
%
This equation admits infinitely many solutions, e.g.
%
\begin{align*}
  &\denot{}{|pair|} = \{(1,2), (2,1)\},\\
  &\denot{}{|pair|} = \{(0,0), (1,2), (2,1)\},\\
  &\denot{}{|pair|} = \{(1,1), (1,2), (2,1)\},\\
  &\cdots
\end{align*}
%
However, we can identify a \emph{least} solution: the set $\{(1,2),(2,1)\}$ is
contained in every other solution.

As we saw previously, a naive translation of this idea in Haskell
\emph{does not work}:

< det :: Ord a => a -> Set a
< det x = singleton x
< (?) :: Ord a => Set a -> Set a -> Set a
< a ? b = union a b
< swap :: (Ord a, Ord b) => Set (a,b) -> Set (b,a)
< swap = Data.Set.map (\(x,y) -> (y,x))
< pair :: Set (Int,Int)
< pair = det (1,2) ? swap pair
<
< >>> pair
< fromListException

The reason it does not work is that under Haskell's (cpo-based) semantics the
equation \eqref{eq:recursive-pair} has a different 
least solution: $\bot$ (i.e. non-termination).\footnote{In Haskell we
  work in the domain
  $\langle \powerset{\mathbb{N}\times\mathbb{N}}\cup\{\bot\}, \sqsubseteq \rangle$, where
  every type is inhabited by $\bot$, representing
  non-termination, and $\bot \sqsubseteq v$ for any value $v$.}
As this additional $\bot$ in the domain is clearly not the desired solution,
we cannot rely on Haskell's native semantics for recursion.

The main contribution of this paper is to reformulate the problem as a
\emph{different} least fixed point problem, for which we can iteratively
compute the solution.
Moreover, our approach incurs minimal overhead for the programmer, compared to
writing the function using the conventional recursive approach.

\subsection{Effect Handlers}
Monads are a way to model side-effects such as non\hyp{}determinism in a pure
functional programming language \cite{wadler1995}.
In this paper, we use effect handlers to construct a monad for
non\hyp{}determinism and recursion.
Effect handlers \cite{kiselyov2013extensible, wu2015fusion} factor the problem
of modeling effectful computations into two parts: first a syntax is
introduced to represent all relevant operations, second effect handlers are
defined that interpret the syntax within a semantic domain.

The syntax of non\hyp{}deterministic computations can be modeled with the following
data type |ND|, which supports three operations: |SuccessND a| is a
deterministic computation with result |a|, |Fail| is a failed computation,
and |Or l r| represents a non\hyp{}deterministic choice between two
non\hyp{}deterministic computations |l| and |r|.

> data ND a
>   =  SuccessND a
>   |  FailND
>   |  OrND (ND a) (ND a)

Because the above data type is a free monad \cite{swierstra2008data} we can
easily define the following |Monad| instance:
                                  
> instance Monad ND where
>   return a = SuccessND a
>   SuccessND a  >>=  f  = f a
>   FailND       >>=  f  = FailND
>   OrND l r     >>=  f  = OrND (l >>= f) (r >>= f)

This monad instance substitutes |f a| for every |SuccessND a| in the data
structure, leaves |Fail| untouched, and recurses on both branches of |Or l r|.
With this monad instance, the example |(1 ? 2) + (1 ? 2)| is expressed as:

> exampleND :: ND Int
> exampleND = do  x <- return 1 `OrND` return 2
>                 y <- return 1 `OrND` return 2
>                 return (x + y)

Values of type |ND a| are abstract syntax trees.
The function\\|exampleND| constructs such an abstract syntax tree.
The interpreter |nd| decodes this tree according to the semantics
defined by $\denot{}{\cdot}$.
It turns |Success| into a singleton set, |Fail| into an empty set, and |Or|
into the union of the interpretations of its branches.

> nd :: Ord a => ND a -> Set a
> nd (SuccessND a)  = singleton a
> nd FailND         = empty
> nd (OrND l r)     = union (nd l) (nd r)

< >>> nd exampleND
< fromList [2,3,4]

The equivalent of |pair| in the abstract syntax is |pairND|:

> pairND :: ND (Int, Int)
> pairND = return (1,2) `OrND` fmap swap pairND where
>   swap (x,y) = (y,x)

< >>> nd pairND
< fromList ...

Encoding the non\hyp{}deterministic syntax as a data type does not solve the
problem of non-termination.
This can be explained by looking at the syntax tree for |pairND|
in Figure~\ref{fig:inf-ast}: it is an
infinite tree with an |Or| node at the root,
a |Success| node in the left branch, and in the right branch another tree
with |Or| at the root after which the pattern repeats.
Obviously the interpreter |nd| does not terminate when interpreting such an
infinite tree.

We have little chance of processing the infinite tree in a finite
time if we do not represent it in a finite way.
Yet the current recursive form of |pairND| hides the recursive call in
the function body, making the construction of an infinite tree unavoidable.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Explicating Recursion}
\label{sec:explicit-recursion}

In order to obtain a finite syntax tree, we have to once more change the
representation of non\hyp{}deterministic computations.
First, we add a constructor to the abstract syntax to explicitly represent a
recursive call. Second, we replace all recursive calls with this constructor,
and finally, we define a new effect handler that interprets the now finite
syntax tree, producing the desired solution.
The following data type models recursive calls in addition to non\hyp{}determinism:

> data NDRec i o a
>   =  Success a                       -- (1)
>   |  Fail                            -- (2)
>   |  Or (NDRec i o a) (NDRec i o a)  -- (3)
>   |  Rec i (o -> NDRec i o a)        -- (4)

The first three constructors (1--3) capture non\hyp{}determinism, exactly like
the previously defined data type |ND|.
The last constructor (4) captures a recursive call: |Rec a k| represents a
recursive call with argument |a :: i|, and continuation |k :: o -> NDRec i o a|.
For convenience, we define four additional smart constructors.
The smart constructor |rec| performs a recursive call and immediately wraps the
result in a successful computation:

> rec :: i -> NDRec i o o
> rec i = Rec i Success

The smart constructor |choice| picks a computation from a list in a
non\hyp{}deterministic fashion.

> choice :: [NDRec i o a] -> NDRec i o a
> choice = foldr Or Fail

The smart constructor |choose| picks an element from a list in a
non\hyp{}deterministic fashion.

> choose :: [a] -> NDRec i o a
> choose = choice . map Success

The smart constructor |guard| returns |()| if its argument is true and
fails otherwise.

> guard :: Bool -> NDRec i o ()
> guard b = if b then return () else Fail

The data type |NDRec| is again a free monad, the corresponding monad instance is:

> instance Monad (NDRec i o) where               -- (1)
>   return a = Success a                         -- (2)
>   Success a  >>=  f = f a                      -- (3)
>   Fail       >>=  f = Fail                     -- (4)
>   Or l r     >>=  f = Or (l >>= f) (r >>= f)   -- (5)
>   Rec i k    >>=  f = Rec i (\x -> k x >>= f)  -- (6)

Lines 1--5 are identical to the |Monad| instance for |ND|.
Line (6) defines that |>>=| of a recursive call is obtained by a new recursive
call to the same argument, but with an extended continuation.
\footnote{The new continuation is the Kleisli composition |k >=> f| where\\
|(>=>) :: Monad m => (a -> m b) -> (b -> m c) -> (a -> m c)|.
}

As an example, consider this latest incarnation of |pair|, written
in the |NDRec| syntax:

> pairNDRec :: () -> NDRec () (Int,Int) (Int,Int)
> pairNDRec () = return (1,2) `Or` do  (x,y) <- rec ()
>                                      return (y,x)

We interpret this program in the next section.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Effect Handler for Explicit Recursion}
\label{sec:effect-handler}

\subsection{Denotational semantics}
\label{sec:denotational-semantics}
In this section we formalize the meaning of the abstract syntax.
The meaning of a non\hyp{}deterministic function $f$ of
type\footnote{In deference to mathematical convention, we use uppercase
  characters for meta-variables $I$ and $O$ when using mathematical syntax.}
|I -> NDRec I O O|, mapping $I$ onto |NDRec I O O| is given by a function
$\denot{}{f} : I \rightarrow \powerset{O}$.
This function maps values of type $I$ onto a subset of the values described
by the type $O$.
%
\[
  \denot{}{\cdot} : (I \rightarrow |NDRec I O O|)
  \rightarrow (I \rightarrow \powerset{O})\\
\]
%
Let us first define the semantics of an easier case: suppose we already
have an environment $s : I \rightarrow \powerset{O}$ that contains
a partial set of solutions for every call $f(a)$, i.e.
%
\begin{align*}
  s(a) \subseteq \denot{}{f}(a) \text{ for every } a \in I
\end{align*}
%
Then for every syntax tree $t : |ND I O O|$ we can define a semantic
function $\denot{R}{t}(s)$.
This semantic function gives us the set of results associated with $t$,
given $s$.
Because the |ND I O O| data type is inductive, we define the function
$\denot{R}{\cdot}$ using structural recursion, as follows:
%
\begin{align}
  \denot{R}{&\cdot} \mathrlap{: |NDRec I O O|
  \rightarrow (I \rightarrow \powerset{O})
  \rightarrow \powerset{O}}
  \nonumber\\
%
  \denot{R}{&|Success x|&}(s) &= \{x\}     \label{eq:sem-r-1}\\
%
  \denot{R}{&|Fail|     &}(s) &= \emptyset \label{eq:sem-r-2}\\
%
  \denot{R}{&|Or l r|   &}(s) &= \denot{R}{l}(s) \cup \denot{R}{r}(s)
  \label{eq:sem-r-3}\\
%
  \denot{R}{&|Rec i k|  &}(s) &= \!\!\bigcup_{x \in s(i)}\!\!\denot{R}{k(x)}(s)
  \label{eq:sem-r-4}
\end{align}

The two base cases are fairly simple: in the deterministic
case~\eqref{eq:sem-r-1} the result is just a singleton set of the
result, and in the failure case~\eqref{eq:sem-r-2} it is the empty set.

There are two inductive cases as well: a binary choice \eqref{eq:sem-r-3} and
a recursive call \eqref{eq:sem-r-4}.
A binary choice is handled by taking the union of the
results of the left and right branches.
A recursive call has an argument $i$ and a continuation $k$.
The result is obtained by finding the set of outcomes in the environment $s$,
and then applying the continuation $k$ to every element $x$ in $s(i)$,
and taking the union of the result.

Now we can of define $\denot{}{\cdot}$ as follows: since
$\denot{R}{f(a)}(\denot{}{f})$ gives the set of outcomes of $f(a)$ given
environment $\denot{}{f}$, and $\denot{}{f}(a)$ gives the set of outcomes of
$f(a)$, the following must hold about $\denot{R}{f(a)}(\denot{}{f})$:
%
\begin{align*}
  &\denot{R}{f(a)}(\denot{}{f}) = \denot{}{f}(a)~~(\forall a \in I)\\
  %
  &\text{ [ equivalence of $\lambda$-abstraction and
      $\forall$-quantification ] }\\
  %
  \iff&\lambda a.\denot{R}{f(a)}(s) = \lambda a.\denot{}{f}(a)\\
  %  
  \iff&\lambda a.\denot{R}{f(a)}(\denot{}{f}) = \denot{}{f}
  \text{ [ $\eta$-reduction ] }\\
  %
  \iff &\denot{}{f} \text{ is a fixed point of }
  \lambda s.\lambda a.\denot{R}{f(a)}(s)
\end{align*}
%
Now all that remains is to choose a canonical fixed point for
$\denot{}{f}$, such that it corresponds to the desired meaning.
Note that environments of the type $I \rightarrow \powerset{O}$
(such as $\denot{}{f}$) are ordered by the ordering relation $\sqsubseteq$:
%
\[ f \sqsubseteq g \iff \forall a \in I: f(a) \subseteq g(a) \]
%
The desired fixed point is \emph{the least fixed point}\footnote{The
  least fixed point is well defined, as the function is always continuous
  (if the syntax tree is finite) and therefore monotonic, but the domain
  $I \rightarrow \powerset{O}$ may not possess the finite ascending chain
  property, preventing us from computing the solution in a finite amount of
  time.}
denoted by $\lfp(\cdot)$, when fixed points are ordered by $\sqsubseteq$:
%
\begin{align}
  \denot{}{f} = \lfp(\lambda s.\lambda a.\denot{R}{f(a)}(s))
  \label{eq:def-denot}
\end{align}
%
To make this more concrete, consider the denotational semantics of |pairNDRec|.

\begin{align*}
  &\denot{}{|pairNDRec ()|}(\texttt{()})\\
%
  &=\lfp(\lambda s.\lambda ().\denot{R}{|pairNDRec ()|}(s))\big(()\big)
  &\text{[ by \eqref{eq:def-denot} ] }\\
%
  &\text{[ $\lambda ().\{(1,2),(2,1)\}$ is the least fixed point ]}\\
%
  &=\big(\lambda ().\{(1,2),(2,1\}~()\big)\\
%
  &\text{ [ function application ]}\\
%
  &\{(1,2),(2,1)\}\\
\end{align*}

We can see that $\lambda ().\{(1,2),(2,1)\}$ is the least fixed point because
it is a fixed point and it is contained in every other fixed point.

\begin{figure}
  \centering
  \begin{tikzpicture}[sibling distance=5em, level distance=3em,
      every node/.style = { align = center }]
    \node{Or}
      child { node {|Success (1,2)|}}
      child { node {|Or|}
        child { node {|Success (2,1)|} }
        child { node {$\cdots$}}};
  \end{tikzpicture}
  \caption{Infinite syntax tree created by |pairND|.}
  \label{fig:inf-ast}
\end{figure}

\begin{figure}
  \centering
  %  \includegraphics[width=0.4\textwidth]{ifl-fin-ast}
  \begin{tikzpicture}[sibling distance=6em, level distance=3em,
      every node/.style = { align = center }]
    \node{Or}
      child { node {|Success (1,2)|} }
      child { node {|Rec ()|\\|\| }
        child { node { |(x,y)| } }
        child { node { |Success (y,x)| }}};
  \end{tikzpicture}
  \caption{Finite syntax tree created by |pairNDRec ()|.}
  \label{fig:fin-ast}
\end{figure}

\subsection{Effect Handler Implementation}
\label{sec:effect-handler-implementation}

In this section we provide a Haskell implementation that correspond to the
denotational semantics from the previous section.
This implementation comes in the form of an effect handler
\cite{kiselyov2013extensible, wu2015fusion}.
Like the denotational semantics, the effect handler is split into two parts: a
part that delivers the solution for the entire function by computing a least
fixed point (similar to $\denot{}{\cdot}$), and the counterpart of
$\denot{R}{\cdot}$, which, given an environment, computes the results for one
syntax tree.

The first part is called |runNDRec| and the second part is called |go|.
The effect handler first computes the least fixed point of the |step| function.
The |step| function is a function that takes an environment (of type
|Map i (Set o)| from \texttt{Data.Map}\footnote{Functions and types residing in
  \texttt{Data.Map} are imported with the qualifier \texttt{M}, except the
  lookup operator ``|!|''.}) and obtains a new environment by
running |go| for every entry in the environment.
The fixed point of this function is again an environment, in which we look up
|i0|, the second argument to |runNDRec|.

> runNDRec  ::  (Ord i, Ord o)
>           =>  (i -> NDRec i o o) -> i -> Set o
> runNDRec expr i0 = lfp step s0 ! i0 where
>   s0 = M.singleton i0 empty
>   step m = foldr (\k -> go k (expr k)) m (M.keys m)

The function |go|, like $\denot{R}{\cdot}$ proceeds by case analysis on the
syntax tree.
However, unlike $\denot{R}{\cdot}$, |go| updates an environment instead of just
returning a set of solutions.
This difference is mainly for programming convenience.

> go  ::  (Ord i, Ord o)
>     =>  i -> NDRec i o o
>     ->  M.Map i (Set o) -> M.Map i (Set o)
> go i (Success a)  m  = M.insertWith union i (singleton a) m
> go i Fail         m  = m
> go i (Or l r)     m  = go i r (go i l m)
> go i (Rec j k)    m  = case M.lookup j m of
>   Nothing  -> M.insert j empty m
>   Just s   -> foldr (go i . k) m (toList s)

If the syntax tree contains a deterministic result (line 4), we simply add
this result to the environment.
If the syntax contains a failure (line 5), the environment remains unchanged.
When presented with a binary choice (line 6), we first update the environment
according to the left branch and then according to the right branch.
Finally, in the case of a recursive call |Rec j k|, we first check
if the map contains an entry for the argument |j| (line 7).
If it has no such entry, we add an entry containing the empty set (line 8),
otherwise we update the environment based on the existing elements (line 9).
Inserting an empty entry for |j|, ensures that the next iteration of |step|
also updates the map for |expr j|.

We still need a function |lfp| to iteratively compute a least fixed point.
Given an initial value, this function keeps calling itself until the result
no longer changes.

> lfp :: Eq a => (a -> a) -> a -> a
> lfp f a0 = let a1 = f a0 in if a1 == a0 then a1 else lfp f a1


As an example, consider the semantics of |pairNDRec|:

< >>> runNDRec pairNDRec ()
< fromList [(1,2),(2,1)]

The denotational semantics and its Haskell implementation coincide as expected,
but only up to termination (as we show in Section~\ref{sec:lattices}).

\myparagraph{Graph Example}
We compute reachability in a cyclic directed
graph using our non\hyp{}determinism framework.
We use the adjacency list representation of a graph where every |Node|
has a label (of type |String|) and a list of adjacent nodes:

> data Node = Node { label :: String, adj :: [Node] }
> instance Eq   Node where n1 == n2 = label n1 == label n2
> instance Ord  Node where n1 <= n2 = label n1 <= label n2

\begin{figure}
  \centering
  \begin{tikzpicture}
    \SetGraphUnit{2}
    \Vertex{1}
    \Vertex[x=2,  y=0]{2}
    \Vertex[x=2,  y=-1]{3}
    \Vertex[x=0,  y=-1]{4}
    \Vertex[x=-1.5, y=0]{5}
    \tikzset{EdgeStyle/.style={->,pos=.5}}
    \Edge(1)(2)
    \Edge(1)(5)
    \Edge(2)(3)
    \Edge(4)(1)
    \Loop[dist=2cm,dir=SO](5)
    \tikzset{EdgeStyle/.style={->,pos=.5, bend left}}
    \Edge(3)(4)
    \Edge(4)(3)
  \end{tikzpicture}
  \caption{The graph used in the reachable and shortest path example.}
  \label{fig:example-graph}
\end{figure}


Our example graph (see Figure~\ref{fig:example-graph}) consists of five nodes.
It contains two cycles: from 3 to 4 and back, and from 5 to itself.

> [n1, n2, n3, n4, n5] = [  Node "1" [n2,n5],
>                           Node "2" [n3],
>                           Node "3" [n4],
>                           Node "4" [n3,n1],
>                           Node "5" [n5]]

Reachability is straightforwardly computed as:

> reach :: Node -> NDRec Node Node Node
> reach n = return n `Or` (choose (adj n) >>= rec)

i.e. |n| is reachable from |n| and all nodes reachable from a neighbor of
|n| are reachable from |n|.

< >>> runNDRec reach n1
< fromList [Node 1, Node 2, Node 3, Node 4, Node 5]


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Dependency Tracking}
\label{sec:dependency-tracking}

The effect handler that was provided in Section~\ref{sec:effect-handler}
computes the least fixed point in a very naive way, resulting in sub par
performance.
To illustrate the problem, consider the following program to compute the
Fibonacci numbers:

> fib :: Int -> NDRec Int Int Int
> fib n  | n == 0     = return 0
>        | n == 1     = return 1
>        | otherwise  = do  f1 <- rec (n - 1)
>                           f2 <- rec (n - 2)
>                           return (f1 + f2)

The evolution of the environment for |runNDRec fib 2| after each application of
|step| is shown in Table~\ref{tab:fib-trace} (ignore the list after the comma
for now).
Every value in the environment is recomputed in every step.
The work performed by the effect handler is monotonic: the work performed
by a single iteration is also performed in all later iterations. In the case
of |fib| this leads to an $\mathcal{O}(n^2)$ runtime.\footnote{modulo a logarithmic
factor for the use of finite maps.}

\begin{table}
  \centering
  \small
  \begin{tabular}{@@{}lll@@{}}
    \toprule
    2: $\emptyset$,[]\\
    \midrule
    2: $\emptyset$,[] & 1: $\emptyset$,|[go 2 . k0]|\\
    \midrule
    2: $\emptyset$,[] & 1: $\{1\}$,|[go 2 . k0]|\\
    \midrule
    2: $\emptyset$,[] & 1: $\{1\}$,|[go 2 . k0]| & 0: $\emptyset$,|[go 1 . l1]|
    \\
    \midrule
    2: $\emptyset$,[] & 1: $\{1\}$,|[go 2 . k0]| & 0: $\{0\}$,|[go 1 . l1]|\\
    \midrule
    2: $\{2\}$,[]     & 1: $\{1\}$,|[go 2 . k0]| & 0: $\{0\}$,|[go 1 . l1]|\\
    \midrule
    2: $\{2\}$,[]     & 1: $\{1\}$,|[go 2 . k0]| & 0: $\{0\}$,|[go 1 . l1]|\\
    \bottomrule    
  \end{tabular}
  \caption{The trace of the environment for |runNDk fib 2|.}
  \label{tab:fib-trace}
\end{table}

This duplication of work has two sources:
%
\begin{enumerate}
\item The naive least fixed point computation |lfp| iteratively applies
  the |step| function which folds over \emph{all} keys in the environment,
  even when the set of outcomes of that key has not changed.
\vspace{-1mm}
\item The function |go| computes too much: the only part of the syntax tree
  that is influenced by a recursive call |Rec j k| is the \emph{continuation}
  |k| of that recursive call.
  Therefore only the continuation needs to be recomputed.
\end{enumerate}
%
Although efficiency is not the main focus of this paper, we would be remiss
not to address such a glaring problem. Especially since the solution is simple:
both problems can be solved by keeping track of which continuation depends on
which recursive call (identified by its argument), and only evaluating the
continuation \emph{once} for every new value of the recursive call.

Figure~\ref{fig:Env} shows the code for the interpreter |runNDk| that tracks
dependencies.
This effect handler relies on seven auxiliary functions defined for the type
|Env i o|.
This type abstracts over environments that contain continuations in addition
to a set of values.
We show their signatures and implementations in Figure~\ref{fig:Env}.

The computation starts with the call to |go| on line 1, which returns the
environment after the least fixed point computed.
From this environment the result is extracted using |!!!|.
The |go| function proceeds by case analysis:
if the syntax tree is |Success x| and the value |x| is not yet in the
environment, the environment is updated by sequentially applying every
continuation that is currently in the environment (line 2-3).
If the syntax tree is a failed computation the environment is unchanged
(line 4).
If the syntax tree is a binary choice (line 5) the environment is updated first
for the left branch, then for the right branch.
If the syntax tree is a recursive call |Rec j k| (line 6-10), and there is
no entry for |j| in the map (line 9), i.e. this is the first time we see
a call to |j|, a continuation for |j| is added (line 7) and the recursive
function is evaluated for |j|.
Otherwise (line 10), a new continuation is added for |j|, and this continuation
is applied to every known result of |j|.
The least fixed point is reached when the |ifNew| check on line 2 no longer succeeds.

The trace for |runNDk fib 2| is identical to the trace for |runND fib 2|,
but now only a single entry is updated in each iteration.
Moreover, the result from the call to |1| only triggers the
continuation |k0 = \f1 -> Rec 0 (\f2 -> return (f1 + f2))| and
the result of |0| only triggers computation of |l1 = \f2 -> return (1 + f2)|,
instead of the entire syntax tree |fib 2|.

{\setlength{\mathindent}{0pt}
\begin{figure*}[tbh]

  \begin{minipage}[t]{0.52\textwidth}

    
> runNDk :: (Ord i, Ord o) => (i -> NDRec i o o) -> i -> Set o
> runNDk expr i0 = go i0 (expr i0) emptyEnv !!! i0 where   -- (1)
>     go i (Success x)  m  = ifNew i x m newEnv where      -- (2)
>       newEnv = foldr ($x) (store i x m) (conts i m)      -- (3)
>     go i Fail         m  = m                             -- (4)
>     go i (Or l r)     m  = go i r (go i l m)             -- (5)
>     go i (Rec j k)    m  =                               -- (6)
>       let newEnv = addCont j (go i . k) m                -- (7)
>       in case results j m of                             -- (8)
>         Nothing  -> go j (expr j) newEnv                 -- (9)
>         Just rs  -> foldr (go i . k) newEnv (toList rs)  -- (10)
>
> ifNew  :: (Ord i, Ord o)
>        => i -> o -> Env i o -> Env i o -> Env i o
> ifNew i o env newEnv =
>  if maybe True (not . member o) (results i env) 
>    then newEnv
>    else env

  \end{minipage}
  \begin{minipage}[t]{0.48\textwidth}

> type Env i o = M.Map i (Set o, [C i o])
> newtype C i o = C { runC :: o -> Env i o -> Env i o }
>
> emptyEnv :: Env i o
> emptyEnv = M.empty
>
> store :: (Ord i, Ord o) => i -> o -> Env i o -> Env i o
> store i o = M.insertWith mappend i (singleton o, [])
>
> results :: Ord i => i -> Env i o -> Maybe (Set o)
> results i = fmap fst . M.lookup i
>
> (!!!) :: Ord i => Env i o -> i -> Set o
> m !!! i = fromJust (results i m)
>
> conts :: Ord i => i -> Env i o -> [o -> Env i o -> Env i o]
> conts i = map runC . M.findWithDefault [] i . fmap snd
>
> addCont  :: (Ord i, Ord o) => i -> (o -> Env i o -> Env i o)
>          -> Env i o -> Env i o
> addCont i k = M.insertWith mappend i (empty, [C k])

  \end{minipage}
\caption{Effect Handler and Environment type for dependency tracking.}
\label{fig:Env}
\end{figure*}}

In summary, we obtain a linear runtime for |fib|.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Lattices}
\label{sec:lattices}

Reconsider the graph defined in Section~\ref{sec:effect-handler}.
Instead of just finding all reachable nodes, we may want to additionally know
the length of the \emph{shortest} path.
The program |sp| is a first attempt at computing the shortest path between
|src| and |dst|.

> sp :: Node -> Node -> NDRec Node Int Int
> sp dst src
>   | dst == src  = return 0
>   | otherwise   = choose (adj src) >>= rec >>= return . (+1)
                                        
The denotational semantics gives us the following value for the expression
|sp n1 rec n2|:
%
\[ \denot{}{|sp n1 rec|}(|n2|) = \{2,4,6,8,\ldots\} \]
%
Since this value is infinite, the effect handler does not terminate.
In order to implement a terminating effect handler, we need to first
define an alternative denotational semantics that at least produces a finite
value, which we can then compute in a finite amount of time.

%-------------------------------------------------------------------------------
\myparagraph{Lattice-based Semantics}
\label{sec:lattices:semantics}
Instead of powersets $\powerset{O}$, we generalize the semantic domain to an
arbitrary complete lattice $L$.
On this domain we define a new denotational semantics for non\hyp{}deterministic
computations.
The signature of this denotational semantics is:
%
\begin{align*}
  \denot{}{\cdot}_L :
  (I \rightarrow |NDRec I L L|) \rightarrow (I \rightarrow L)
\end{align*}
%
where $\langle L,\sqsubseteq \rangle$ is a complete lattice.

A complete lattice is a partially ordered set $\langle S,\sqsubseteq\rangle$ such that for
every subset $X \in S$ there exists a least upper bound, i.e. an element
$\sqcup X$ that is the least element that is larger than every element
in $X$, more formally:
%
\begin{definition}[Complete lattice]
  A complete lattice $\langle L,\sqcup \rangle$ is a partially ordered set $\langle L,\sqsubseteq \rangle$
  where $\forall X \subseteq L$ there exists a least upper bound
  $\sqcup X \in L$, such that:
%  
  \[
  \forall z \in L:\sqcup X \sqsubseteq z \iff \forall x \in X: x \sqsubseteq z
  \]
\end{definition}
%
It follows from the definition that $\sqcup X$ is unique.
A complete lattice $L$ always has a least element $\bot$ that is smaller than
all other elements.
It corresponds to the least upper bound of the empty set:
$\bot = \sqcup\emptyset$.
With a slight abuse of notation we also write $a \sqcup b$ to denote the least
upper bound $\sqcup\{a,b\}$, pronounced ``join''.


As before, we first consider the semantics for the case where we already
possess an environment $s : I \rightarrow L$ containing the (partial) solution
for every call $f(a)$, i.e.
%
\[ s(a) \sqsubseteq \denot{}{f}_L(a) \text{ for every } a \in I \]
%
Then for every syntax tree $t : |NDRec I L L|$ we can define the denotational
semantics $\denot{R}{\cdot}_L$ in the new setting:
%
\begin{align*}
  \denot{R}{&\cdot}_L \mathrlap{: |NDRec I L L|
  \rightarrow (I \rightarrow L) \rightarrow L}\\
  \denot{R}{&|Success x|&}_L(s) &= x\\
  \denot{R}{&|Fail|     &}_L(s) &= \bot\\
  \denot{R}{&|Or l r|   &}_L(s) &= \denot{R}{l}_L(s) \sqcup \denot{R}{r}_L(s)\\
  \denot{R}{&|Rec j k|  &}_L(s) &= \denot{R}{k(s(j))}_L(s)
\end{align*}
%
This semantics is very similar to the previously defined semantics for sets.
In particular, the semantics $\denot{R}{\cdot}$ almost corresponds to the
semantics $\denot{R}{\cdot}_L$ specialized to the powerset of the output type
($\denot{R}{\cdot}_{\powerset{O}}$).
We will make this relationship more precise in Section~\ref{sec:generalized-effect-handlers}.

For $\denot{}{f}_L$ we would again like to compute the least fixed point of
$\lambda s.\lambda a.\big(\denot{R}{f(a)}_L(s)\big)$.
However, unlike for sets, this function is not guaranteed to be
monotonic for arbitrary lattices in general, so a least fixed point might not
always exist.
Instead, we define the operator~$\monotonicit$:
%
\begin{definition}
The operator~$\monotonicit$ is defined as:
\begin{align*}
  f \monotonicit 0       &= \bot\\
  f \monotonicit (i + 1) &= f \monotonicit i \sqcup f(f \monotonicit i)\\
  f \monotonicit \infty  &= \bigsqcup_{i=0}^\infty (f \monotonicit i)
\end{align*}
\end{definition}
%
Observe that the sequence
$f \monotonicit 0$, $f \monotonicit 1$, $f \monotonicit 2,\ldots$,
$f \monotonicit \infty$ is non-decreasing.\footnote{For environments, assume
the order $\preccurlyeq$ defined as:
\[ s_1 \preccurlyeq s_2 \iff \forall a \in I: s_1(a) \sqsubseteq s_2(a)\]}

If the domain of $f$ has the \emph{finite ascending chain property}
this sequence is finite, and then $f \monotonicit \infty$ must be a fixed
point.
Thus, we define~$\denot{}{\cdot}_L$ as:
%
\[
\denot{}{f}_L = (\lambda s.\lambda a.\denot{R}{f(a)}_L(s)) \monotonicit \infty
\]
%
Moreover, if $\lambda s.\lambda a.\denot{R}{f(a)}_L(s)$ is continuous, then
$\monotonicit$ computes the least fixed point, i.e.
%
\[
\lfp(\lambda s.\lambda a.\denot{R}{f(a)}_L(s))
= (\lambda s.\lambda a.\denot{R}{f(a)}_L(s)) \monotonicit \infty = \denot{}{f}_L
\]
%
Otherwise, $\monotonicit$ may compute a fixed point that is strictly greater
than the least fixed point.
In practice, however, almost all functions are continuous.

%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
\myparagraph{Shortest Path Example}
For our shortest path problem, the relevant partial order is
$\langle \mathbb{N}~\cup~\{\infty\}, \sqsubseteq \rangle$ where
%
\[
  a \sqsubseteq b \iff a = \infty \text{ or } b \leq a
\]
%
which is the reverse order of the canonical order on $\mathbb{N}$, i.e.
smaller (shorter) is better.
Since the canonical order is well-founded (i.e. has no infinite descending
chains), $\sqsubseteq$ has the finite ascending chain property.
The relevant lattice is $\langle \mathbb{N}\cup\{\infty\},\sqcup \rangle$ where
%
\[
  \sqcup X = \begin{cases}
    \infty  & \text{ if } X = \emptyset\\
    \min_{\sqsubseteq} X  & \text{ otherwise}
  \end{cases}
\]
%
In this lattice the least fixed point exists, since continuation
|return . (+1)| in |sp n1| is continuous.
%
\[ \denot{}{|sp n1|}_{\mathbb{N}\cup\{\infty\}}(|n2|) = 3\]
%
The length of the shortest path from node~2 to node~1 is indeed 3.
When a path does not exist, we get the following length:
%
\[\denot{}{|sp n1|}_{\mathbb{N}\cup\{\infty\}}(|n5|) = \infty\]
%
Notice the similarity to solving a problem with \emph{Dynamic Programming}.
The idea of dynamic programming is to compute the optimal solution to a problem
by combining optimal solutions to smaller instances of the same problem.
In this setting, recursive calls correspond to instances of the same problem.
The lattice allows us to keep only the optimal solution to a given instance,
from which the result of a larger instance is computed.

%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
\myparagraph{Subset Sum Example}
Another example is the following:\\
\emph{
  Given a list of integer numbers and an integer $s$, find the shortest
  non-empty sublist that sums to $s$.}

To solve this problem, first observe that lists form a lattice when ordered by
descending length, if we adjoin a bottom element |InfList| representing any
list of infinite length.
This is embodied by the |Shortest|-datatype (see
Section~\ref{sec:effect-handler-implementation} for the definition of
|Lattice|).

> data Shortest = InfList | List [Int]
>
> cons :: Int -> Shortest -> Shortest
> cons n InfList    = InfList
> cons n (List ns)  = List (n:ns)
> 
> instance Lattice Shortest where
>   bottom = InfList
>   join InfList a = a
>   join a InfList = a
>   join (List a) (List b)
>     | length a <= length b  = List a
>     | otherwise             = List b

The function |sss| returns the |Shortest| list which sums to its first
argument and is a sublist of of its second argument.

> sss :: (Int,[Int]) -> NDRec (Int,[Int]) Shortest Shortest
> sss (n,[])
>   | n == 0     =  return (List [])
>   | otherwise  =  Fail
> sss (n, x:xs)  = choice [  rec (n,xs),
>                            fmap (cons x) (rec (n - x, xs))]

When the input list is empty we can only construct an empty list with sum $0$.
When the input list is non-empty, the shortest list summing to |n| is obtained
by either recursively searching for a list summing to |n|,  or |cons|-ing |x|
onto a list that sums to |n-x|.

The function $\denot{R}{|sss (n,xs)|}_L$ is continuous for all ($n$, $xs$).
Moreover, the environment possesses a finite ascending chain property:
every recursive call decreases the size of the input list by one.
Then, given a finite input list, there can only be a finitely many recursive
calls.

For instance, consider the application of |sss| to |(10,[5,0,5])|:
%
\[ \denot{}{|sss|}(10,[5,0,5]) = List [5,5]\]
%
Indeed |sum [5,5] == 10|.
Note that the list |[5,0,5]| itself also sums to 10.
However, only the shortest list is retained.

\myparagraph{Grammar Analysis example}
We demonstrate the added power of the lattice-based semantics by solving a
simple grammar analysis problem.
Informally, a grammar is a list of rules or productions:

> type Grammar = [Production]

A production is of the form

> data Production = Symbol :--> [Symbol]

There is a single |Symbol| to the left of |:-->|, called the head, and zero or
more symbols, the body, on the right hand side. Symbols can
be either terminals or non-terminals:

> type Symbol = Char
> isTerminal :: Symbol -> Bool
> isTerminal s = not (isUpper s)

Terminals may not appear in the head of a rule.
For our purpose we distinguish terminals and non-terminals based on whether
they are upper or lower case characters.

Now, consider the following grammar:

> grammar :: Grammar
> grammar = [  'E' :--> "T Z",    'E' :--> "(E)",
>              'Z' :--> "+ T Z",  'Z' :--> "+ (E)",
>              'Z' :--> "",       
>              'T' :--> "a",      'T' :--> "1"]

This grammar describes a simple expression language consisting of one
identifier (|a|), one literal (1) and a binary operator (|+|).
We implement a program that analyses grammars for nullability:
a symbol is |Nullable| if it derives the empty string.
Nullability is characterized by a function from |Symbol| to |Any|, where
|Any| is a lattice of booleans, where |join| is disjunction and |False|
is |bottom|.

< newtype Any = Any { getAny :: Bool }
< instance Lattice Any where
<   bottom = Any False
<   join (Any a) (Any b) = Any (a || b)

Given this lattice, the implementation of |nullable| is straightforward:

> nullability :: Grammar -> Symbol -> NDRec Symbol Any Any
> nullability g s
>   |  isTerminal s = return (Any False)
>   |  otherwise    = do
>        head :--> body <- choose g
>        guard (head == s)
>        nullables <- mapM rec body
>        return (Any (and (map getAny nullables)))

A terminal symbol is never nullable.
In order to decide if a non-terminal symbol |s| is nullable, we need to check if there
exists a production with |s| in the head and with a body consisting of
nullable symbols.
Note that |s| may also occur in the body.

We evaluate |nullability| for |grammar|:
%
\begin{align*}
  \denot{}{|nullability grammar|}_{|Any|}(|'T'|) &= |Any False|\\
  \denot{}{|nullability grammar|}_{|Any|}(|'Z'|) &= |Any True|\\
  \denot{}{|nullability grammar|}_{|Any|}(|'E'|) &= |Any False|
\end{align*}
%
We have demonstrated that a semantics based on lattices allows us to
tackle problems we could not previously solve in a finite amount of time.
Note that we again arrived at this solution by computing a least fixed point,
but of a different function.
Furthermore, only the semantics of the syntax has changed, the syntax itself is
unmodified. Hence, to implement this semantics only the effect handler
needs to be reworked.

%-------------------------------------------------------------------------------
\subsection{Generalized Effect Handler}
\label{sec:generalized-effect-handlers}

This section shows that the semantics from
Section~\ref{sec:lattices:semantics} is strictly more general than the
semantics from Section~\ref{sec:effect-handler-implementation}.
The |NDRec| monad is a free monad.
More precisely, it is the coproduct monad of two other free monads
respectively implementing non\hyp{}determinism and recursion.
In this section we make this coproduct explicit by following the data types 
\`a la carte approach \cite{swierstra2008data}.
All free monads have the following shape:

> data Free f a = Return a | Free (f (Free f a))

They either return a pure value (|Return|) or an impure effect (|Free|),
constructed using |f|.
When |f| is a |Functor|, |Free f| is a monad:

> instance Functor f => Monad (Free f) where
>   return a = Return a
>   Return a  >>=  f = f a
>   Free g    >>=  f = Free (fmap (>>= f) g)

Functions interpreting |Free| are built using the function |fold|:

> fold :: Functor f => (a -> b) -> (f b -> b) -> Free f a -> b
> fold ret alg (Return a)  = ret a
> fold ret alg (Free f)    = alg (fmap (fold ret alg) f)

where the first argument |ret| is applied to pure values and the second
argument |alg| is an algebra that is applied to impure values.

We use the coproduct |+| of two |Functor|s to express the combination of
two effects:

> data (f + g) a = Inl (f a) | Inr (g a) deriving Functor

This data type is similar to the |Either| type, but it works on type
constructors (kind |* -> *|) instead of types (kind |*|).
We use the following two effect signatures to express recursion and
non\hyp{}determinism respectively.
Their functor instances are automatically derived by GHC.

> data RecF i o a = RecF i (o -> a)  deriving Functor
> data NDF a = OrF a a | FailF       deriving Functor

For convenience, we label the combined free monad (which is isomorphic to
the |NDRec| type) with a type synonym |NDRecF|, and define some
smart constructors for it:

> type NDRecF i o = Free (NDF + RecF i o)
>
> orF  ::  NDRecF i o a ->  NDRecF i o a ->  NDRecF i o a
> orF l r = Free (Inl (OrF l r))
>
> chooseF :: [a] -> NDRecF i o a
> chooseF = foldr orF failF . map return where
>   failF = Free (Inl FailF)
>
> recF :: i -> NDRecF i o o
> recF i = Free (Inr (RecF i Return))

\myparagraph{Effect Handler Implementation for Lattices}
The typeclass\\|Lattice| is defined as follows:

> class Lattice l where
>  bottom  :: l
>  join    :: l -> l -> l

This typeclass prioritizes the view of the $\sqcup$-operator as a binary
operator because this is the most useful in practice.

Now we replace every |FailF| in a value of type |NDRecF| with |bottom|
and every |OrF| with |join|:

> runND  :: (Functor f, Lattice o)
>        => Free (NDF + f) o -> Free f o
> runND  = fold return alg where
>   alg (Inl FailF)      = return bottom
>   alg (Inl (OrF l r))  = join <$> l <*> r
>   alg (Inr f)          = Free f

The effect handler |runND| effectively eliminates the |NDF| effect from
the syntax tree.
Only the recursive effect |RecF i o| remains.
This effect is then interpreted by |runRec|:

> runRec  :: (Ord i, Eq o, Lattice o)
>         => (i -> Free (RecF i o) o) -> i -> o
> runRec  expr i0 = lfp step s0 ! i0 where
>   s0       = M.singleton i0 bottom
>   step m   = foldr (\k -> go (expr k) k) m (M.keys m)
>   go expr  = fold onReturn alg expr where
>     onReturn x i = M.insertWith join i x
>     alg (RecF j k) i m = case M.lookup j m of
>       Nothing  -> k bottom i (M.insert j bottom m)
>       Just l   -> k l i m

Note that |runRec| does not expect a plain effect, but an effectful function
|i -> Free (RecF i o) o|, and also returns a function |i -> o|.
Essentially, |runRec| is a memoizing fixpoint operator applied to the recursive
equation defined by |expr|.

Reconsider the shortest path program from Section~\ref{sec:lattices}:

> spF :: Node -> Node -> NDRecF Node Dist Dist
> spF dst src  | src == dst  = return 0
>              | otherwise   = do  n <- chooseF (adj src)
>                                  d <- recF n
>                                  return (d + 1)

where the |Dist| datatype and its instances are defined as:

> data Dist = InfDist | Dist Int deriving (Eq, Show)
> instance Lattice Dist where
>   bottom = InfDist
>   join  InfDist    a          = a
>   join  a          InfDist    = a
>   join  (Dist d1)  (Dist d2)  = Dist (min d1 d2)
>
> instance Num Dist where
>   InfDist  +  a        =  InfDist
>   a        +  InfDist  =  InfDist
>   Dist d1  +  Dist d2  =  Dist (d1 + d2)
>   fromInteger = Dist . fromInteger 

Note that |Dist| has a |Lattice|-instance corresponding to the lattice
$(\mathbb{N}\cup\{\infty\},\sqcup)$ from Section~\ref{sec:lattices:semantics}.
Combining |runND| and |runRec| gives an effect handler |runRec . (runND .)|
implementing the semantic function $\denot{}{\cdot}_L$.
For |spF| this effect handler computes the expected result:

< >>> runRec (runND . spF n1) n2
< Dist 2
< >>> runRec (runND . spF n1) n5
< InfDist


\myparagraph{Lifting Continuations}
The semantic functions $\denot{R}{\cdot}$ and $\denot{R}{\cdot}_L$ only differ
in the case for |Rec j k|: $\denot{R}{\cdot}$ computes the union of the
|k| applied to every element of $s(j)$, while $\denot{R}{\cdot}_L$
directly applies the |k| to $s(j)$.
The similarity between $\denot{R}{\cdot}$ and $\denot{R}{\cdot}_{\powerset{O}}$
suggests that we can obtain the behaviour of the former with the latter if we
just change the continuation in |Rec j k| appropriately.
More formally, we want to construct a continuation |k'| such that:
%
\begin{align*}
  \denot{R}{|Rec j k|}(s) = \denot{R}{|Rec j k'|}_{\powerset{O}}(s)
\end{align*}
%
This is achieved by the following code, that lifts a computation in the
|Free (NDF + RecF i o)|-monad to one in the |Free (NDF + Rec i (Set o))|-monad.

> liftRec  :: Ord o
>          => Free (NDF + RecF i    o       ) o
>          -> Free (NDF + RecF i (  Set o)  ) (Set o)
> liftRec = fold (return . singleton) alg where
>   alg (Inl f)           = Free (Inl f)
>   alg (Inr (RecF i k))  = Free (Inr (RecF i k')) where
>     k' = fmap unions . traverse k . toList


Since the powerset $\langle\powerset{O},\cup\rangle$ forms a lattice, we can define a
|Lattice| instance for |Set|:

> instance Ord a => Lattice (Set a) where
>  bottom  = empty
>  join    = union

Composing |liftRec|, |runND| and |runRec| yields a complete effect handler for
|Free (NDF + RecF i o)|, corresponding to $\denot{}{\cdot}$:

> runNDRecF  :: (Ord i, Ord o)
>            => (i -> Free (NDF + RecF i o) o)
>            -> (i -> Set o)
> runNDRecF f = runRec (runND . liftRec . f)

\myparagraph{Examples}
Redefining our running example:

> pairF :: () -> NDRecF () (Int,Int) (Int, Int)
> pairF () = return (1,2) `orF` do  (x,y) <- recF ()
>                                   return (y,x)

leads to the following familiar result:

< >>> runRecF pairF ()
< fromList [(1,2),(2,1)]

To recapitulate, we have shown how to implement the effect handler for
$\denot{}{\cdot}_L$ for |NDRecF|.
On top of this we implement the effect handler for $\denot{}{\cdot}$ for sets,
showing that the semantics for lattices generalize the semantics for sets.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Mutual Recursion}
The syntax we have defined so far has a serious limitation: it can only handle
a single function at a time.
This section lifts this limitation by extending the syntax to support several,
potentially mutually recursive functions within the same non\hyp{}deterministic
computation.
Moreover, these functions are allowed to possess different argument and return
types.

\myparagraph{Motivating Example}
We demonstrate the added power with another grammar analysis.
We implement a program that finds the |First| set of a symbol.
A symbol $X$ is in the |First| set of another symbol $Y$ if any of the
strings derived from $Y$ start with $X$.
Solving |First| requires also solving |Nullable|.

Recall that nullability is characterized by a function from |Symbol| to |Any|.
First is a also characterized by a function, but one from |Symbol| to
|Set Symbol| (recall that |Set| is also a lattice).
We encode this fact with a Generalized Algebraic Data
Type (GADT) \cite{jones2006simple}:

> data Analyze o where
>   Nullable  :: Symbol -> Analyze Any
>   First     :: Symbol -> Analyze (Set Symbol)

The arguments to the \emph{data} constructors |Nullable| and |First| indicate
the argument type (in both cases |Symbol|).
The argument to the \emph{type} constructor |Analyze| indicates the
return type (|Any| or |Set Symbol|).

The syntax data type |NDM| is a variation of |NDRec|:

> data NDM i a where
>   SuccessM  ::  a -> NDM i a
>   FailM     ::  NDM i a
>   OrM       ::  NDM i a -> NDM i a -> NDM i a
>   RecM      ::  Lattice o
>             =>  i o -> (o -> NDM i a) -> NDM i a

The recursive call constructor |RecM| expects an existentially qualified
parameter |o| (which must be a lattice).
The previous syntax introduces |o| in the type, and as such fixes |o| to one
particular type for the entire syntax tree.
Because of the existential parameter, |NDM| \emph{does} allow calls with
different return types.
Furthermore, the argument is of type |i o|.
When |i| is a GADT, such as |Analyze|, the
type |i o| will only be inhabited for the desired |o|s (e.g. |Any| and
|Set Symbol| for |Analyze|).

We also define four additional smart constructors |recM|,\\|chooseM|, |choiceM|
and |guardM|.
Their meaning is analogous to the smart constructors for |NDRec|.
For brevity, their signature and implementation is omitted here.
With these smart constructors we define explicit recursive calls for
|nullable| and |first|.

> nullable :: Symbol -> NDM Analyze Bool
> nullable = fmap getAny . recM . Nullable
>
> first :: Symbol -> NDM Analyze Symbol
> first s = recM (First s) >>= chooseM . toList

Solving |Nullable| and |First| is straightforward:

> analyze :: Grammar -> Analyze o -> NDM Analyze o
> analyze g (Nullable s) 
>   |  isTerminal s = return (Any False)
>   |  otherwise    = do
>        head :--> body <- chooseM g
>        guardM (head == s)
>        nullables <- mapM nullable body
>        return (Any (and nullables))
> analyze g (First s)
>   |  isTerminal s = return (singleton s)
>   |  otherwise = do
>        head :--> body <- chooseM g
>        guardM (head == s)
>        nullables <- mapM nullable body
>        let  nulls     = takeWhile  snd (zip body nullables)
>             notNulls  = dropWhile  snd (zip body nullables)
>             prefix    = nulls ++ take 1 notNulls
>             firsts    = map (first . fst) prefix
>        terminal  <- choiceM firsts
>        return (singleton terminal)

In order to determine the |First| set of a symbol |s|, we need to find the
longest nullable prefix of the body for every production with |s| in the head.
Then we find the |First| sets of every symbol in the prefix and the symbol
directly following the prefix.

\myparagraph{Implementation}
From |Analyze| we derive the actual input and output types that need to
be stored in the environment:

> type EnvM i = M.Map (Input i) (Output i)
>
> data Input (i :: * -> *) where
>   Input :: Lattice o => i o -> Input i
>
> data Output (i :: * -> *) where
>   Output :: i o -> o -> Output i

The data types |Input| and |Output| wrap a particular instantiation of the
|Analyze| type constructor in an existential quantification.
The implementation of the |Eq| and |Ord| instances is elided for brevity.

We use the typeclass |Coerce| to type-safely coerce |Output Analyze| back to
|Any| or |Set Symbol|, depending on which constructor of |Analyze| is
pattern-matched.

> class Coerce c where
>   coerce :: c o -> Output c -> Maybe o
>
> instance Coerce Analyze where
>   coerce (Nullable s)  (Output (Nullable t) o)  = Just o
>   coerce (First s)     (Output (First t) o)     = Just o
>   coerce a             o                        = Nothing

Now we lift |join| to |Output|s: 

> joinO  :: (Coerce i, Lattice o)
>        => i o -> Output i -> Output i -> Output i
> joinO i o1 o2 =
>   maybe o1 (Output i) (join  <$>  coerce i o1
>                              <*>  coerce i o2)

At last we have all the pieces to define the effect handler.
The function |goM| evaluates an |NDM i o|, given an environment
|EnvM i| and |runNDM| computes the least fixed point of |goM|:
%{
%format . = "\,.\,"

> goM  :: (Coerce i, Lattice o, Ord (Input i))
>      => i o -> NDM i o -> EnvM i -> EnvM i
> goM  i  (SuccessM x)  m =
>   M.insertWith (joinO i) (Input i) (Output i x) m
> goM  i  FailM         m = m
> goM  i  (OrM l r)     m = goM i r (goM i l m)
> goM  i  (RecM j k)    m =
>   case M.lookup (Input j) m >>= coerce j of
>     Nothing  -> M.insert (Input j) (Output j bottom) m
>     Just s   -> goM i (k s) m
>
> runNDM  
>  :: (Ord (Input i), Eq (Output i), Coerce i, Lattice a)
>  => (forall o . Lattice o => i o -> NDM i o) -> i a -> a
> runNDM expr i0 = 
>   fromJust (coerce i0 (lfp step s0 ! Input i0)) where
>     s0 = M.singleton (Input i0) (Output i0 bottom)
>     step m = foldr goM' m (M.keys m) where
>       goM' (Input k) = goM k (expr k)

%}
This implementation is not very different from |runNDRec|
(see Section~\ref{sec:effect-handler}), except that some wrapping and
unwrapping of |Input| and |Output| is required.
Note that dependency tracking is orthogonal to the problem of mutual
recursion, i.e. dependency tracking is easily added.

\myparagraph{Evaluation}
Using |runNDM| we now solve |First|, which requires solving |Nullable|,
\emph{in the same computation}.

> analyzeF :: Grammar -> Symbol -> [Symbol]
> analyzeF g = toList . runNDM (analyze g) . First

< >>> map (analyzeF grammar) ['E','Z','T']
< ["(1a", "+", "1a"]

The computed |First| sets of \texttt{E},\texttt{Z} and \texttt{T} are
the results one would expect from the definition: an expression may
start with a parenthesis, a literal or an identifier, and similarly for
sub-expressions and terms.
We now extend |First| to strings of symbols:

> firstS :: Grammar -> [Symbol] -> Set Symbol
> firstS g s =
>  let  nulls     = takeWhile  analyzeN s
>       notNulls  = dropWhile  analyzeN s
>       prefix    = nulls ++ take 1 notNulls
>       analyzeN  = getAny . runNDM (analyze g) . Nullable
>  in unions $ map  (runNDM (analyze g) . First) prefix

So called \emph{left-recursive conflicts} occur when the |First| sets of
bodies of two rules with the same head have a non-empty intersection.
The following function |hasConflict| uses |firstS| of rule bodies to detect
conflicts:

> hasConflict :: Grammar -> Bool
> hasConflict g =
>   let  ruleHead (h  :--> _)  = h
>        ruleBody (_  :--> b)  = b
>        ruleConflict  [r]  = False
>        ruleConflict  rs   = not (null (foldr1 intersection firsts))
>          where firsts = map (firstS g . ruleBody) rs
>   in or $ map ruleConflict $ groupBy ((==) `on` ruleHead) g

Our example grammar is conflict free:

< >>> hasConflict grammar
< False

However, in the following grammar the rules for 'E' do conflict:

< grammar2 :: Grammar
< grammar2 = [  'E' :--> "E + T",  'E' :--> "T",
<               'T' :--> "a",      'T' :--> "1"]
< >>> hasConflict grammar2
< True

Note that the implementation of |firstS| and |hasConflict| is not very
efficient: in between calls to |runNDM| the results are simply thrown away.
We see two ways to resolve this: either have |runNDM| return the environment
|EnvM| to directly perform look-ups, or embed |firstS| and |hasConflict| in
|NDM| as well.
Such an embedding requires extending |Analyze| with new constructors for
|FirstS| and |HasConflict|.
This is left as an exercise to the reader.
In a more generic setting, one could imagine using a
``data types \`a la carte''-approach to solve this extension more
satisfactorily.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Benchmarks}
\label{sec:benchmarks}

To demonstrate the performance impact of dependency tracking, we evaluate
several benchmarks with and without dependency-tracking effect handlers and
compare their results.

We consider five problems: computing (large) fibonacci numbers,
(a variation of) the knapsack problem, the nqueens problem, computing
strongly connected components in a directed graph and
finding the shortest path in a directed graph.

The benchmarks were build using the Criterion-benchmark harness and compiled
using GHC 7.10.2 (with optimization setting \texttt{-O2}) on a 64-bit Linux
machine with an Intel Core i7-2600 @@ 3.6 Ghz and 16 GB of RAM.
All code and results are available in the the Bitbucket
repository.\footnote{\url{https://bitbucket.org/AlexanderV/thesis},\\
  in the \texttt{IFL2015/benchmarks} directory.}

\begin{table}
  \centering
  \small
  \begin{tabular}{@@{}lrrr@@{}}
    \toprule
                           & Size & Naive     & Dependency\\
                           &      &           & \multicolumn{1}{l}{Tracking}\\
    \midrule
    \textbf{Fibonacci}     & 800  & 455.7     & 13.77\\
                           & 805  & 462.8     & 13.85\\
                           & 810  & 469.3     & 13.97\\
    \midrule
    \textbf{Knapsack}      & 10   & 155.9     & 8.185\\
                           & 15   & 226.6     & 12.32\\
                           & 20   & 309.0     & 16.04\\
    \midrule
    \textbf{NQueens}       & 7    & 4.864     & 1.259\\
                           & 9    & 152.9     & 26.79\\
                           & 10   & 914.8     & 132.0\\
    \midrule
    \textbf{SCC}           & 30   & 57.31     & 8.843\\
                           & 33   & 104.9     & 10.67\\
                           & 35   & 97.97     & 12.67\\
    \midrule
    \textbf{Shortest Path} & 8000 & 1306      & 676.6\\
                           & 8500 & 1362      & 718.7\\
                           & 9000 & 1451      & 775.1\\
    \bottomrule
  \end{tabular}
  \caption{The benchmark results in milliseconds.}
  \label{tab:benchmarks}
\end{table}

Table~\ref{tab:benchmarks} shows the ordinary least squares regression of all
samples of the execution time.
The $R^2$ goodness of fit was above 0.99 in all cases.
All values are in milliseconds.

For \emph{Fibonacci} the first column is the index of the fibonacci
number that is computed.
For \emph{Knapsack} it specifies the number of items to choose from
(the capacity of the knapsack is fixed to 200).
For \emph{NQueens}, it indicates the number of queens that need to be placed
on the board.
For \emph{SCC} it is the number of nodes in the graph (the number of edges
is fixed to half the number of possible edges).
Finally, for \emph{Shortest Path} it is the number of edges in the graph.

The results show that effect handlers with dependency tracking massively
outperform the naive effect handlers.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related Work}
\label{sec:related-work}

There are various related works.

%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
\myparagraph{Least fixed point computations}
In their seminal work~\cite{scott1971toward} Scott and Strachey have introduced
the idea of denotational mathematical semantics.
In particular, they already define the semantics of programming language
syntax based on least fixed points.

Jeannin et al. \cite{jeannin2013programming} give a category theoretic
treatment of ``Non-well-Founded computations''.
They give many examples, several of which can be implemented in our
non\hyp{}deterministic framework.
Others are beyond our iterative least fixed point solver, and
require more sophisticated techniques, for instance, Gaussian elimination.

The use of least fixed points for solving grammar analysis problems has been
extensively treated by Jeuring and Swierstra \cite{jeuring1995constructing}.

It is a well-known result that dynamic programming can be viewed
as memoization of recursive equations.
The related work section of Swadi et al. gives an
overview~\cite{swadi2006monadic}.


%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
\myparagraph{Effect Handlers}
Algebraic effects as a way to write effectful programs are the subject of much
contemporary research~\cite{wu2015fusion, kiselyov2013extensible}.

New languages have been created specifically for studying algebraic effect
handlers: \emph{Eff}~\cite{bauer2015programming} extends ML with syntax
constructs for effect handlers and
\emph{Frank}~\cite{mcbride2012the} which has a Haskell-like syntax and tracks
effect in its type system.

\myparagraph{Non\hyp{}determinism}
The classical introduction of non\hyp{}determinism in functional programming based
on the list monad is due to Wadler~\cite{wadler1985}.
Hinze ~\cite{hinze2000deriving} derives a backtracking monad transformer from
a declarative specification.
This transformer adds backtracking (non\hyp{}determinism and failure) to any
monad.

Kiselyov et al.~\cite{kiselyov2005backtracking} improve on Hinze's work
by implementing backtracking monad transformers which support fair
non\hyp{}deterministic choice.
They present three different implementations:
the classical approach based on \emph{Streams} (i.e. lazy lists), one based on
continuation passing style and another one based on control operators for
delimited continuations.

%- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - 
\myparagraph{Tabling in Prolog}
Since Prolog has native non\hyp{}determinism, it too suffers from the issues
we discussed in the introduction: non\hyp{}determinism and recursion can interact
to cause non-termination even when a finite solution does exist.
The semantics of definite logic programs is given by the least fixed
point of the \emph{immediate consequence operator} $T_P$~\cite{vanEmden1976the}
that strongly resembles the semantics given by $\denot{}{\cdot}$.
\emph{Tabling} is a technique that produces a different semantics
in the same way we produce different semantics for non\hyp{}determinism.
Several Prolog implementations support tabling, such as
XSB-Prolog~\cite{swift2012xsb, xsbbook},
B-Prolog~\cite{zhou2012language}, YapProlog~\cite{rocha2000},
ALS-Prolog~\cite{guo2001simple} and Mercury~\cite{somogyi2006tabling}. 
XSB-Prolog in particular also implements lattice-based answer subsumption
\cite{swift2010tabling} which strongly resembles and has partially inspired the
techniques of Section~\ref{sec:lattices}.
Other systems provide similar functionality through \textit{mode-directed tabling}
\cite{santos2012efficient, zhou2010mode, guo2008simplifying}.

\myparagraph{Explicit Recursion}
We are not the first to tame unbridled recursion by representing recursive
calls explicitly.
Devriese and Piessens \cite{devriese2011explicitly, devriese2012finally} 
overcome the limitations of so-called ``$\omega$-regular'' grammars by
representing recursive occurrences of non-terminals in production bodies
explicitly.

McBride \cite{mcbride2015turing}, working in the dependently typed total
language Agda,\footnote{\url{http://wiki.portal.chalmers.se/agda/pmwiki.php}}
gives a free monadic model for recursive calls. Several possible semantics are
discussed, but none based on fixed points.

Oliveira and Cook \cite{oliveira2012} extend traditional algebraic datatypes
to with explicit definition and manipulation of cycles, offering a practical
and convenient way of programming graphs in Haskell.
Their approach to explicating recursion differs from ours. Where we use the
smart constructor |rec| to indicate a recursive call, they instead use
\emph{open recursion}.
While this is a good fit for the explicit graph data structure they work with,
for our purposes (non-deterministic computations) the former style is more
convenient.

%===============================================================================
\section{Conclusions}
\label{sec:conclusions}
We have described mutually recursive non\hyp{}deterministic computations in a
free monadic fashion, and have given these descriptions a concise denotational
semantics in terms of sets, and more generally in terms of lattices.
We have shown how to efficiently implement these semantics in Haskell using
Effect Handlers.

\section{Acknowledgements}
We thank Maciej Pir\`og for pointing out related work, and
Georgios Karachalias, Steven Keuchel, Amr Saleh and Paolo Torrini,
Maarten Van den Heuvel and the IFL 2015 participants for their helpful
feedback.
We are grateful to the anonymous reviewers of the IFL 2015 PC commitee
for their useful comments.
This research was partially supported by the Flemish Fund for Scientific
Research (FWO).
This work was partially supported by project GRACeFUL, which has received
funding from the European Union's Horizon 2020 research and innovation
programme under grant agreement No 640954.


\bibliographystyle{abbrv}
\begin{thebibliography}{10}

\bibitem{bauer2015programming}
A.~Bauer and M.~Pretnar.
\newblock Programming with algebraic effects and handlers.
\newblock {\em Journal of Logical and Algebraic Methods in Programming},
  84(1):108 -- 123, 2015.
\newblock Special Issue: The 23rd Nordic Workshop on Programming Theory (NWPT
  2011) Special Issue: Domains X, International workshop on Domain Theory and
  applications, Swansea, 5-7 September, 2011.

\bibitem{oliveira2012}
B.~C. d.~S.~Oliveira and W.~R. Cook.
\newblock Functional programming with structured graphs.
\newblock In {\em {ICFP}}, pages 77--88. {ACM}, 2012.

\bibitem{devriese2011explicitly}
D.~Devriese and F.~Piessens.
\newblock Explicitly recursive grammar combinators.
\newblock In {\em Practical Aspects of Declarative Languages, PADL}, volume~11,
  pages 84--98. Springer, 2011.

\bibitem{devriese2012finally}
D.~Devriese and F.~Piessens.
\newblock Finally tagless observable recursion for an abstract grammar model.
\newblock {\em Journal of Functional Programming}, 22:757--796, 2012.

\bibitem{guo2001simple}
H.-F. Guo and G.~Gupta.
\newblock A simple scheme for implementing tabled logic programming systems
  based on dynamic reordering of alternatives.
\newblock In {\em Proceedings of the 17th International Conference on Logic
  Programming}, pages 181--196. Springer-Verlag, 2001.

\bibitem{guo2008simplifying}
H.-F. Guo and G.~Gupta.
\newblock Simplifying dynamic programming via mode-directed tabling.
\newblock {\em Software—Practice \& Experience}, 38(1):75--94, 2008.

\bibitem{hinze2000deriving}
R.~Hinze.
\newblock Deriving backtracking monad transformers.
\newblock In {\em In The International Conference on Functional Programming
  (ICFP)}, 2000.

\bibitem{jeannin2013programming}
J.-B. Jeannin, D.~Kozen, and A.~Silva.
\newblock Language constructs for non-well-founded computation.
\newblock In {\em Programming Languages and Systems}, volume 7792 of {\em
  Lecture Notes in Computer Science}, pages 61--80. Springer Berlin Heidelberg,
  2013.

\bibitem{jeuring1995constructing}
J.~Jeuring and D.~Swierstra.
\newblock Constructing functional programs for grammar analysis problems.
\newblock In {\em Proceedings of the seventh international conference on
  Functional programming languages and computer architecture}, pages 259--269.
  ACM, 1995.

\bibitem{kiselyov2013extensible}
O.~Kiselyov, A.~Sabry, and C.~Swords.
\newblock {Extensible Effects}: An alternative to monad transformers.
\newblock In {\em Proceedings of the 2013 ACM SIGPLAN Symposium on Haskell},
  Haskell '13, pages 59--70, New York, NY, USA, 2013. ACM.

\bibitem{kiselyov2005backtracking}
O.~Kiselyov, C.-c. Shan, D.~P. Friedman, and A.~Sabry.
\newblock Backtracking, interleaving, and terminating monad
  transformers:(functional pearl).
\newblock In {\em ACM SIGPLAN Notices}, volume~40, pages 192--203. ACM, 2005.

\bibitem{mcbride2012the}
C.~McBride.
\newblock {\em The {Frank} Manual}, 2012.

\bibitem{mcbride2015turing}
C.~McBride.
\newblock Turing-completeness totally free.
\newblock In {\em Mathematics of Program Construction}, volume 9129 of {\em
  Lecture Notes in Computer Science}, pages 257--275. Springer International
  Publishing, 2015.

\bibitem{jones2006simple}
S.~Peyton~Jones, D.~Vytiniotis, S.~Weirich, and G.~Washburn.
\newblock Simple unification based type inference for {GADTs}.
\newblock In {\em In The International Conference on Functional Programming
  (ICFP)}, 2006.

\bibitem{rocha2000}
R.~Rocha, F.~Silva, and V.~Santos~Costa.
\newblock {YapTab}: A tabling engine designed to support parallelism.
\newblock In {\em Conference on Tabulation in Parsing and Deduction}, pages
  77--87, 2000.

\bibitem{santos2012efficient}
J.~Santos and R.~Rocha.
\newblock Efficient support for mode-directed tabling in the {YapTab} {Tabling}
  {System}.
\newblock {\em CICLOPS 2012}, page~41, 2012.

\bibitem{scott1971toward}
D.~Scott and C.~Strachey.
\newblock {\em Toward a mathematical semantics for computer languages},
  volume~1.
\newblock 1971.

\bibitem{somogyi2006tabling}
Z.~Somogyi and K.~Sagonas.
\newblock Tabling in {Mercury}: design and implementation.
\newblock In {\em Proceedings of the 8th international conference on Practical
  Aspects of Declarative Languages}, pages 150--167. Springer-Verlag, 2006.

\bibitem{swadi2006monadic}
K.~Swadi, W.~Taha, O.~Kiselyov, and E.~Pasalic.
\newblock A monadic approach for avoiding code duplication when staging
  memoized functions.
\newblock In {\em Proceedings of the 2006 ACM SIGPLAN Symposium on Partial
  Evaluation and Semantics-based Program Manipulation}, PEPM '06, pages
  160--169, New York, NY, USA, 2006. ACM.

\bibitem{swierstra2008data}
W.~Swierstra.
\newblock Data types {\`a} la carte.
\newblock {\em Journal of functional programming}, 18(04):423--436, 2008.

\bibitem{swift2010tabling}
T.~Swift and D.~S. Warren.
\newblock Tabling with answer subsumption: {Implementation}, applications and
  performance.
\newblock In {\em Logics in Artificial Intelligence}, pages 300--312. Springer,
  2010.

\bibitem{swift2012xsb}
T.~Swift and D.~S. Warren.
\newblock {XSB}: {Extending} {Prolog} with tabled logic programming.
\newblock {\em Theory and Practice of Logic Programming}, 12(1-2):157--187,
  2012.

\bibitem{vanEmden1976the}
M.~H. Van~Emden and R.~A. Kowalski.
\newblock {The} {Semantics} of {Predicate} {Logic} {As} a {Programming}
  {Language}.
\newblock {\em J. ACM}, 23(4):733--742, Oct. 1976.

\bibitem{wadler1985}
P.~Wadler.
\newblock How to replace failure by a list of successes a method for exception
  handling, backtracking, and pattern matching in lazy functional languages.
\newblock In {\em Functional Programming Languages and Computer Architecture},
  pages 113--128. Springer, 1985.

\bibitem{wadler1995}
P.~Wadler.
\newblock Monads for functional programming.
\newblock In {\em Advanced Functional Programming}, pages 24--52. Springer,
  1995.

\bibitem{xsbbook}
D.~S. Warren.
\newblock {\em Programming in {Tabled} {Prolog}}, volume~1.
\newblock \url{http://www3.cs.stonybrook.edu/~warren/xsbbook/}, 1999.

\bibitem{wu2015fusion}
N.~Wu and T.~Schrijvers.
\newblock Fusion for free - efficient algebraic effect handlers.
\newblock In {\em Mathematics of Program Construction - 12th International
  Conference, {MPC} 2015, K{\"{o}}nigswinter, Germany, June 29 - July 1, 2015.
  Proceedings}, pages 302--322, 2015.

\bibitem{zhou2012language}
N.-F. Zhou.
\newblock The language features and architecture of {B}-{P}rolog.
\newblock {\em Theory and Practice of Logic Programming}, 12(1-2):189--218,
  2012.

\bibitem{zhou2010mode}
N.-f. Zhou, Y.~Kameya, and T.~Sato.
\newblock Mode-directed tabling for dynamic programming, machine learning, and
  constraint solving.
\newblock In {\em Proceedings of the International Conference on Tools with
  Artificial Intelligence (ICTAI)}, 2010.

\end{thebibliography}

\end{document}

%%% Local Variables: 
%%% mode: latex
%%% End: 
