{- | This module implements Dijkstra's Algorithm on the graphs of module
     "Bench.Graph.Graph".

     For more information about Dijkstra's Algorithm see
     <http://en.wikipedia.org/wiki/Dijkstra%27s_algorithm>.

     Some conventions for the weights are adopted: All valid weights are
     assumed to be non-negative. Hence we can use -1.0 to denote positive
     infinity.

     Author: Alexander Vandenbroucke
-}
module Bench.Graph.Dijkstra (dijkstra) where

import           Control.Monad
import           Control.Monad.ST
import qualified Data.Vector.Generic.Mutable as GM
import qualified Data.Vector.Unboxed as UV
import qualified Data.PQueue.Prio.Min as PQM
import qualified Data.Foldable as F

import           Bench.Graph.Graph


-- | Execute dijkstra's algorithm for a 'Vertex' in a 'Graph'.
--   All initial weights must be non-negative.
--   The result is a vector that lists the weight of the minimal weight path
--   to every vertex in the graph.
--  -1.0 denotes infinity in the result.
dijkstra :: Graph -> Vertex -> UV.Vector Weight
dijkstra g v =
   -- (-1.0) denotes infinity
  let t = UV.generate (size g) (\i -> if i == v then 0.0 else -1.0)
  in UV.modify (dijkstra' g v) t

-- | Helper function, computes dijkstra's algorithm in the 'ST'-monad.
dijkstra' ::
  Graph
  -> Vertex
  -> UV.MVector s Weight
  -> ST s ()
dijkstra' g v0 mt = void (go (PQM.singleton 0 v0)) where
  go pq1 =
    case PQM.minView pq1 of
     Nothing -> return pq1
     Just (v, pq2) -> do
       let vs = neighbours v g
       wv <- GM.unsafeRead mt v
       updateAll (map (\(u,w) -> (u, w+wv)) vs) pq2 mt >>= go

-- | Update the given weights in the ST monad, and return newly discovered nodes.
--   There might be a bug in this: nodes that are already in the queue do not
--   have their position updated when a shorter distance is discovered.
updateAll ::
  [(Vertex, Weight)]
  -> PQM.MinPQueue Weight Vertex
  -> UV.MVector s Weight
  -> ST s (PQM.MinPQueue Weight Vertex)
updateAll vs pq0 mt = do
    F.foldrM update pq0 vs
  where
    update (b,w) pq = do
      w0 <- GM.unsafeRead mt b
      when (w0 < 0 || w < w0) (GM.write mt b w)
      if w0 < 0 then
        return (PQM.insert w b pq)
      else 
        return pq
