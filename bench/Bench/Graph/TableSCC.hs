{- | This module implements an algorithm to find all SCCs in a 'Graph'

     > Let G = (V,E) be a graph.
     > A subset S of V is a Strongly Connected Component (SCC) iff
     > for all u,v in S: v is reachable from u and u is reachable from v.

     We use @SCC(u)@ to denote the SCC of @u@. Note that @SCC(u)@ is uniquely
     defined for all @u@

     Author: Alexander Vandenbroucke
-}

module Bench.Graph.TableSCC (scc, sccIter, sccAdv, sccAdvLattice) where

import           Bench.Graph.Graph
import           Open
import           Table as T
import qualified Table.Advanced as A
import qualified Table.Formal as F
import qualified Table.Lattice.Advanced as AL

import qualified Data.Set as S

-- | Nondeterministically compute all vertices reachable from a given 'Vertex'
--   in the 'Graph'.
reach :: Nondet m => Graph -> Open (Vertex -> m Vertex)
reach g r a = return a <|> do b <- neighboursD a g
                              r b
-- | Compute @SCC(v)@ for a 'Vertex' @v@.
scc :: Graph -> Vertex -> TabledProgram Vertex Vertex Vertex
scc g x =
  let gInv = invert g
  in TP $
     declare (reach g)    $ \reachFor  ->
     declare (reach gInv) $ \reachBack ->
     expression $ do
       y  <- reachFor  x
       y' <- reachBack x
       if y == y' then return y else T.fail

-- | Non-deterministically compute all adjacent vertices of a 'Vertex' in a
--   'Graph'.
neighboursD :: Nondet m => Vertex -> Graph -> m Vertex
neighboursD v g = foldr (<|>) T.fail $ map (return . fst) $ neighbours v g 

-- | Solve 'scc' using 'A.ts' "Table.Advanced".
sccIter :: Graph -> Vertex -> S.Set Vertex
sccIter g x = F.iterTableSemantics (scc g x)

-- | Solve 'scc' using 'A.ts' "Table.Advanced".
sccAdv :: Graph -> Vertex -> S.Set Vertex
sccAdv g x = A.ts (scc g x)

-- | Solve 'scc' using 'A.tl' from "Table.Lattice.Advanced".
sccAdvLattice :: Graph -> Vertex -> S.Set Vertex
sccAdvLattice g x = AL.tl (scc g x)
