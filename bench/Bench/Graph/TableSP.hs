{- | This module implements an algorithm similar to Dijkstra's algorithm using
     tabling.

     Author: Alexander Vandenbroucke
-}


{-# LANGUAGE TypeSynonymInstances  #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Bench.Graph.TableSP (
  shortestPathProg,
  tabledSPAdv,
  tabledSPIter,
  tabledSP
  )
where

import Table as T
import Table.Lattice.Advanced as A
import Table.Lattice as L
import Table.Lattice.Iter as IL
import Bench.Graph.Graph

import Control.Applicative (Applicative(..))
import qualified Data.Vector.Unboxed as UV
import Data.Monoid

-- | Shortest-path predicate for a given 'Vertex' in a 'Graph'.
--
--   A path of zero weight exists from any vertex to itself.
--   If a path of weight @w0@ to a vertex @b@ exists, then there also exists
--   a path of weight @w0+w@ to vertex @c@ using some edge @(b,c)@ of
---  weight @w@.
shortestPath ::
  (Nondet m, Applicative m) => Graph -> Vertex -> Open (() -> m Edge)
shortestPath g a sp () =
  return (edge g a 0) <|> do Edge _ b w1 <- sp ()
                             Edge _ c w2 <- neighboursD b g
                             return (edge g c (w1 + w2))

-- | Non-deterministically compute all adjacent vertices of a 'Vertex' in a
--   'Graph'.
neighboursD :: Nondet m => Vertex -> Graph -> m Edge
neighboursD v g = foldr (<|>) T.fail $ map edge' $ neighbours v g where
  edge' = return . uncurry (edge g)

-- | Obtain the weight along paths of minimal weight from a given 'Vertex' to
--   all other vertices in the 'Graph'
shortestPathProg :: Graph -> Vertex -> TabledProgram () Edge Edge 
shortestPathProg g a = TP $ declare (shortestPath g a) $ \sp ->
                            expression $ sp ()

-- | Solve 'shortestPathProg' using advanced tabling 'A.tl' from
--   "Table.Lattice.Advanced".
tabledSPAdv :: Graph -> Vertex -> UV.Vector Weight
tabledSPAdv g a = unwrap $ A.tl $ shortestPathProg g a

-- | Solve 'shortestPathProg' using advanced tabling 'A.iterTl' from
--   "Table.Lattice.Iter".
tabledSPIter :: Graph -> Vertex -> UV.Vector Weight
tabledSPIter g a = unwrap $ IL.iterTl $ shortestPathProg g a


-- | Solve 'shortestPathProg' using simple tabling 'L.tl' from
--   "Table.Lattice".
tabledSP :: Graph -> Vertex -> UV.Vector Weight
tabledSP g a = unwrap $ L.tl $ shortestPathProg g a

-- | @'Edge' n v w@ where @n@ = #vertices in the graph,
--                        @v@ = target vertex and
--                        @w@ = edge weight
data Edge = Edge Int Vertex Weight

-- | Construct an edge,
edge :: Graph        -- ^ in 'Graph' g,
        -> Vertex    -- ^ to 'Vertex' v,
        -> Weight    -- ^ of 'Weight' w.
        -> Edge
edge g v w = Edge (size g) v w

-- | A wrapper around a vector containing weights.
data WVec = WEmpty | WVec (UV.Vector Weight) deriving (Eq, Show)

-- | Turn a 'WVec' into the vector it contains.
unwrap :: WVec -> UV.Vector Weight
unwrap WEmpty   = UV.empty
unwrap (WVec v) = v

instance Monoid WVec where
  mempty = WEmpty
  mappend WEmpty m = m
  mappend m WEmpty = m
  mappend (WVec a) (WVec b) = WVec (UV.zipWith combine a b) where
    combine (-1.0) dB = dB
    combine dA (-1.0) = dA
    combine dA dB = min dA dB

instance Lattice WVec where
  WEmpty =<= _      = True
  _      =<= WEmpty = False
  WVec a =<= WVec b = UV.and $ UV.zipWith compareWeight a b where
    compareWeight _ (-1.0) = True
    compareWeight (-1.0) _ = False
    compareWeight wA wB = wB <= wA
  lub    = foldr (<>) mempty
  bottom = mempty

instance Coerce Edge WVec where
  coerce (Edge n v w) =
    WVec $ UV.generate n (\i -> if i == v then w else -1.0)
  uncoerce WEmpty = []
  uncoerce (WVec uv) =
      UV.ifoldr (\v w l -> if w == -1.0 then l else Edge n v w:l) [] uv
    where
      n = UV.length uv

