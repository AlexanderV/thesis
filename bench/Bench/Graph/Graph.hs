{- | This module implements weighted directed graphs with an adjacency list
     representation.

     For benchmarking purposes, this module provides a function 'generateGraph',
     that builds a random graph.

     Graphs can be inverted with 'invert'.

     Author: Alexander Vandenbroucke
-} 

{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Bench.Graph.Graph (
  Graph(..),
  Vertex,
  Weight,
  generateGraph,
  neighbours,
  size,
  invert
  )
where

import           Control.DeepSeq
import qualified Control.Monad as M (forM_)
import           Data.Vector ((!))
import qualified Data.Vector as V
import qualified Data.Vector.Generic as G
import qualified Data.Vector.Generic.Mutable as GM

import           System.Random


type Vertex = Int
type Weight = Double

-- | The main 'Graph' datatype.
newtype Graph = Graph {
  -- | All edges in the graph, in adjacency list representation.
  edges :: V.Vector [(Vertex, Weight)] 
  } deriving (NFData, Show)

-- | The number of vertices in the graph.
size :: Graph -> Int
size = V.length . edges

-- | The neighbours (i.e. the adjacent vertices) of a 'Vertex'.
neighbours :: Vertex -> Graph -> [(Vertex, Weight)]
neighbours v g = edges g ! v

-- | Generate a random 'Graph' that has a desired number of vertices and edges,
--   a weight is picked uniformly distributed from a given range for each edge.
generateGraph :: Int -> Int -> (Double, Double) -> StdGen -> IO (Graph, StdGen)
generateGraph noV noE weightRange g = do
    v <- GM.new noV
    GM.set v []
    g' <- addEdges noE g v 
    e <- G.freeze v
    return (Graph e, g')
  where
    addEdges 0 g0  _ = return g0
    addEdges n g0 v = do 
      let (a, g1) = randomR (0, noV-1) g0
          (b, g2) = randomR (0, noV-1) g1
          (w, g3) = randomR weightRange g2
      adj <- GM.read v a
      if a == b || b `elem` (map fst adj) then
        addEdges n g3 v      -- reject
      else do
        GM.write v a ((b, w):adj) -- accept
        addEdges (n-1) g3 v

{- | Invert the 'Graph'

     The inverted graph of a directed grap G = (V,E) is the graph G' = (V, E')
     s.t.

     > (v,w) in E' <=> (w,v) in E
-}
invert :: Graph -> Graph
invert g = Graph $ V.modify invert' (V.replicate (size g) []) where
  invert' tm = V.forM_ (V.imap (\i l -> (i, l)) (edges g)) $ \(i,l) ->
    M.forM_ l $ \(j, w) -> do adj <- GM.read tm j
                              GM.write tm j ((i,w):adj)
