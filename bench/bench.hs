import           Bench.Graph.Dijkstra
import           Bench.Graph.Graph
import           Bench.Graph.TableSCC
import           Bench.Graph.TableSP

import           Table.Knapsack.Knapsack (generateItems, mkWeight, mkValue)
import           Table.Knapsack

import           Control.DeepSeq
import           Control.Monad.State
import           Criterion.Main
import qualified Data.Set as S
import qualified Data.Vector.Unboxed as UV
import           System.Random

seed :: Int
seed = 372306 -- from random.org

vert :: Int
--vert = 2000
vert = 10

main :: IO ()
main = flip evalStateT (mkStdGen seed) $ do
  g2  <- ggraph 2
  g4  <- ggraph 4
  g8  <- ggraph 8
  
  -- 100 vertices is a bit too much for these tests
  g2small <- ggraphsmall 2
  g4small <- ggraphsmall 4
  g8small <- ggraphsmall 8

  ksItems <- StateT $ do
    let wRange = (mkWeight 1, mkWeight 100)
        vRange = (mkValue  1, mkValue  100)
    return . generateItems 35 wRange vRange 

  force g2 `seq` force g4 `seq` force g8 `seq`
    force g2small `seq` force g4small `seq` force g8small `seq`
    force ksItems `seq`
    lift $ defaultMain [
      bgroup "shortest-path" [
        bench "2" $ nf (avgDistance dijkstra) g2,
        bench "4" $ nf (avgDistance dijkstra) g4,
        bench "8" $ nf (avgDistance dijkstra) g8],
      bgroup "shortest-path-iter" [
        bench "2" $ nf (avgDistance tabledSPIter) g2,
        bench "4" $ nf (avgDistance tabledSPIter) g4,
        bench "8" $ nf (avgDistance tabledSPIter) g8],
      bgroup "shortest-path-adv" [
        bench "2" $ nf (avgDistance tabledSPAdv) g2,
        bench "4" $ nf (avgDistance tabledSPAdv) g4,
        bench "8" $ nf (avgDistance tabledSPAdv) g8],
      bgroup "shortest-path-formal" [
        bench "2" $ nf (avgDistance tabledSP) g2],
      
      bgroup "scc-adv" [
        bench "2" $ nf (avgSCC sccAdv) g2small,
        bench "4" $ nf (avgSCC sccAdv) g4small,
        bench "8" $ nf (avgSCC sccAdv) g8small],
      bgroup "scc-adv-lattice" [
        bench "2" $ nf (avgSCC sccAdvLattice) g2small,
        bench "4" $ nf (avgSCC sccAdvLattice) g4small,
        bench "8" $ nf (avgSCC sccAdvLattice) g8small],
      bgroup "scc-iter" [
        bench "2" $ nf (avgSCC sccIter) g2small,
        bench "4" $ nf (avgSCC sccIter) g4small,
        bench "8" $ nf (avgSCC sccIter) g8small],
      
      bgroup "knapsack-iter" [
        bench  "10" $ nf (tabledKSIter ksItems) 10,
        bench  "11" $ nf (tabledKSIter ksItems) 11,
        bench  "12" $ nf (tabledKSIter ksItems) 12,
        bench  "20" $ nf (tabledKSIter ksItems) 20,
        bench  "30" $ nf (tabledKSIter ksItems) 30,
        bench  "40" $ nf (tabledKSIter ksItems) 40,
        bench  "50" $ nf (tabledKSIter ksItems) 50,
        bench "100" $ nf (tabledKSIter ksItems) 100,
        bench "200" $ nf (tabledKSIter ksItems) 200],
      bgroup "knapsack-advanced" [
        bench  "50" $ nf (tabledKSAdv ksItems) 50,
        bench "100" $ nf (tabledKSAdv ksItems) 100,
        bench "200" $ nf (tabledKSAdv ksItems) 200,
        bench "400" $ nf (tabledKSAdv ksItems) 400]]


ggraph :: Int -> StateT StdGen IO Graph
ggraph f = StateT $ generateGraph vert (f*vert) (1.0, 1.0)

ggraphsmall :: Int -> StateT StdGen IO Graph
ggraphsmall f = StateT $ generateGraph 20 (f*20) (1.0, 1.0)


avgDistance :: (Graph -> Vertex -> UV.Vector Weight) -> Graph -> Weight
avgDistance d g = let n = size g
                      (ds, counts) = unzip $ map (avgDVertex . d g) [0..(n-1)]
                  in sum ds / fromIntegral (sum counts)

avgDVertex :: UV.Vector Weight -> (Weight, Int)
avgDVertex v = let v' = UV.filter (/= -1.0) v
               in (UV.sum v', UV.length v')

avgSCC :: (Graph -> Vertex -> S.Set Vertex) -> Graph -> Double
avgSCC d g = let n = size g
             in sum (map (fromIntegral . S.size . d g) [0..(n-1)])
                /
                fromIntegral n

